import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from collections import OrderedDict

from cms.models import CMSPlugin, TreeNode

root_nodes = TreeNode.objects.filter(parent__isnull=True)

print(root_nodes)