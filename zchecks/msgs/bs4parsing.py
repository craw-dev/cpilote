import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from filer.models.filemodels import File


import bs4
from django.utils.http import unquote_plus

filepath = "/var/log/cpilote/cp_warning2.log"

with open(filepath, 'r') as f:
    data = f.read()

# s = "/media/filer_public_thumbnails/filer_public/35/ed/35ed6a33-20d2-4424-ac34-51b5e8ddaac1/200317_c05.jpg__645x403_q85_subsampling-2.jpg"
# lpos = s.rfind('/', 0) + 1
# filename = s[lpos:]
# pathname = s[:lpos]

def get_filerfile(mypath):
    size_option = {'size': (400, 300), 'crop': True}
    filename = ''
    filepath = ''
    try:
        fileimg = File.objects.get(file=mypath)
        if fileimg._file_size > 100000:
            filepath = fileimg.file.get_thumbnail(size_option).path
        else:
            filepath = fileimg.file.path
    except File.DoesNotExist:
        # This should never happen, since the file is in media/filer_public
        print("FILE DOES NOT EXIST: {}".format(mypath))
    return filepath, filename

def get_media_file_and_path(path):
    path2 = mypath.replace("/media/", "")
    path2 = path2.replace("filer_public_thumbnails/", "")
    # Cut path into 2
    path2 = path2.split("__")
    print('path2[0]: {}'.format(path2[0]))
    filepath, filename = get_filerfile(path2[0])
    return filepath, filename


soup = bs4.BeautifulSoup(data, "html.parser")

imgs = soup.body.find_all("img")
for img in imgs:
    src = str(img["src"])
    # unquote_plus (url decode pour garder les , espaces, etc.)
    src = unquote_plus(src)
    # print(src)
    # Keep only filer images
    if src.find("/media/filer_public") == 0:
        print(src)
        fullpath, filename = get_media_file_and_path(src)
        print(fullpath)



