import os
import django
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from filer.models.filemodels import File
from django.contrib.contenttypes.models import ContentType
# from filer.models.imagemodels import Image as ImageModel
from PIL import Image
from config import settings

# Filer
# original_filename = filename
# file = path
def get_img(searchtxt):
    img_list = File.objects.filter(original_filename__icontains=searchtxt)
    return img_list

def get_img_large(size):
    """
    sized expressed in Bytes
    100000 = 100kB
    """
    img_ctype = ContentType.objects.get(model="image")
    # img_list = File.objects.filter(_file_size__gt=size, polymorphic_ctype=img_ctype)
    img_list = File.objects.filter(_file_size__gt=size)
    return img_list

def resize_img(imgpath):
    fullpath = "{0}{1}".format(settings.MEDIA_ROOT, str(imgpath))
    image = Image.open(fullpath)
    width = image.width
    height = image.height
    factor = 1
    if width > height:
        if width > 800:
            factor = 800 / width
    else:
        if height > 800:
            factor = 800 / height
    size = (int(width * factor), int(height * factor))
    image = image.resize(size)
    image.save(fullpath)
    file_size = os.stat(fullpath).st_size
    return image, file_size


# imgl = get_img("velux1")
imgl = get_img_large(100000)

for img in imgl:
    # print(img.original_filename, img._width, img._height, img._file_size)

    # image, file_size = resize_img(img.file)
    # img._width = image.width
    # img._height = image.height
    # img._file_size = file_size
    # img.save()
    try:
        print(img.original_filename, img._width, img._height, img._file_size)
    except AttributeError as error:
        print(img, error)
    except Exception as xcpt:
        print("Undefinded Exception: ", xcpt)
        