import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from filer.models.filemodels import File
from config import settings


def load_logo(keyword):
    """
    Search logo files in Filer logo folder
    """
    logo = File.objects.all().filter(folder__name='centrespilotes', original_filename__icontains=keyword)[0]
    # print(logo.original_filename)
    # print(logo.file)
    # logopath = "{0}{1}".format(settings.MEDIA_ROOT, str(logo.file))
    # print(logopath)
    # logoimg = self.attach_image(logofile, logopath)
    # self.attach(logoimg)
    return logo.file

lf = load_logo("cepicop")
print(lf)

