import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from django.utils import timezone
from django.db.models.functions import Lower 

from accounts.models import Profile, ListeEnvoi, ProfileTmp, GeoZip, Inscription, CentrePilote, Profession
from msg.models import Msg

profession_agri = Profession.objects.get(code='AGRI')
print(profession_agri)
profession_public = Profession.objects.get(code='PUBLIC')
print(profession_public)
profession_entr = Profession.objects.get(code='ENTR')
print(profession_entr)
profession_domagro = Profession.objects.get(code='DOMAGRO')
print(profession_domagro)
profession_conseil = Profession.objects.get(code='CONSEIL')
print(profession_conseil)


# user = Profile.objects.get(email='p.houben@cra.wallonie.be')
# odk_admin = user.groups.filter(name='odk-admin').exists()



