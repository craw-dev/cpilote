import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from accounts.models import GeoZip
# 4671 - Blégny DoesNotExist for profiletmp_id = 114
# 4357 - Verlaine DoesNotExist for profiletmp_id = 125
# 1367 - Ramilies-Offus DoesNotExist for profiletmp_id = 126
# 4537 - Seraing-le-Château DoesNotExist for profiletmp_id = 129
# 4460 - Horion Hozémont DoesNotExist for profiletmp_id = 131
# 4250 - Ligney DoesNotExist for profiletmp_id = 133
# 1360 - Thorembais-les-Béguines DoesNotExist for profiletmp_id = 137
# 4347 - Fexhe-le-Haut-Clocher DoesNotExist for profiletmp_id = 141
# 5003 - Namur DoesNotExist for profiletmp_id = 153
# 5190 - Moustier DoesNotExist for profiletmp_id = 158
# 3798 - Voeren DoesNotExist for profiletmp_id = 159
# 4263 - Tourinne-la-Chaussée DoesNotExist for profiletmp_id = 170
# 4263 - Tourinne-la-Chaussée DoesNotExist for profiletmp_id = 174
# 1315 - Sart-Risbart DoesNotExist for profiletmp_id = 179
# 4250 - Hollogne-sur-Geer DoesNotExist for profiletmp_id = 180
# 4250 - Hollogne-sur-Geer DoesNotExist for profiletmp_id = 182
# 3724 - Kortessem DoesNotExist for profiletmp_id = 183
# 4218 - Héron DoesNotExist for profiletmp_id = 193
# 4470 - Saint-Georges DoesNotExist for profiletmp_id = 197
# 1360 - Malèves-Ste-Marie DoesNotExist for profiletmp_id = 198
# 4458 - Juprelle DoesNotExist for profiletmp_id = 201
# 9747 - Enscherange (G-D Luxembourg) DoesNotExist for profiletmp_id = 209


gz = GeoZip.objects.filter(postal_code='4432')
print(gz)

gz = GeoZip.objects.filter(place_name='Blégny')
print(gz)

