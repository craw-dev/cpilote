import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from django.utils import timezone
from django.db.models.functions import Lower 

from accounts.models import Profile, ListeEnvoi, ProfileTmp, GeoZip, Inscription

# get @hero passwd
hero = Profile.objects.get(email='cplv@hero.com')
hero_pass = hero.password

# le_list=['carottes']
# le_qs = ListeEnvoi.objects.filter(code__in=le_list)
listeenvoi = ListeEnvoi.objects.get(code='carottes')

profile_tmp = ProfileTmp.objects.all()
for tmp in profile_tmp:  
    if tmp.code_postal is not None:
        code_postal = tmp.code_postal.strip()
        localite = tmp.localite.strip().lower()
        # print(f'{code_postal} - {localite}')
        try:
            gz = GeoZip.objects.annotate(lower_place_name=Lower("place_name")).get(postal_code=code_postal, lower_place_name=localite)
            # print(gz)
        except GeoZip.DoesNotExist:
            gz_wrong = f'{code_postal} - {localite} DoesNotExist for profiletmp_id = {tmp.id}\n'
            print(gz_wrong)
            print('Re-execute check_zip_users.py !!!')
            break
    else:
        gz = None
    #
    # Start Pofile Insert
    #
    try:
        user = Profile.objects.get(email=tmp.email)
    except Profile.DoesNotExist:
        try:
            user, created = Profile.objects.create(
                email=tmp.email,
                email_confirmed=True,
                password=hero_pass,
                is_superuser=False,
                first_name=tmp.first_name,
                last_name=tmp.last_name,
                is_staff=False,
                is_active=True,
                date_joined=timezone.now(),
                mobile=tmp.mobile,
                phone=tmp.phone,
                fax=tmp.fax,
                company=tmp.company,
                # profession=tmp.profession,
                nr_producteur=tmp.nr_producteur,
                street=tmp.street,
                zip=gz,
                # modeprod=tmp.modeprod,
                code_postal=tmp.code_postal,
                localite=tmp.localite
            )
            print(f'user {user.email} created')
        except Exception as err:
            print(f"Error while creating {tmp.email}")
            print(err)
            print("---------------------------------")

    try:
        inscr, created = Inscription.objects.get_or_create(
            profile=user,
            listeenvoi=listeenvoi
        )
        print(f'user {user.email} inscrit à la liste envoi {listeenvoi}')
    except Exception as inscr_err:
        print(f"Error while creating inscriptions for {tmp.email}")
        print(inscr_err)
        print("---------------------------------")
