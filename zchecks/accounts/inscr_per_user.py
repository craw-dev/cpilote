sql1 = '''select p.id, i.listeenvoi_id, i.id as inscription_id, i.is_valid as inscr_valid,
        COALESCE(p.mobile, p.phone, '') as tel,
        p.email,
        CASE WHEN trim(last_name) <> '' THEN concat(upper(last_name), ' ', first_name) ELSE username END as full_name,
        f.description
    from accounts_profile p
    inner join accounts_profession f on p.profession_id = f.id
    left join (select * from accounts_inscription where listeenvoi_id = %(pk)s) i on p.id=i.profile_id
    where p.email_confirmed = True and p.is_active = True'''
sql2 = '''and (lower(p.last_name) like %(txtsearch)s 
        or lower(p.first_name) like %(txtsearch)s
        or lower(p.email) like %(txtsearch)s
        or lower(p.username) like %(txtsearch)s
        )'''
sql3 = "order by p.date_joined desc"
sqlfull = sql1 + sql2 + sql3

print(sqlfull)