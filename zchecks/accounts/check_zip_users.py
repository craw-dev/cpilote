#
# loop over temp profile and output not found zip - localité in file zchecks/accounts/wrong_zip.log
#
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from django.db.models.functions import Lower 

from accounts.models import ProfileTmp, GeoZip

profile_tmp = ProfileTmp.objects.all()
# profile_tmp = ProfileTmp.objects.filter(id=4134)
with open("zchecks/accounts/wrong_zip.log", "w") as log_file:
    for tmp in profile_tmp:
        # code_postal
        if tmp.code_postal is None:
            code_postal = None
        else:
            code_postal = tmp.code_postal.strip()
        # localite
        if tmp.localite is None:
            localite = None
        else:
            localite = tmp.localite.strip().lower()

        # Corrections
        if localite is not None:
            if 'allemagne' in localite:
                tmp.localite = None
            if '.' in localite:
                tmp.localite = None
            if localite == '':
                tmp.localite = None                                          
            if localite == "ans":
                tmp.code_postal = '4430'
            if code_postal == "4671":
                tmp.localite = "barchon"
                tmp.save()               
            if code_postal == "5003":
                tmp.localite = "saint-marc"
            if code_postal == "L-8390":
                tmp.localite = "nospelt"                                           
            if localite == "verlaine":
                tmp.code_postal = "4537"
            if localite == "moustier":
                tmp.localite = "moustier-sur-sambre"              
            if localite == "ligney":
                tmp.code_postal = "4254"                
            if localite == "ramilies-offus" or localite == "ramilies":
                tmp.localite = "ramillies"
            if localite == "horion hozémont":
                tmp.localite = "horion-hozémont"
            if localite == "voeren":
                tmp.localite = "Fouron-Le-Comte"
            if localite == "tourinne-la-chaussée":
                tmp.localite = "tourinne"
            if code_postal == "3724":
                tmp.localite == "vliermaal"
            if code_postal == "4470":
                tmp.localite = "saint-georges-sur-meuse"
            if localite == "malèves-ste-marie":
                tmp.localite = "malèves-sainte-marie-wastinnes"
            if code_postal == "4458":
                tmp.localite = "fexhe-slins"
            if localite == "enscherange (g-d luxembourg)":
                tmp.code_postal = "L-9747"                
                tmp.localite = "enscherange"
            if 'burdinne' in localite :
                tmp.localite = "burdinne"
            if 'kathelyne' in localite:
                tmp.localite = 'Sint-Katelijne-Waver'
            if 'heron' in localite:
                tmp.localite = 'héron'
            if '(nl)' in localite:
                tmp.localite = None
                tmp.code_postal = None                               
            if localite == 'froid-chapelle':
                tmp.localite = 'Froidchapelle'
            if localite == 'rixensart':
                tmp.code_postal = '1330'
            if 'bourseigne' in localite:
                tmp.localite = 'Bourseigne-Vieille'
            if 'bourseigne' in localite:
                tmp.localite = 'Bourseigne-Vieille'                 
            if localite == 'havre':
                tmp.localite = 'Havré'
            if localite == 'bossoire':
                tmp.localite = 'Bossière'
            if localite == "strée":
                tmp.localite = "Strée-Lez-Huy"
            if "doiceau" in localite:
                tmp.localite = "Grez-Doiceau"
            if "sauveniere" in localite:
                tmp.localite = "Sauvenière"                                                                                                        
            if localite == "malmédy":
                tmp.localite = "malmedy"
            if code_postal == "4701":
                tmp.localite = "Kettenis"
            if code_postal == "4218":
                tmp.localite = "Couthuin"
            if code_postal == "4654":
                tmp.localite = "Charneux"
            if code_postal == "59280":
                tmp.localite = "Armentières"                        
            if localite == "saint-leger":
                tmp.localite = "Saint-Léger"
            if "saint-simeon" in localite:
                tmp.localite = "Houtain-Saint-Siméon"
            if "lenuik" in localite:
                tmp.localite = "Lennik"                                                                                           
            if localite == "flostay":
                tmp.localite = "Flostoy"
            if "saint amand" in localite:
                tmp.localite = "saint-amand"
            if "hetstappe" in localite:
                tmp.localite = "Herstappe"
            if "saint servais" in localite:
                tmp.localite = "saint-servais"                                
            if "libramont" in localite:
                tmp.localite = "Libramont-Chevigny"
            if "rosières" in localite:
                tmp.localite = "rosières"
            if "malèves" in localite:
                tmp.localite = "Malèves-Sainte-Marie-Wastinnes"
            if "lieve vrouw waver" in localite:
                tmp.localite = "Onze-Lieve-Vrouw-Waver"
            if "leest" in localite:
                tmp.localite = "leest"
            if "saint denis" in localite:
                tmp.localite = "Saint-Denis-Bovesse"
            if "herinnes" in localite:
                tmp.localite = "Hérinnes-Lez-Pecq"
            if "éghezée" in localite:
                tmp.localite = "Eghezée"
            if "jemeppe" in localite:
                tmp.localite = "Jemeppe-Sur-Sambre"

            tmp.save()                            


            # print(f'{code_postal} - {localite}')
            try:
                gz = GeoZip.objects.annotate(lower_place_name=Lower("place_name")).get(postal_code=tmp.code_postal, lower_place_name=tmp.localite)
                # print(gz)
            except GeoZip.DoesNotExist:
                gz_wrong = f'{tmp.code_postal} - {tmp.localite} DoesNotExist for profiletmp_id = {tmp.id}\n'
                log_file.write(gz_wrong)
                log_file.write(f"-- Code Postal pour {tmp.localite} ------------------------------------\n")
                gz_cp = GeoZip.objects.filter(place_name=tmp.localite)
                for gz_cp1 in gz_cp:
                    log_file.write(f"{gz_cp1}\n")

                log_file.write(f"---Localité pour {tmp.code_postal} ------------------------------------\n")
                gz_loc = GeoZip.objects.filter(postal_code=tmp.code_postal)
                for gz_loc1 in gz_loc:
                    log_file.write(f"{gz_loc1}\n")

                log_file.write('*************************************************\n')


