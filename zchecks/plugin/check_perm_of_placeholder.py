import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from cms.plugin_pool import plugin_pool
from django.contrib.auth import get_permission_codename, get_user_model
from cms.models.placeholdermodel import Placeholder 
from accounts.models import Profile

plugin_class = plugin_pool.get_plugin('TextPlugin')

# ptr_id = 613 => select placeholder_id from public.cms_cmsplugin where id = 613
myplaceholder = Placeholder.objects.get(id=224)
print(myplaceholder)
user = Profile.objects.get(email='mvdk27-cepicop@yahoo.com')
print(user)

# def has_add_plugin_permission(self, user, plugin_type):
#     if not permissions.has_plugin_permission(user, plugin_type, "add"):
#         return False

#     if not self.has_change_permission(user): ici
#         return False
#     return True


def has_change_permission(placeholder, user):
    """
    Returns True if user has permission
    to change all models attached to this placeholder.
    """
    from cms.utils.permissions import get_model_permission_codename

    attached_models = placeholder._get_attached_models()
    print(attached_models)

    # if not attached_models:
    #     # technically if placeholder is not attached to anything,
    #     # user should not be able to change it but if is superuser
    #     # then we "should" allow it.
    #     return user.is_superuser

    attached_objects = placeholder._get_attached_objects()
    print(attached_objects)

    for obj in attached_objects:
        try:
            perm = obj.has_placeholder_change_permission(user)
        except AttributeError:
            model = type(obj)
            change_perm = get_model_permission_codename(model, 'change')
            perm = user.has_perm(change_perm)

        if not perm:
            return False
    return True

flagret = has_change_permission(myplaceholder, user)
print(flagret)


