import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from cms.plugin_pool import plugin_pool
from django.contrib.auth import get_permission_codename, get_user_model
from accounts.models import Profile

plugin_class = plugin_pool.get_plugin('TextPlugin')
user = Profile.objects.get(email='mvdk27-cepicop@yahoo.com')

print(user)

# def has_add_plugin_permission(self, user, plugin_type):
#     if not permissions.has_plugin_permission(user, plugin_type, "add"):
#         return False

#     if not self.has_change_permission(user):
#         return False
#     return True


# permissions.has_plugin_permission return user.has_perm(codename)
def get_model_permission_codename(model, action):
    opts = model._meta
    return opts.app_label + '.' + get_permission_codename(action, opts)
codename = get_model_permission_codename(
    plugin_class.model,
    action='add',
)
print(codename)
print(user.has_perm(codename))

# has_change_permission


