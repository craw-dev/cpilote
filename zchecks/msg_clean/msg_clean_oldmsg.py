import os
import django
import bs4

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
# from filer.models.filemodels import File
from config import settings

from msg.models import Msg

# cmsplugin_ptr = models.OneToOneField(CMSPlugin,)
# body = models.TextField(_('body'))
# search_fields = ('body',)

msg = Msg.objects.get(id=151)
msgdet = msg.msgdet_set.all()

for d in msgdet:
    print(d.short_text)
   
    soup = bs4.BeautifulSoup(d.short_text, "html.parser")
    for p_tag in soup.find_all("p"):
        p_tag["style"] = ""
    for span_tag in soup.find_all("span"):
        span_tag.attrs.clear()
    print('--------------------------------------------------')
    bodydet = str(soup)
    bodydet_clean = bodydet.replace('<span>','').replace('</span>', '').replace('<p style="">','<p>')

    print(bodydet_clean)
    d.short_text = bodydet_clean
    d.save()
