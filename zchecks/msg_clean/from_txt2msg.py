import os
import django
import bs4

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from filer.models.filemodels import File
from djangocms_text_ckeditor.models import Text
from djangocms_picture.models import Picture
from cms.models.pluginmodel import CMSPlugin
from cms.models.placeholdermodel import Placeholder
from config import settings
from msg.models import Msg, MsgDet

# Bottom-Up approach
######################

# first get text plugin
# txtplug = CMSPlugin.objects.filter(plugin_type='TextPlugin')

# loop over text plugin
# for plugin in txtplug:
#     txt = Text.objects.filter(cmsplugin_ptr=plugin, body__icontains='velo1')

txt = Text.objects.filter(body__icontains='sclérotinia')
# txt = Text.objects.filter(body__icontains='ESC Oidium.png').values('body')

if txt:
    # print(txt)
    txt_list = list(txt)
    print(txt_list)
    for txt1 in txt_list:
        plugid = txt1.id
        print(plugid)
        # print(txt1.body)

        plugin = CMSPlugin.objects.get(id=plugid, plugin_type='TextPlugin')
        # slot = Placeholder.objects.get(id=plugin.placeholder_id)
        # print(slot)

        # print(plugin.placeholder_id)
        msgdet = MsgDet.objects.get(short_text2__id=plugin.placeholder_id)
        print(msgdet)
        # print(f'msg_id={msgdet.msg_id}')

else:
    print('nothing found')


