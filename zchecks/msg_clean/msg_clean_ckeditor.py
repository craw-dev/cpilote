import os
import django
import bs4

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from filer.models.filemodels import File
from djangocms_text_ckeditor.models import Text
from djangocms_picture.models import Picture
from cms.models.pluginmodel import CMSPlugin
from config import settings

from msg.models import Msg

# cmsplugin_ptr = models.OneToOneField(CMSPlugin,)
# body = models.TextField(_('body'))
# search_fields = ('body',)

msg = Msg.objects.get(id=207)
msgdet = msg.msgdet_set.all()

for d in msgdet:
    plugin = CMSPlugin.objects.get(placeholder_id=d.short_text2.id, plugin_type='TextPlugin')

    cketxt = Text.objects.get(cmsplugin_ptr=plugin)
    
    soup = bs4.BeautifulSoup(cketxt.body, "html.parser")
    for p_tag in soup.find_all("p"):
        p_tag["style"] = ""
    for span_tag in soup.find_all("span"):
        span_tag.attrs.clear()
    
    bodydet = str(soup)
    bodydet_clean = bodydet.replace('<span>','').replace('</span>', '').replace('<p style="">','<p>')

    print(bodydet_clean)
    cketxt.body = bodydet_clean
    cketxt.save()
