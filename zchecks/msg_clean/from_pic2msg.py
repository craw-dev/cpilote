import os
import django
import bs4

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()
from filer.models.filemodels import File
from djangocms_text_ckeditor.models import Text
from djangocms_picture.models import Picture
from cms.models.pluginmodel import CMSPlugin
from config import settings
from msg.models import Msg, MsgDet

# Top- Down approach
######################

# msg = Msg.objects.get(id=211)
# msgdet = msg.msgdet_set.all()
# for d in msgdet:
#     plugin = CMSPlugin.objects.get(placeholder_id=d.short_text2.id, plugin_type='TextPlugin')
#     print(plugin.id)
#     cketxt = Text.objects.get(cmsplugin_ptr=plugin)
#     print(cketxt.body)


# Bottom-Up approach
######################

# first get text plugin
# txtplug = CMSPlugin.objects.filter(plugin_type='TextPlugin')

# loop over text plugin
# for plugin in txtplug:
#     txt = Text.objects.filter(cmsplugin_ptr=plugin, body__icontains='velo1')

txt = Text.objects.filter(body__icontains='stade esc.png').values('body')
# txt = Text.objects.filter(body__icontains='ESC Oidium.png').values('body')

if txt:
    txt_list = list(txt)
    for txt1 in txt_list:
        txtbody = txt1['body']
        idpos = txtbody.find('id="')
        # print(txtbody[idpos:])
        idnexttag = txtbody.find('"', idpos+4)
        # print(idnexttag)
        plugid = txtbody[idpos+4:idnexttag]
        print(plugid)

        plugin = CMSPlugin.objects.get(id=plugid, plugin_type='PicturePlugin')
        # print(plugin.placeholder_id)

        msgdet = MsgDet.objects.get(short_text2__id=plugin.placeholder_id)
        print(f'msg_id={msgdet.msg_id}')

else:
    print('nothing found')


