import os
import re


def is_year(myvar):
    try:
        yyyy = int(myvar)
        if yyyy > 1990 and yyyy < 2050:
            return True
        else:
            return False
    except ValueError:
        return False

def get_month_digit(myvar):
    """return None if not found"""
    re_pattern = re.compile(r"(\d{4}-)(0[1-9]|1[012])")
    m = re_pattern.match(myvar)
    if m:
        return m.group(2)
    return m

path = os.path.dirname(os.path.abspath(__file__))

# pattern to get YYYY-MM => MM
# re_pattern = re.compile(r"(\d{4}-)(0[1-9]|1[012])")

year_list = [i for i in os.listdir(path)]
year_list.sort()
data = {}
for year in year_list:
    if is_year(year):
        path_report = os.path.join(path, year)
        files = os.listdir(path_report)
        files.sort()
        report = {i[-7:][:2] for i in files if i.endswith(".html")}

        data[year] = report
print(data)

