﻿-- à la première migration, copier coller le fichier 0001_initial.py de accounts.data_insert

CREATE EXTENSION dblink;
-- CENTRE PILOTE
INSERT INTO accounts_centrepilote
SELECT id, code, code_maj, nom, description, is_active
FROM dblink('dbname=cpcms_00',
		'SELECT id, code, code, nom, description, is_active FROM accounts_centrepilote')
AS a(id integer, code text, code_maj text, nom text, description text, is_active boolean);

SELECT pg_catalog.setval('public.accounts_centrepilote_id_seq', 11, false);

--LISTE ENVOI
INSERT INTO accounts_listeenvoi (id, nom, description, centrepilote_id)
SELECT id, nom, description, centrepilote_id
FROM dblink('dbname=cpcms_00',
	'select i.id, i.nom as nom, i.description, i.centre_pilote_id from inscriptions_listeenvois i, accounts_centrepilote cp
	where i.centre_pilote_id = cp.id')
AS a(id integer, nom text, description text, centrepilote_id integer);
SELECT pg_catalog.setval('public.accounts_listeenvoi_id_seq', 16, false);

-- ACCOUNTS
INSERT INTO accounts_regionagricole
SELECT id, code, nom
FROM dblink('dbname=cpcms_00',
		'SELECT id, code, nom FROM accounts_regionagricole')
AS a(id integer, code text, nom text);
SELECT pg_catalog.setval('public.accounts_regionagricole_id_seq', 16, false);


INSERT INTO accounts_profession
SELECT id, nom, description
FROM dblink('dbname=cpcms_00',
		'SELECT id, nom, description FROM accounts_profession')
AS a(id integer, nom text, description text);
SELECT pg_catalog.setval('public.accounts_profession_id_seq', 5, false);



INSERT INTO accounts_profile (id, password, last_login, is_superuser, first_name, last_name, email, email_confirmed, is_staff, is_active, date_joined,
mobile, phone, fax, nr_producteur, company, street, code_postal, localite, profession_id, force_password_change)
SELECT a.id, password, last_login, is_superuser, first_name, last_name, email, email_confirmed, is_staff, is_active, date_joined,
tel_sms, phone, fax, nr_producteur, nom_societe, adresse, code_postal, localite, profession_id, force_password_change
FROM dblink('dbname=cpcms_00',
		'SELECT auth_user.id, password, last_login, is_superuser, first_name, last_name, email, False, is_staff, is_active, date_joined,
		tel_sms, phone, fax, nr_producteur, nom_societe, adresse, code_postal, localite, profession_id, true
		FROM auth_user, accounts_cpprofile a
		where auth_user.id=a.user_id')
AS a(id integer, password text, last_login date, is_superuser boolean, first_name text, last_name text,
email text, email_confirmed boolean, is_staff boolean, is_active boolean, date_joined date,
tel_sms text, phone text, fax text, nr_producteur text, nom_societe text, adresse text, code_postal text, localite text, profession_id integer, force_password_change boolean)
where id > 12496

select max(id) from accounts_profile
SELECT pg_catalog.setval('public.accounts_profile_id_seq', 12496, false);

--update accounts_profile set email_confirmed = True where id between 100 and 110;
--update accounts_profile set email_confirmed = True where id between 1001 and 1009;

update accounts_profile set email = 'mvdk27-emilie@yahoo.com', username='emilie', first_name='Emilie', last_name='JOLIE' where username='toto1';
update accounts_profile set email = 'mvdk27-pierre@yahoo.com', username='pierre2', first_name='Pierot', last_name='MONAMI' where username='toto3';
delete from accounts_profile where username like 'toto%';

update accounts_profile set email_confirmed = True;
update accounts_profile set last_name = upper(last_name);

update accounts_profile set manage_cp_id = 2 where username in ('cepicop', 'pierre2', 'emilie')
update accounts_profile set force_password_change = False where username in ('cepicop', 'pierre2', 'emilie', 'admin')
-- 101 cepicop, 1003 pierre, 8833 p.houben


/* to be done in cpcms_00
--step 1
create table inscr as
select centre_pilote_id, user_id
from inscriptions_inscription where statut = 'active'
*/

-- step 2
insert into accounts_inscription (listeenvoi_id, profile_id, is_valid)
SELECT centre_pilote_id, user_id, True
FROM dblink('dbname=cpcms_00',
	'select centre_pilote_id, user_id from inscr')
AS a(centre_pilote_id integer, user_id integer);


-- combine step1 & 2:si profile_id absent (vérifier avec outer join si on perd bcp d'utilisateur
/*
insert into accounts_inscription (listeenvoi_id, profile_id, is_valid)
select centre_pilote_id, user_id, True
from dblink('dbname=cpcms_00', 'select centre_pilote_id, user_id from inscriptions_inscription where statut = ''active''')
AS i(centre_pilote_id integer, user_id integer), accounts_profile as profile
where profile.id = i.user_id
*/


-- MSG
insert into msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, listeenvoi_id)
SELECT id, msg_date, title, statut, centre_pilote_id, msg_created, liste_envois_id
FROM dblink('dbname=cpcms_00',
	'select id, msg_date, title, statut, centre_pilote_id, msg_created, liste_envois_id
	from msg_msg')
AS a(id integer, msg_date date, title text, statut text, centre_pilote_id integer, msg_created date, liste_envois_id integer);

select max(id) from msg_msg
SELECT pg_catalog.setval('public.msg_msg_id_seq', 154, false);

-- MSG_CATEGORIE
INSERT INTO msg_categorie (id, code, nom, icone, couleur)
SELECT id, code, nom, icone, couleur
FROM dblink('dbname=cpnew',
	'select id, code, nom, icone, couleur
	from msg_categorie')
AS (id integer, code text, nom text, icone text, couleur text);

SELECT pg_catalog.setval('public.msg_categorie_id_seq', 3, false);

-- FILER_FOLDER
INSERT INTO filer_folder (id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id)
SELECT id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id
FROM dblink('dbname=cpcms_00',
	'select id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id
	from filer_folder')
AS (id integer, name text, uploaded_at date, created_at date, modified_at date,
lft integer, rght integer, tree_id integer, level integer, owner_id integer, parent_id integer);

select max(id) from filer_folder
SELECT pg_catalog.setval('public.filer_folder_id_seq', 42, false);

-- FILER_FILE
INSERT INTO filer_file (id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description,
uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id)
SELECT id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description,
uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id
FROM dblink('dbname=cpcms_00',
	'select id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description,
uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id
	from filer_file')
AS (id integer, file text, _file_size bigint, sha1 text, has_all_mandatory_data boolean, original_filename text, name text, description text,
uploaded_at timestamp, modified_at timestamp, is_public boolean, folder_id integer, owner_id integer, polymorphic_ctype_id integer);

select max(id) from filer_file
SELECT pg_catalog.setval('public.filer_file_id_seq', 533, false);

/* ATTENTION correspondance avec polymorphic_ctype_id qui n'est pas la même dans la nouvelle DB 
=> 2 update nécessaire pour changer IMAGE id et FILE id */
select * from django_content_type where app_label = 'filer'

--select distinct polymorphic_ctype_id from filer_file

-- IMAGE id of django_content_type: 28 from cpcms_00 become 38 from cpilote
update filer_file set polymorphic_ctype_id = 38 where polymorphic_ctype_id = 28

-- FILE id of django_content_type: 25 from cpcms_00 become 35 from cpilote
update filer_file set polymorphic_ctype_id = 35 where polymorphic_ctype_id = 25



-- FILER_IMAGE
INSERT INTO filer_image (file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location)
SELECT file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location
FROM dblink('dbname=cpcms_00',
	'select file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location
	from filer_image')
AS (file_ptr_id integer, _height integer, _width integer, date_taken timestamp, default_alt_text text, default_caption text, author text,
must_always_publish_author_credit boolean, must_always_publish_copyright boolean, subject_location text);
-- no sequence for filer_image!

-- MSG DETAIL
insert into msg_msgdet (id, title, short_text, categorie_id, msg_id)
SELECT id, title, short_text, categorie_id, msg_id
FROM dblink('dbname=cpcms_00',
	'select id, title, short_text, categorie_id, msg_id
	from msg_msgdet')
AS a(id integer, title text, short_text text,
categorie_id integer, msg_id integer)

select max(id) from msg_msgdet
SELECT pg_catalog.setval('public.msg_msgdet_id_seq', 458, false);


-- EASY_THUMBNAIL_SOURCE
INSERT INTO easy_thumbnails_source (id, storage_hash, name, modified)
SELECT id, storage_hash, name, modified
FROM dblink('dbname=cpcms_00',
	'select id, storage_hash, name, modified
	from easy_thumbnails_source')
AS (id integer, storage_hash text, name text, modified timestamp)
select max(id) from easy_thumbnails_source
SELECT pg_catalog.setval('public.easy_thumbnails_source_id_seq', 561, false);


