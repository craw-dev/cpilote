--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.18
-- Dumped by pg_dump version 9.6.18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: msg_categorie; Type: TABLE DATA; Schema: public; Owner: cpnewadmin
--

INSERT INTO public.msg_categorie (id, code, nom, icone, couleur) VALUES (1, 'avert', 'Avertissement', 'fa-exclamation-triangle', '#ee9528');
INSERT INTO public.msg_categorie (id, code, nom, icone, couleur) VALUES (2, 'comm', 'Communication', 'fa-bullhorn', '#549127');
INSERT INTO public.msg_categorie (id, code, nom, icone, couleur) VALUES (3, 'event', 'Événement', 'fa-calendar-alt', '#7cbdc9');


--
-- Name: msg_categorie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpnewadmin
--

SELECT pg_catalog.setval('public.msg_categorie_id_seq', 3, false);


--
-- PostgreSQL database dump complete
--

