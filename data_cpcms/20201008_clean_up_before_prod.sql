﻿-- STEP by STEP DB tasks 4 cleanup
truncate table cms_aliaspluginmodel
truncate table djangocms_file_file
delete from cms_cmsplugin
delete from msg_msgdet where msg_id > 150
delete from msg_msg where id > 150
delete from cms_placeholder

SELECT pg_catalog.setval('public.msg_msg_id_seq', 151, false);
SELECT pg_catalog.setval('public.msg_msgdet_id_seq', 379, false);

SELECT pg_catalog.setval('public.cms_placeholder_id_seq', 1, false);

SELECT pg_catalog.setval('public.easy_thumbnails_source_id_seq', 561, false);

select count(*) from easy_thumbnails_source

delete from easy_thumbnails_thumbnail
delete from easy_thumbnails_source
delete from easy_thumbnails_thumbnaildimensions

delete from filer_image
delete from filer_file
truncate table filer_folder
delete from filer_thumbnailoption

select * from accounts_profile where work4_cp_id=2
update accounts_profile set manage_cp_id = 2, work4_cp_id = 2 where email = 'od.cepicop@centrespilotes.be'

insert profile from cpcms
update accounts_profile set force_password_change = false

select max(id) from accounts_profile
SELECT pg_catalog.setval('public.accounts_profile_id_seq', 12505, false);

select max(id) from msg_msg
SELECT pg_catalog.setval('public.msg_msg_id_seq', 164, false);

INSERT INTO filer_folder (id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id)
SELECT id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id
FROM dblink('dbname=cpcms_00',
	'select id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id
	from filer_folder')
AS (id integer, name text, uploaded_at date, created_at date, modified_at date,
lft integer, rght integer, tree_id integer, level integer, owner_id integer, parent_id integer);

select max(id) from filer_folder
SELECT pg_catalog.setval('public.filer_folder_id_seq', 44, false);

-----------------------------------------
-- FILER_FILE
INSERT INTO filer_file (id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description,
uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id, mime_type)
SELECT id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description,
uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id, concat('image/',CASE WHEN right(original_filename, 3)='jpg' THEN 'jpeg' ELSE right(original_filename, 3) END) mime_type
FROM dblink('dbname=cpcms_00',
	'select id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description,
uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id
	from filer_file')
AS (id integer, file text, _file_size bigint, sha1 text, has_all_mandatory_data boolean, original_filename text, name text, description text,
uploaded_at timestamp, modified_at timestamp, is_public boolean, folder_id integer, owner_id integer, polymorphic_ctype_id integer)
where right(original_filename, 3) in ('jpg', 'png', 'gif')

select max(id) from filer_file
SELECT pg_catalog.setval('public.filer_file_id_seq', 551, false);

/* ATTENTION correspondance avec polymorphic_ctype_id qui n'est pas la même dans la nouvelle DB 
=> 2 update nécessaire pour changer IMAGE id et FILE id */
select * from django_content_type where app_label = 'filer'

--select distinct polymorphic_ctype_id from filer_file

-- IMAGE id of django_content_type: 28 from cpcms_00 become 38 from cpilote
update filer_file set polymorphic_ctype_id = 38 where polymorphic_ctype_id = 28

-- FILE id of django_content_type: 25 from cpcms_00 become 35 from cpilote
update filer_file set polymorphic_ctype_id = 35 where polymorphic_ctype_id = 25


-- FILER_IMAGE
INSERT INTO filer_image (file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location)
SELECT file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location
FROM dblink('dbname=cpcms_00',
	'select file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location
	from filer_image')
AS (file_ptr_id integer, _height integer, _width integer, date_taken timestamp, default_alt_text text, default_caption text, author text,
must_always_publish_author_credit boolean, must_always_publish_copyright boolean, subject_location text);
-- no sequence for filer_image!


-- MSG DETAIL
insert into msg_msgdet (id, title, short_text, categorie_id, msg_id)
SELECT id, title, short_text, categorie_id, msg_id
FROM dblink('dbname=cpcms_00',
	'select id, title, short_text, categorie_id, msg_id
	from msg_msgdet')
AS a(id integer, title text, short_text text,
categorie_id integer, msg_id integer)
where msg_id > 150
--order by id desc limit 15

select max(id) from msg_msgdet
SELECT pg_catalog.setval('public.msg_msgdet_id_seq', 475, false);


-- EASY_THUMBNAIL_SOURCE
INSERT INTO easy_thumbnails_source (id, storage_hash, name, modified)
SELECT id, storage_hash, name, modified
FROM dblink('dbname=cpcms_00',
	'select id, storage_hash, name, modified
	from easy_thumbnails_source')
AS (id integer, storage_hash text, name text, modified timestamp)
select max(id) from easy_thumbnails_source
SELECT pg_catalog.setval('public.easy_thumbnails_source_id_seq', 579, false);







