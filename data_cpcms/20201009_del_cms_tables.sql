select * from cms_cmsplugin --where id in (138, 139)

delete from cms_page_placeholders
delete from cms_title
delete from cms_page
delete from cms_treenode
delete from cms_urlconfrevision
delete from cms_usersettings

delete from djangocms_text_ckeditor_text
delete from cms_cmsplugin

SELECT pg_catalog.setval('public.cms_cmsplugin_id_seq', 1, false);