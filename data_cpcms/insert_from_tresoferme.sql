﻿
-- accounts_geozip
------------------
INSERT INTO accounts_geozip (id, country_code, postal_code, place_name,
	admin_name1, admin_code1, admin_name2, admin_code2, admin_name3, admin_code3,
	latitude, longitude, accuracy, regionagricole_id)
SELECT id, country_code, postal_code, place_name,
	admin_name1, admin_code1, admin_name2, admin_code2, admin_name3, admin_code3,
	latitude, longitude, accuracy, agricultural_region_id
FROM dblink('dbname=tresoferme',
		'SELECT id, country_code, postal_code, place_name,
	admin_name1, admin_code1, admin_name2, admin_code2, admin_name3, admin_code3,
	latitude, longitude, accuracy, agricultural_region_id FROM accounts_geozip')
AS (id integer, country_code text, postal_code text, place_name text,
	admin_name1 text, admin_code1 text, admin_name2 text, admin_code2 text, admin_name3 text, admin_code3 text,
	latitude decimal, longitude decimal, accuracy integer, agricultural_region_id integer);


select max(id) from accounts_geozip
SELECT pg_catalog.setval('public.accounts_geozip_id_seq', 58930, false);



-- copy LU if needed
/*COPY accounts_geozip(country_code, postal_code, place_name,
	admin_name1, admin_code1, admin_name2, admin_code2, admin_name3, admin_code3,
	latitude, longitude, accuracy)
FROM '/home/pat/Documents/GeoNames/LU/LU.txt' DELIMITER E'\t' CSV HEADER;*/

-- dummy query
/*select p.code_postal, p.localite, z.postal_code, z.place_name
from accounts_profile p, accounts_geozip z
where z.postal_code < p.code_postal
and lower(z.place_name) < lower(p.localite)
and p.code_postal <> ''*/

-- create table with all zip to be found
create table loc as (
select id, code_postal, localite
from accounts_profile
where code_postal <> '')

-- get zip unfound
select l.id, l.code_postal, l.localite, z.postal_code, z.place_name
from loc l left outer join accounts_geozip z
on (z.postal_code = l.code_postal and lower(z.place_name) = lower(l.localite))
where z.postal_code is null and l.localite <> ''

-- create unique pairs postal_code, place_name
create table zipunq as
select postal_code, place_name from accounts_geozip
where country_code = 'BE'
group by postal_code, place_name
having count(*) = 1

-- update localite based on zipunq with code_postal OK
UPDATE loc
SET localite=zipunq.place_name
FROM zipunq
WHERE loc.code_postal=zipunq.postal_code

-- Specific UPDATES
update loc set code_postal = '1340' where localite = 'Ottignies';
update loc set code_postal = '5360' where localite = 'Hamois';
update loc set code_postal = '8790' where localite = 'Waregem';
update loc set localite = 'Meix-Le-Tige' where localite like 'Meix%';
update loc set localite = 'Chaussée-Notre-Dame-Louvignies' where localite = 'Chaussée-Notre-Dame';
-- LU upd
update loc set localite = 'Meispelt', code_postal='L-8291' where code_postal = '8291';
update loc set localite = 'Lellig', code_postal='L-6839' where code_postal = '6839';
-- FR upd
update loc set localite = 'Cugny' where code_postal = '02480';
update loc set localite = 'Auchy-lez-Orchies' where code_postal = '59310';
update loc set localite = 'Abancourt' where code_postal = '59268';
update loc set localite = 'Inchy-en-Artois', code_postal = '62860' where code_postal = '6286';
update loc set localite = 'Beaurevoir' where code_postal = '02110';

-- We lose 1 line with codepostal 1230
UPDATE accounts_profile
SET zip_id = m.geozip_id
FROM (
select l.id as profile_id, z.id as geozip_id
from loc l inner join accounts_geozip z
on (z.postal_code = l.code_postal and lower(z.place_name) = lower(l.localite))
) m
WHERE accounts_profile.id = m.profile_id

select * from loc