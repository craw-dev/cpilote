"""agricogest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# djauth/urls.py
from . import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from accounts import ajax_call
from msg import ajax_call as msg_ajax_call
from django.conf.urls import url


urlpatterns = [
    path('chapeau/', admin.site.urls),
    path('accounts/', include('accounts.urls'), name='accounts'),
    path('agenda/', include('agenda.urls'), name='agenda'),
    path('accueil/', include('info.urls'), name='info'),
    path('odk/', include('odk.urls'), name='odk'),
    # path('culture/', include('cult.urls'), name='cult'),
    url(r'^filer/', include('filer.urls')),
    path('', include('cms.urls'), name='cpcms'), # HBP attention BASE_URL du CMS
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns = urlpatterns + [
    path('ajax/ddl_zip', ajax_call.ddl_zip, name='ajax_ddl_zip'),
    path('ajax/ddl_contact', ajax_call.ddl_contact, name='ajax_ddl_contact'),
    path('ajax/inscription/upd', ajax_call.inscription_update, name='inscription_update'),
    path('ajax/inscription_block/create', ajax_call.inscription_block_create, name='inscription_block_create'),
    path('ajax/msgdet/del', msg_ajax_call.msgdet_delete, name='msgdet_delete'),
]


# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns = [
#         path('__debug__/', include(debug_toolbar.urls)),
#     ] + urlpatterns
