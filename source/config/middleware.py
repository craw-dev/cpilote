# -*- coding: utf-8 -*-
from django.shortcuts import redirect

def is_profile_complete(get_response):
    """
    A gérer en paralèle avec config.middleware.ProfileUpdateView
    """

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        response = get_response(request)
        user = request.user
        if "ajax" not in request.path and "upd" not in request.path and "static" not in request.path:
            if user.is_authenticated:
                if (user.company == '' or
                    user.profession is None or
                    user.zip is None or
                    (user.profession.id < 4 and user.modeprod is None)):
                        return redirect('profile_update', user.id)

        return response

    return middleware
