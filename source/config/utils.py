# -*- coding: utf-8 -*-
import logging
from datetime import datetime
from django.utils.formats import get_format
from django.core import mail
from django.core.mail import get_connection
from django.db import connection
from .settings import EMAIL_BACKEND, PROJECT_NAME


__author__ = 'Patrick HOUBEN'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2019, Patrick HOUBEN'
__license__ = 'GPLv3'
__date__ = '2019-01-18'
__version__ = '0.1'
__status__ = 'Development'


# def get_nbrec_filter(sql_string, params=()):
#     """
#     https://exceptionshub.com/django-count-rawqueryset.html
#     """
#     cursor = connection.cursor()
#     try:
#         cursor.execute(sql_string, params)
#         myids = [x[0] for x in cursor]
#         return len(myids)
#     finally:
#         cursor.close()
def is_year(myvar):
    try:
        yyyy = int(myvar)
        if yyyy > 1990 and yyyy < 2050:
            return True
        else:
            return False
    except ValueError():
        return False



def get_nbrec(sql_string, params=()):
    """
    Inspired from https://exceptionshub.com/django-count-rawqueryset.html
    and https://exceptionshub.com/django-count-rawqueryset.html (not applicable here)
    Count(*) record from raw sql
    """
    cursor = connection.cursor()
    cursor.execute(sql_string, params)
    row = cursor.fetchone()
    return row[0]


def parse_date(date_str):
    """
    https://stackoverflow.com/questions/22918095/django-string-to-date-format
    Parse date from string by DATE_INPUT_FORMATS of current language
    """
    for item in get_format('DATE_INPUT_FORMATS'):
        try:
            return datetime.strptime(date_str, item).date()
        except (ValueError, TypeError):
            continue
    return None


def build_error_message(cause, error, user):
    "Not used in 20200210"
    if (cause or error or user) is None:
        return "Please define cause, error and user"
    msg = "Cause: {0} - Error: {1} - User: {2}".format(cause, error, user)
    return msg


class MailingAdmins(logging.Handler):
    """
    Simple email to Admins, inspired from django.utils.log.AdminEmailHandler
    """
    subject = "{0}: SERVER ERROR 500".format(PROJECT_NAME)

    def __init__(self, subject=subject, user=None):
        super().__init__()
        self.subject = subject
        self.user = user

    def emit(self, record):
        # if user passed as extra: logger.error(msg, extra={'user': request.user})
        # but passed in msg
        self.subject += ' | user: {0}'.format(record.user)
        self.send_mail(subject=self.subject,
                       message=self.format(record), fail_silently=True)

    def send_mail(self, subject, message, *args, **kwargs):
        mail.mail_admins(subject, message, *args,
                         connection=self.connection(), **kwargs)

    def connection(self):
        return get_connection(backend=EMAIL_BACKEND, fail_silently=True)


class UserLogFilter(logging.Filter):
    def filter(self, record):
        if not hasattr(record, 'user'):
            record.user = '--'
        return True
