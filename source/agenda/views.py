# -*- coding: utf-8 -*-
"""
agenda.views description
"""
__author__ = 'hbp'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2017, Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2021-06-07'
__version__ = '1.0'
__status__ = 'Development'

# External
from datetime import datetime, date, timedelta
from calendar import monthrange
from braces.views import LoginRequiredMixin

# Django
from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
from django.views.generic import DetailView, UpdateView, DeleteView, CreateView
from django.views.generic.edit import ModelFormMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext as _
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test, login_required

# App
from msg.views import user_is_manager
from accounts.views import MessageMixin
from .models import Event
from .forms import EventForm
from .utils import ContestCalendar


def add_month(my_date):
    days_in_month = monthrange(my_date.year, my_date.month)[1]
    return (my_date + timedelta(days=days_in_month))


def del_month(my_date):
    days_in_month = monthrange(my_date.year, my_date.month)[1]
    return (my_date - timedelta(days=days_in_month))


def home(request):
    """
    Show calendar of events this month
    """
    current_date = date.today()
    return event_list(request, current_date.year, current_date.month)


def event_list(request, p_year, p_month):
    """
    Show calendar of events for specified month and year
    """
    l_year = int(p_year)
    l_month = int(p_month)
    mid_date = datetime(l_year, l_month, 15)
    current_date = date.today()
    # lCalendarToMonth = datetime(l_year, l_month, monthrange(l_year, l_month)[1])
    # event_qs = Event.objects.filter(begin_date__gte=current_date, begin_date__lte=lCalendarToMonth)
    event_qs = Event.objects.filter(begin_date__year=l_year, begin_date__month=l_month)
    cp_calendar = ContestCalendar(event_qs).formatmonth(l_year, l_month)

    from_date = del_month(mid_date)
    to_date = add_month(mid_date)
    from_date2 = del_month(from_date)
    to_date2 = add_month(to_date)

    return render(request, 'agenda/event_calendar.html',
        {
            'cp_calendar': mark_safe(cp_calendar),
            'mid_date': mid_date,
            'from_date': from_date,
            'to_date': to_date,
            'from_date2': from_date2,
            'to_date2': to_date2,
            'current_date': current_date,
            'breadcrumb_display': 'Agenda'
        })


class EventDetailView(DetailView):
    model = Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.object.get_title()
        context['breadcrumb_display'] = self.object.get_title()
        if "copy_confirm" in self.request.path_info:
            confirm_msg = _("Cliquer sur confirmer pour copier l'événement ci-dessous. COPIE sera ajouté au titre.")
            confirm_url = reverse("agenda:copy", args=[str(self.object.id)])
        return context         


class EventDeleteView(LoginRequiredMixin, MessageMixin, DeleteView):
    model = Event
    template_name = "agenda/event_detail.html"
    success_url = reverse_lazy("agenda:event_home")
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['confirm_message'] = _("Supprimer cet événement?")
        context['title'] = "{} » Supprimer".format(self.object.get_title())
        context['breadcrumb_display'] = context['title']
        return context    


class EventUpdateView(LoginRequiredMixin, MessageMixin, UpdateView):
    model = Event
    form_class = EventForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "{} » Modifier".format(self.object.get_title())
        context['breadcrumb_display'] = context['title']
        return context


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.centrepilote = self.request.user.manage_cp
        obj.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Agenda » Ajouter événement'
        context['breadcrumb_display'] = context['title']
        return context


# @login_required
# @user_passes_test(user_is_manager)
# def event_copy(request, event_id):
#     """
#     Copier événement
#     """
#     event = Event.objects.get(id=event_id)
#     org_set = event.organisation_set.all()

    # eventcopy = Event.objects.create(
    #     centrepilote=event.centrepilote,
    #     title=event.title,
    #     short_desc=event.short_desc,
    #     event_type=event.event_type,
    #     culture_txt=event.culture_txt,
    #     modeprod=event.modeprod,
    #     visio_link=event.visio_link,
    #     public_cible=event.public_cible,

    #     begin_date=event.begin_date,
    #     begin_time=event.begin_time,
    #     end_date=event.end_date,
    #     end_time=event.end_time,
    #     street=event.street,
    #     codepostal=event.codepostal,
    #     place=event.place,
    # )

    # eventcopy.organisation.add(org_set)

    # return redirect(eventcopy.get_absolute_url())

@login_required
@user_passes_test(user_is_manager)
def event_copy(request, pk):
    """
    Copier événement
    """
    event = Event.objects.get(id=pk)
    

    # obj=Signup_teacher.objects.get(id=4) #2nd Method
    if request.method == 'POST':  
        form = EventForm(request.POST or None)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.centrepilote = event.centrepilote
            obj.save()
            orgs = event.organisation.all()
            for org in orgs:
                obj.organisation.add(org.id)
            # userid=form.cleaned_data['userid']
            # return render(request,'agenda_detail.htm')
            messages.success(request, "Événement copié")
            return redirect(obj.get_absolute_url())
    else:
        form = EventForm(instance=event)
        messages.info(request, "Adapter la date et l'heure de l'événement avant de sauver.")

    context = {
        'form': form,
        'object': event
    }
    

    return render(request, 'agenda/event_form.html', context)
