# encoding:utf-8
'''
Created on 07/06/2021
@author: p.houben
'''
from django.urls import path
# from django.views.i18n import JavaScriptCatalog
from agenda import views

app_name = "agenda"

urlpatterns = [
    path('', views.home, name='event_home'),
    path('<p_year>/<p_month>/', views.event_list, name='event_list'),
    path('<pk>', views.EventDetailView.as_view(), name='event_detail'),
    path("<pk>/copy", views.event_copy, name="event_copy"),
    path('<pk>/upd', views.EventUpdateView.as_view(), name='event_update'),
    path('<pk>/del', views.EventDeleteView.as_view(), name='event_delete'),
    path('add/', views.EventCreateView.as_view(), name='event_create'),
    # path('jsi18n/', JavaScriptCatalog.as_view(), name='js_catalog'),
]
