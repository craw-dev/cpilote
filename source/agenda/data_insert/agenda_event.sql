--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Ubuntu 13.3-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.3 (Ubuntu 13.3-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: agenda_event; Type: TABLE DATA; Schema: public; Owner: cpiloteadmin
--

INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (13, 'Ath', '', 'visit', 'Céréales', NULL, '2021-06-25', '10:00:00', NULL, NULL, 'rue de l''agriculture', '7800 Ath', 'Ferme expérimentale et pédagogique', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (14, 'Ath', '', 'visit', 'Céréales', NULL, '2021-06-23', '09:30:00', NULL, NULL, 'rue de l''agriculture', '7800 Ath', 'Ferme expérimentale et pédagogique', 2, 4);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (10, 'Acosse', '', 'visit', 'Froment, Escourgeon, Blé dur', NULL, '2021-06-24', '13:30:00', NULL, '16:30:00', 'rue de Hannêche, 19', '4219 Acosse', 'M. Gathy', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (1, 'Chièvres', '', 'visit', 'Froment, Triticale, Epeautre, Petit épeautre, Orge brassicole hiver', NULL, '2021-06-16', '10:00:00', '2021-06-16', '12:00:00', 'Rue d''Ath', '7950 Chièvres', 'Moulin de la Hunelle', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (2, 'Horion-Hozémont', '', 'visit', 'Froment, Triticale, Epeautre, Petit épeautre, Orge brassicole hiver', NULL, '2021-06-17', '15:00:00', NULL, NULL, 'Rue du Saou (50°38''24''''-5°22''59''''E)', '4460 Horion-Hozémont', 'M. de Grady', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (4, 'Ernage', '', 'visit', 'Froment, Epeautre, Blé dur, Tournesol', NULL, '2021-06-17', '14:00:00', '2021-06-17', '16:30:00', 'Chemin de la Haute Baudeceet', '5030 Hernage', 'M. Vandeputte', 2, 1);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (3, 'Roloux', '', 'visit', 'Froment', NULL, '2021-06-17', '17:00:00', NULL, NULL, '50° 39'' 01'''' N - 5° 24'' 32'''' E', '4347 Roloux', 'M. Royer', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (5, 'Pailhe', '', 'visit', 'Froment', NULL, '2021-06-18', '14:00:00', NULL, NULL, '50° 25'' 50'''' N - 5° 16'' 24'''' E', '4560 Paihe', 'Ass. Tasiaux', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (7, 'Bombaye', '', 'visit', 'Froment', NULL, '2021-06-21', '10:00:00', NULL, NULL, '50° 44'' 20'''' N - 5° 44'' 40'''' E', '4607 Bombaye', 'M. Henry', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (9, 'Lonzée', '', 'visit', 'Orge brassicole', NULL, '2021-06-22', '16:00:00', NULL, '18:00:00', 'Vieux chemin de Namur   50°32''51.2"N 4°43''57.5"E', '5030 Lonzée (Gembloux)', 'Phillipe et Charles Van Eyck', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (8, 'Gesves', '', 'visit', 'Froment, Epeautre', NULL, '2021-06-21', '13:30:00', '2021-06-21', '16:30:00', 'Rue du Pourrain, 32', '5340 Gesves', 'M. Deloy', 2, 1);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (12, 'Isnes', '', 'visit', 'Colza, Nouvelles cultures et Protéagineux : Pois, Féverole, Lupin', NULL, '2021-06-24', '14:00:00', NULL, '16:30:00', 'Route d''Eghezée 50°29''58.5"N 4°44''02.7"E', '5032 Isnes', 'Ferme de la Faculté de Gembloux Agro-Bio Tech', 2, NULL);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (11, 'Lonzée', '', 'visit', 'Froment, Froment-pois, Escourgeon, Orge de printemps, Orge brassicole, Avoine, Lin oléagineux', NULL, '2021-06-24', '10:00:00', NULL, '14:00:00', 'Vieux chemin de Namur   50°32''51.2"N 4°43''57.5"E', '5030 Lonzée (Gembloux)', 'Phillipe et Charles Van Eyck', 2, 1);
INSERT INTO public.agenda_event (id, title, short_desc, event_type, culture_txt, visio_link, begin_date, begin_time, end_date, end_time, street, codepostal, place, centrepilote_id, public_cible_id) VALUES (6, 'Evelette (Ohey)', '', 'visit', 'Froment, Epeautre, Triticale, Blé dur', NULL, '2021-06-18', '14:00:00', '2021-06-18', '16:30:00', 'Rue de L''Orgalise (50°25''21''''N-5°10''07''''E)', '5354 Evelette (Ohey)', 'M. Tonglet', 2, 1);


--
-- Name: agenda_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpiloteadmin
--

SELECT pg_catalog.setval('public.agenda_event_id_seq', 14, true);


--
-- PostgreSQL database dump complete
--

