from django.db import models
from ckeditor.fields import RichTextField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from accounts.models import CentrePilote, GeoZip, ModeDeProduction, Profession
from info.models import WebLink


class Event(models.Model):
    EVENT_TYPE = (
        ("event", "Événement"),
        ("train", "Formation"),
        ("visit", "Visite"),
    )    
    centrepilote = models.ForeignKey(
        CentrePilote,
        on_delete=models.PROTECT,
        verbose_name=_("Centre Pilote")
    )  
    title = models.CharField(_("Titre"), max_length=255)
    short_desc = RichTextField(_("Descriptif"), blank=True, null=True)
    event_type = models.CharField(
        _("Type d'événement"),
        max_length=5,
        choices=EVENT_TYPE,
        default="visit"
    )    
    culture_txt = models.CharField(_("Cultures"), max_length=500,
        help_text=_("Liste des cultures séparé par une virgule")
        )
    modeprod = models.ManyToManyField(
        ModeDeProduction, verbose_name=_("Type d'essais"),
        help_text=_("Maintenir la touche 'Ctrl' enfoncée pour sélectionner plusieurs essais")
    )
    visio_link = models.URLField(_("Visio link"), blank=True, null=True,
        help_text=_("URL de la visio si nécessaire")
    )
    public_cible = models.ForeignKey(
        Profession, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_('Public cible'),
        help_text=_("Ne rien préciser si tout public")    
    )
    begin_date = models.DateField(_("Date"))
    begin_time = models.TimeField(_("Heure début"))
    end_date = models.DateField(_("Date fin"), blank=True, null=True)
    end_time = models.TimeField(_("Heure fin"), blank=True, null=True)
    street = models.CharField(_("Rue + Nr"), max_length=255, blank=True, null=True, help_text=_("Nom de rue, numéro"))
    # ziploc = models.ForeignKey(GeoZip, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_("Code postal"))
    codepostal = models.CharField(_('Code postal'), max_length=255, blank=True, null=True, help_text=_('CP et localisation'))
    place = models.CharField(_("Lieu dit/Ferme"), max_length=255, blank=True, null=True)
    organisation = models.ManyToManyField(WebLink, verbose_name="Organisé par", blank=True,
        help_text=_("Maintenir la touche 'Ctrl' enfoncée pour sélectionner plusieurs organisateurs")
    )

    class Meta:
        verbose_name = _("Agenda")
        ordering = ["begin_date", "begin_time"]
    
    def beginend_time(self):
        # begin_date = self.begin_date.strftime('%A %d %B %Y')
        begin_time = self.begin_time.strftime('%H:%M')
        if self.end_time:
            end_time = self.end_time.strftime('%H:%M')
            return f'<span class="time-display">{begin_time}</span><span class="m-2">&raquo;</span><span class="time-display">{end_time}</span>'
        else:
            return f'<span class="time-display">{begin_time}</span>'

    @property
    def get_public_cible(self):
        if self.public_cible is None:
            return "Tout public"
        else:
            return self.public_cible.description

    @property
    def get_modeprod_list(self):
        retstr = ", ".join([a.nom for a in self.modeprod.all()])
        return retstr.strip()

    @property
    def get_organisation_list(self):
        retstr = " ".join([a.get_logo() for a in self.organisation.all()])
        return retstr.strip()

    @property
    def get_organisation_list_large(self):
        retstr = " ".join([a.get_logo_large() for a in self.organisation.all()])
        return retstr.strip()

    def get_absolute_url(self):
        return reverse('agenda:event_detail', args=[str(self.id)])

    def get_title(self):
        begin_date = self.begin_date.strftime('%d/%m/%Y')
        begin_time = self.begin_time.strftime('%H:%M')
        return "{0} » {1} {2} - {3}".format(self._meta.verbose_name, self.title, begin_date, begin_time)

    def __str__(self):
        return self.title