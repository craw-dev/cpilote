from django.contrib import admin

from .models import Event


class EventAdmin(admin.ModelAdmin):
    list_display = ('centrepilote', 'title', 'begin_date', 'street', 'codepostal')
    list_filter = ('centrepilote',)
    search_fields = ('title', 'short_desc',)


admin.site.register(Event, EventAdmin)
