# encoding:utf-8
# AdminDateWidget & AdminTimeWidget inspired from https://www.youtube.com/watch?v=OD6kZH7-rOU
# https://stackoverflow.com/questions/38601/using-django-time-date-widgets-in-custom-form#comment2861960_72284
from django.forms import ModelForm
from django.contrib.admin.widgets import FilteredSelectMultiple, AdminDateWidget, AdminTimeWidget
from django.utils.translation import ugettext_lazy as _


from .models import Event
from info.models import WebLink


class EventForm(ModelForm):
    """Event form"""
    class Meta:
        model = Event
        fields = ["organisation",
            "title", "event_type",
            "culture_txt", "modeprod", "visio_link", 
            "begin_date", "begin_time", "end_time",
            "street", "codepostal", "place", 
            "public_cible", "short_desc"]
        widgets = {
            "organisation" : FilteredSelectMultiple("organisation", False),
            'begin_date': AdminDateWidget(),
            'begin_time': AdminTimeWidget(),
            'end_time': AdminTimeWidget(),
        }
    class Media:
        extend = True
        css = {
            'all': [
                # 'css/dist/bootstrap.min.css',               
                # 'admin/css/base.css', # fout en l'air les nav tabs
                'admin/css/widgets.css',
                # 'cms/css/3.8.0/cms.base.css',
            ]
        }
        js = (
            'js/django_global.js',
            'admin/js/core.js',
            # 'admin/js/SelectBox.js',
            # 'admin/js/SelectFilter2.js',
            'admin/js/calendar.js',
            'admin/js/admin/DateTimeShortcuts.js',
            'ckeditor/ckeditor-init.js',
            'ckeditor/ckeditor/ckeditor.js',
        )        

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['organisation'].queryset = WebLink.objects.filter(menucateg='partenaire')
        # self.fields['begin_date'].value = parse_date(self.fields['begin_date'])