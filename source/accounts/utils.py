import logging

from django.utils import timezone
from django.db.models.functions import Lower 
from accounts.models import Profile, ListeEnvoi, ProfileTmp, GeoZip, Inscription, Profession

LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")

def create_users(hero_email='cplv@hero.com', le_str=''):
    """
    hero_email: genereric user containing strong password users creation
    lisetenvoi_list: list of listeenvoi
    Run 'zchecks/accounts/check_zip_users.py' before
    """
    # get @hero passwd
    hero = Profile.objects.get(email=hero_email)
    hero_pass = hero.password

    # get listeenvoi
    le_list = le_str.split(',')
    le_qs = ListeEnvoi.objects.filter(code__in=le_list)

    profile_tmp = ProfileTmp.objects.all()
    for tmp in profile_tmp:
        if tmp.code_postal is None:
            code_postal = None
        else:
            code_postal = tmp.code_postal.strip()
        if code_postal is not None and code_postal != 'NULL':
            if tmp.localite is None:
                localite = None
            else:
                localite = tmp.localite.strip().lower()
            # print(f'{code_postal} - {localite}')
            try:
                gz = GeoZip.objects.annotate(lower_place_name=Lower("place_name")).get(postal_code=code_postal, lower_place_name=localite)
                # print(gz)
            except GeoZip.DoesNotExist:
                gz_wrong = f'{code_postal} - {localite} DoesNotExist for profiletmp_id = {tmp.id}\n'
                LOG.info(gz_wrong)
                return 'Re-execute zchecks/accounts/check_zip_users.py !!!'
        else:
            gz = None

        #
        # Select Profession instance
        #
        if tmp.profession is None:
            profession = None
        else:
            profession = Profession.objects.get(code=tmp.profession)
        #
        # Start Pofile Insert
        #
        try:
            user = Profile.objects.get(email=tmp.email)
        except Profile.DoesNotExist:
            try:
                user = Profile.objects.create(
                    email=tmp.email,
                    email_confirmed=True,
                    password=hero_pass,
                    is_superuser=False,
                    first_name=tmp.first_name,
                    last_name=tmp.last_name,
                    is_staff=False,
                    is_active=True,
                    date_joined=timezone.now(),
                    mobile=tmp.mobile,
                    phone=tmp.phone,
                    fax=tmp.fax,
                    company=tmp.company,
                    profession = profession,
                    profession_txt=tmp.profession_txt,
                    jobdesc=tmp.jobdesc,
                    nr_producteur=tmp.nr_producteur,
                    street=tmp.street,
                    zip=gz,
                    # modeprod=tmp.modeprod,
                    code_postal=tmp.code_postal,
                    localite=tmp.localite
                )
                LOG.info(f'user {user.email} created')
            except Exception as err:
                LOG.error(f"Error while creating {tmp.email}")
                LOG.error(err)
                return 'Error create user'

            # Inscrire seulement les nouveaux
            try:
                for le in le_qs:
                    inscr, created = Inscription.objects.get_or_create(
                        profile=user,
                        listeenvoi=le
                    )
                    LOG.info(f'user {user.email} inscrit à la liste envoi {le}')
            except Exception as inscr_err:
                LOG.error(f"Error while creating inscriptions for {tmp.email}")
                LOG.error(inscr_err)
                return 'Error inscription user'

    return 'Success, check log info'