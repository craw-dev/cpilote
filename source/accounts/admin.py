# encoding:utf-8
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext as _
# from django.contrib.auth.forms import (
#     AdminPasswordChangeForm, UserChangeForm, UserCreationForm,
# )

from .models import Profile, ListeEnvoi, Inscription, CentrePilote, Profession, ModeDeProduction, CentrePiloteLocation

# change title in admin
admin.site.site_header = _('Centres Pilotes: Administration')
admin.site.site_title = _('Centres Pilotes: Administration')


class ProfileAdmin(UserAdmin):
    add_form_template = 'admin/auth/user/add_form.html'
    list_display = ('email', 'full_name_display', 'profession', 'company')
    filter_horizontal = ('groups',)
    ordering = ('-last_login',)
    search_fields = ('first_name', 'last_name', 'email', 'company')
    list_filter = ('profession', 'work4_cp', 'is_active', 'is_staff')
    fieldsets = [
        (_('Utilisateur'), {'fields': ['email', 'first_name',
                                       'last_name', 'password']}),
        (_('Entreprise-ASBL'), {'fields': ['company', 'profession', 'modeprod', 'jobdesc', 'jobdesc_order']}),
        (_('Accès'), {'fields': ['email_confirmed', 'is_active', 'is_staff', 'manage_cp', 'work4_cp', 'groups']}),
        (_('Date'), {'fields': ['date_joined', 'last_login']}),
        (_('Contact'), {'fields': ['mobile', 'phone', 'street',]})
    ]
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )    
    # form = UserChangeForm
    # add_form = UserCreationForm
    # change_password_form = AdminPasswordChangeForm

    def full_name_display(self, profile):
        return profile.get_full_name()
    full_name_display.short_description = _('Nom')


class ListeEnvoiAdmin(admin.ModelAdmin):
    list_display = ('centrepilote', 'code', 'nom', 'description')
    list_filter = ('nom',)
    search_fields = ('nom', 'description',)

class InscriptionAdmin(admin.ModelAdmin):
    list_display = ('full_name_display', 'listeenvoi')
    list_filter = ('listeenvoi__centrepilote', 'listeenvoi',)
    search_fields = ('profile__last_name', 'profile__first_name', 'profile__email')
    ordering = ('profile__last_name', 'profile__first_name',)

    def full_name_display(self, inscr):
        return inscr.profile.get_full_name()
    full_name_display.short_description = _('Nom')


# class CPLocationTabularInline(admin.TabularInline):
#     model = CentrePiloteLocation
#     # ne fonctionne pas

class CentrePiloteAdmin(admin.ModelAdmin):
    # inlines = [CPLocationTabularInline] ne fonctionne pas
    list_display = ('acronyme', 'nom', 'secteur',)
    list_filter = ('acronyme',)
    search_fields = ('nom', 'description',)


class CPLocationAdmin(admin.ModelAdmin):
    list_display = ('centrepilote', 'intitule', 'title', 'subtitle',)
    list_filter = ('centrepilote',)
    search_fields = ('intitule', 'title', 'subtitle', 'street')

# admin.site.unregister(User)

admin.site.register(CentrePilote, CentrePiloteAdmin)
admin.site.register(CentrePiloteLocation, CPLocationAdmin)
admin.site.register(ListeEnvoi, ListeEnvoiAdmin)
admin.site.register(Inscription, InscriptionAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Profession)
admin.site.register(ModeDeProduction)

