'''
Created on 21 aug 2018
@author: p.houben
'''
import logging

from braces.views import LoginRequiredMixin, UserPassesTestMixin

from django.core.exceptions import PermissionDenied
from django.views import generic
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.db import IntegrityError
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from .models import Inscription, ListeEnvoi
from .forms import ListeEnvoiForm, InscriptionSearchForm
from .views import MessageMixin
from config.settings import PROJECT_NAME

LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")


@login_required
def inscription_1_user(request, pk):
    """
    Listing inscription par centrepilote
    """
    user_model = get_user_model()
    myuser = user_model.objects.get(pk=pk)
    listeenvois = ListeEnvoi.objects.raw('''
        select le.id, le.nom, inscr.listeenvoi_id, coalesce(inscr.is_valid, False) as is_valid, le.centrepilote_id
        from accounts_listeenvoi as le
        inner join accounts_centrepilote as cp on le.centrepilote_id = cp.id
        left join (select listeenvoi_id, is_valid
                    from accounts_inscription
                    where profile_id = {0}) inscr
        on inscr.listeenvoi_id = le.id
        where cp.is_sending_msg = True
        order by le.centrepilote_id ASC
        '''.format(pk))

    # ne pas changer user: request.user!
    context = {
        'user': request.user,
        'listeenvois': listeenvois,
        'myuser': myuser,
        'title': f"Inscriptions » {myuser.get_full_name()}",
        'breadcrumb_display': f"Inscriptions » {myuser.get_full_name()}",
    }

    return render(request, 'accounts/select_inscription1user_form.html', context)


class ListeEnvoiListView(LoginRequiredMixin, UserPassesTestMixin, generic.ListView):
    model = ListeEnvoi

    def test_func(self, user, *args, **kwargs):
        self.user = user
        if user.is_manager:
            return True
        else:
            raise PermissionDenied

    def get_queryset(self):
        qs = self.model.objects.filter(centrepilote__id=self.user.manage_cp.id)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f"Liste envoi » {self.request.user.manage_cp}"
        context['breadcrumb_display'] = context['title']
        return context         


class ListeEnvoiCreateView(LoginRequiredMixin, MessageMixin, generic.CreateView):
    model = ListeEnvoi
    form_class = ListeEnvoiForm
    template_name = "accounts/listeenvoi_form.html"
    success_url = reverse_lazy("listeenvoi_list")

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.centrepilote = self.request.user.manage_cp
        obj.save()
        self.success_url = self.model.get_absolute_url(obj)
        return super(ListeEnvoiCreateView, self).form_valid(form)


class ListeEnvoiDetailView(LoginRequiredMixin, generic.DetailView):
    model = ListeEnvoi

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.object.get_title()
        context['breadcrumb_display'] = self.object.get_title()
        return context    


class ListeEnvoiUpdateView(LoginRequiredMixin, MessageMixin, generic.UpdateView):
    model = ListeEnvoi
    form_class = ListeEnvoiForm
    template_name = "accounts/listeenvoi_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f"{self.object.get_title()} » Modifier"
        context['breadcrumb_display'] = self.object.get_title()
        return context    


class ListeEnvoiDeleteView(LoginRequiredMixin, MessageMixin, generic.DeleteView):
    model = ListeEnvoi
    form_class = ListeEnvoiForm
    template_name = "accounts/listeenvoi_detail.html"
    success_url = reverse_lazy("listeenvoi_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f"{self.object.get_title()} » Supprimer"
        context['breadcrumb_display'] = self.object.get_title()
        return context

    # def dispatch(self, request, *args, **kwargs):
    #     self.user = request.user
    #     return super().dispatch(request, *args, **kwargs)

    # def is_deletable(self, obj):
    #     """
    #     return a list of related objects if not deletable
    #     Inspired from https://gist.github.com/freewayz/69d1b8bcb3c225bea57bd70ee1e765f8
    #     """
    #     related_list = []
    #     # get all the related object
    #     for rel in obj._meta.get_fields():
    #         try:
    #             # check if there is a relationship with at least one related object
    #             related = rel.related_model.objects.filter(**{rel.field.name: obj})
    #             if related.exists():
    #                 related_list.append(related)
    #         except AttributeError:  # an attribute error for field occurs when checking for AutoField
    #             pass  # just pass as we dont need to check for AutoField
    #     return related_list

    # def delete(self, request, *args, **kwargs):
    #     """
    #     Delete Liste Envoi
    #     Check related before delete and display warning message
    #     If DB Integrity Error, display error
    #     """
    #     obj = self.model.objects.get(pk=kwargs["pk"])
    #     # check related before delete
    #     related_list = self.is_deletable(obj)
    #     if related_list:
    #         messages.warning(request,
    #             _("La suppression de cette liste d'envoi " +\
    #             "entrainera la suppression de toutes les inscriptions qui y font référence.")
    #         )
    #         # HBP: list inscription per liste envoi
    #         # for related in related_list:
    #         #     related_model = related.model._meta.verbose_name
    #         #     related_values = [i.__str__() for i in related.all()]
    #         #     msg = "{0}: {1}".format(related_model, related_values)
    #         #     messages.warning(request, msg)
    #         return render(request, template_name=self.template_name, context={'object': obj})
        
    #     # delete() but display nice error if integrity error
    #     return(super().delete(request, *args, **kwargs))


    # def get_success_url(self):
    #     return reverse('listeenvoi_list', args=[str(self.request.user.id)])


# from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from config.utils import get_nbrec

@login_required
def inscription_1_listeenvoi(request, pk):
    """
    Listing user inscription par listeenvoi
    HBP: possible de faire les recherches par jQuery et simplifier la requête et pas de pagination!
    """
    # Upd objects and queryset display
    user_model = get_user_model()
    listeenvoi = get_object_or_404(ListeEnvoi, pk=pk)

    def sort_user(request):
        """
        if sortBy presence is pair=>desc, unpair=>asc
        """
        if request.GET.get("sortBy"):
            myurl = request.get_full_path()
            urlcut = myurl.split('&')
            mysort = [x for x in urlcut if 'sortBy' in x]
            for i in mysort:
                n = urlcut.count(i)
                if (n % 2) == 0:
                    sorting = " order by {} desc".format(i[7:])
                else:
                    sorting = " order by {}".format(i[7:])
        else:
            sorting = " order by p.date_joined desc"
        return sorting

    params = {'pk': pk}
    sql_where = ""
    # Adapt query based on search
    if request.GET.get("txtsearch"):
        txtsearch = request.GET.get("txtsearch", None).lower()
        txtsearch = '%{}%'.format(txtsearch)
        params['txtsearch'] = txtsearch
        sql_where = ''' and (
            lower(p.last_name) like %(txtsearch)s or
            lower(p.first_name) like %(txtsearch)s or
            lower(p.email) like %(txtsearch)s
        )'''

    if request.GET.get("zipsearch"):
        zipsearch = request.GET.get("zipsearch", None).lower()
        # No % at the begining in order to search on starting postal_code or place_name
        zipsearch = '{}%'.format(zipsearch)
        params['zipsearch'] = zipsearch
        sql_where += ''' and (
            lower(z.postal_code) like %(zipsearch)s or
            lower(z.place_name) like %(zipsearch)s
        )'''

    if request.GET.get("province"):
        province = request.GET.get("province", None).upper()
        if province != "ALL":
            params['province'] = province
            sql_where += " and upper(z.admin_code2) = %(province)s"

    if request.GET.get("profession"):
        profession = request.GET.get("profession", None)
        # LOG.debug(profession)
        if profession != "":
            params['profession'] = profession
            sql_where += " and f.id = %(profession)s"

    if request.GET.get("inscrflag"):
        inscr = request.GET.get("inscrflag")
        if inscr == "OUI":
            sql_where += " and i.id is not null"
        if inscr == "NON":
            sql_where += " and i.id is null"

    if request.GET.get("validflag"):
        valid = request.GET.get("validflag")
        if valid == "OUI":
            sql_where += " and i.is_valid = True"
        if valid == "NON":
            sql_where += " and i.is_valid = False"

    sql_select = '''select p.id, i.listeenvoi_id, i.id as inscription_id, i.is_valid as inscr_valid,
            COALESCE(p.mobile, p.phone, '') as tel,
            p.email,
            CASE WHEN trim(last_name) <> '' THEN concat(first_name, ' ', upper(last_name)) ELSE email END as full_name,
            COALESCE(f.description,'') as description, concat(z.country_code, ' ', z.postal_code, ' ', z.place_name) as zip_dispaly'''
    
    sql_from = '''
        from accounts_profile p
        left join accounts_profession f on p.profession_id = f.id
        left join accounts_geozip z on p.zip_id = z.id
        left join (select * from accounts_inscription where listeenvoi_id = %(pk)s) i on p.id=i.profile_id
        where p.is_active = True'''

    sql_sort = sort_user(request)

    sql_full = sql_select + sql_from + sql_where + sql_sort
    # LOG_DEBUG.debug(sql_full)
    profiles = user_model.objects.raw(sql_full, params)
    # get filter rec and total rec
    sql_filter = "select count(*) " + sql_from + sql_where
    nbrec_filter = get_nbrec(sql_filter, params)
    sql_total = "select count(*) from accounts_profile where is_active = True"
    nbrec_total = get_nbrec(sql_total, params)
    
    # Check box update
    # if request.method == 'POST':
    #     # get back ListeEnvoi instance
    #     user_model = get_user_model()
    #     listeenvoi = get_object_or_404(ListeEnvoi, pk=pk)
    #     # selected elements provided by the form
    #     selected = request.POST.getlist("users")
    #     selected_valid = request.POST.getlist("inscription_valid")
    #     LOG.debug('selected = {}'.format(selected))
    #     LOG.debug('selected_valid = {}'.format(selected_valid))

    #     for ticked in selected:

    #         ticked_inst = user_model.objects.get(id=ticked)
    #         inscr, created = Inscription.objects.for_user(ticked_inst).get_or_create(profile=ticked_inst, listeenvoi=listeenvoi)
    #         # Set True when selected_valid
    #         if ticked in selected_valid:
    #             inscr.is_valid = True
    #         else:
    #             inscr.is_valid = False
    #         inscr.save()

    #     # Get unselected elements and remove them
    #     try:
    #         qs = Inscription.objects.filter(listeenvoi=listeenvoi).exclude(profile_id__in=selected)
    #         qs.delete()
    #     except Exception as xcpt:
    #         LOG.error(xcpt)
    #         msg = _("Erreur lors de la mise à jour des inscriptions.")
    #         messages.error(request, msg)
    #         return False

    #     messages.success(request, _('Modifications sauvegardées'), fail_silently=True)
    #     return redirect('inscription_1_listeenvoi', pk)

    # Pagination
    # paginate_by = 500
    # paginator = Paginator(profiles, paginate_by)
    # page_obj = request.GET.get('page', '1')
    # try:
    #     page_obj = paginator.page(page_obj)
    # except PageNotAnInteger:
    #     page_obj = paginator.page(1)
    # except EmptyPage:
    #     page_obj = paginator.page(paginator.num_pages)
    # view = {
    #     'paginate_by': paginate_by,
    # }
    
    context = {'profiles': profiles,
               'listeenvoi': listeenvoi,
               'nbrec_filter': nbrec_filter,
               'nbrec_total': nbrec_total,
            #    'view': view,
            #    'page_obj': page_obj,
            #    'is_paginated': True,
               'form': InscriptionSearchForm(request)
            }

    return render(request, 'accounts/select_inscription1listeenvoi_form.html', context)
