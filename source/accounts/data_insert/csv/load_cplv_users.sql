-- select max(id) from accounts_profile;

truncate table accounts_profiletmp;
COPY public.accounts_profiletmp(
    last_name, first_name,
    code_postal, localite, email, profession_txt, profession, jobdesc)
--FROM '/opt/pipenv/cpilote/source/accounts/data_insert/csv/cplv_users.csv' DELIMITER ',' CSV HEADER;    
FROM '/home/pat/dev/pipenv/cpilote/source/accounts/data_insert/csv/cplv_users.csv' DELIMITER ',' CSV HEADER;

--SELECT pg_catalog.setval('public.accounts_listeenvois_id_seq', 17, false);