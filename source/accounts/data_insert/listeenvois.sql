
--

INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (1, 'default', 'liste par défaut', 'info@cehw.be', 1);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (3, 'default', 'liste par défaut', 'info@cepifruit.be', 3);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (4, 'default', 'liste par défaut', 'info@cim.be', 4);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (5, 'default', 'liste par défaut', 'info@cplv.be', 5);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (6, 'default', 'liste par défaut', 'info@cpm.be', 6);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (7, 'default', 'liste par défaut', 'info@fiwap.be', 7);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (8, 'default', 'liste par défaut', 'info@fourragemieux.be', 8);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (9, 'default', 'liste par défaut', 'info@gfw.be', 9);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (10, 'default', 'liste par défaut', 'info@irbab.be', 10);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (11, 'default', 'liste par défaut', 'info@uap.be', 11);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (2, 'default', 'liste par défaut', 'cadcoasbl@cadcoasbl.be', 2);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (12, 'carah & friends', 'pour les proches du carah', 'info@carah.be', 7);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (15, 'colza', 'Colza', 'cadcoasbl@cadcoasbl.be', 2);
INSERT INTO public.accounts_listeenvoi (id, nom, description, code, centrepilote_id) VALUES (16, 'orge B', 'Orge Brassicole', 'cadcoasbl@cadcoasbl.be', 2);


--
-- Name: accounts_listeenvoi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpnewadmin
--

SELECT pg_catalog.setval('public.accounts_listeenvoi_id_seq', 17, false);
