UPDATE public.accounts_centrepilote SET nom='L&#39;Union Ardenaise des Pépiniéristes',short_text='<p style="text-align:justify">L&#39;Union Ardenaise des P&eacute;pini&eacute;ristes UAP est une organisation professionnelle active sur la r&eacute;gion du Sud-Est de la Wallonie, essentiellement les provinces de Luxembourg, Namur et Li&egrave;ge. Elle regroupe les entreprises actives dans deux secteurs&nbsp;principaux :</p>

<ul>
	<li style="text-align: justify;">les p&eacute;pini&egrave;res foresti&egrave;res et ornementales</li>
	<li style="text-align: justify;">les producteurs de sapins de No&euml;l</li>
</ul>',siteweb='http://uap.be/',acronyme='UAP',secteur='sapins de Noël' WHERE code = 'uap'