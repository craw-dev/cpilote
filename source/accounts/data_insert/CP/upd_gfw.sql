UPDATE public.accounts_centrepilote SET nom='Groupement des Fraisiéristes Wallons',short_text='<p>Le <strong>G</strong>roupement des <strong>F</strong>raisi&eacute;ristes <strong>W</strong>allons <strong>GFW</strong> est une asbl cr&eacute;&eacute;e en 2002 &agrave; la demande du secteur. Depuis 2006, elle est soutenue et reconnue comme Centre Pilote.</p>

<h3>Missions</h3>

<ul>
	<li>Aider &agrave; l&#39;installation les nouveaux producteurs de fraises et de petits fruits</li>
	<li>Aider&nbsp;les producteurs dans la r&eacute;ussite de leur culture</li>
	<li>Participer au d&eacute;veloppement du secteur</li>
	<li>Assurer l&#39;encadrement technique des producteurs.</li>
</ul>

<h3>Fonctionnement</h3>

<p>R&eacute;alisation d&#39;essais au&nbsp;jardin d&#39;essais du Centre Pilote et chez certains&nbsp;producteurs.</p>',
siteweb='https://centrespilotes.be/cp/gfw/',acronyme='GFW',secteur='fraises et petits fruits ligneux' WHERE code = 'gfw'