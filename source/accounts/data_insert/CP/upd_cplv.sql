UPDATE public.accounts_centrepilote SET nom='Centre Provincial Liégeois des Productions Végétales et Maraîchères',short_text='<p>Le <strong>C</strong>entre <strong>P</strong>rovincial <strong>L</strong>i&eacute;geois des Productions <strong>V&eacute;g&eacute;</strong>tales et <strong>Mar</strong>a&icirc;ch&egrave;res,&nbsp;CPL-VEGEMAR&nbsp;est une asbl priv&eacute;e reconnue par la Wallonie comme Centre Pilote pour le d&eacute;veloppement et la vulgarisation des cultures l&eacute;gumi&egrave;res &agrave; destination de l&rsquo;industrie.</p>

<h3>Missions</h3>

<ul>
	<li>Accompagnement et formation de l&#39;agriculteur (avertissements, conseils, certification, agriculture biologique, ...)</li>
	<li>R&eacute;alisation d&#39;essais pour produire des nouvelles r&eacute;f&eacute;rences agronomiques</li>
</ul>

<h3>Fonctionnement</h3>

<p>Nos techniciens encadrent des producteurs de pois, carottes, &eacute;pinards, choux de Bruxelles, f&egrave;ves des marais, haricots, c&eacute;r&eacute;ales, chicor&eacute;es &agrave; inuline, chanvre industriel, herbe, biomasse...</p>',
siteweb='https://centrespilotes.be/cp/cplv/',acronyme='CPL-Végémar',secteur='cultures légumières pour l&#39;industrie'
WHERE code = 'cplv';