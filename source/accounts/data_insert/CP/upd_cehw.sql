UPDATE public.accounts_centrepilote SET nom='Centre d’Essais Horticoles de Wallonie',short_text='<p>Le <strong>C</strong>entre d&rsquo;<strong>E</strong>ssais <strong>H</strong>orticoles de <strong>W</strong>allonie&nbsp;<strong>CEHW</strong> est une asbl priv&eacute;e reconnue par la Wallonie comme Centre Pilote pour le d&eacute;veloppement et la vulgarisation en <u><strong>horticulture ornementale</strong></u>.</p>

<h3>Missions</h3>

<ul>
	<li><strong>Exp&eacute;rimentations pratiques</strong>&nbsp;dans le secteur de l&#39;horticulture ornementale (cultures florales et p&eacute;pini&egrave;res)</li>
	<li><strong>Conseils techniques</strong>&nbsp;aux producteurs professionnels wallons du secteur ornemental</li>
	<li><strong>Encadrement et aide au d&eacute;veloppement&nbsp;</strong>des entreprises</li>
	<li><strong>Vulgarisation&nbsp;</strong>des r&eacute;sultats de recherches effectu&eacute;es par le CEHW et par la recherche fondamentale belge et europ&eacute;enne</li>
</ul>

<h3>Fonctionnement</h3>

<p>Les membres du CEHW sont des<strong>&nbsp;producteurs professionnels</strong>. Le programme d&#39;activit&eacute;s est &eacute;tabli annuellement&nbsp;<strong>par la profession</strong>&nbsp;au travers des comit&eacute;s techniques (secteur p&eacute;pini&egrave;res et secteur plantes non ligneuses).</p>

<p>Le financement est assur&eacute; par les membres, le Service Public de Wallonie &ndash; Direction G&eacute;n&eacute;rale Op&eacute;rationnelle Agriculture, Direction de la Recherche et du d&eacute;veloppement et la Province de Hainaut.</p>',siteweb='https://www.cehw.be',acronyme='CEHW',secteur='horticulture ornementale' WHERE code = 'cehw'