UPDATE public.accounts_centrepilote SET nom='Centre Pilote Céréales et Oléo-Protéagineux',short_text='<p>Le <strong>Ce</strong>ntre <strong>Pi</strong>lote <strong>C</strong>&eacute;r&eacute;ales et <strong>O</strong>l&eacute;o-<strong>P</strong>rot&eacute;agineux&nbsp;<strong>CePiCOP</strong> est une asbl&nbsp;priv&eacute;e reconnue par la Wallonie comme Centre Pilote pour le d&eacute;veloppement et la vulgarisation <strong>des</strong>&nbsp;<strong>c&eacute;r&eacute;ales (except&eacute; ma&iuml;s), des ol&eacute;agineux et des prot&eacute;agineux.</strong></p>

<h3>Missions</h3>

<ul>
	<li>Essais au champ: vari&eacute;t&eacute;s, fumure, protection phytosanitaire, nouvelles pratiques...</li>
	<li>Aide &agrave; la d&eacute;cision: avertissements hebdomadaires en c&eacute;r&eacute;ales et colza en p&eacute;riode critique</li>
	<li>Synth&egrave;se: <a href="http://www.livre-blanc-cereales.be/" target="_blank">Livre Blanc C&eacute;r&eacute;ales</a>, conf&eacute;rences</li>
	<li>Outil&nbsp;de tra&ccedil;abilit&eacute;: carnet de champ</li>
	<li>Mise &agrave; jour des produits phytopharmaceutiques autoris&eacute;s en Belgique.</li>
</ul>

<p>Le CePiCOP est reconnu comme <strong>Centre de formation pour la <a href="https://fytoweb.be/fr/phytolicence" target="_blank">phytolicence</a></strong>.</p>',siteweb='https://centrespilotes.be/cp/cepicop/',acronyme='CePiCOP',secteur='céréales (excepté maïs), oléagineux, protéagineux.' WHERE code = 'cepicop'