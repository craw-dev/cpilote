UPDATE public.accounts_centrepilote SET nom='Fillière Wallonne de la Pomme de terre',short_text='<p>La FIWAP &eacute;tend ses activit&eacute;s &agrave; toute la Wallonie et regroupe les PRODUCTEURS de plants (via le GWPPPDT-UPR), les PRODUCTEURS de pomme de terre de consommation, les N&Eacute;GOCIANTS de plants et de pomme de terre de consommation, l&rsquo;INDUSTRIE DE TRANSFORMATION, la RECHERCHE SCIENTIFIQUE et des repr&eacute;sentants de la F&eacute;d&eacute;ration Wallonne de l&rsquo;Agriculture (FWA). Tous les maillons de la cha&icirc;ne sont pr&eacute;sents au sein d&rsquo;un ORGANISME INTERPROFESSIONNEL cr&eacute;&eacute; et g&eacute;r&eacute; PAR LES PROFESSIONNELS.<br />
Soutenue par les Pouvoirs Publics R&eacute;gionaux Wallons, la FIWAP assure les missions de Centre Pilote Pommes de terre et encadre la production d&rsquo;une pomme de terre de QUALIT&Eacute; et sa VALORISATION OPTIMALE, afin de contribuer au d&eacute;veloppement harmonieux du secteur.<br />
Plus de 425 membres actuels (repr&eacute;sentant plus de 25.000 ha de culture et la majorit&eacute; du n&eacute;goce et de l&rsquo;industries actifs en Wallonie) ont rejoint la FIWAP et b&eacute;n&eacute;ficient de ses services :</p>

<h4><strong>INFORMATIONS TECHNIQUES:</strong></h4>

<ul>
	<li>Diverses publications telles que FIWAP-Info, listes phyto, fiches techniques des vari&eacute;t&eacute;s, brochures th&eacute;matiques&hellip;</li>
	<li>Travail de proximit&eacute; sur le terrain: suivis des parcelles et hangars de r&eacute;f&eacute;rence, conseils individuels, r&eacute;unions coins de&nbsp; champ et coins de hangars, d&eacute;monstration d&rsquo;op&eacute;rations culturales, excursion annuelle.</li>
</ul>

<h4><strong>INFORMATIONS ECONOMIQUES:</strong></h4>

<ul>
	<li>Synth&egrave;ses des march&eacute;s belges et &eacute;trangers diffus&eacute;es par:
	<ul>
		<li>SMS hebdomadaire,</li>
		<li>Fax ou courriel hebdomadaire le mardi.</li>
	</ul>
	</li>
	<li>Suivi et information des co&ucirc;ts de production et des contrats de livraison &agrave; l&rsquo;industrie</li>
	<li>Pommak: la transparence instantan&eacute;e en ligne: www.pommak.be ou via l&rsquo;application android Pommak</li>
</ul>

<p><strong>REPR&Eacute;SENTATION</strong> aupr&egrave;s des Pouvoirs publics belges et &eacute;trangers: probl&eacute;matiques phytosanitaires, agriculture contractuelle, promotion, cotations,&hellip;</p>

<p><strong>PARTICIPATION</strong> active au <a href="http://fiwap.be/nepg/">NEPG</a> (groupe des producteurs de pomme de terre du nord-ouest europ&eacute;en).</p>

<p>La FIWAP tente de stimuler les SYNERGIES entre tous les maillons de la fili&egrave;re, d&rsquo;ORIENTER LA RECHERCHE SCIENTIFIQUE vers les probl&egrave;mes majeurs et de FAIRE CIRCULER L&rsquo;INFORMATION technique, &eacute;conomique et statistique &agrave; travers toute la fili&egrave;re.</p>',
siteweb='https://fiwap.be/',acronyme='FIWAP',secteur='pomme de terre' WHERE code = 'fiwap';