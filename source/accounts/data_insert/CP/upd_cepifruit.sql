UPDATE public.accounts_centrepilote SET nom='Centre Pilote Fruitier',short_text='<p>L&#39;objectif principal est de fournir aux producteurs des informations permettant d&#39;optimiser les m&eacute;thodes de conduite de leurs vergers afin d&#39;assurer la rentabilit&eacute; des exploitations.</p>

<h3>Missions</h3>

<ul>
	<li>Encadrement technique des arboriculteurs en Production Fruiti&egrave;re Int&eacute;gr&eacute;e (PFI)</li>
	<li>Validation des techniques de production int&eacute;gr&eacute;e et des mesures de protection de l&#39;environnement et de la sant&eacute;</li>
	<li>R&eacute;daction et actualisation du cahier des charges Fruitnet pour la production int&eacute;gr&eacute;e des fruits &agrave; p&eacute;pins</li>
	<li>Aide &agrave; la mise au point de cahiers des charges pour la production int&eacute;gr&eacute;e d&#39;autres cultures fruiti&egrave;res et/ou l&eacute;gumi&egrave;res</li>
</ul>

<h3>Fonctionnement</h3>

<p>Les essais et l&#39;encadrement mis en place par le Centre Pilote sont r&eacute;alis&eacute;s en pratique par les techniciens des 2 ASBL partenaires du projet (CEF, GAWI). Les exp&eacute;rimentations sont r&eacute;alis&eacute;es, soit chez les producteurs, soit dans le verger exp&eacute;rimental du CEF ou de Profruit.</p>',siteweb='http://www.asblgawi.com/',acronyme='CEPIFRUIT',secteur='arboriculture fruitière' WHERE code = 'cepifruit'