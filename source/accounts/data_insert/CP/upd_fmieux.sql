UPDATE public.accounts_centrepilote SET nom='Fourrages mieux',short_text='<p>Fourrages Mieux est une ASBL active dans le conseil et la vulgarisation des techniques agricoles li&eacute;es principalement aux <strong>prairies</strong> mais aussi &agrave; la culture de <strong>luzerne</strong>, de <strong>c&eacute;r&eacute;ales</strong> <strong>immatures</strong> ou de <strong>betteraves fourrag&egrave;res</strong>.</p>

<h3>Missions</h3>

<ul>
	<li>R&eacute;alisation d&rsquo;exp&eacute;rimentations dans les conditions de la pratique</li>
	<li>Mise en place de projets de d&eacute;monstration</li>
	<li>Encadrement des agriculteurs sur le plan technique, &eacute;conomique et environnemental</li>
	<li>D&eacute;veloppement du secteur par des programmes coordonn&eacute;s et des actions ponctuelles</li>
	<li>Vulgarisation de l&rsquo;information</li>
	<li>Am&eacute;lioration des techniques existantes et l&rsquo;examen des possibilit&eacute;s de mise en &oelig;uvre de nouvelles techniques</li>
	<li>R&eacute;sum&eacute;s des rapports d&#39;activit&eacute;s</li>
</ul>

<h3>Historique</h3>

<p>L&rsquo;ASBL Fourrages Mieux (FM) a &eacute;t&eacute; cr&eacute;&eacute; le 4 juillet 1997 &agrave; l&rsquo;initiative du Minist&egrave;re F&eacute;d&eacute;ral des Classes Moyennes et de l&rsquo;Agriculture suite &agrave; la r&eacute;organisation de ses services ext&eacute;rieurs de vulgarisation. Depuis avril 2004, FM est reconnu comme Centre Pilote pour le secteur des fourrages par le Service public de Wallonie (SPW). FM rassemble au sein de l&rsquo;ASBL les diff&eacute;rents acteurs de Wallonie qui s&rsquo;occupent de vulgarisation et/ou de recherche au niveau des prairies et des fourrages. Le si&egrave;ge social de l&rsquo;ASBL est &eacute;tabli au Centre d&rsquo;Economie Rural &agrave; Marloie.</p>',
siteweb='http://www.fourragesmieux.be/index.html',acronyme='FOURRAGES-MIEUX',secteur='prairies, luzerne, céréales immatures, betteraves fourragères' WHERE code = 'fmieux'