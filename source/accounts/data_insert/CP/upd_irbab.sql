UPDATE public.accounts_centrepilote SET nom='Institut Royal Belge pour l&#39;Amélioration de la Betterave',short_text='<p style="text-align:justify">L&rsquo;IRBAB a pour objet la coordination et l&rsquo;initiative de la recherche fondamentale, la recherche appliqu&eacute;e sur les intrants de la culture de la betterave et la vulgarisation des r&eacute;sultats vers les professionnels de la culture. Il peut &eacute;galement assurer l&rsquo;&eacute;tude et la vulgarisation des possibilit&eacute;s d&rsquo;am&eacute;lioration de la chicor&eacute;e et de sa culture.</p>

<p style="text-align:justify">Il aura plus pr&eacute;cis&eacute;ment les missions suivantes&nbsp;:</p>

<ul>
	<li style="text-align: justify;">Pour ce qui est de la recherche fondamentale, essentiellement la coordination et l&rsquo;initiative de la recherche fondamentale en sous-traitant celle-ci aux universit&eacute;s ou aux institutions sp&eacute;cialis&eacute;es et en donnant une opinion sur le choix et les r&eacute;sultats des essais. Ceci implique que l&rsquo;IRBAB maintienne une capacit&eacute; d&rsquo;&eacute;valuation et un niveau de connaissance suffisants des recherches entreprises au niveau europ&eacute;en. La mission d&rsquo;initiative et de coordination doit s&rsquo;inscrire dans le cadre des priorit&eacute;s d&eacute;finies par l&rsquo;interprofession.</li>
	<li style="text-align: justify;">La recherche appliqu&eacute;e sur les intrants de la culture de la betterave (semences, engrais, produits phytosanitaires et machines) tout en envisageant le d&eacute;veloppement de plates-formes communes (protocoles d&rsquo;essais, mise en place, &hellip;) avec les centres de recherches des pays voisins pour diminuer les co&ucirc;ts et augmenter l&rsquo;efficacit&eacute; des recherches. Concernant les coproduits, l&rsquo;IRBAB doit centraliser et vulgariser les connaissances relatives &agrave; l&rsquo;utilisation des coproduits (pulpes, &hellip;) dans l&rsquo;esprit de leur valorisation optimale. Les recherches pour de nouvelles applications des coproduits doivent normalement &ecirc;tre men&eacute;es par des centres sp&eacute;cialis&eacute;s et des universit&eacute;s qui peuvent sous-traiter &agrave; l&rsquo;IRBAB.</li>
	<li style="text-align: justify;">La vulgarisation vers les betteraviers, avec notamment pour objectif l&rsquo;optimisation de la production d&rsquo;un maximum de sucre blanc par hectare, en terme de rendement betterave par hectare, richesse et extractibilit&eacute;, dans le respect de la l&eacute;gislation et de l&rsquo;environnement.</li>
</ul>

<p style="text-align:justify">Le PVBC (Programme Vulgarisation Betterave Chicor&eacute;e) a comme t&acirc;ches principales :</p>

<ul>
	<li style="text-align: justify;">La coordination de la recherche en chicor&eacute;e : semences, protection de la culture, d&eacute;sherbage, lutte contre les maladies, techniques de r&eacute;colte, conservation,</li>
	<li style="text-align: justify;">La vulgarisation en betterave et en chicor&eacute;e,</li>
	<li style="text-align: justify;">Le service avertissement en betterave et chicor&eacute;e</li>
</ul>',
siteweb='https://www.irbab-kbivb.be/fr/',acronyme='IRBAB',secteur='betteraves et chicorées' WHERE code = 'irbab'