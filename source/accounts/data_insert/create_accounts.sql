--For date_joined here is the string format to use '2021-06-03 15:08:01.842125+02',

insert into accounts_profile (email, email_confirmed, password, 
is_superuser, first_name, last_name, is_staff, is_active, date_joined, force_password_change)
values ('vegemar@provincedeliege', true, 'pbkdf2_sha256$100000$dPJR7JF4BYtZ$9zOrDcVR4XusxeT7Vwjbp0UEtkcLV3+vxWO76yci+K0=',
false, 'Secrétariat', 'VÉGÉMAR', false, false, current_timestamp, false)