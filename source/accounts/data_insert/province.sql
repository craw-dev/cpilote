INSERT INTO accounts_province (id, code, name, region_id) VALUES (1,'wbr', 'brabant wallon', (SELECT id FROM accounts_region WHERE code = 'wal'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (2,'wht', 'hainaut', (SELECT id FROM accounts_region WHERE code = 'wal'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (3,'wlg', 'liège', (SELECT id FROM accounts_region WHERE code = 'wal'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (4,'wlx', 'luxembourg', (SELECT id FROM accounts_region WHERE code = 'wal'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (5,'wna', 'namur', (SELECT id FROM accounts_region WHERE code = 'wal'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (6,'vli', 'limbourg', (SELECT id FROM accounts_region WHERE code = 'vlg'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (7,'van', 'anvers', (SELECT id FROM accounts_region WHERE code = 'vlg'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (8,'vov', 'flandre orientale', (SELECT id FROM accounts_region WHERE code = 'vlg'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (9,'vbr', 'brabant flamand', (SELECT id FROM accounts_region WHERE code = 'vlg'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (10,'vwv', 'flandre occidentale', (SELECT id FROM accounts_region WHERE code = 'vlg'));
INSERT INTO accounts_province (id, code, name, region_id) VALUES (11,'bru', 'bruxelles capitale', (SELECT id FROM accounts_region WHERE code = 'bru'));

SELECT pg_catalog.setval('public.accounts_province_id_seq', 11, true);
