--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: accounts_profession; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.accounts_profession (id, nom, description) VALUES (1, 'agriculteur', 'agriculteur');
INSERT INTO public.accounts_profession (id, nom, description) VALUES (2, 'horticulteur', 'horticulteur');
INSERT INTO public.accounts_profession (id, nom, description) VALUES (3, 'arboriculteur', 'arboriculteur');
INSERT INTO public.accounts_profession (id, nom, description) VALUES (4, 'conseiller technique', 'conseiller technique');
INSERT INTO public.accounts_profession (id, nom, description) VALUES (5, 'entreprise', 'entreprise-société');


--
-- Name: accounts_profession_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.accounts_profession_id_seq', 5, true);


--
-- PostgreSQL database dump complete
--

