# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def load_from_sql():

    from config.settings import BASE_DIR
    import os
    sql_statements= "SET client_encoding = 'UTF8';"
    sql_statements += open(os.path.join(BASE_DIR,'accounts/data_insert/region.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR,'accounts/data_insert/regionagricole.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR,'accounts/data_insert/province.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR,'accounts/data_insert/zip.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, 'accounts/data_insert/centrepilote.sql'), 'r').read()    
    sql_statements += open(os.path.join(BASE_DIR,'accounts/data_insert/profile.sql'), 'r').read()
    # sql_statements += open(os.path.join(BASE_DIR,'accounts/data_insert/auth_group.sql'), 'r').read()
    # sql_statements += open(os.path.join(BASE_DIR,'accounts/data_insert/django_site.sql'), 'r').read()
    
    return sql_statements


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(load_from_sql()),
    ]
