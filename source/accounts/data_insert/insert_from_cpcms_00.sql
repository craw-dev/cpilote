﻿INSERT INTO accounts_region
SELECT id, code, nom
FROM dblink('dbname=cpcms_00',
		'SELECT id, code, nom FROM accounts_region')
AS a(id integer, code text, nom text);

INSERT INTO accounts_province
SELECT id, code, nom, region_id
FROM dblink('dbname=cpcms_00',
		'SELECT id, code, nom, region_id FROM accounts_province')
AS a(id integer, code text, nom text, region_id integer);

INSERT INTO accounts_regionagricole
SELECT id, code, nom
FROM dblink('dbname=cpcms_00',
		'SELECT id, code, nom FROM accounts_regionagricole')
AS a(id integer, code text, nom text);


INSERT INTO accounts_zip
SELECT id, code, localite, region_agricole_id, province_id
FROM dblink('dbname=cpcms_00',
		'SELECT id, code, localite, region_agricole_id, province_id FROM accounts_codepostal')
AS a(id integer, code text, localite text, region_agricole_id integer, province_id integer);


INSERT INTO accounts_profession
SELECT id, nom, description
FROM dblink('dbname=cpcms_00',
		'SELECT id, nom, description FROM accounts_profession')
AS a(id integer, nom text, description text);


INSERT INTO cult_centrepilote
SELECT id, code, nom, description, is_active
FROM dblink('dbname=cpcms_00',
		'SELECT id, code, nom, description, is_active FROM accounts_centrepilote')
AS a(id integer, code text, nom text, description text, is_active boolean);


INSERT INTO accounts_profile (id, password, last_login, is_superuser, username, first_name, last_name, email, email_confirmed, is_staff, is_active, date_joined,
mobile, phone, fax, nr_producteur, company, street, code_postal, localite, profession_id)
SELECT a.id, password, last_login, is_superuser, username, first_name, last_name, email, email_confirmed, is_staff, is_active, date_joined,
tel_sms, phone, fax, nr_producteur, nom_societe, adresse, code_postal, localite, profession_id
FROM dblink('dbname=cpcms_00',
		'SELECT auth_user.id, password, last_login, is_superuser, username, first_name, last_name, email, False, is_staff, is_active, date_joined,
		tel_sms, phone, fax, nr_producteur, nom_societe, adresse, code_postal, localite, profession_id
		FROM auth_user, accounts_cpprofile a
		where auth_user.id=a.user_id')
AS a(id integer, password text, last_login date, is_superuser boolean, username text, first_name text, last_name text,
email text, email_confirmed boolean, is_staff boolean, is_active boolean, date_joined date,
tel_sms text, phone text, fax text, nr_producteur text, nom_societe text, adresse text, code_postal text, localite text, profession_id integer)

update accounts_profile set email_confirmed = True where id between 100 and 110
update accounts_profile set email_confirmed = True where id between 1001 and 1009


INSERT INTO accounts_responsablecp
SELECT id, centrepilote_id, user_id
FROM dblink('dbname=cpcms_00',
		'SELECT id, centre_pilote_id, user_id FROM accounts_responsablecp')
AS a(id integer, centrepilote_id integer, user_id integer);

INSERT INTO accounts_profile_groups (id, profile_id, group_id)
SELECT id, user_id, group_id
FROM dblink('dbname=cpcms_00',
		'SELECT id, user_id, group_id FROM auth_user_groups')
AS a(id integer, user_id integer, group_id integer);


/* to be done in cpcms_00
--step 1
create table inscr as
select centre_pilote_id, user_id
from inscriptions_inscription where statut = 'active'
*/

-- step 2
insert into accounts_inscription (listeenvoi_id, profile_id, is_valid)
SELECT centre_pilote_id, user_id, True
FROM dblink('dbname=cpcms_00',
	'select centre_pilote_id, user_id from inscr')
AS a(centre_pilote_id integer, user_id integer);


-- combine step1 & 2:si profile_id absent (vérifier avec outer join si on perd bcp d'utilisateur
insert into accounts_inscription (listeenvoi_id, profile_id, is_valid)
select centre_pilote_id, user_id, True
from dblink('dbname=cpcms_00', 'select centre_pilote_id, user_id from inscriptions_inscription where statut = ''active''')
AS i(centre_pilote_id integer, user_id integer), accounts_profile as profile
where profile.id = i.user_id

--update accounts_profile set manage_cp_id = 2 where id in (101, 1003, 8833)


--insert into accounts_listeenvoi (code, nom, description, centrepilote_id)
SELECT code, nom, description, centrepilote_id
FROM dblink('dbname=cpcms_00',
	'select cp.code, i.nom as nom, i.description, i.centre_pilote_id from inscriptions_listeenvois i, accounts_centrepilote cp
	where i.centre_pilote_id = cp.id')
AS a(code text, nom text, description text, centrepilote_id integer);