DELETE FROM auth_group;

-- INSERT INTO auth_group (id, name) VALUES (1, 'EMPEREUR'); pas besoin car superuser flag
INSERT INTO auth_group (id, name) VALUES (1, 'ROI');
INSERT INTO auth_group (id, name) VALUES (3, 'SUPPORT');
INSERT INTO auth_group (id, name) VALUES (5, 'BASIQUE');
INSERT INTO auth_group (id, name) VALUES (7, 'VOIR');
INSERT INTO auth_group (id, name) VALUES (9, 'NADA');

SELECT pg_catalog.setval('public.auth_group_id_seq', 9, true);
