--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Ubuntu 13.2-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.2 (Ubuntu 13.2-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: accounts_centrepilote; Type: TABLE DATA; Schema: public; Owner: cpiloteadmin
--

INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (4, 'cim', 'Centre Interprofessionnel Maraîcher', true, NULL, '<p>Le Centre Interprofessionnel Mara&icirc;cher, en abr&eacute;g&eacute; C.I.M., est une ASBL n&eacute;e suite au rassemblement de plusieurs producteurs mara&icirc;chers wallons.<br />
<br />
Son but est de mettre en place et de stabiliser une structure de vulgarisation et de services pour la production de l&eacute;gumes en Wallonie.<br />
<br />
A partir de 2000, la Direction G&eacute;n&eacute;rale de l&rsquo;Agriculture du Minist&egrave;re de la R&eacute;gion Wallonne a reconnu l&rsquo;association comme CENTRE PILOTE en production mara&icirc;ch&egrave;re pour le march&eacute; frais.</p>', 'https://www.legumeswallons.be/', NULL, 'CIM', 'légumes pour le marché du frais');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (6, 'cipf', 'Centre Indépendant de Promotion Fourragère', true, NULL, '<p>Le CIPF est une asbl active depuis plus de 50 ans dans l&rsquo;exp&eacute;rimentation et la vulgarisation de tout ce qui concerne la culture du ma&iuml;s. Il fait &eacute;galement partie du Centre Pilote Ma&iuml;s. Depuis 2007, le CIPF travaille aussi sur diff&eacute;rents th&egrave;mes li&eacute;s &agrave; la culture du miscanthus.</p>

<p>Soutenu par le Service public de Wallonie, et bas&eacute; dans les locaux de l&#39;UCL (ELIA) &agrave; Louvain-la-Neuve, le CIPF organise divers essais dans diff&eacute;rentes r&eacute;gions agricoles, diffuse l&#39;information et assure l&#39;encadrement technique aupr&egrave;s des agriculteurs et des autres acteurs de la fili&egrave;re.</p>

<h3>Historique</h3>

<p style="text-align:justify">Le CIPF (Centre Ind&eacute;pendant de Promotion Fourrag&egrave;re) est une association d&rsquo;agriculteurs qui a vu le jour en 1959. Dans les ann&eacute;es 50, la culture du ma&iuml;s en &eacute;tait &agrave; ses d&eacute;buts et pas encore tr&egrave;s bien ma&icirc;tris&eacute;e. Cette culture se heurtait &agrave; un gros obstacle&nbsp;: les cultivateurs n&rsquo;osaient pas semer du ma&iuml;s de peur de ne pouvoir le r&eacute;colter et les entrepreneurs n&rsquo;achetaient pas les machines incertains de pouvoir les amortir. Les techniques culturales et surtout de conservation (pas de b&acirc;che plastique) restaient &agrave; d&eacute;couvrir pour beaucoup. Il en allait de m&ecirc;me pour sa valorisation dans les rations (compl&eacute;ments min&eacute;raux prot&eacute;iques n&eacute;cessaires). Il fallait donc une association qui puisse promouvoir et vulgariser la culture du ma&iuml;s en Belgique aupr&egrave;s des agriculteurs. A partir de 1970 et pendant les ann&eacute;es suivantes, les surfaces de ma&iuml;s ont fortement augment&eacute; d&rsquo;ann&eacute;e en ann&eacute;e. Face &agrave; l&rsquo;essor de cette culture en Belgique, un service technique s&rsquo;est d&eacute;velopp&eacute; progressivement au sein du CIPF. En 1980, l&rsquo;UCL a accueilli l&rsquo;&eacute;quipe technique dans ses locaux facultaires. Au fil des ann&eacute;es, le CIPF est devenu un centre ind&eacute;pendant incontournable dans le cadre de la vulgarisation en ma&iuml;s. Il peut r&eacute;pondre aux diverses inqui&eacute;tudes des ma&iuml;siculteurs ainsi qu&rsquo;&agrave; leurs demandes de conseils.</p>', 'https://cipf.be/fr', NULL, 'CIPF', 'maïs');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (7, 'fiwap', 'Fillière Wallonne de la Pomme de terre', true, NULL, '<p>La FIWAP &eacute;tend ses activit&eacute;s &agrave; toute la Wallonie et regroupe les PRODUCTEURS de plants (via le GWPPPDT-UPR), les PRODUCTEURS de pomme de terre de consommation, les N&Eacute;GOCIANTS de plants et de pomme de terre de consommation, l&rsquo;INDUSTRIE DE TRANSFORMATION, la RECHERCHE SCIENTIFIQUE et des repr&eacute;sentants de la F&eacute;d&eacute;ration Wallonne de l&rsquo;Agriculture (FWA). Tous les maillons de la cha&icirc;ne sont pr&eacute;sents au sein d&rsquo;un ORGANISME INTERPROFESSIONNEL cr&eacute;&eacute; et g&eacute;r&eacute; PAR LES PROFESSIONNELS.<br />
Soutenue par les Pouvoirs Publics R&eacute;gionaux Wallons, la FIWAP assure les missions de Centre Pilote Pommes de terre et encadre la production d&rsquo;une pomme de terre de QUALIT&Eacute; et sa VALORISATION OPTIMALE, afin de contribuer au d&eacute;veloppement harmonieux du secteur.<br />
Plus de 425 membres actuels (repr&eacute;sentant plus de 25.000 ha de culture et la majorit&eacute; du n&eacute;goce et de l&rsquo;industries actifs en Wallonie) ont rejoint la FIWAP et b&eacute;n&eacute;ficient de ses services :</p>

<h4><strong>INFORMATIONS TECHNIQUES:</strong></h4>

<ul>
	<li>Diverses publications telles que FIWAP-Info, listes phyto, fiches techniques des vari&eacute;t&eacute;s, brochures th&eacute;matiques&hellip;</li>
	<li>Travail de proximit&eacute; sur le terrain: suivis des parcelles et hangars de r&eacute;f&eacute;rence, conseils individuels, r&eacute;unions coins de&nbsp; champ et coins de hangars, d&eacute;monstration d&rsquo;op&eacute;rations culturales, excursion annuelle.</li>
</ul>

<h4><strong>INFORMATIONS ECONOMIQUES:</strong></h4>

<ul>
	<li>Synth&egrave;ses des march&eacute;s belges et &eacute;trangers diffus&eacute;es par:
	<ul>
		<li>SMS hebdomadaire,</li>
		<li>Fax ou courriel hebdomadaire le mardi.</li>
	</ul>
	</li>
	<li>Suivi et information des co&ucirc;ts de production et des contrats de livraison &agrave; l&rsquo;industrie</li>
	<li>Pommak: la transparence instantan&eacute;e en ligne: www.pommak.be ou via l&rsquo;application android Pommak</li>
</ul>

<p><strong>REPR&Eacute;SENTATION</strong> aupr&egrave;s des Pouvoirs publics belges et &eacute;trangers: probl&eacute;matiques phytosanitaires, agriculture contractuelle, promotion, cotations,&hellip;</p>

<p><strong>PARTICIPATION</strong> active au <a href="http://fiwap.be/nepg/">NEPG</a> (groupe des producteurs de pomme de terre du nord-ouest europ&eacute;en).</p>

<p>La FIWAP tente de stimuler les SYNERGIES entre tous les maillons de la fili&egrave;re, d&rsquo;ORIENTER LA RECHERCHE SCIENTIFIQUE vers les probl&egrave;mes majeurs et de FAIRE CIRCULER L&rsquo;INFORMATION technique, &eacute;conomique et statistique &agrave; travers toute la fili&egrave;re.</p>', 'https://fiwap.be/', NULL, 'FIWAP', 'pomme de terre');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (10, 'irbab', 'Institut Royal Belge pour l''Amélioration de la Betterave', true, NULL, '<p style="text-align:justify">L&rsquo;IRBAB a pour objet la coordination et l&rsquo;initiative de la recherche fondamentale, la recherche appliqu&eacute;e sur les intrants de la culture de la betterave et la vulgarisation des r&eacute;sultats vers les professionnels de la culture. Il peut &eacute;galement assurer l&rsquo;&eacute;tude et la vulgarisation des possibilit&eacute;s d&rsquo;am&eacute;lioration de la chicor&eacute;e et de sa culture.</p>

<p style="text-align:justify">Il aura plus pr&eacute;cis&eacute;ment les missions suivantes&nbsp;:</p>

<ul>
	<li style="text-align: justify;">Pour ce qui est de la recherche fondamentale, essentiellement la coordination et l&rsquo;initiative de la recherche fondamentale en sous-traitant celle-ci aux universit&eacute;s ou aux institutions sp&eacute;cialis&eacute;es et en donnant une opinion sur le choix et les r&eacute;sultats des essais. Ceci implique que l&rsquo;IRBAB maintienne une capacit&eacute; d&rsquo;&eacute;valuation et un niveau de connaissance suffisants des recherches entreprises au niveau europ&eacute;en. La mission d&rsquo;initiative et de coordination doit s&rsquo;inscrire dans le cadre des priorit&eacute;s d&eacute;finies par l&rsquo;interprofession.</li>
	<li style="text-align: justify;">La recherche appliqu&eacute;e sur les intrants de la culture de la betterave (semences, engrais, produits phytosanitaires et machines) tout en envisageant le d&eacute;veloppement de plates-formes communes (protocoles d&rsquo;essais, mise en place, &hellip;) avec les centres de recherches des pays voisins pour diminuer les co&ucirc;ts et augmenter l&rsquo;efficacit&eacute; des recherches. Concernant les coproduits, l&rsquo;IRBAB doit centraliser et vulgariser les connaissances relatives &agrave; l&rsquo;utilisation des coproduits (pulpes, &hellip;) dans l&rsquo;esprit de leur valorisation optimale. Les recherches pour de nouvelles applications des coproduits doivent normalement &ecirc;tre men&eacute;es par des centres sp&eacute;cialis&eacute;s et des universit&eacute;s qui peuvent sous-traiter &agrave; l&rsquo;IRBAB.</li>
	<li style="text-align: justify;">La vulgarisation vers les betteraviers, avec notamment pour objectif l&rsquo;optimisation de la production d&rsquo;un maximum de sucre blanc par hectare, en terme de rendement betterave par hectare, richesse et extractibilit&eacute;, dans le respect de la l&eacute;gislation et de l&rsquo;environnement.</li>
</ul>

<p style="text-align:justify">Le PVBC (Programme Vulgarisation Betterave Chicor&eacute;e) a comme t&acirc;ches principales :</p>

<ul>
	<li style="text-align: justify;">La coordination de la recherche en chicor&eacute;e : semences, protection de la culture, d&eacute;sherbage, lutte contre les maladies, techniques de r&eacute;colte, conservation,</li>
	<li style="text-align: justify;">La vulgarisation en betterave et en chicor&eacute;e,</li>
	<li style="text-align: justify;">Le service avertissement en betterave et chicor&eacute;e</li>
</ul>', 'https://www.irbab-kbivb.be/fr/', NULL, 'IRBAB', 'betteraves et chicorées');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (11, 'uap', 'L''Union Ardenaise des Pépiniéristes', true, NULL, '<p style="text-align:justify">L&#39;Union Ardenaise des P&eacute;pini&eacute;ristes UAP est une organisation professionnelle active sur la r&eacute;gion du Sud-Est de la Wallonie, essentiellement les provinces de Luxembourg, Namur et Li&egrave;ge. Elle regroupe les entreprises actives dans deux secteurs&nbsp;principaux :</p>

<ul>
	<li style="text-align: justify;">les p&eacute;pini&egrave;res foresti&egrave;res et ornementales</li>
	<li style="text-align: justify;">les producteurs de sapins de No&euml;l</li>
</ul>', 'http://uap.be/', NULL, 'UAP', 'sapins de Noël');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (2, 'cepicop', 'Centre Pilote Céréales et Oléo-Protéagineux', true, 576, '<p>Le <strong>Ce</strong>ntre <strong>Pi</strong>lote <strong>C</strong>&eacute;r&eacute;ales et <strong>O</strong>l&eacute;o-<strong>P</strong>rot&eacute;agineux&nbsp;<strong>CePiCOP</strong> est une asbl&nbsp;priv&eacute;e reconnue par la Wallonie comme Centre Pilote pour le d&eacute;veloppement et la vulgarisation <strong>des</strong>&nbsp;<strong>c&eacute;r&eacute;ales (except&eacute; ma&iuml;s), des ol&eacute;agineux et des prot&eacute;agineux.</strong></p>

<h3>Missions</h3>

<ul>
	<li>Essais au champ: vari&eacute;t&eacute;s, fumure, protection phytosanitaire, nouvelles pratiques...</li>
	<li>Aide &agrave; la d&eacute;cision: avertissements hebdomadaires en c&eacute;r&eacute;ales et colza en p&eacute;riode critique</li>
	<li>Synth&egrave;se: <a href="http://www.livre-blanc-cereales.be/" target="_blank">Livre Blanc C&eacute;r&eacute;ales</a>, conf&eacute;rences</li>
	<li>Outil&nbsp;de tra&ccedil;abilit&eacute;: carnet de champ</li>
	<li>Mise &agrave; jour des produits phytopharmaceutiques autoris&eacute;s en Belgique.</li>
</ul>

<p>Le CePiCOP est reconnu comme <strong>Centre de formation pour la <a href="https://fytoweb.be/fr/phytolicence" target="_blank">phytolicence</a></strong>.</p>', 'https://centrespilotes.be/cp/cepicop/', NULL, 'CePiCOP', 'céréales (excepté maïs), oléagineux, protéagineux.');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (5, 'cplv', 'Centre Provincial Liégeois des Productions Végétales et Maraîchères', true, 88, '<p>Le <strong>C</strong>entre <strong>P</strong>rovincial <strong>L</strong>i&eacute;geois des Productions <strong>V&eacute;g&eacute;</strong>tales et <strong>Mar</strong>a&icirc;ch&egrave;res,&nbsp;CPL-VEGEMAR&nbsp;est une asbl priv&eacute;e reconnue par la Wallonie comme Centre Pilote pour le d&eacute;veloppement et la vulgarisation des cultures l&eacute;gumi&egrave;res &agrave; destination de l&rsquo;industrie.</p>

<h3>Missions</h3>

<ul>
	<li>Accompagnement et formation de l&#39;agriculteur (avertissements, conseils, certification, agriculture biologique, ...)</li>
	<li>R&eacute;alisation d&#39;essais pour produire des nouvelles r&eacute;f&eacute;rences agronomiques</li>
</ul>

<h3>Fonctionnement</h3>

<p>Nos techniciens encadrent des producteurs de pois, carottes, &eacute;pinards, choux de Bruxelles, f&egrave;ves des marais, haricots, c&eacute;r&eacute;ales, chicor&eacute;es &agrave; inuline, chanvre industriel, herbe, biomasse...</p>', 'https://centrespilotes.be/cp/cplv/', NULL, 'CPL-Végémar', 'cultures légumières pour l''industrie');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (9, 'gfw', 'Groupement des Fraisiéristes Wallons', true, 18, '<p>Le <strong>G</strong>roupement des <strong>F</strong>raisi&eacute;ristes <strong>W</strong>allons <strong>GFW</strong> est une asbl cr&eacute;&eacute;e en 2002 &agrave; la demande du secteur. Depuis 2006, elle est soutenue et reconnue comme Centre Pilote.</p>

<h3>Missions</h3>

<ul>
	<li>Aider &agrave; l&#39;installation les nouveaux producteurs de fraises et de petits fruits</li>
	<li>Aider&nbsp;les producteurs dans la r&eacute;ussite de leur culture</li>
	<li>Participer au d&eacute;veloppement du secteur</li>
	<li>Assurer l&#39;encadrement technique des producteurs.</li>
</ul>

<h3>Fonctionnement</h3>

<p>R&eacute;alisation d&#39;essais au&nbsp;jardin d&#39;essais du Centre Pilote et chez certains&nbsp;producteurs.</p>', 'https://centrespilotes.be/cp/gfw/', NULL, 'GFW', 'fraises et petits fruits ligneux');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (1, 'cehw', 'Centre d’Essais Horticoles de Wallonie', true, 10, '<p>Le <strong>C</strong>entre d&rsquo;<strong>E</strong>ssais <strong>H</strong>orticoles de <strong>W</strong>allonie&nbsp;<strong>CEHW</strong> est une asbl priv&eacute;e reconnue par la Wallonie comme Centre Pilote pour le d&eacute;veloppement et la vulgarisation en <u><strong>horticulture ornementale</strong></u>.</p>

<h3>Missions</h3>

<ul>
	<li><strong>Exp&eacute;rimentations pratiques</strong>&nbsp;dans le secteur de l&#39;horticulture ornementale (cultures florales et p&eacute;pini&egrave;res)</li>
	<li><strong>Conseils techniques</strong>&nbsp;aux producteurs professionnels wallons du secteur ornemental</li>
	<li><strong>Encadrement et aide au d&eacute;veloppement&nbsp;</strong>des entreprises</li>
	<li><strong>Vulgarisation&nbsp;</strong>des r&eacute;sultats de recherches effectu&eacute;es par le CEHW et par la recherche fondamentale belge et europ&eacute;enne</li>
</ul>

<h3>Fonctionnement</h3>

<p>Les membres du CEHW sont des<strong>&nbsp;producteurs professionnels</strong>. Le programme d&#39;activit&eacute;s est &eacute;tabli annuellement&nbsp;<strong>par la profession</strong>&nbsp;au travers des comit&eacute;s techniques (secteur p&eacute;pini&egrave;res et secteur plantes non ligneuses).</p>

<p>Le financement est assur&eacute; par les membres, le Service Public de Wallonie &ndash; Direction G&eacute;n&eacute;rale Op&eacute;rationnelle Agriculture, Direction de la Recherche et du d&eacute;veloppement et la Province de Hainaut.</p>', 'https://www.cehw.be', NULL, 'CEHW', 'horticulture ornementale');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (3, 'cepifruit', 'Centre Pilote Fruitier', true, 658, '<p>L&#39;objectif principal est de fournir aux producteurs des informations permettant d&#39;optimiser les m&eacute;thodes de conduite de leurs vergers afin d&#39;assurer la rentabilit&eacute; des exploitations.</p>

<h3>Missions</h3>

<ul>
	<li>Encadrement technique des arboriculteurs en Production Fruiti&egrave;re Int&eacute;gr&eacute;e (PFI)</li>
	<li>Validation des techniques de production int&eacute;gr&eacute;e et des mesures de protection de l&#39;environnement et de la sant&eacute;</li>
	<li>R&eacute;daction et actualisation du cahier des charges Fruitnet pour la production int&eacute;gr&eacute;e des fruits &agrave; p&eacute;pins</li>
	<li>Aide &agrave; la mise au point de cahiers des charges pour la production int&eacute;gr&eacute;e d&#39;autres cultures fruiti&egrave;res et/ou l&eacute;gumi&egrave;res</li>
</ul>

<h3>Fonctionnement</h3>

<p>Les essais et l&#39;encadrement mis en place par le Centre Pilote sont r&eacute;alis&eacute;s en pratique par les techniciens des 2 ASBL partenaires du projet (CEF, GAWI). Les exp&eacute;rimentations sont r&eacute;alis&eacute;es, soit chez les producteurs, soit dans le verger exp&eacute;rimental du CEF ou de Profruit.</p>', 'http://www.asblgawi.com/', NULL, 'CEPIFRUIT', 'arboriculture fruitière');
INSERT INTO public.accounts_centrepilote (id, code, nom, is_active, logo_id, short_text, website, vatnr, acronyme, secteur) VALUES (8, 'fmieux', 'Fourrages mieux', true, 17, '<p>Fourrages Mieux est une ASBL active dans le conseil et la vulgarisation des techniques agricoles li&eacute;es principalement aux <strong>prairies</strong> mais aussi &agrave; la culture de <strong>luzerne</strong>, de <strong>c&eacute;r&eacute;ales</strong> <strong>immatures</strong> ou de <strong>betteraves fourrag&egrave;res</strong>.</p>

<h3>Missions</h3>

<ul>
	<li>R&eacute;alisation d&rsquo;exp&eacute;rimentations dans les conditions de la pratique</li>
	<li>Mise en place de projets de d&eacute;monstration</li>
	<li>Encadrement des agriculteurs sur le plan technique, &eacute;conomique et environnemental</li>
	<li>D&eacute;veloppement du secteur par des programmes coordonn&eacute;s et des actions ponctuelles</li>
	<li>Vulgarisation de l&rsquo;information</li>
	<li>Am&eacute;lioration des techniques existantes et l&rsquo;examen des possibilit&eacute;s de mise en &oelig;uvre de nouvelles techniques</li>
	<li>R&eacute;sum&eacute;s des rapports d&#39;activit&eacute;s</li>
</ul>

<h3>Historique</h3>

<p>L&rsquo;ASBL Fourrages Mieux (FM) a &eacute;t&eacute; cr&eacute;&eacute; le 4 juillet 1997 &agrave; l&rsquo;initiative du Minist&egrave;re F&eacute;d&eacute;ral des Classes Moyennes et de l&rsquo;Agriculture suite &agrave; la r&eacute;organisation de ses services ext&eacute;rieurs de vulgarisation. Depuis avril 2004, FM est reconnu comme Centre Pilote pour le secteur des fourrages par le Service public de Wallonie (SPW). FM rassemble au sein de l&rsquo;ASBL les diff&eacute;rents acteurs de Wallonie qui s&rsquo;occupent de vulgarisation et/ou de recherche au niveau des prairies et des fourrages. Le si&egrave;ge social de l&rsquo;ASBL est &eacute;tabli au Centre d&rsquo;Economie Rural &agrave; Marloie.</p>', 'http://www.fourragesmieux.be/index.html', NULL, 'FOURRAGES-MIEUX', 'prairies, luzerne, céréales immatures, betteraves fourragères');


--
-- PostgreSQL database dump complete
--

