INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (2, '1', 'Dunes');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (3, '2', 'Polders');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (4, '3', 'Sablonneuse');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (5, '4', 'Campine');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (6, '5', 'Sablo-limoneuse');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (7, '6', 'Limoneuse');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (8, '7', 'Campine hennuyère');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (9, '8', 'Condroz');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (10, '9', 'Herbagère');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (11, '10', 'Herbagère (Fagne)');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (12, '11', 'Famenne');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (13, '12', 'Ardenne');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (14, '13', 'Jurassique');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (15, '14', 'Haute Ardenne');
INSERT INTO public.accounts_agriculturalregion (id, code, name) VALUES (16, '15', 'Global');

SELECT pg_catalog.setval('public.accounts_agriculturalregion_id_seq', 16, true);