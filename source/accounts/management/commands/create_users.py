# -*- coding: utf-8 -*-
"""
Insert loanrefund record into Expense
input param: user_id & loan_id
"""
from django.core.management.base import BaseCommand, CommandError
from accounts.utils import create_users


class Command(BaseCommand):
    """
    cmd example:
    ./manage.py create_users --hero_email='cplv@hero.com' --le_list='vegemar_newsletter,vegemar_membres'
    """
    help = 'Create users list based on ProfileTmp table need 2 params'

    def add_arguments(self, parser):
        parser.add_argument(
            '--hero_email', help="email with generic password",
        )
        parser.add_argument(
            '--le_list', help="list of liste envoi",
        )                   

    def handle(self, *args, **options):
        try:
            hero_email = options['hero_email']
            le_list = options['le_list']
            result = create_users(hero_email, le_list)
            self.stdout.write(self.style.SUCCESS(result))
        except Exception as xcpt:
            raise CommandError('Failed "{0}"'.format(xcpt))
