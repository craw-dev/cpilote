# -*- coding: utf-8 -*-
"""
accounts.cms_plugins
date: 20200813
"""
from datetime import timedelta, date
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin

from accounts.models import CentrePilote

from agenda.models import Event
from msg.models import Msg


@plugin_pool.register_plugin
class MsgCaroussel(CMSPluginBase):
    name = _("Message Caroussel")
    model = CMSPlugin
    render_template = "plugin/msg_caroussel.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        dte6week = timezone.now()- timedelta(days=30)
        recent_msgs = Msg.objects.filter(msg_date__gte=dte6week).order_by('-msg_date')
        context.update({
            'recent_msgs': recent_msgs,
        })
        return context


@plugin_pool.register_plugin
class MsgLast(CMSPluginBase):
    name = _("Derniers Messages")
    model = CMSPlugin
    render_template = "plugin/msg_last.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        # dte6week = timezone.now()- timedelta(days=30)
        # msg_list = Msg.objects.filter(statut='envoye', msg_date__gte=dte6week).order_by('-msg_date')
        msg_list = Msg.objects.filter(statut='envoye')[:4]
        context.update({
            'msg_list': msg_list,
        })
        return context


@plugin_pool.register_plugin
class EventList(CMSPluginBase):
    name = _("Événement Liste")
    model = CMSPlugin
    render_template = "plugin/event_list.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        event_list = Event.objects.filter(begin_date__gte=date.today())
        context.update({
            'event_list': event_list,
        })
        return context


@plugin_pool.register_plugin
class CPListPlugin(CMSPluginBase):
    name = _("CentrePilote Liste")
    model = CMSPlugin
    render_template = "plugin/cp_list_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        hosted_in = CentrePilote.objects.filter(cporder__lt=4)
        hosted_out = CentrePilote.objects.filter(cporder__gte=4)
        context.update({
            'hosted_in': hosted_in,
            'hosted_out': hosted_out,
        })
        return context


@plugin_pool.register_plugin
class CPHostedPlugin(CMSPluginBase):
    name = _("CentrePilote Hébergé")
    model = CMSPlugin
    render_template = "plugin/cp_hosted.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        hosted_in = CentrePilote.objects.filter(cporder__lt=4)
        context.update({
            'hosted_in': hosted_in,
        })
        return context


@plugin_pool.register_plugin
class CPDetailPlugin(CMSPluginBase):
    name = _("CentrePilote Detail")
    model = CMSPlugin
    render_template = "plugin/cp_presentation.html" # fake, no importance here
    cache = False
    user_model = get_user_model()

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        request = context['request']
        cppath = request.get_full_path()
        if 'cp' in cppath:
            cpcode = cppath.split('/')[2]
            # try:
            #     cpobj = CentrePilote.objects.get(code=cpcode)
            # except CentrePilote.DoesNotExist:
            #     cpobj = CentrePilote.objects.get(code='na')
            
            cpobj = get_object_or_404(CentrePilote, code=cpcode)

            profile_list = self.user_model.objects.filter(work4_cp=cpobj).order_by('jobdesc_order')
            context.update({
                'object': cpobj,
                'profile_list': profile_list,
                'cploc_list': cpobj.centrepilotelocation_set.all()
            })
        return context


@plugin_pool.register_plugin
class CPPresentation(CPDetailPlugin):
    name = _("CentrePilote Présentation")
    render_template = "plugin/cp_presentation.html"


@plugin_pool.register_plugin
class CPTeam(CPDetailPlugin):
    name = _("CentrePilote Équipe")
    render_template = "plugin/cp_team.html"


@plugin_pool.register_plugin
class CPLocation(CPDetailPlugin):
    name = _("CentrePilote Contact")
    render_template = "plugin/cp_location.html"
