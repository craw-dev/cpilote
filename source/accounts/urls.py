# users/urls.py
# from django.urls import path
# from . import views

"""
accounts.urls description
'profile_edit' overwrites 'userena_profile_edit'
"""
# from django.contrib.auth import urls
from django.urls import path, include
from django.utils.encoding import force_str
from . import views
from django.contrib.auth.views import LogoutView
from . import views_abo
# from django.contrib.auth import urls

# app_name = "accounts" do not use because django url doesn't

urlpatterns = [
    path('signup/', views.SignUpView.as_view(), name='signup'),
    path('signup_done/', views.SignUpDoneView.as_view(), name='signup_done'),
    path('signup_confirm/<uidb64>/<token>/', views.signup_confirm, name='signup_confirm'),
    path('signup_failed/', views.SignUpFailedView.as_view(), name='signup_failed'),

    path('password_change/', views.MyPasswordChangeView.as_view(), name='my_password_change'),
    path('password_change/done/', views.MyPasswordChangeDoneView.as_view(), name='my_password_change_done'),

    path('profile_list', views.ProfileList.as_view(), name='profile_list'),
    path('add/', views.ProfileCreateView.as_view(), name='profile_create'),
    path('<pk>', views.ProfileDetailView.as_view(), name='profile_detail'),
    path('<pk>/upd', views.ProfileUpdateView.as_view(), name='profile_update'),
    path('<pk>/del', views.ProfileDeleteView.as_view(), name='profile_delete'),
    path('', include('django.contrib.auth.urls')),
]

urlpatterns += [
    # 1 user => liste envois
    path('inscription/<pk>', views_abo.inscription_1_user, name='inscription_1_user'),
    # pk = userpk cp is added at the end due to a bug in url path function
    path('listeenvoi/', views_abo.ListeEnvoiListView.as_view(), name='listeenvoi_list'),
    path('listeenvoi/add', views_abo.ListeEnvoiCreateView.as_view(), name='listeenvoi_create'),
    # pk = liste envoi
    path('listeenvoi/<pk>/detail', views_abo.ListeEnvoiDetailView.as_view(), name='listeenvoi_detail'),
    path('listeenvoi/<pk>/upd', views_abo.ListeEnvoiUpdateView.as_view(), name='listeenvoi_update'),
    path('listeenvoi/<pk>/del', views_abo.ListeEnvoiDeleteView.as_view(), name='listeenvoi_delete'),
    # 1 liste envoi => users | pk = listeenvoi
    path('listeenvoi/<pk>/inscription', views_abo.inscription_1_listeenvoi, name='inscription_1_listeenvoi'),
]

urlpatterns += [
    path('cp_list', views.CPListView.as_view(), name='cp_list'),
    path('centrepilote/<pk>', views.CPDetailView.as_view(), name='cp_detail'),
    path('centrepilote/<pk>/upd', views.CPUpdateView.as_view(), name='cp_update'),
    path('centrepilote/<pk>/localisation/add', views.CPLocCreateView.as_view(), name='cploc_create'),
    path('centrepilote/localisation/<pk>/upd', views.CPLocUpdateView.as_view(), name='cploc_update'),    
]
