# -*- coding: utf-8 -*-
"""
accounts.ajax_call
Specific module designed for ajax call
"""
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.db.models import Q
from django.shortcuts import render

from config.settings import PROJECT_NAME
from .models import ListeEnvoi, Inscription, GeoZip


import logging

LOG = logging.getLogger(__name__)


@login_required
def ddl_zip(request):
    """
    used in accounts.forms.ProfileSignUpForm and accounts.forms.CPLocForm
    """
    qs = GeoZip.objects.none()
    zipsearch = request.GET.get("zipsearch", None)
    if zipsearch is not None:
        if zipsearch.isnumeric():
            qs = GeoZip.objects.filter(postal_code__istartswith=zipsearch)
        else:
            qs = GeoZip.objects.filter(place_name__istartswith=zipsearch)

    return render(
        request, "dropdown/dropdown_list.html", {"ddl_list": qs}
    )


@login_required
def ddl_contact(request):
    """
    used in accounts.forms.CPLocForm
    """
    user_model = get_user_model()
    qs = user_model.objects.none()
    contactsearch = request.GET.get("contactsearch", None)
    where = Q(pk__gt=0)
    if contactsearch is not None:
        where &= (
            Q(first_name__istartswith=contactsearch) |
            Q(last_name__istartswith=contactsearch) |
            Q(company__icontains=contactsearch) |
            Q(email__icontains=contactsearch)
        )
        qs = user_model.objects.filter(where)
    return render(
        request, "dropdown/dropdown_list.html", {"ddl_list": qs}
    )


@login_required
def inscription_update(request):
    if request.method == "POST":
        # 1. Collect vars
        try:
            user_id = request.POST.get("user_id")
            user_id = int(user_id)

            user_checked = request.POST.get("user_checked")
            if user_checked == "true":
                user_checked = True
            else:
                user_checked = False

            inscr_valid = request.POST.get("inscr_valid")
            if inscr_valid == "true":
                inscr_valid = True
            else:
                inscr_valid = False

            listeenvoi_id = request.POST.get("listeenvoi_id")
            listeenvoi_id = int(listeenvoi_id)
        except (ValueError, TypeError) as err:
            data = {"msg": "{}".format(err)}

        # 2. Get ListeEnvoi instance
        try:
            listeenvoi = ListeEnvoi.objects.get(id=listeenvoi_id)
        except ListeEnvoi.DoesNotExist:
            data = {"msg": "ListeEnvoi.DoesNotExist", "user_id": user_id, "listeenvoi_id": listeenvoi_id}

        # 3. Get User instance
        try:
            user_model = get_user_model()
            user = user_model.objects.get(id=user_id)
        except user_model.DoesNotExist:
            data = {"msg": "User.DoesNotExist", "user_id": user_id, "listeenvoi_id": listeenvoi_id}

        # 4. Inscription update
        try:
            inscr, created = Inscription.objects.get_or_create(profile=user, listeenvoi=listeenvoi)
            inscr.is_valid = inscr_valid
            inscr.save()
            if not user_checked:
                inscr.delete()
            data = {"msg": "inscription_update DONE!", "user_id": user_id, "listeenvoi_id": listeenvoi_id}

        except Exception as err:
            data = {"msg": "{}".format(err)}

    return JsonResponse(data)


def inscription_block_get_vars(request):
    err_flag = False
    msg = ""
    if request.method == "POST":
        # 1. Collect vars
        try:
            user_id_list = request.POST.getlist("user_id_list[]")
            listeenvoi_id = request.POST.get("listeenvoi_id")
            action = request.POST.get("action")
            what = request.POST.get("what")
            listeenvoi_id = int(listeenvoi_id)
        except (ValueError, TypeError) as err:
            err_flag = True
            msg = err

        # 2. Get ListeEnvoi instance
        try:
            listeenvoi = ListeEnvoi.objects.get(id=listeenvoi_id)
        except ListeEnvoi.DoesNotExist:
            err_flag = True
            msg = "ListeEnvoi.DoesNotExist"

        # 3. Get User qs
        try:
            user_model = get_user_model()
            user_qs = user_model.objects.filter(id__in=user_id_list)
        except Exception as err:
            err_flag = True
            msg = err
    else:
        err_flag = True
        msg = "Not a POST request"

    data = {
        "user_id_list": user_id_list,
        "listeenvoi_id": listeenvoi_id,
        "action": action,
        "what": what,
        "listeenvoi": listeenvoi,
        "user_qs": user_qs,
        "msg": msg
    }
    return err_flag, data


@login_required
def inscription_block_create(request):
    err_flag, data = inscription_block_get_vars(request)
    if not err_flag:
        if data["what"] == "inscription":
            # Inscription create or delete
            try:
                if data["action"] == "true":
                    # add user inscriptions
                    for user in data["user_qs"]:
                        try:
                            Inscription.objects.create(profile=user, listeenvoi=data["listeenvoi"])
                        except IntegrityError as integrity:
                            pass
                    msg = "{} utilisateurs ajoutés à la liste d'envoi '{}'".format(data["user_qs"].count(), data["listeenvoi"])

                else:
                    inscr_qs = Inscription.objects.filter(profile__id__in=data["user_id_list"], listeenvoi__id=data["listeenvoi_id"])
                    inscr_qs.delete()
                    msg = "{} utilisateurs supprimés de la liste d'envoi '{}'".format(data["user_qs"].count(), data["listeenvoi"])

            except Exception as msg:
                pass

        if data["what"] == "validation":
            # Inscription validation (do not delete because when a user subscribe he is automatically validated)
            try:
                if data["action"] == "true":
                    # validate user inscriptions
                    for user in data["user_qs"]:
                        inscr = Inscription.objects.get(profile=user, listeenvoi=data["listeenvoi"])
                        inscr.is_valid = True
                        inscr.save()
                    msg = "{} utilisateurs validés pour la liste d'envoi '{}'".format(data["user_qs"].count(), data["listeenvoi"])

                else:
                    # unvalidate user inscriptions
                    for user in data["user_qs"]:
                        inscr = Inscription.objects.get(profile=user, listeenvoi=data["listeenvoi"])
                        inscr.is_valid = False
                        inscr.save()
                    msg = "{} utilisateurs invalidés de la liste d'envoi '{}'".format(data["user_qs"].count(), data["listeenvoi"])

            except Exception as msg:
                pass

        data = {"msg": "{}".format(msg)}

    return JsonResponse(data)
