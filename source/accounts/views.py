# -*- coding: utf-8 -*-
"""
accounts.views
"""
import logging

from braces.views import LoginRequiredMixin, UserPassesTestMixin
# Divers
from django.views.generic import ListView, DetailView, UpdateView, CreateView, TemplateView, View, DeleteView
from django.urls import reverse, resolve, reverse_lazy
from django.shortcuts import redirect, render, resolve_url
from django.template.loader import render_to_string
from django.db.models import Q
from django.core.exceptions import PermissionDenied
# Contrib
from django.contrib.sites.shortcuts import get_current_site
from django.contrib import messages
from django.contrib.auth.views import LogoutView, LoginView, PasswordChangeView, PasswordChangeDoneView
from django.contrib.auth import get_user_model
# from django.contrib.auth.decorators import login_required
# Utils
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.translation import ugettext as _
# from django.utils.decorators import method_decorator
# Accounts App
from .tokens import account_activation_token
from .forms import ProfileUpdateForm, ProfileSignUpForm, CPLocForm
from .formsearch import TxtSearchForm
from config.settings import PASSWORD_RESET_TIMEOUT_DAYS, LOGIN_REDIRECT_URL, ADMINS
from .models import CentrePilote, CentrePiloteLocation, Profession

LOG = logging.getLogger(__name__)


def user_is_manager(user):
    """
    L'utilisateur est-il gestionnaire d'un cp?
    Oui => return true
    """
    if user.is_authenticated and user.is_manager:
        return True
    else:
        return False


class MessageMixin():
    """
    Message management
    !!!url MUST be defined with add, upd and del keywords!!!
    """
    @property
    def build_message(self):
        action = ""
        gender = ""
        """Build message"""
        if "upd" in self.request.path:
            action = "modifié"
        if "add" in self.request.path:
            action = "ajouté"
        if "del" in self.request.path:
            action = "supprimé"
        if self.model.__name__.lower() in ("listeenvoi"):
            gender = "e"
        else:
            gender = ""

        who = self.model._meta.verbose_name
        if hasattr(self, 'object'):
            if self.object is not None:
                who = self.object.get_title()            

        msg = "{0} {1}{2} !".format(who, action, gender)
        return msg

    def form_valid(self, form):
        """display message when form is valid"""
        messages.info(self.request, self.build_message)
        return super(MessageMixin, self).form_valid(form)

    def delete(self, request, *args, **kwargs):
        """MessageMixin hooks to form_valid which is not present on DeleteView
        => Use of delete method to display success msg"""
        self.object = self.model.objects.get(pk=kwargs["pk"])
        messages.info(self.request, self.build_message)
        return super(MessageMixin, self).delete(request, *args, **kwargs)


class LoginANDPassesTest(LoginRequiredMixin, UserPassesTestMixin):
    def test_func(self, user, *args, **kwargs):
        """Inherit from braces.views.UserPassesTestMixin"""
        if user_is_manager(user):
            return True
        else:
            raise PermissionDenied


class ProfileCreateView(LoginANDPassesTest, CreateView):
    """
    Allow creation of user
    Added on 2021-10-07
    """
    model = get_user_model()
    form_class = ProfileUpdateForm
    # success_url = reverse_lazy("profile_list")

    def get_form_kwargs(self):
        """used to pass user in form and display tab4 accordingly"""
        kwargs = super().get_form_kwargs()
        kwargs["connected_user"] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['url_name'] = resolve(self.request.path_info).url_name
        context['title'] = f"{self.model._meta.verbose_name} » " + _("Ajouter")
        context['breadcrumb_display'] = context['title']
        return context 


class SignUpView(CreateView):
    """
    SignUpView with email activation
    https://simpleisbetterthancomplex.com/tutorial/2017/02/18/how-to-create-user-sign-up-view.html#sign-up-with-confirmation-mail
    """
    template_name = 'signup_form.html'
    form_class = ProfileSignUpForm
    success_url = 'signup_done'
#     extra_context={'PASSWORD_RESET_TIMEOUT_DAYS': PASSWORD_RESET_TIMEOUT_DAYS}

    def form_valid(self, form):
        new_user = form.save()
        new_user.is_active = False
        new_user.save()
        
        current_site = get_current_site(self.request)
        email_subject = _("Inscription sur le site {0}".format(current_site.name))
        
        email_body = render_to_string('signup_email.html', {
            'user': new_user,
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
            'token': account_activation_token.make_token(new_user),
            'protocol': 'https' if self.request.is_secure() else 'http',
        })
        new_user.email_user(email_subject, email_body)
            
        return redirect(self.success_url)


class SignUpDoneView(TemplateView):
    template_name = 'signup_done.html'
    title = _("Merci pour votre inscription.")
    extra_context = {'PASSWORD_RESET_TIMEOUT_DAYS': PASSWORD_RESET_TIMEOUT_DAYS,
                   'title': title}


def signup_confirm(request, uidb64, token):
    UserModel = get_user_model()
    user = None
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel.objects.get(pk=uid)
    except Exception as e:
        LOG.error("userid:{0}, error:{1}".format(uid, e))
        return redirect('signup_failed')

    if not account_activation_token.check_token(user, token):
        return render(request, 'signup_failed_token.html', {'admin_email': ADMINS[0][1]})
    else:
        user.is_active = True
        user.email_confirmed = True
        user.save()
        return render(request, 'signup_ok.html')
        

class SignUpFailedView(TemplateView):
    template_name = 'signup_failed.html'
    title = _("Votre inscription a échoué.")
    extra_context = {'PASSWORD_RESET_TIMEOUT_DAYS': PASSWORD_RESET_TIMEOUT_DAYS,
                   'title': title}
    

class ProfileList(LoginANDPassesTest, ListView):
    """
    List of users
    """
    model = get_user_model()
    title = "Mes utilisateurs"
    form = TxtSearchForm
    totalrec = 0
    # paginate_by = 500

    def build_where(self):
        where = Q(pk__gt=0)
        if self.request.GET.get("txtsearch"):
            search_list = self.request.GET.get("txtsearch", None).split()
            for search_item in search_list:
                where &= (
                    Q(company__icontains=search_item)
                    | Q(email__icontains=search_item)
                    | Q(first_name__icontains=search_item)
                    | Q(last_name__icontains=search_item)
                    | Q(zip__place_name__icontains=search_item)
                    | Q(zip__postal_code__icontains=search_item)
                )
        return where

    def sort_user(self):
        """
        if sortBy presence is pair=>desc, unpair=>asc
        """
        if self.request.GET.get("sortBy"):
            sorting = []
            myurl = self.request.get_full_path()
            urlcut = myurl.split('&')
            mysort = [x for x in urlcut if 'sortBy' in x]
            for i in mysort:
                ordering = ""
                n = urlcut.count(i)
                # si modulo=0 => nbr pair => desc
                if (n % 2) == 0:
                    ordering = "-"
                sorting.append("{0}{1}".format(ordering, i[7:]))
            # Clean up sorting when more than 2 elements and keep last clicked item
            sorting_set = set(sorting)
            if len(sorting_set) > 1:
                sorting = [sorting[-1]]
        else:
            sorting = ['-date_joined']
        return sorting

    # Redefine queryset to get only associated users by cp and exclude myself
    def get_queryset(self):
        qs = self.model.objects.all()
        self.totalrec = qs.count
        # qs = Profile.objects.filter(abonnementlisteenvoi__listeenvoi__centrepilote=user.manage_cp).exclude(id=user.id)
        qs = qs.filter(self.build_where())
        qs = qs.order_by(*self.sort_user())
        self.form = TxtSearchForm(self.request.GET)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['url_name_list'] = 'profile_list'
        context['totalrec'] = self.totalrec
        return context


class ProfileDetailView(LoginRequiredMixin, DetailView):
    model = get_user_model()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['url_name'] = resolve(self.request.path_info).url_name
        context['title'] = self.object.get_title()
        context['breadcrumb_display'] = self.object.get_title()
        return context   


class ProfileUpdateView(LoginRequiredMixin, MessageMixin, UpdateView):
    model = get_user_model()
    form_class = ProfileUpdateForm
    manage_user = "NO"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Set manage_user var used in form ProfileUpdateForm and jquery (profile_form.html)
        if (self.request.user != self.object) and self.request.user.is_manager:
            self.manage_user = "YES"
        else:
            self.manage_user = "NO"
            if (self.object.company == '' or
                self.object.profession is None or
                self.object.zip is None):
                messages.warning(request, _("Veuillez mettre à jour votre profile (Code postal, Entreprise, Profession, etc.) avant de poursuive svp."), fail_silently=True)
        return super().get(request, *args, **kwargs)

    def get_form_kwargs(self):
        """used to pass user in form and display tab4 accordingly"""
        kwargs = super(ProfileUpdateView, self).get_form_kwargs()
        kwargs["connected_user"] = self.request.user
        kwargs["manage_user"] = self.manage_user
        if self.object.is_worker:
            employe_public = Profession.objects.get(code="PUBLIC")
            kwargs["initial"] = {
                "company": self.request.user.work4_cp,
                "profession": employe_public
                }
        return kwargs

    def get_success_url(self):
        return reverse('profile_detail', args=[str(self.object.id)])  

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['manage_user'] = self.manage_user
        context['title'] = "{} » Modifier".format(self.object.get_title())
        context['breadcrumb_display'] = context['title']
        return context        


class ProfileDeleteView(LoginRequiredMixin, MessageMixin, DeleteView):
    """Delete User"""
    model = get_user_model()
    # obligatoire sinon django pointe sur modelname_confirm_delete.html
    template_name = "accounts/profile_detail.html"
    success_url = reverse_lazy("profile_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['confirm_message'] = _("Supprimer cet utilisateur?")
        context['title'] = "{} » Supprimer".format(self.object.get_title())
        context['breadcrumb_display'] = context['title']
        return context   


class MyPasswordChangeView(PasswordChangeView):
    success_url = reverse_lazy('my_password_change_done')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f"{self.request.user} » Modifier mot de passe"
        context['breadcrumb_display'] = context['title']
        return context


class MyPasswordChangeDoneView(DetailView):
    template_name = 'accounts/profile_detail.html'
    model = get_user_model()
    user = None

    def dispatch(self, *args, **kwargs):
        self.user = self.model.objects.get(id=self.request.user.id)
        self.user.force_password_change = False
        self.user.save()
        messages.success(self.request, _("Votre mot de passe à été mis à jour."))
        return super().dispatch(*args, **kwargs)

    def get_object(self):
        return self.user


class CPListView(ListView):
    model = CentrePilote


class CPDetailView(DetailView):
    model = CentrePilote
    user_model = get_user_model()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        profile_list = self.user_model.objects.filter(work4_cp=self.object).order_by('jobdesc_order')
        context['profile_list'] = profile_list
        context['cploc_list'] = self.object.centrepilotelocation_set.all()
        return context


class CPUpdateView(UpdateView):
    model = CentrePilote
    template_name = "accounts/accounts_form.html"
    fields = (
        "acronyme", "nom", "secteur", "short_text", "siteweb",
        "facebook_link", "twitter_link", "linkedin_link", "youtube_link",
        "googleplus_link", "ms_link",
        "logo", "is_sending_msg", "vatnr")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['content_title'] = self.object.get_title()
        context['cp_pk'] = self.kwargs['pk']
        return context

# class CPLocDetailView(DetailView):
#     model = CentrePiloteLocation

class CPLocCreateView(CreateView):
    model = CentrePiloteLocation
    template_name = "accounts/accounts_form.html"
    form_class = CPLocForm
    cpobj = None

    def get_form_kwargs(self):
        """pass cp_instance"""
        kwargs = super().get_form_kwargs()
        self.cpobj = CentrePilote.objects.get(pk=self.kwargs['pk'])
        kwargs["cpobj"] = self.cpobj
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        content_subtitle = "{} » Nouveau".format(self.model._meta.verbose_name)
        context['content_subtitle'] = content_subtitle
        context['content_title'] = self.cpobj.get_title()
        context['cp_pk'] = self.kwargs['pk']
        return context


class CPLocUpdateView(UpdateView):
    model = CentrePiloteLocation
    template_name = "accounts/accounts_form.html"
    form_class = CPLocForm

    def get_form_kwargs(self):
        """pass cp_instance"""
        kwargs = super().get_form_kwargs()
        kwargs["cpobj"] = self.object.centrepilote
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cp_pk'] = self.object.centrepilote_id
        return context
       