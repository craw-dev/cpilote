# encoding:utf-8
from django.forms import (Form, TextInput, CharField, HiddenInput)
from django.utils.translation import ugettext_lazy as _

class TxtSearchForm(Form):
    txtsearch = CharField(
        max_length=100,
        required=False,
        widget=TextInput(attrs={
            "class": "form-control",
            "placeholder": _("Rechercher")
            })
        )
        