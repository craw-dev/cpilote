# -*- coding: utf-8 -*-
"""
accounts.models description
"""
import logging
from ckeditor.fields import RichTextField
from filer.fields.image import FilerImageField

from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractUser, Group

from django.urls import reverse
from django.utils.translation import ugettext as _
from django.db.models.constants import LOOKUP_SEP
from django.core.exceptions import FieldDoesNotExist
from django.utils.encoding import force_text
from info.models import WebLink

__author__ = 'openHBP'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2020 Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2020-05-19'
__version__ = '2.0'
__status__ = 'Development'


LOG = logging.getLogger(__name__)


class RegionAgricole(models.Model):
    """
    Régions agricoles (belges)
    """
    code = models.CharField(max_length=4)
    nom = models.CharField(max_length=100)


class GeoZip(models.Model):
    """
    Source: GeoNames.org
    Zip from https://download.geonames.org/export/zip/
    """
    country_code = models.CharField(_('Pays'), max_length=2)
    postal_code = models.CharField(_('Code postal'), max_length=20)
    place_name = models.CharField(_('Localité'), max_length=180)
    admin_name1 = models.CharField(_('Etat'), max_length=100, blank=True, null=True)
    admin_code1 = models.CharField(_('Code Etat'), max_length=20, blank=True, null=True)
    admin_name2 = models.CharField(_('Province/Département'), max_length=100, blank=True, null=True)
    admin_code2 = models.CharField(_('Code Province/Département'), max_length=20, blank=True, null=True)
    admin_name3 = models.CharField(_('Commune'), max_length=100, blank=True, null=True)
    admin_code3 = models.CharField(_('Code commune'), max_length=20, blank=True, null=True)
    latitude = models.DecimalField(_('Latitude wgs84'), max_digits=9, decimal_places=6)
    longitude = models.DecimalField(_('Longitude wgs84'), max_digits=9, decimal_places=6)
    accuracy = models.PositiveSmallIntegerField(_('Précision'), blank=True, null=True)
    regionagricole = models.ForeignKey(RegionAgricole,
                                            on_delete=models.CASCADE,
                                            null=True, blank=True)
    class Meta:
        verbose_name = _("Localité")
        ordering = ['country_code', 'postal_code', 'place_name']
        unique_together = ('country_code', 'postal_code', 'place_name')
        indexes = [
            models.Index(fields=['country_code'], name='country_code_idx'),
            models.Index(fields=['postal_code'], name='postal_code_idx'),
        ]

    def __str__(self):
        return '{0} {1} {2}'.format(self.country_code, self.postal_code, self.place_name)


class ModeDeProduction(models.Model):
    """
    Table des professions
    """
    id = models.PositiveSmallIntegerField(primary_key=True)
    nom = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=255)

    class Meta:
        verbose_name = _("Mode de production")
        ordering = ["id"]

    def __str__(self):
        return self.nom


class Profession(models.Model):
    """
    Table des professions
    """
    PROF_TYPE = (
        ("agriculteur", "agriculteur"),
        ("organisme", "organisme"),
    )
    id = models.PositiveSmallIntegerField(primary_key=True)
    code = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=255)
    ptype = models.CharField(
        _("Type de profession"),
        max_length=12,
        choices=PROF_TYPE,
        default="agriculteur",
        help_text=_("Agriculteur? ou Organisme?"),
    )

    class Meta:
        verbose_name = _("Profession")
        ordering = ["description"]

    def __str__(self):
        return self.description


class ContactGeneric(models.Model):
    """
    Abstract class for contact info
    """
    mobile = models.CharField(_('Tel portable'), max_length=15, blank=True, null=True,
        help_text="0 ou prefixe pays suivi de 9 chiffres")
    phone = models.CharField(_('Tel fixe'), max_length=15, blank=True, null=True,
        help_text="0 ou prefixe pays suivi de 8 à 9 chiffres")
    fax = models.CharField(_('Fax'), max_length=15, blank=True, null=True,
        help_text="0 ou prefixe pays suivi de 8 à 9 chiffres")
    company = models.CharField(
        verbose_name=_("Entreprise/Organisation"), max_length=255, blank=True, null=True)
    profession_txt = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Profession description'))
    profession = models.ForeignKey(
        Profession, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_('Profession'))
    jobdesc = models.CharField(_("Fonction/métier"), max_length=255, blank=True, null=True,
        help_text=_("Example: Chef de projet, Assistant administratif, Responsable Finances, Administration & Communication, etc."))
    jobdesc_order = models.PositiveSmallIntegerField(_("Fonction/métier ordre"), blank=True, null=True)
    nr_producteur = models.CharField(
        # unique=True,
        verbose_name=_("Numéro producteur"),
        max_length=12,
        blank=True,
        null=True,
        # help_text=_("Respecter le format '123456789-12'"),
    )
    street = models.CharField(_('Rue'), max_length=255, blank=True, null=True, help_text=_('Rue, nr rue'))
    zip = models.ForeignKey(
        GeoZip, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_('Code postal'))
    modeprod = models.ForeignKey(
        ModeDeProduction, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_('Mode de production')
    )
    code_postal = models.CharField(
        verbose_name=_("Code postal"), max_length=10, blank=True, null=True
    )
    localite = models.CharField(
        verbose_name=_("Localité"), max_length=255, blank=True, null=True
    )

    @property
    def get_phone(self):
        if self.mobile is None:
            if self.phone is None:
                return ""
            else:
                return self.phone
        else:
            return self.mobile

    class Meta:
        abstract = True


class CentrePilote(models.Model):
    """
    Listing des centres pilotes
    """
    id = models.PositiveSmallIntegerField(primary_key=True)
    code = models.CharField(max_length=20, unique=True)
    acronyme = models.CharField(_("Acronyme"), max_length=50)
    nom = models.CharField(_("Nom"), max_length=255)
    secteur = models.CharField(_("Secteur"), max_length=255)
    cporder = models.PositiveSmallIntegerField(_("Ordre affichage"), default=0)
    short_text = RichTextField(_("Descriptif"), blank=True, null=True)
    is_sending_msg = models.BooleanField(_("Envois avertissements actif"), default=False,
        help_text=_("Cochez cette case pour rendre vos listes d'envois visibles."))
    siteweb = models.URLField(_("Site web"), blank=True, null=True)
    facebook_link = models.URLField(_("Facebook link"), blank=True, null=True)
    twitter_link = models.URLField(_("Twitter link"), blank=True, null=True)
    linkedin_link = models.URLField(_("Linkedin link"), blank=True, null=True)
    youtube_link = models.URLField(_("YouTube link"), blank=True, null=True)
    googleplus_link = models.URLField(_("Google+ link"), blank=True, null=True)
    ms_link = models.URLField(_("Microsoft link"), blank=True, null=True)
    logo = FilerImageField(related_name="logocp", null=True, blank=True, on_delete=models.CASCADE)
    vatnr = models.CharField(_("Nr TVA ou d'entreprise"), max_length=13, blank=True, null=True)

    class Meta:
        verbose_name = _("Centre Pilote")
        ordering = ['cporder', 'code']

    def get_facebook_href(self, class_param="btn btn-main2 m-1", txt_param='<i class="fab fa-facebook-f"></i>'):
        if self.facebook_link:
            return f'<a class="{class_param}" href="{self.facebook_link}" target="_blank">{txt_param}</a>'
        else:
            return ''

    def get_twitter_href(self, class_param="btn btn-main2 m-1", txt_param='<i class="fab fa-twitter"></i>'):
        if self.twitter_link:
            return f'<a class="{class_param}" href="{self.twitter_link}" target="_blank">{txt_param}</a>'
        else:
            return ''

    def get_linkedin_href(self, class_param="btn btn-main2 m-1", txt_param='<i class="fab fa-linkedin-in"></i>'):
        if self.linkedin_link:
            return f'<a class="{class_param}" href="{self.linkedin_link}" target="_blank">{txt_param}</a>'
        else:
            return ''

    def get_youtube_href(self, class_param="btn btn-main2 m-1", txt_param='<i class="fab fa-youtube"></i>'):
        if self.youtube_link:
            return f'<a class="{class_param}" href="{self.youtube_link}" target="_blank">{txt_param}</a>'
        else:
            return ''

    def get_googleplus_href(self, class_param="btn btn-main2 m-1", txt_param='<i class="fab fa-google-plus-g"></i>'):
        if self.googleplus_link:
            return f'<a class="{class_param}" href="{self.googleplus_link}" target="_blank">{txt_param}</a>'
        else:
            return ''

    def get_ms_href(self, class_param="btn btn-main2 m-1", txt_param='<i class="fab fa-microsoft"></i>'):
        if self.ms_link:
            return f'<a class="{class_param}" href="{self.ms_link}" target="_blank">{txt_param}</a>'
        else:
            return ''

    @property
    def get_social_link(self):
        return f'{self.get_facebook_href()}{self.get_twitter_href()} \
            {self.get_linkedin_href()}{self.get_youtube_href()} \
            {self.get_googleplus_href()}{self.get_ms_href()}'

    @property
    def get_social_link_email(self):
        fb_class = "soc-btn fb"
        fb_text = "Facebook"
        tw_class = "soc-btn tw"
        tw_text = "Twitter"
        in_class = "soc-btn in"
        in_text = "Linkedin"
        yt_class = "soc-btn yt"
        yt_text = "YouTube"
        gp_class = "soc-btn gp"
        gp_text = "Google+"
        ms_class = "soc-btn ms"
        ms_text = "Microsoft"
        myretstr = f'{self.get_facebook_href(fb_class, fb_text)}{self.get_twitter_href(tw_class, tw_text)} \
            {self.get_linkedin_href(in_class, in_text)}{self.get_youtube_href(yt_class, yt_text)} \
            {self.get_googleplus_href(gp_class, gp_text)}{self.get_ms_href(ms_class, ms_text)}'
        return myretstr.strip()

    def get_vatnr(self):
        if self.vatnr is None:
            return ""
        else:
            return "BCE ({0})".format(self.vatnr)

    def get_logopath(self):
        # logopath = self.logo.thumbnails['admin_directory_listing_icon']
        logopath = self.logo.thumbnails['admin_sidebar_preview']
        return logopath

    def get_title(self):
        return "{0} » {1}".format(self._meta.verbose_name, self.nom)

    def get_absolute_url(self):
        return reverse('cp_detail', args=[str(self.id)])

    def __str__(self):
        return "{0}".format(self.acronyme)


class ProfileTmp(models.Model):
    """
    Temporary table with new user list
    """
    email = models.EmailField(_('email address'))
    first_name = models.CharField(_('first name'), max_length=150, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    mobile = models.CharField(_('Tel portable'), max_length=15, blank=True, null=True)
    phone = models.CharField(_('Tel fixe'), max_length=15, blank=True, null=True)
    fax = models.CharField(_('Fax'), max_length=15, blank=True, null=True)
    company = models.CharField(verbose_name=_("Entreprise/Organisation"), max_length=255, blank=True, null=True)
    profession_txt = models.CharField(max_length=255, blank=True, null=True)
    profession = models.CharField(max_length=100, blank=True, null=True)
    modeprod = models.CharField(max_length=100, blank=True, null=True)
    jobdesc = models.CharField(_("Fonction/métier"), max_length=255, blank=True, null=True,
        help_text=_("Example: Chef de projet, Assistant administratif, Responsable Finances, Administration & Communication, etc."))
    jobdesc_order = models.PositiveSmallIntegerField(_("Fonction/métier ordre"), blank=True, null=True)
    nr_producteur = models.CharField(verbose_name=_("Numéro producteur"), max_length=12, blank=True, null=True)
    street = models.CharField(_('Rue'), max_length=255, blank=True, null=True, help_text=_('Rue, nr rue'))
    code_postal = models.CharField(verbose_name=_("Code postal"), max_length=10, blank=True, null=True)
    localite = models.CharField(verbose_name=_("Localité"), max_length=255, blank=True, null=True)

    def get_full_name(self):
        fullname = '{0} {1}'.format(self.first_name, self.last_name.upper())
        if fullname.strip() == '':
            if self.company is None or self.company.strip() == '':
                fullname = self.email
            else:
                fullname = self.company
        return str(fullname)

    def __str__(self):
        return self.get_full_name()


class Profile(AbstractUser, ContactGeneric):
    """
    Custom user class with additional fields comming from ContactGeneric)
    Check here if needed
    https://www.fomfus.com/articles/how-to-use-email-as-username-for-django-authentication-removing-the-username
    """
    username = None
    # force_password_change = models.BooleanField(default=True)
    email = models.EmailField(_('email address'), unique=True)
    email_confirmed = models.BooleanField(default=False, verbose_name=_("Email confirmé"),
                                          help_text=_("Précise si la demande d'inscription a été confirmée par email ou pas."))
    manage_cp = models.ForeignKey(
        CentrePilote,
        on_delete=models.PROTECT,
        verbose_name=_("Gère le centre pilote"),
        blank=True,
        null=True,
        # help_text=_("Gestionnaire du centre pilote."),
        related_name="user_manage"
    )
    work4_cp = models.ForeignKey(
        CentrePilote,
        on_delete=models.PROTECT,
        verbose_name=_("Travaille pour le centre pilote"),
        blank=True,
        null=True,
        # help_text=_("Gestionnaire du centre pilote."),
        related_name="user_work"
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'mobile', 'zip']

    class Meta:
        verbose_name = _("Utilisateur")
        ordering = ['-date_joined']

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self._meta.get_field('username').verbose_name = 'Identifiant'

    @property
    def is_manager(self):
        return (self.manage_cp is not None)

    @property
    def is_worker(self):
        return (self.work4_cp is not None)

    @property
    def is_odk_admin(self):
        return self.groups.filter(name='odk-admin').exists()

    @property
    def is_odk_user(self):
        return self.groups.filter(name='odk-user').exists()

    def get_absolute_url(self):
        return reverse('profile_detail', args=[str(self.id)])

    def get_update_url(self):
        return reverse('profile_update', args=[str(self.id)])

    def get_full_name(self):
        fullname = '{0} {1}'.format(self.first_name, self.last_name.upper())
        if fullname.strip() == '':
            if self.company is None or self.company.strip() == '':
                fullname = self.email
            else:
                fullname = self.company
        return str(fullname)

    def get_title(self):
        return "{0} » {1}".format(self._meta.verbose_name, self.get_full_name())

    # def get_insciption_cp(self):
    #     return ", ".join([str(a.nom) for a in self.inscription_cp.all()])

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def save(self, *args, **kwargs):
        """
        Inscrire le gestionnaire d'un centrepilote à ses listes d'envois
        ajouter aussi work4_cp
        """
        if self.manage_cp is not None:
            # self.work4_cp = self.manage_cp
            listeenvoi = ListeEnvoi.objects.filter(centrepilote=self.manage_cp)
            for le in listeenvoi:
                inscr, created = Inscription.objects.for_user(self).get_or_create(profile=self, listeenvoi=le)
                inscr.save()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.get_title()


# class UserGeneric(models.Model):
#     """
#     User always in working tables tables (IN & OUT)
#     """
#     profile = models.ForeignKey(Profile, verbose_name = _("Utilisateur"), on_delete=models.CASCADE)
#     class Meta:
#         abstract = True
class CentrePiloteLocation(models.Model):
    """
    Listing des sites pour 1 centre pilote
    """
    centrepilote = models.ForeignKey(CentrePilote, verbose_name=_("Centre Pilote"), on_delete=models.CASCADE)
    intitule = models.CharField(_("Intitulé"), max_length=255)
    title = models.CharField(_("Titre"), max_length=255)
    subtitle = models.CharField(_("Sous-titre"), max_length=255, blank=True, null=True)
    importancenr = models.PositiveSmallIntegerField(_("Ordre affichage"), default=0, help_text=_("Tapez 0 pour ne pas afficher dans la signature"))
    contact = models.ForeignKey(Profile, on_delete=models.CASCADE, blank=True, null=True)
    street = models.CharField(_("Rue + Nr"), max_length=255, blank=True, null=True, help_text=_("Nom de rue, numéro"))
    zip = models.ForeignKey(
        GeoZip, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_("Code postal"))
    logo = FilerImageField(related_name="logocpsub", null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Localisation/Site")
        ordering = ["title"]

    def get_absolute_url(self):
        return reverse("cp_detail", args=[str(self.centrepilote.id)])

    def __str__(self):
        return "{0}".format(self.title)



class ListeEnvoi(models.Model):
    """
    Liste d'envoi par centre pilote
    Permet d'envoyer des messages à des groupes de personnes
    """
    code = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)
    description = models.CharField(verbose_name=_("Périodicité"), max_length=255)
    centrepilote = models.ForeignKey(CentrePilote, verbose_name=_("Centre Pilote"), on_delete=models.CASCADE)
    infotxt = RichTextField(_("Communication supplémentaire"),
        help_text=_("Texte générique qui s'affiche automatiquement à la fin de chaque E-mail associé à cette liste d'envoi"),
        blank=True, null=True)
    partenaires = models.ManyToManyField(WebLink, verbose_name="Partenaires",
        help_text=_("Maintenir la touche 'Ctrl' enfoncée pour sélectionner plusieurs partenaires")
    )
    inscr_auto = models.BooleanField(_("Inscription automatique?"), default=True,
        help_text=_("Inscription aux E-mails dès la création d'un nouveau compte sur le site.")
    )
    class Meta:
        verbose_name = _("Liste envoi")
        unique_together = ('centrepilote', 'nom')
        ordering = ['nom']

    @property
    def get_partenaires_list(self):
        retstr = " ".join([a.get_logo() for a in self.partenaires.all()])
        return retstr.strip()

    @property
    def get_partenaires_list_large(self):
        return " ".join([a.get_logo_large() for a in self.partenaires.all()])

    @property
    def get_partenaires_list4email(self):
        return " ".join([a.get_logo4email() for a in self.partenaires.all()])

    @property
    def get_partenaires_list4email_preview(self):
        return " ".join([a.get_logo4email_preview() for a in self.partenaires.all()])

    def get_absolute_url(self):
        return reverse('listeenvoi_detail', args=[str(self.id)])

    def get_title(self):
        return "{0} » {1}".format(self._meta.verbose_name, self.nom)

    def __str__(self):
        return '{0}'.format(self.nom)

#################
# Inscription CP
#################

class UserManager(models.Manager):
    def get_queryset(self):
        return super(UserManager, self).get_queryset()

    def for_user(self, user):
        return super(UserManager, self).get_queryset().filter(profile=user)


class Inscription(models.Model):
    """
    Table des abonnements aux listes d'envois CP par utilisateur.
    Ajouter ici une colonne avec éventuel montant à payer si service payant
    """
    profile = models.ForeignKey(Profile, verbose_name=_("Utilisateur"), on_delete=models.CASCADE)
    listeenvoi = models.ForeignKey(ListeEnvoi, verbose_name=_("Liste envoi"), on_delete=models.CASCADE)
    is_valid = models.BooleanField(_("Valide"), default=True)

    objects = UserManager()

    class Meta:
        verbose_name = _("Inscription")
        unique_together = ('profile', 'listeenvoi')

    def get_inscription_list(self, listeenvoi):
        emaillist = self.filter(listeenvoi=listeenvoi).values_list('profile__email', flat=True)
        return emaillist

#################
# SIGNALS
#################

def create_listeenvoi_signal(instance, created, **kwargs):
    if created:
        try:
            # get managers of cp
            managers = Profile.objects.filter(manage_cp=instance.centrepilote)
            for manager in managers:
                inscr, addnew = Inscription.objects.get_or_create(
                    profile=manager,
                    listeenvoi=instance,
                    is_valid=True
                )
                inscr.save()
        except Profile.DoesNotExist:
            pass

post_save.connect(receiver=create_listeenvoi_signal, sender=ListeEnvoi, weak=False)


def create_profile_signal(instance, created, **kwargs):
    if created:
        try:
            # get listing of active liste-envoi
            le_list = ListeEnvoi.objects.filter(centrepilote__is_sending_msg=True, inscr_auto=True)
            for le in le_list:
                inscr, addnew = Inscription.objects.get_or_create(
                    profile=instance,
                    listeenvoi=le,
                    is_valid=True
                )
                inscr.save()
        except ListeEnvoi.DoesNotExist:
            pass

post_save.connect(receiver=create_profile_signal, sender=Profile, weak=False)
