# encoding:utf-8
"""
Inspired from
https://simpleisbetterthancomplex.com/tutorial/2016/08/24/how-to-create-one-time-link.html
and
https://simpleisbetterthancomplex.com/tutorial/2017/02/18/how-to-create-user-sign-up-view.html#sign-up-with-confirmation-mail
"""
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from six import text_type

class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            text_type(user.pk) + text_type(timestamp) +
            text_type(user.email_confirmed)
        )

account_activation_token = AccountActivationTokenGenerator()
