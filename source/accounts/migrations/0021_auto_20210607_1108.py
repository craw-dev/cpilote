# Generated by Django 3.1.6 on 2021-06-07 09:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0020_auto_20210603_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profiletmp',
            name='jobdesc_order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Fonction/métier ordre'),
        ),
        migrations.AlterField(
            model_name='profiletmp',
            name='nr_producteur',
            field=models.CharField(blank=True, max_length=12, null=True, verbose_name='Numéro producteur'),
        ),
    ]
