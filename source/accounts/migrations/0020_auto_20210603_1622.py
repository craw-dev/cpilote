# Generated by Django 3.1.6 on 2021-06-03 14:22

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0019_auto_20210602_1220'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileTmp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='adresse électronique')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='prénom')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='nom')),
                ('mobile', models.CharField(blank=True, max_length=15, null=True, verbose_name='Tel portable')),
                ('phone', models.CharField(blank=True, max_length=15, null=True, verbose_name='Tel fixe')),
                ('fax', models.CharField(blank=True, max_length=15, null=True, verbose_name='Fax')),
                ('company', models.CharField(blank=True, max_length=255, null=True, verbose_name='Entreprise/Organisation')),
                ('profession', models.CharField(blank=True, max_length=100, null=True)),
                ('modeprod', models.CharField(blank=True, max_length=100, null=True)),
                ('jobdesc', models.CharField(blank=True, help_text='Example: Chef de projet, Assistant administratif, Responsable Finances, Administration & Communication, etc.', max_length=255, null=True, verbose_name='Fonction/métier')),
                ('jobdesc_order', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Fonction/métier ordre')),
                ('nr_producteur', models.CharField(blank=True, max_length=12, null=True, unique=True, verbose_name='Numéro producteur')),
                ('street', models.CharField(blank=True, help_text='Rue, nr rue', max_length=255, null=True, verbose_name='Rue')),
                ('code_postal', models.CharField(blank=True, max_length=10, null=True, verbose_name='Code postal')),
                ('localite', models.CharField(blank=True, max_length=255, null=True, verbose_name='Localité')),
            ],
        ),
        migrations.AlterField(
            model_name='listeenvoi',
            name='infotxt',
            field=ckeditor.fields.RichTextField(blank=True, help_text="Texte générique qui s'affiche automatiquement à la fin de chaque E-mail associé à cette liste d'envoi", null=True, verbose_name='Communication supplémentaire'),
        ),
    ]
