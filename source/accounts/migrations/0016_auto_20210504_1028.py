# Generated by Django 3.1.6 on 2021-05-04 08:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0015_auto_20210503_1527'),
    ]

    operations = [
        migrations.RenameField(
            model_name='centrepilote',
            old_name='siteweb',
            new_name='siteweb',
        ),
        migrations.RemoveField(
            model_name='centrepilote',
            name='is_active',
        ),
        migrations.AddField(
            model_name='centrepilote',
            name='is_sending_msg',
            field=models.BooleanField(default=False, help_text='Envoie des avertissements sur cette plateforme.', verbose_name='Envois avertissements actif'),
        ),
    ]
