--select count(email) from accounts_profiletmp
/* user in DB not present in TMP*/
select a.email, a.first_name, a.last_name, to_char(a.date_joined, 'dd/mm/YYYY') from (
select y.* from accounts_inscription x inner join accounts_profile y on x.profile_id=y.id where x.listeenvoi_id=5
	) as a left outer join accounts_profiletmp b
	on a.email = b.email
where b.email is null
order by a.date_joined

/* user in TMP not present in DB */
select b.email, b.first_name, b.last_name from (
select y.* from accounts_inscription x inner join accounts_profile y on x.profile_id=y.id where x.listeenvoi_id=5
	) as a right outer join accounts_profiletmp b
	on a.email = b.email
where a.email is null