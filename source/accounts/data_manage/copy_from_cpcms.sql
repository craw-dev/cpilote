﻿/* province */
INSERT INTO accounts_province(code, name, region_id)
SELECT src.code, src.name, src.region_id
FROM dblink('dbname=cpcms_00','SELECT code, nom, region_id FROM accounts_province')
AS src(code character(100), name character(100), region_id integer);

/* region agricole */
INSERT INTO accounts_agriculturalregion(code, name)
SELECT src.code, src.name
FROM dblink('dbname=cpcms_00','SELECT code, nom FROM accounts_regionagricole')
AS src(code character(100), name character(100));

/* zip */
INSERT INTO accounts_zip(code, locality, province_id, agricultural_region_id)
SELECT src.code, src.localite, src.province_id, src.region_agricole_id
FROM dblink('dbname=cpcms_00','SELECT code, localite, province_id, region_agricole_id FROM accounts_codepostal')
AS src(code character(100), localite character(100), province_id integer, region_agricole_id integer);
