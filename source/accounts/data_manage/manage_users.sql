﻿/*
Script SQL gestion utilisateurs
*/

update public.accounts_cpprofile set nr_producteur = Null
where length(nr_producteur) > 12

-- Voir s'il existe des doublons
select email, count(*) from public.auth_user group by email having count(*) > 1

/* user profile */
select u.last_name, u.first_name, p.nr_producteur, p.localite
from public.accounts_cpprofile p, public.auth_user u
where u.id=p.user_id
and u.username like 'deval%'
and email = 'devalckeneer.peremans@gmail.com'

/* guardian */
select * from public.guardian_userobjectpermission where user_id = 11851

/* user */
select id, username, email from public.auth_user where email like 'houbenp%'
select * from public.auth_user where last_name like 'Bertel'
