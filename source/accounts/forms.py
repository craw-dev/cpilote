# encoding:utf-8
from __future__ import unicode_literals

import re
import logging
from collections import OrderedDict
from django.contrib.auth import get_user_model
from django.forms import (Form, ModelForm, ValidationError, Select, ChoiceField,
                          FileField, ModelChoiceField, TextInput, CharField, BooleanField,
                          HiddenInput, Textarea)
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.tokens import default_token_generator
# django.contrib.auth.forms.UserCreationForm
from django.contrib.auth.forms import UsernameField
# from django.contrib.auth import password_validation
from django.utils.translation import ugettext_lazy as _

from .models import Profile, ListeEnvoi, GeoZip, Profession, CentrePiloteLocation, CentrePilote
from .formsearch import TxtSearchForm
from info.models import WebLink


LOG = logging.getLogger(__name__)


class BaseForm(Form):
    """
    Overwrites BaseForm class which strips leading and trailing spaces
    source: https://chriskief.com/2012/12/21/trim-spaces-in-django-forms/
    """
    # strip leading or trailing whitespace
    def _clean_fields(self):
        for name, field in self.fields.items():
            # value_from_datadict() gets the data from the data dictionaries.
            # Each widget type knows how to retrieve its own data, because some
            # widgets split data over several HTML fields.
            value = field.widget.value_from_datadict(self.data, self.files, self.add_prefix(name))
            try:
                if isinstance(field, FileField):
                    initial = self.initial.get(name, field.initial)
                    value = field.clean(value, initial)
                else:
                    if isinstance(value, str):
                        value = field.clean(value.strip())
                    else:
                        value = field.clean(value)
                self.cleaned_data[name] = value
                if hasattr(self, 'clean_%s' % name):
                    value = getattr(self, 'clean_%s' % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self.add_error(name, e)
 
 
class Form(BaseForm, Form):
    pass
  
  
class ModelForm(BaseForm, ModelForm):
    pass


class PhoneCheck(object):
    """
    Regexp for BE, FR, LU, NL phone pattern
    """
    def _test_phone(self, nr):
        """
        3 groups separated by any signs (space, -, /, etc.)
        1. 0 or 1 + sign
        2. prefix can be 0, 31, 32, 33 or 352
        3. between 8 and 9 digit
        """
        re_pattern = re.compile(r"^(\+?)(0|31|32|33|352)\D*(\d\D*){8,9}$")
        if re_pattern.match(nr) and len(nr) > 0:
            return True
        else:
            return False

    # def set_32(self, nr):
    #     if nr[0] == '0':
    #         nr = '+32' + nr[1:]
    #     return nr


class ProfileUpdateForm(PhoneCheck, ModelForm):
    """
    Base form used for fields that are always required
    Customised form in order to remove privacy, mugshot.
    """
    zipsearch = CharField(label=_("Localité recherche"), max_length=10,
                        required=False,
                        help_text=_("Encodez les 3 premiers chiffres/caractères du code postal ou de la localité"),
                        widget=TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche code postal ou localité"),
                            "onkeyup": 'fillin_ddl_zip()'
                            })
                        )    
    class Meta:
        model = Profile
        fields = ('first_name', 'last_name',
                  'email', 'mobile', 'phone', 'street', 'zip',
                  'company', 'profession', 'modeprod', 'nr_producteur', 'jobdesc',
                  'jobdesc_order', 'work4_cp', 'is_active')
        widgets = {
            "nr_producteur": TextInput(attrs={"placeholder": "Respecter le format '123456789-12'"}),
        }

    class Media:
        extend = False
        js = (
            'js/django_global.js',
            'admin/js/core.js',
            'js/load_ddl_accounts.js',
        )

    def __init__(self, *args, **kwargs):
        self.connected_user = kwargs.pop("connected_user", None)
        self.manage_user = kwargs.pop("manage_user", None)
        super(ProfileUpdateForm, self).__init__(*args, **kwargs)
        # if self.connected_user.is_manager and self.clicked_user is not None:
        if self.manage_user == "YES":           
            self.fields['company'].required = False
            self.fields['profession'].required = False
            self.fields['zip'].required = False
        else:
            self.fields['last_name'].required = True
            self.fields['first_name'].required = True
            self.fields['company'].required = True
            self.fields['profession'].required = True
            self.fields['zip'].required = True

        # Data filled in and Zip_queryset must be adapted
        qs = GeoZip.objects.none()
        if 'zip' in self.data:
            zip_id = self.data.get('zip')
            if zip_id != '':
                qs = GeoZip.objects.filter(pk=int(zip_id))
        elif self.instance.zip:
            zip_id = self.instance.zip.id
            qs = GeoZip.objects.filter(pk=zip_id)
        self.fields['zip'].queryset = qs

        manager_fields = [
            ('jobdesc_order', self.fields['jobdesc_order']),
            ('work4_cp', self.fields['work4_cp']),
            ('is_active', self.fields['is_active']),
            ]
        # reorder the fields
        user_fields = [
            ('first_name', self.fields['first_name']),
            ('last_name', self.fields['last_name']),
            ('email', self.fields['email']),
            ('mobile', self.fields['mobile']),
            ('phone', self.fields['phone']),
            ('street', self.fields['street']),
            ('zipsearch', self.fields['zipsearch']),
            ('zip', self.fields['zip']),
            ('company', self.fields['company']),
            ('profession', self.fields['profession']),
            ('modeprod', self.fields['modeprod']),
            ('nr_producteur', self.fields['nr_producteur']),            
            ('jobdesc', self.fields['jobdesc']),
        ]
        if self.connected_user.is_manager:
            new_order = user_fields + manager_fields
        else:
            new_order = user_fields 
            del self.fields['jobdesc_order']
            del self.fields['work4_cp']
            del self.fields['is_active']        
        self.fields = OrderedDict(new_order)


    def clean_nr_producteur(self):
        if self.cleaned_data['nr_producteur'] is not None:
            test_pattern = re.compile(r'^([0-9]{9}-[0-9]{2})$')
            if test_pattern.match(self.cleaned_data['nr_producteur']):
                return self.cleaned_data['nr_producteur']
            else:
                raise ValidationError('Respecter le format (ex: 123456789-12)')

    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']           
        if first_name == '' and self.manage_user == "NO":
            raise ValidationError(_('Compléter votre prénom svp.'))
        else:
            return(first_name.capitalize())

    def clean_last_name(self):
        last_name = self.cleaned_data['last_name']
        if last_name == '' and self.manage_user == "NO":
            raise ValidationError(_('Compléter votre nom svp.'))
        else:
            return(last_name.upper())

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone is None or phone == '':
            return None
        elif self._test_phone(phone):
            return phone
        else:
            raise ValidationError(_('Respecter le format: (0) \
                                    suivi de 8 à 9 chiffres.'))

    def clean_mobile(self):
        mobile = self.cleaned_data['mobile']
        if mobile is None or mobile == '':
            return None
        elif self._test_phone(mobile):
            return mobile
        else:
            raise ValidationError(_('Respecter le format: (0) \
                                    suivi de 9 chiffres.'))

    def clean(self):
        cleaned_data = super().clean()
        company = cleaned_data.get("company")
        manage_cp = cleaned_data.get("manage_cp")
        work4_cp = cleaned_data.get("work4_cp")
        if manage_cp is None and work4_cp is None and company is not None:
            if company.strip() == "":
                raise ValidationError(_('Compléter un nom de société/organisation svp.'))
        return cleaned_data
    
    
class ProfileSignUpForm(UserCreationForm):
    """   
    based on django UserCreationForm
    """
    class Meta:
        model = get_user_model()
        fields = ("email",)

    def clean_email(self):
        email = self.cleaned_data.get("email")
        try:
            Profile.objects.get(email=email)
            raise ValidationError(_('Cette adresse électronique existe déjà dans la base de données!'))
        except Profile.DoesNotExist:
            pass
        return email


class ListeEnvoiForm(ModelForm):
    """Liste envoi"""
    class Meta:
        model = ListeEnvoi
        fields = ["nom", "description", "partenaires", "infotxt", "inscr_auto"]
        widgets = {
            "partenaires" : FilteredSelectMultiple("Partenaires", False)
        }
    class Media:
        extend = False
        css = {
            'all': [
                'admin/css/widgets.css'
            ]
        }
        js = (
            'js/django_global.js',
            'admin/js/core.js',
            'admin/js/SelectBox.js',
            'admin/js/SelectFilter2.js',
            'ckeditor/ckeditor-init.js',
            'ckeditor/ckeditor/ckeditor.js',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['partenaires'].queryset = WebLink.objects.filter(menucateg='partenaire')


class InscriptionSearchForm(TxtSearchForm):
    INSCR_LIST = [("ALL", "--------"),
                ("OUI", "Inscrit"),
                ("NON", "Pas inscrit")]
    VALID_LIST = [("ALL", "--------"),
                ("OUI", "Validé"),
                ("NON", "Non validé")]
    EMPTY_LIST = [("ALL", "Province")]
    zipsearch = CharField(max_length=10,
                        required=False,
                        widget=TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche code postal ou localité"),
                            })
    )                   
    province = ChoiceField(
        choices=(EMPTY_LIST +
            list(GeoZip.objects.filter(country_code='BE')
                .values_list('admin_code2', 'admin_name2')
                .distinct('admin_name2')
                .order_by('admin_name2'))
        ),
        required=False,
        widget=Select(attrs={"class": "form-control"}),
    )
    profession = ModelChoiceField(
        queryset=Profession.objects.all(),
        required=False,
        widget=Select(attrs={"class": "form-control"}),
    )
    inscrflag = ChoiceField(
                    choices=INSCR_LIST,
                    required=False,
                    widget=Select(attrs={'class': "form-control"})
    )
    validflag = ChoiceField(
                    choices=VALID_LIST,
                    required=False,
                    widget=Select(attrs={'class': "form-control"})
    )

    def __init__(self, request):
        super().__init__()
        self.fields["txtsearch"].widget=TextInput(attrs={
                                    "class": "form-control",
                                    "placeholder": _("Rechercher Identifiant, Nom Prénom, Email")
                                })
        # Set init
        self.fields["txtsearch"].initial = request.GET.get('txtsearch', None)
        self.fields["zipsearch"].initial = request.GET.get('zipsearch', None)
        self.fields["province"].initial = request.GET.get('province', None)
        self.fields["profession"].initial = request.GET.get('profession', None)
        self.fields["inscrflag"].initial = request.GET.get('inscrflag', None)
        self.fields["validflag"].initial = request.GET.get('validflag', None)


class CPLocForm(ModelForm):
    user_model = get_user_model()

    contactsearch = CharField(label=_("Contact recherche"), max_length=10,
                        required=False,
                        help_text=_("Encodez les 3 premiers caractères de recherche (nom, prénom, société, email)"),
                        widget=TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche contact"),
                            "onkeyup": 'fillin_ddl_contact()'
                            })
                        )
    zipsearch = CharField(label=_("Localité recherche"), max_length=10,
                        required=False,
                        help_text=_("Encodez les 3 premiers chiffres/caractères du code postal ou de la localité"),
                        widget=TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche code postal ou localité"),
                            "onkeyup": 'fillin_ddl_zip()'
                            })
                        )

    class Meta:
        model = CentrePiloteLocation
        widgets = {'centrepilote': HiddenInput()}
        fields = ["centrepilote", "intitule", "title", "subtitle", "importancenr", "contact", "street", "zip"]

    class Media:
        extend = False
        js = (
            'js/django_global.js',
            'admin/js/core.js',
            'js/load_ddl_accounts.js',
        )

    def __init__(self, *args, **kwargs):
        cpobj = kwargs.pop('cpobj', None)
        super().__init__(*args, **kwargs)
        # Set CP instance (Hidden in Meta)
        self.fields['centrepilote'].initial = cpobj
        # Data filled in and Zip_queryset must be adapted
        if 'zip' in self.data:
            zip_id = self.data.get('zip')
            qs = GeoZip.objects.filter(pk=zip_id)
        elif self.instance.zip:
            zip_id = self.instance.zip.id
            qs = GeoZip.objects.filter(pk=zip_id)
        else:
            qs = GeoZip.objects.none()
        self.fields['zip'].queryset = qs
        
        # Data filled in and contact_queryset must be adapted
        if 'contact' in self.data:
            contact_id = self.data.get('contact')
            qs = self.user_model.objects.filter(pk=contact_id)
        elif self.instance.contact:
            contact_id = self.instance.contact.id
            qs = self.user_model.objects.filter(pk=contact_id)
        else:
            qs = self.user_model.objects.none()
        self.fields['contact'].queryset = qs

        # reorder the fields
        new_order = [
            ('centrepilote', self.fields['centrepilote']),
            ('intitule', self.fields['intitule']),
            ('title', self.fields['title']),
            ('subtitle', self.fields['subtitle']),
            ('importancenr', self.fields['importancenr']),
            ('contactsearch', self.fields['contactsearch']),
            ('contact', self.fields['contact']),       
            ('street', self.fields['street']),
            ('zipsearch', self.fields['zipsearch']),
            ('zip', self.fields['zip']),
            # ('logo', self.fields['logo']),
        ]
        # new_order.extend(list(self.fields.items())[6:])
        self.fields = OrderedDict(new_order)

    def form_valid(self, form):
        """change qs after update"""
        contact_id = self.cleaned_data.get("contact").strip()
        # LOG.debug("form_valid contact_id={}".format(contact_id))
        qs = self.user_model.objects.filter(pk=contact_id)
        self.fields['contact'].queryset = qs
        return super().form_valid(form)
