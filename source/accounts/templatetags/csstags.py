# -*- coding: utf-8 -*-
"""
Template tags to modify classes of a field directly into the templates.

Inspired from http://vanderwijk.info/blog/adding-css-classes-formfields-in-django-templates/

"""
import logging
from datetime import date
from urllib.parse import urlencode
from django import template
from django.db.models.query import QuerySet
from django.db.models import Sum, Min

register = template.Library()

__author__ = 'hbp'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2018, Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2018-08-06'
__version__ = '1.0'
__status__ = 'Development'

LOG = logging.getLogger(__name__)

@register.filter(name='addcss')
def addcss(field, css):
    """
    Add a css to the existing classes of the field
    """
    class_old = field.field.widget.attrs.get('class', None)
    class_new = class_old + ' ' + css if class_old else css
    return field.as_widget(attrs={"class": class_new})


@register.filter(name='removecss')
def removecss(field, css):
    """
    Remove the provided class from the existing classes of the field
    """
    class_old = field.field.widget.attrs.get('class', None)
    if class_old:
        if css in class_old:
            class_new = class_old.replace(css, '')
            return field.as_widget(attrs={"class": class_new})

        else:
            return field
    else:
        return field


@register.filter(name='replacecss')
def replacecss(field, css):
    """
    Replaces the class attribute with the provided one
    """
    return field.as_widget(attrs={"class": css})


@register.simple_tag
def get_verbose_field_name(objname, field_name):
    """
    Returns verbose_name for a field.
    """
    # Check wether objname is a QuerySet of directly the object instance!
    # QuerySet option should not be used since we do first() for each call in header
    if isinstance(objname, QuerySet):
        objcopy = objname
        instance = objcopy.first()
    else:
        instance = objname
    fname = instance._meta.get_field(field_name).verbose_name
    return fname.capitalize()


@register.simple_tag
def get_verbose_model_class_name(objname):
    """
    Returns verbose_name for a field.
    """
    # Check wether objname is a QuerySet of directly the object instance!
    if isinstance(objname, QuerySet):
        objcopy = objname
        instance = objcopy.first()
    else:
        instance = objname
    return instance._meta.verbose_name.capitalize()


@register.simple_tag
def define(val=None):
    return val


@register.simple_tag
def frompageline(pagenr, recnr, totrec):
    """From page"""
    if totrec == 0:
        return 0
    if pagenr == 1:
        return 1
    else:
        return ((pagenr-1)*recnr)+1


@register.simple_tag
def topageline(pagenr, recnr, totrec):
    """To page"""
    totrec = int(totrec)
    if pagenr == 1:
        result = recnr
    else:
        result = pagenr * recnr

    if result > totrec:
        return totrec
    else:
        return result


# https://stackoverflow.com/questions/2047622/how-to-paginate-django-with-other-get-variables
@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.dict()
    query.update(kwargs)
    return urlencode(query)


@register.filter(name='lookup')
def lookup(d, key):
    if type(d) is dict:
        return d.get(key)
    else:
        return d
# {{ msg.summary_img.thumbnails|lookup:'admin_sidebar_preview' }}


@register.filter(name='lookup_element')
def lookup_element(d, args):
    key, element = args.split(',')
    element = int(element)
    return d[key][element]


# @register.simple_tag
# def get_dict_value(dic, key):
#     if type(dic) is dict:
#         ret = dic.get(key, "")
#         if ret == 'None':
#             ret = ''
#         return ret
#     else:
#         return dic
# <li>{% get_dict_value child.attr 'soft_root' %}</li>


@register.filter
def next(some_list, current_index):
    """
    Returns the next element of the list using the current index if it exists.
    Otherwise returns an empty string.
    """
    try:
        return some_list[int(current_index) + 1] # access the next element
    except:
        return '' # return empty string in case of exception


@register.filter
def previous(some_list, current_index):
    """
    Returns the previous element of the list using the current index if it exists.
    Otherwise returns an empty string.
    """
    try:
        return some_list[int(current_index) - 1] # access the previous element
    except:
        return '' # return empty string in case of exception
