from django import template
from accounts.models import CentrePilote

register = template.Library()


@register.simple_tag
def get_cpname(mypath=None):
    cpcode = 'main'
    cpacro = 'main'
    cplogo = 'main'

    plist = mypath.split('/')
    if len(plist) > 2 and plist[1] == 'cp' and plist[2] != '':
        cpcode = plist[2]
        cp = CentrePilote.objects.get(code=cpcode)
        cpacro = cp.acronyme
        cplogo = cp.get_logopath()

    return {'code': cpcode, 'acro': cpacro, 'logo': cplogo}

