"use strict";

/******************************************
CP generic script with different functions
- highlightMenu()
- scrollFunction()
- changeBanner()
- date_2_string()
- current_date()
- current_date_add_weeks()

variable naming convention
    url: http://localhost:8000/msg/?listeenvoi=3
    base_url = http://localhost:8000
    root_url = /msg/
    rootarg_url = /msg/?listeenvoi=3 
*******************************************/
var root_url = window.location.pathname;
var full_url = window.location.href;
var rootarg_url = full_url.split(base_url).pop(); //not used

var cp_path = root_url.split("/cp/").pop();
var cp_name = cp_path.split("/")[0];
/*****************/

/* FUNCTIONS     */

/*****************/
// highlight Menu according to root_url

function highlightMenu() {
  $(".menu-link").each(function () {
    menu_path = '^' + this.pathname.slice(0, -1) + '$';
    menu_path_regexp = new RegExp(menu_path);

    if (menu_path_regexp.test(root_url)) {
      $(this).addClass('active');
    }
  });
} // Hide/Display scrollTop button if scroll down of 20px


function scrollFunction() {
  var mybutton = document.getElementById("GoToTop");

  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
} // change Banner Image


function changeBanner(cp_name_ok) {
  // d = new Date();
  // var imgpath = "/static/img/banner/banner-" + cp_path_ok + ".webp?" + d.getTime();
  var imgpath = "/static/img/banner/orig/banner-" + cp_name_ok + "-orig.webp"; // $("img#banner").attr("src", imgpath);

  $("#cp-nav-img").css("background-image", "url(" + imgpath + ")");
}

function date_2_string(dt) {
  var day = ("0" + dt.getDate()).slice(-2);
  var month = ("0" + (dt.getMonth() + 1)).slice(-2);
  var today = dt.getFullYear() + "-" + month + "-" + day;
  return today;
}

function current_date() {
  var now = new Date();
  return date_2_string(now);
}

function current_date_add_weeks(n) {
  var now = new Date();
  var now_week = new Date(now.setDate(now.getDate() + n * 7));
  return date_2_string(now_week);
}
/*****************/

/* DIRECT ACCESS */

/*****************/


highlightMenu();

window.onscroll = function () {
  scrollFunction();
};

$('.goBack').on('click', function () {
  window.history.back();
}); // https://stackoverflow.com/questions/3480771/how-do-i-check-if-string-contains-substring
// Banner has to change when /cp/ + cp_name is found in root_url

if (/cp/i.test(root_url)) {
  if (root_url == '/cp/') {
    cp_path = "/cp/";
    cp_name = "main";
  } else {
    cp_path = "/cp/" + cp_name;

    if (cp_name == "cepicop") {
      cp_name2 = root_url.split(cp_path).pop().split("/")[1];

      if (cp_name2 != 'oleagineux' & cp_name2 != 'proteagineux') {
        changeBanner(cp_name);
      } else {
        changeBanner(cp_name + '-' + cp_name2); // pour subdivision dans le même cp (cepicop)
      }
    } else {
      changeBanner(cp_name);
    }
  }

  ;
}