function submitForm() {
    $("#form_txtsearch").submit();
}

$(document).ready(function () {
    $("#id_txtsearch").change(submitForm);
    $("#id_profession").change(submitForm);
    $("#id_province").change(submitForm);
    $("#id_zipsearch").change(submitForm);
    $("#id_inscrflag").change(submitForm);
    $("#id_validflag").change(submitForm);
});