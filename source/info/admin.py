# encoding:utf-8
from django.contrib import admin
from django.utils.translation import ugettext as _
from django.db import models

from .models import WebLink


class WebLinkAdmin(admin.ModelAdmin):
    list_display = ('acronyme', 'nom', 'domaine')
    list_filter = ('domaine', 'menucateg')
    search_fields = ('nom', 'short_desc', )


admin.site.register(WebLink, WebLinkAdmin)