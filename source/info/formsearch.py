# encoding:utf-8
from django.forms import (Form, TextInput, CharField, ChoiceField, Select)
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

from accounts.formsearch import TxtSearchForm
from .models import WebLink


class WebLinkSearchForm(TxtSearchForm):
    domaine = ChoiceField(choices=WebLink.DOMAINE_CHOICES,
        required=False,
        widget=Select(attrs={'class': "form-control"})
        )

    def __init__(self, *args):
        self.param = args[0]
        super().__init__(*args)

    def build_where(self):
        where = Q(pk__gt=0)

        txtsearch = self.param.get("txtsearch", None)
        if txtsearch is not None and txtsearch != '':
            search_item = txtsearch.strip()
            where = Q(acronyme__icontains=search_item) | Q(nom__icontains=search_item) | Q(short_desc__icontains=search_item)

        domaine = self.param.get("domaine", None)
        if domaine:
            where &= Q(domaine=domaine)

        return where        
        