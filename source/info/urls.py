# encoding:utf-8
'''
Created on 13/04/2020
@author: p.houben
'''
from django.urls import path
from .views import (
    WebLinkList, WebLinkDetail, weblink_detail,
    goaccess_list, accueil, goaccess_report, goaccess_report_year
)

app_name = "info"

urlpatterns = [
    path('', accueil, name='accueil'),
    path('autres/', WebLinkList.as_view(menucateg='autre'), name='autre'),
    path('partenaires/', WebLinkList.as_view(menucateg='partenaire'), name='partenaire'),
    path('outils/', WebLinkList.as_view(menucateg='outil'), name='outil'),
    # ne pas changer ci-dessous, c'est une popup avec appel ajax!
    path('detail/', weblink_detail, name='weblink_detail'), 
    path('statistiques/', goaccess_list, name='cpilote_stat'),
    path('statistiques/mensuelle/<month>', goaccess_report, name='goaccess_report'),
    path('statistiques/annuelle/<year>', goaccess_report_year, name='goaccess_report_year'),
]
