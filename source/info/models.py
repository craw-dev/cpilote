from filer.fields.image import FilerImageField
from ckeditor.fields import RichTextField

from django.db import models
from django.utils.translation import ugettext_lazy as _
# from accounts.models import GeoZip


class WebLink(models.Model):
    """
    Table liens utiles
    """
    DOMAINE_CHOICES = (
        (None, "-- Domaine --"),
        ("10-ACC", "Accompagnement"),
        ("20-BIO", "Bio - Local"),
        ("30-CPS", "Centre Pilote"),
        ("40-ENC", "Encadrement"),
        ("50-REC", "Recherche"),
        ("60-SPR", "Province"),
        ("65-WAL", "Wallonie"),
        ("70-AUT", "Autres liens"),
        ("90-CAT", "Catalogue"),
        ("94-DIA", "Diagnostique"),
        ("99-AUT", "Autre")     
    )
    CATEG_CHOICES = (
        (None, "-- Catégorie --"),
        ("outil", "Outils"),
        ("partenaire", "Partenaires"),
        ("autre", "Liens utiles"),
    )    
    code = models.CharField(max_length=20, unique=True)
    acronyme = models.CharField(_("Acronyme"), max_length=50)
    nom = models.CharField(max_length=255, unique=True)
    logo = FilerImageField(related_name="weblink_logo", on_delete=models.PROTECT, null=True, blank=True, verbose_name=_("Logo"))
    short_desc = RichTextField(verbose_name=_("Description"), blank=True, null=True)
    menucateg = models.CharField(_("Menu Categ"), max_length=20, choices=CATEG_CHOICES)
    domaine = models.CharField(_("Domaine"), max_length=6, choices=DOMAINE_CHOICES, blank=True, null=True)
    siteweb = models.URLField(_("Site web"), blank=True, null=True)
    contact = models.CharField(max_length=255, blank=True, null=True)
    tel = models.CharField(_('Tel'), max_length=15, blank=True, null=True,
        help_text="0 ou prefixe pays suivi de 8 à 9 chiffres")
    email = models.EmailField(_('Email'), blank=True, null=True)
    rue = models.CharField(_('Rue, nr de rue'), max_length=255, blank=True, null=True, help_text=_('Rue, nr rue'))
    codepostal = models.CharField(_('Code postal'), max_length=255, blank=True, null=True, help_text=_('CP et localisation'))
    # codepostal = models.ForeignKey(
    #     GeoZip, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_('Code postal'))

    class Meta:
        verbose_name = _("Liens utile")
        ordering = ['acronyme']

    def get_logopath(self):
        logopath = self.logo.thumbnails['admin_directory_listing_icon']
        return logopath

    def get_logo(self):
        imgpath = self.get_logopath()
        imgtag = f'<img src="{imgpath}?reloadV44" alt="{self.acronyme}"/>'
        imghref = f'<a href="{self.siteweb}" target="_blank">{imgtag}</a>'
        return imghref

    def get_logo_large(self):
        # imgpath = self.get_logopath('admin_sidebar_preview')
        imgpath = self.logo.url
        imgtag = f'<img class="img-100 img-fluid" src="{imgpath}?reloadV44" alt="{self.acronyme}"/>'
        imghref = f'<a href="{self.siteweb}" target="_blank">{imgtag}</a>'
        return imghref

    def get_logo4email(self):
        filepath = self.get_logopath()
        pathlist = filepath.split('/')
        pathlen = len(pathlist)
        filename_full = pathlist[pathlen-1]
        imgtag = f'<img class="logo" src="cid:{filename_full}" alt="{self.acronyme}"/>'
        imghref = f'<a href="{self.siteweb}" target="_blank">{imgtag}</a>'
        return imghref

    def get_logo4email_preview(self):
        filepath = self.get_logopath()
        imgtag = f'<img class="logo" src="{filepath}" alt="{self.acronyme}"/>'
        imghref = f'<a href="{self.siteweb}" target="_blank">{imgtag}</a>'
        return imghref

    def get_title(self):
        title = f'<a href="{self.siteweb}" target="_blank">{self.acronyme}</a>'
        if self.acronyme != self.nom:
            title += f' {self.nom}'
        return title

    def __str__(self):
        return self.acronyme
