# -*- coding: utf-8 -*-
"""
info.views
"""
import os
from datetime import timedelta
from django.utils import timezone
from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, TemplateView
from filer.models.filemodels import File
from info.models import WebLink
from msg.models import Msg
from config.settings import BASE_DIR
from accounts.formsearch import TxtSearchForm
from .formsearch import WebLinkSearchForm


def is_year(myvar):
    try:
        yyyy = int(myvar)
        if yyyy > 1990 and yyyy < 2050:
            return True
        else:
            return False
    except ValueError:
        return False


@login_required
def goaccess_list(request):
    path = os.path.join(BASE_DIR, 'templates', 'goaccess')
    year_list = [i for i in os.listdir(path)]
    year_list.sort()
    data = {}
    for year in year_list:
        if is_year(year):
            path_report = os.path.join(path, year)
            files = os.listdir(path_report)
            files.sort()
            report = {i[-12:][:7]: i for i in files if i.endswith(".html")}
            data[year] = report

    return render(
        request,
        'base_goaccess.html',
        {'title': 'Statistiques visites des CentresPilotes',
        'breadcrumb_display': 'Statistiques visites',
        'data': data
        }
    )


@login_required
def goaccess_report(request, month):
    report = request.GET.get('report', None)
    year = request.GET.get('year', None)
    if report is None:
        return render(request, '404.html')
    else:
        return render(
            request,
            'goaccess/{0}/{1}'.format(year, report)
        )


@login_required
def goaccess_report_year(request, year):
    return render(
        request,
        'goaccess/cpilote-{0}.html'.format(year)
    )


def accueil(request):
    cp_master_schema = File.objects.get(original_filename='cp-master-schema.png')
    dte6week = timezone.now()- timedelta(days=30)
    recent_msgs = Msg.objects.filter(msg_date__gte=dte6week).order_by('-msg_date')

    context = {
        'recent_msgs': recent_msgs,
        'cp_master_schema': cp_master_schema,
        }
    return render(request, 'info/accueil.html', context)


class WebLinkList(ListView):
    model = WebLink
    form = WebLinkSearchForm(None)
    template_name = "info/weblink_list.html"
    menucateg = 'autre'
    breadcrumb_display = ''
    url_name_list = ''
    totalrec = 0

    def __init__(self, menucateg='autre'):
        super().__init__()
        self.menucateg = menucateg
        if self.menucateg == 'outil':
            self.template_name = "info/outil_list.html"
        self.breadcrumb_display = ''.join([x[1] for x in self.model.CATEG_CHOICES if x[0]==self.menucateg])
        self.url_name_list = f'info:{self.menucateg}'

    def get_queryset(self):
        qs = self.model.objects.filter(menucateg=self.menucateg)
        self.totalrec = qs.count
        self.form = WebLinkSearchForm(self.request.GET)
        where = self.form.build_where()
        qs = qs.filter(where)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['breadcrumb_display'] = self.breadcrumb_display
        context['url_name_list'] = self.url_name_list
        context['totalrec'] = self.totalrec
        return context


class WebLinkDetail(DetailView):
    model = WebLink
    # template_name = 'modal/modal_info.html'


def weblink_detail(request):
    """
    Show detail
    """
    if request.method == "GET":
        wlid = request.GET.get("wlid")
    wl = WebLink.objects.get(pk=wlid)

    return render(
        request,
        "info/weblink_detail_modal.html",
        {"object": wl},
    )
