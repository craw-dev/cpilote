from django.apps import AppConfig


class InfoConfig(AppConfig):
    name = "info"
    app_label = "Info"
