# Generated by Django 3.1.6 on 2021-06-07 13:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0004_auto_20210528_1411'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='weblink',
            options={'ordering': ['nom'], 'verbose_name': 'Liens utile'},
        ),
        migrations.AlterField(
            model_name='weblink',
            name='domaine',
            field=models.CharField(blank=True, choices=[(None, '-- Domaine --'), ('10-ACC', 'Accompagnement'), ('20-BIO', 'Bio - Local'), ('30-CPS', 'Centre Pilote'), ('40-ENC', 'Encadrement'), ('50-REC', 'Recherche'), ('60-SPR', 'Province'), ('65-WAL', 'Wallonie'), ('70-AUT', 'Autres liens'), ('90-CAT', 'Catalogue'), ('94-DIA', 'Diagnostique'), ('99-AUT', 'Autre')], max_length=6, null=True, verbose_name='Domaine'),
        ),
    ]
