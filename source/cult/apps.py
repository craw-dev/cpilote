from django.apps import AppConfig


class CultConfig(AppConfig):
    name = 'cult'
    verbose_name = 'Cultures, Ravageurs & Maladies'
