# -*- coding: utf-8 -*-
"""
cult.models description
"""
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext as _
from filer.fields.image import FilerImageField
from django.db import models

__author__ = "openHBP"
__email__ = "p.houben@cra.wallonie.be"
__copyright__ = "Copyright 2020, Patrick Houben"
__license__ = "GPLv3"
__date__ = "2020-08-13"
__version__ = "2.0"
__status__ = "Development"


class Categorie(models.Model):
    """
    Catégories de ravageurs et maladies (chamignon, bactérie, gibier, vers, etc.)
    """

    type = models.CharField(max_length=8)
    nom = models.CharField(max_length=20)

    class Meta:
        app_label = "cult"

    def __str__(self):
        return "{0}-{1}".format(self.type, self.nom)


class Ram(models.Model):
    """
    Ravageurs et Maladies
    """

    IMG_POS = (("left", _("Gauche")), ("right", _("Droite")))
    code = models.CharField(
        max_length=255, blank=True, null=True, help_text=_("Nom latin")
    )
    nom = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text=_("Nom commun [lié à la culture]"),
    )
    categorie = models.ForeignKey(
        Categorie, on_delete=models.CASCADE, default=1, help_text=_("Categorie")
    )
    description = models.CharField(max_length=255, blank=True, null=True)
    conditions = models.TextField(
        blank=True, null=True, help_text=_("Conditions favorables au développement")
    )
    sources = models.TextField(
        blank=True, null=True, help_text=_("Sources de contamination")
    )

    class Meta:
        verbose_name = _("Ravageur")
        ordering = ['code']

    def __str__(self):
        return "{0} ({1})".format(self.code, self.nom)


class RamPhoto(models.Model):
    """
    Images illustratives des différents ravageurs et maladies
    """
    ram = models.ForeignKey(Ram, blank=True, null=True, on_delete=models.CASCADE,
            verbose_name=_('Ravageurs et Maladies'))
    ramillu = FilerImageField(null=True, blank=True, on_delete=models.CASCADE,
            related_name="img_ram", verbose_name=_('Image illustrative'))


class Culture(models.Model):
    """
    Table des cultures suivies par les centres pilotes
    """

    code = models.CharField(max_length=255, unique=True, blank=True, null=True)
    nom = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    ram = models.ManyToManyField(
        Ram, verbose_name="Ravageurs et maladies", through="Ram2Cult"
    )

    def __str__(self):
        return self.nom


class Ram2Cult(models.Model):
    ram = models.ForeignKey(Ram, on_delete=models.CASCADE, related_name="ram_of", verbose_name=_("Ravageur"))
    culture = models.ForeignKey(
        Culture, on_delete=models.CASCADE, related_name="from_culture"
    )
    symptome = models.TextField(
        blank=True, null=True, help_text=_("Indications pour identification")
    )

    class Meta:
        verbose_name = _("Ravageur culture")
        ordering = ['ram']


# class CentrePilote(models.Model):
#     """
#     Listing des centres pilotes
#     """
#     code = models.CharField(max_length=20, unique=True)
#     code_maj = models.CharField(max_length=20)
#     nom = models.CharField(max_length=255)
#     description = models.CharField(max_length=255)
#     is_active = models.BooleanField(_("Actif"), default=False)
#     culture = models.ManyToManyField(Culture)

#     class Meta:
#         verbose_name = _("Centre Pilote")
#         ordering = ['nom']

#     def __str__(self):
#         return "{0}".format(self.description)
