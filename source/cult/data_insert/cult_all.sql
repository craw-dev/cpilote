--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: cult_categorie; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.cult_categorie (id, type, nom) VALUES (1, 'MALADIE', 'Champignon');
INSERT INTO public.cult_categorie (id, type, nom) VALUES (2, 'MALADIE', 'Bactérie');
INSERT INTO public.cult_categorie (id, type, nom) VALUES (3, 'MALADIE', 'Virus');
INSERT INTO public.cult_categorie (id, type, nom) VALUES (4, 'RAVAGEUR', 'Insecte');
INSERT INTO public.cult_categorie (id, type, nom) VALUES (8, 'RAVAGEUR', 'Mollusque');
INSERT INTO public.cult_categorie (id, type, nom) VALUES (5, 'RAVAGEUR', 'Mammifère');
INSERT INTO public.cult_categorie (id, type, nom) VALUES (6, 'RAVAGEUR', 'Oiseau');
INSERT INTO public.cult_categorie (id, type, nom) VALUES (7, 'RAVAGEUR', 'Némathode');


--
-- Name: cult_categorie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.cult_categorie_id_seq', 8, true);


--
-- Data for Name: cult_culture; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.cult_culture (id, code, nom, description) VALUES (1, 'all', 'Toute culture', 'Toute culture');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (2, 'ail', 'ail', 'ail');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (3, 'ail_automne', 'ail d''automne', 'ail d''automne');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (4, 'asperge_verte', 'asperge verte', 'asperge verte');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (5, 'betterave', 'betterave', 'betterave');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (6, 'froment', 'froment', 'froment');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (7, 'buis', 'buis', 'buis');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (8, 'carotte', 'carotte', 'carotte');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (9, 'cerisier', 'cerisier', 'cerisier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (10, 'chicoree', 'chicorée', 'chicorée');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (11, 'chou', 'chou', 'chou');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (12, 'chou_de_bruxelles', 'choux de bruxelles', 'choux de bruxelles');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (13, 'chrysantheme', 'chrysanthème', 'chrysanthème');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (14, 'colza', 'colza', 'colza');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (15, 'courge', 'courge', 'courge');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (16, 'courgette', 'courgette', 'courgette');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (17, 'escourgeon', 'escourgeon', 'escourgeon');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (18, 'feve', 'fève', 'fève');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (19, 'fraisier', 'fraisier', 'fraisier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (20, 'framboisier', 'framboisier', 'framboisier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (21, 'groseillier', 'groseillier', 'groseillier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (22, 'laitue', 'laitue', 'laitue');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (23, 'mais', 'maïs', 'maïs');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (24, 'murier', 'mûrier', 'mûrier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (25, 'navet', 'navet', 'navet');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (26, 'oignon', 'oignon', 'oignon');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (27, 'orge', 'orge', 'orge');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (28, 'persil', 'persil', 'persil');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (29, 'poirier', 'poirier', 'poirier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (30, 'pois_feverole', 'pois et féverole', 'pois et féverole');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (31, 'pomme_de_terre', 'pomme de terre', 'pomme de terre');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (32, 'pommier', 'pommier', 'pommier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (33, 'prairie', 'prairie', 'prairie');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (34, 'prunier', 'prunier', 'prunier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (35, 'radis', 'radis', 'radis');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (36, 'sapin_de_noel', 'sapin de noël', 'sapin de noël');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (37, 'tomate', 'tomate', 'tomate');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (38, 'fourrage', 'fourrage', 'fourrage');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (39, 'ornement', 'plantes ornamentales', 'plantes ornamentales');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (40, 'abricotier', 'abricotier', 'abricotier');
INSERT INTO public.cult_culture (id, code, nom, description) VALUES (41, 'pecher', 'pêcher', 'pêcher');


--
-- Name: cult_culture_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.cult_culture_id_seq', 1, false);


--
-- Data for Name: cult_ram; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (64, 'drechslera tritici-repentis', 'helminthosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (76, 'fusarium', 'fusariose des épis', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (77, 'gaeumannomyces graminis tritici', 'piétin échaudage', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (105, 'mycosphaerella graminicola', 'septoriose du blé', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (110, 'oculimacula yallundae', 'piétin-vrese', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (73, 'erysiphe betae', 'oïdium', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (27, 'beet necrotic yellow vein virus', 'rhizomanie', NULL, ' ', 'Sol', 3);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (7, 'alternaria alternata', 'alternariose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (14, 'aphanomyces sp.', 'pied noir', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (119, 'peronospora farinosa', 'mildiou', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (39, 'calonectria henricotae (ex cylindrocladium)', 'calonectria', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (40, 'calonectria pseudonaviculata (ex cylindrocladium)', 'calonectria', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (29, 'blumeria graminis', 'oïdium du blé', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (95, 'limax', 'limace', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (15, 'aphidoidea', 'puceron ailé', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (17, 'aphis fabae', 'puceron noir de la fève', 'De couleur noir, ils font de 1,5 à 2,5 mm', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (107, 'myzus ascalonicus', 'puceron vert de l''échalotte', 'Aptère de 1,5 à 2 m, vert brunâtre. Cornicules légèrement renflées.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (108, 'myzus persicae', 'puceron vert du pêcher ', 'Vert clair de 1,2 à 1,5 mm de long', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (63, 'drechslera teres', 'helminthosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (74, 'erysiphe graminis', 'oïdium', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (144, 'puccinia simplex', 'rouille naine', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (157, 'rhynchosporium secalis', 'rhynchosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (160, 'sclerotinia sclerotorium', 'sclerotinia', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (32, 'botrytis cinerea', 'botrytis', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (111, 'oiseaux', 'oiseaux', ' ', ' ', ' ', 6);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (51, 'corneille', 'corneille', ' ', ' ', ' ', 6);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (127, 'pigeon', 'pigeon', ' ', ' ', ' ', 6);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (49, 'colletotrichum graminicola', 'anthracnose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (21, 'apodemus agrarius', 'mulot', 'Petit mammifère rongeur sauteur jusqu''à 80 cm et nageur.', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (8, 'alternaria cichorii', 'alternariose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (159, 'rouille', 'rouille', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (56, 'cylindrosporium concentricum', 'cylindrosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (130, 'podosphaera aphanis', 'oïdium', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (166, 'sphaerotheca mors-uvae', 'oïdium', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (34, 'bremia lactucae', 'mildiou', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (175, 'thrips', 'thrips', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (145, 'puccinia striiformis', 'rouille jaune', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (6, 'agriote lineatus', 'taupin des moissons (larve)', 'Coléoptère de forme allongée, à carapace dure et de couleur sombre, présentant de fines stries sur ses élytres et un thorax et une une tête couvertes de poils très fins et courts. Son corps mesure de 0,7 à 1 cm de long.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (136, 'pseudomonas syringae', 'pseudomonas', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (137, 'pseudomonas syringae', 'pseudomonas', NULL, ' ', ' ', 2);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (180, 'uromyces betae', 'rouille', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (163, 'sitobion avenae', 'puceron de l''épi', 'De 2 à 3 mm, allongé. Couleur: du vert au rouge en passant par le jaune. Il conolise le limbe des feuilles supérieures, puis se développe sur les épis dès leur sortie.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (124, 'phytomyza gymnostoma', 'mouche mineuse', 'Petites mouches grisâtres de 3 mm de long, avec une grosse tête jaune. Pattes foncées avec des articulation jaunâtres.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (52, 'crioceris asparagi', 'criocère', 'Ses élytres sont noires, marqués de taches rectangulaires claires plutôt jaunâtres, ses pattes et antennes sont noires. Le pronotum est cylindrique d''un brun rougeâtre.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (28, 'blaniulus guttulatus', 'blaniule', 'De 8 à 18 mm de long, de couleur blanchâtre, de chaque côté se trouve un rang de point orange rouge.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (47, 'collembola', 'collembole', 'Couleur parfois vive, plus souvent gris foncé, bleuté, blanchâtre, de petite taille 2 à 3 mm.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (83, 'helminthosporium turcicum', 'helminthosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (88, 'kabatiella zeae', 'kabatiellose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (9, 'alternaria solani', 'alternariose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (48, 'colletotrichum coccodes', 'dartrose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (82, 'helminthosporium solani', 'gale argentée', NULL, ' ', 'Conservation', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (24, 'arion hortensis', 'limace grise', 'Taille moyenne, de couleur gris-beige, tachetée chez l''adulte et violacée/rouge chez le jeune. Aspect fripé.', ' ', ' ', 8);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (61, 'deroceras reticulatum', 'limace noire', ' ', ' ', ' ', 8);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (78, 'globodera pallida', 'nématode à kyste', NULL, ' ', ' ', 7);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (79, 'globodera rostochiensis', 'nématode à kyste', NULL, ' ', ' ', 7);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (25, 'arvicola terrestris', 'campagnol terrestre', 'Entre 12,5 et 22 cm. Brun foncé.', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (102, 'microtus agrestis', 'campagnol agreste', 'Entre 8,5 et 2 cm. Gris-brun sur le dos et plus foncé sur le ventre.', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (103, 'microtus arvalis', 'campagnol des champs', 'Entre 8 et 12 cm de longueur. Dos brun, ventre blanc.', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (100, 'metopolophium dirhodum', 'pucerons', 'Adulte femelle aptère, de forme ovoïde et de couleur vert-jaunâtre clair, mesure de 1,8 à 2,8 mm. Le notum est marqué au centre d''une ligne verte foncée. ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (181, 'ustilago maydis', 'charbon commun', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (118, 'perenospora destructor', 'mildiou', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (148, 'ramularia collo-cygni', 'ramulariose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (173, 'talpa europea', 'taupe européenne', 'De 15 à 20 cm de long pour une masse de 60 à 140 g. Son corps est cylindrique. Couvert de fourrure faire de poils sombres (gris à noir).', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (141, 'puccinia allii', 'rouille', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (43, 'cercospora beticola', 'cercosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (147, 'ramularia betae', 'ramulariose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (152, 'rhizoctonia violacea', 'rhizoctone violet', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (143, 'puccinia recondita', 'rouille brune', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (142, 'puccinia horiana', 'rouille', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (120, 'phoma lingam', 'phoma', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (46, 'collectotrichum actuatum', 'collectotrichum acutatum', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (50, 'contarinia pyrivora', 'cécidomyie des poirettes', 'Entre 2 et 3 mm. Moucheron.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (22, 'archips rosana', 'tordeuse des buissons', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (168, 'stemphylium', 'stemphylium', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (146, 'puceron non ailé', 'puceron non ailé', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (16, 'aphidoidea ailé', 'puceron ailé', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (45, 'cicadella', 'cicadelles', 'De petite taille, de couleur blanchâtre, brune, beige ou verte.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (89, 'lapin', 'lapin', ' ', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (93, 'lepus', 'lièvre', ' ', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (90, 'léma', 'criocères', 'De 5 mm de long. De couleur bleu ou avec le thorax rouge. Elle ronge le parenchyme entre les nervures des feuilles au printemps en formant des plages décolorées.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (113, 'opomyza florum', 'mouche jaune', 'Attaque dans le semi-froment', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (164, 'sitodiplosis mosellana', 'cécidomyie orange', '2 à 3 mm fr longueur, jaune-orangée, longues pattes marron.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (176, 'thysanoptère', 'thrips', 'Taille minuscule, allongé et aux ailes bordées de soies longues et fine.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (138, 'psila rosae', 'mouche de la carotte', 'De couleur noirâtre à pattes jaunes et mesure 4 à 5 mm de long. La tête est noire avec des joues blanches. l''abdomen est allongé.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (162, 'semiaphis dauci', 'puceron de la carotte', 'De petite taille, de 1,3 à 1,6 mm avec une couleur bleu-vert. Sa tête et ses pattes et ses courtes cornicules sont de couleurs foncées sur un abdomen assez clair. Ses antennes sont courtes et sa cauda longue et légèrement colorée.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (65, 'drosophila suzukii', 'drosophile à ailes tachetées', 'Mouche de petite taille, 2 à 3 mm de long, aux yeux rouges, au thorax brun clair ou jaunâtre et avec des bandes transversales noires sur l''abdomen. Les antennes sont courtes et trapues avec une arista plumeuse.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (106, 'mysus cerasi', 'puceron noir du cerisier', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (149, 'rhagoletis cerasi', 'mouche de la cerise', 'Petite mouche de 5 mm de long, aux ailes transparentes ornées de taches sombres.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (59, 'delia platura', 'mouche des semis', 'Mouche grise de 3 à 6 mm de longueur', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (134, 'protrama radicus', 'puceron', 'De 2,8 à 3 mm, coloration très claire due à une pulvérulence blanche qui recouvre le corps, avec de légère bandes sombres.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (161, 'scutigerella immaculata', 'scutigérelle', 'De 6 à 8 mm de long, ovale et applati. Face dorsale forme un bouclier.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (177, 'tipula', 'tipule', 'Long asticot gris qui vit dans le sol où elle se nourrit de racines de plantes.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (10, 'alticinae', 'altise', 'Ils criblent les feuilles de petits trous plutôt ronds. Ils sont de couleurs foncée et lustrée et atteignent de 2 à 5 mm de longueur.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (129, 'plutella xylostella', 'teignes', 'Petit papillon de 12 à 17 mm d''envergure aux ailes en toit au repos. Ses ailes antérieures, de couleur marron, sont étroies avec une frange au bord postérieur.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (35, 'brevicoryne brassicae', 'puceron', 'De 2,1 à 2,6 mm de long, globuleux, vert, recouvert d''une pruinosité blanche. Le cauda triangulaire et les cornicules sont de taille égales.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (60, 'delia radicum', 'mouche du chou', 'Mouche grise de 6 à 8 mm marquée de taches noires.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (126, 'pieris brassicae', 'chenille', 'Papillon blanc marqué de noir au bord de l''apex des ailes antérieures. La longueur de l''aile varie de 28 à 33 mm. Elle est de couleur blanche et possède une grande tache apicale noire de forme concave', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (44, 'ceutorhynchus assimilis', 'charançon', 'De 2,5 à 3 mm. De couleurs: gris noir avec des stries dorsales. Provoque des blessures qui favorisent la ponte.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (98, 'meligethes aeneus', 'méligèthe', 'Il se nourrit de boutons floraux, du pollen. Il perce grâce à leur mandibules le bouton qui avortera par après et ne donnera pas de graine.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (139, 'psylliodes chrysocephalus', 'larves d''altises', 'De 4,5 à 5 mm. De couleur: bleu azure avec les pattes noires. Il se nourrit du colza à partir du stade juste semé jusqu''au stade plantule.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (154, 'rhopalosiphum padi', 'puceron', 'Forme globuleuse, corps vert fonccé, cornicules courtes, sombres et renflées à l''extrémité, avec des tâches rougeâtre autour de leur insertion.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (13, 'anthonomus rubi', 'anthonome', 'De 4,5 à 5 mm de long. Corps brun noirâtre recouvert d''une fine pubescence grise. Bande gris clair en forme de V sur la partie postérieures des élytres.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (75, 'frankliniella occidentalis', 'thrips', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (37, 'byturus urbanus', 'ver de framboise', 'Coléoptère de 5 mm de long, de couleur brune et recouvert de poils fins, jaune dorée. Sa tête plutôt grosse comparée au reste du corps, est ornée de deux antennes en forme de massue.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (4, 'agrilus sinuatus', 'bupreste', 'Entre 7 et 10 mm. Couleur cuivrée.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (12, 'anthonomus pyri', 'anthonome du poirier', 'Entre 4 et 5 mm. Brun. Bande transversale et ligne blanche sur le dos.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (38, 'cacopsylla pyri', 'psylle', 'Mesure de 1,5 à 6 mm et a une forme massive, ressemblant à de minuscules cigales aux antennes plus longues, à tête large avec antennes fines. Il y a deux lobes céphalliques saillants frontaux.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (55, 'cydia pyri', 'carpocapse', 'Papillon de 18 mm d''envergure dont les ailes antérieures sont grisâtres, avec aux extrémités une large tache brune bordée de lignes dorées. Les ailes postérieures sont brunes avec les bords ciliés. ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (58, 'dasineura pyri', 'cécidomyie des feuilles du poirier', 'Entre 1,5 et 2 mm. Blanc jaunâtre. Apode. Cocon dans le sol à partir de mi mai.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (70, 'eriophyes pyri', 'phytopte', 'Inférieur à 0,2 mm.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (85, 'hoplocampa brevis', 'hoplocampe', 'Entre 4 et 5 mm. Orange et noir.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (121, 'phyllobes', 'charançon', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (185, 'zeuzea pyrina', 'zeuzère', 'Papillon de 50 à 60 mm d''envergure chez la femelle; le mâle, plus petit, mesure de35 à 40 mm. Les ailes sont blanches, parsemées de taches bleuâtres. Le thorax blanc porte six taches bleues.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (165, 'sitona linaetus', 'sitone', 'De couleur gris verdâtre à brun rougeâtre, mesure de 4 à 5 mm de long. Le corps est orné de 3 discrètes lignes claires sur le thorax et les élytres.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (18, 'aphis frangulae', 'petit jaune ou puceron de la bourdaine', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (19, 'aphis nasturtii', 'petit jaune ou puceron du nerprun', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (26, 'aulacorthum solani', 'puceron tacheté de la pdt', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (69, 'epitrix', 'epitrix', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (92, 'leptinotarsa decemlineata', 'doryphore', 'Insecte de l''ordre des coléoptères et de la famille des chrysomélidés aux élytres jaunes rayés de noir.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (11, 'anthonomus pomorum', 'anthonome du pommier', 'Entre 4 et 5 mm. Brun noirâtre. Dessin en V et point blanc sur le dos.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (20, 'aphis pomi', 'puceron vert', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (36, 'bryobia', 'acarien brun', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (42, 'canicula', 'chenille', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (53, 'cydia pomonella', 'carpocapse', 'De 16 à 19 mm d''envrgure. Tache ovale, brune, bordée de 2 lignes d''un brun doré brillant, à reflets mordorés, sur ailes antérieures grises. Ailes postérieures brun rougeâtre.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (57, 'dasineura mali', 'cécidomyie des feuilles du pommier', 'Entre 1,5 et 2 mm. Blanc jaunâre puis ocre. Apode.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (66, 'dysaphis plantaginea', 'puceron cendré', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (67, 'dysaphis sp.', 'puceron des galles rouges', '2 mm. Globuleux. Ressemble au puceron cendré du pommier', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (71, 'eriosoma lanigerum', 'puceron lanigère', 'Les adultes sont de couleur marron et mesurent environ 2 mm de long. Recouverts d''un abondant amas cotonneux blancs. Les cornicules sont peu visibles.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (84, 'heyda nubiferana', 'tordeuse verte des bourgeons', 'Entre 17 à 21 mm. Brun, argenté et noir. Extrémité blanche.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (86, 'hoplocampa testudinea', 'hoplocampe', 'Mesure de 5 à 7 mm de long et ressemble à une guêpe et à une mouche. Tête jaune, noir brillant sur la face dorsale et brun jaunâtre sur la face ventrale.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (91, 'lepidosaphes ulmi', 'cochenille virgule', 'Le bouclier, brun clair ou foncé, mesure 2 à 3,5 mm de long; il est ovale, allongé et souvent courbé en forme de virgule. A la partie antérieure du bouclier, les exuvies rouge-brun s''empilent.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (96, 'lyonetia clerkella', 'mineuse sinueuse', 'Entre 8 et 9 mm. Blanc argenté.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (112, 'operophtera brumata', 'cheimatobie', 'Mâle: 25 mm. Ailes grises. Femelle: 8 à 10 mm. Ailes atrophiées.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (114, 'palomena prasina', 'punaise verte', 'Mesure de 12 à 14 mm. Sa couleur verte la rend très mimétique; elle devient brune à l''approche de l''hiver mais redevient verte au retour du printemps.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (115, 'pandemis heparana', 'pandemis', 'Entre 16 à 22 mm. Brun jaunâtre à brun roux.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (81, 'gymnosporangium sabinae', 'rouille grillagée', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (104, 'monilinia fructigena', 'monilioses', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (128, 'pleopora allii', 'stemphyliose ', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (174, 'taphrina deformans', 'cloque du pêcher', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (125, 'phytophthora infestans', 'mildiou', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (150, 'rhizoctonia solani', 'rhizoctone brun', NULL, 'Froid et humide', 'Sol et plants', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (132, 'potato virus x', 'virus x', NULL, ' ', ' ', 3);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (133, 'potato virusy', 'virus y', NULL, ' ', ' ', 3);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (117, 'pectobacterium carotovorum', 'pourriture molle', NULL, ' ', ' ', 2);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (135, 'pseudomonas aeruginosa', 'pseudomonas', NULL, ' ', ' ', 2);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (170, 'streptomyces sp.', 'gales communes', NULL, 'Sols légers, aérés, pH élevé', 'Sol', 2);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (72, 'erwinia amylovora', 'feu bactérien', NULL, ' ', ' ', 2);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (30, 'botryosphaeria obtusa', 'black rot', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (109, 'néonectria galligena', 'chancre européen', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (131, 'podosphaera leucotricha', 'oïdium', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (182, 'venturia inaequalis', 'tavelure', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (97, 'macrosiphum euphorbiae', 'puceron vert et rose de la pdt', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (62, 'drechslera', 'helminthosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (140, 'puccinia', 'rouille', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (158, 'rhyncosporium', 'rhynchosporiose', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (184, 'xanthomonas campestris', 'xanthomonas campestris', NULL, ' ', ' ', 2);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (101, 'microdochium nivale', 'pourriture des neiges', NULL, ' ', ' ', 1);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (80, 'grande tordeuse', 'grande tordeuse', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (178, 'tordeuse de l''automne', 'tordeuse de l''automne', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (1, 'acanthoscelides obtectus', 'bruche', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (116, 'panonychus ulmi', 'acarien rouge', '0,4 mm. Rouge brunâtre.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (122, 'phyllobius oblongus', 'phyllobes', 'Entre 4 et 8 mm. Brun.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (153, 'rhopalosiphum insertum', 'puceron vert migrant', 'Entre 1,5 et 2 mm. Vert clair. Tache noire sur chaque cornicule.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (156, 'rhynchites coeruleus', 'rhynchite coupe-bourgeon', 'De 2,5 à 3,5 mm. Bleu brillant, rostre aussi long que le prothorax. Antennes insérées au milieu du rostre et se terminant par une forte massue. Elytre rectangulaires fortement ponctués et recouverts d''une fine pubescence brune dressée', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (169, 'stigmella malella', 'mineuse élargie', '4 mm. Noir avec des bandes argentées.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (172, 'synanthedon myopaeformis', 'sésie', 'Papillon de 25 mm d''envergure, au corps bleu noirâtre marqué d''une bande rouge sur le premier segment du thorax?', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (179, 'typhlodromus pyri', 'araignée rouge', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (3, 'adoxophyes orana', 'tordeuse de la pelure', 'Papillon de 12 à 17 mm d''envergure avec des ailes antérieures présentant une alternance de bandes brunes et claires. Les ailes postérieures sont grises et argentées à franges blanchâtres.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (23, 'argyrotaenia ljungiana', 'eulia', 'Entre 12 à 17 mm. Alternance de bandes brunes et claires.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (41, 'campylomma verbasci', 'punaise de molene', 'Mesure 3 mm de long, est de forme ovale et d''une teinte allant du vert pâle au brun clair. Les antennes sont segmentées et les pattes postérieures présentent des taches noires et sont recouvertes d''épines.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (54, 'cydia prunivorana', 'tordeuse des fruits', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (68, 'enarmonia formosana', 'tordeuse de l''écorce', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (94, 'leucoptera scitella', 'mineuse cerclée', 'Petite chenille aplatie de 10 mm. Les papillons de 7 à 15 mm possèdent une importante frange alaire.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (123, 'phyllonorycter blancardella', 'mineuse marbrée', 'Petit papillon nocturne de 4 à 5 mm de longueur et dont l''evergure des ailes atteint de 7 à 8 mm. Des motifs blancs, noirs et or décorent ses ailes et renvoient des reflets irisés sous le soleil.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (155, 'rhynchites aequatus', 'rhynchite', 'Entre 2,5 et 4 mm. Thorax bronzé et élytres rouges ou jaunes.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (167, 'spilonota ocellana', 'tordeuse rouge', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (5, 'agriolimax reticulatus', 'limace', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (99, 'melolontha melolontha', 'hanneton', 'Il mesure de 20 à 30 mm de longueur et est facilement identifiable à son vol bruyant au coucher du soleil.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (33, 'brachycaudus helichrysi', 'puceron vert', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (87, 'hyalopteris pruni', 'puceron farineux', ' ', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (2, 'adelges piceae', 'puceron lanigère', 'Ils sont minuscules, la femelle est noire et les œufs et les larves sont rosés.', ' ', ' ', 4);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (171, 'sus scrofa', 'sanglier', 'Peut peser de 150 à 160 kg pour le mâle et 100 kg pour la femelle. Sa longueur varie de 1,10 m à 1,80 m. Corps trapu et tête volumineuse.', ' ', ' ', 5);
INSERT INTO public.cult_ram (id, code, nom, description, conditions, sources, categorie_id) VALUES (183, 'venturia pirina', 'tavelure', '', ' ', ' ', 1);


--
-- Data for Name: cult_ram2cult; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (1, 'Taches foncées avec un point noir au centre et entrouées d''un halo jaunâtre', 6, 64);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (2, 'Epillets roses-orangés et la glume noirâtre, brunissement sur col de l''épi', 6, 76);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (3, 'Epis blanchâtre, dessèchés et vides, racines et la base de la tige sont noires et nécrosées', 6, 77);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (4, 'Plages aqueuses pouvant devenir brunes et nécrotiques, pyncides observées', 6, 105);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (5, 'Taches ocellées au bord brunâtre et diffus, stromas visibles dans les gaines', 6, 110);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (6, 'Urédospores allongées jaunes-oragnes et alignées entres les nervures des feuilles', 6, 145);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (7, 'Feutrage blanchâtre (en juillet) et en fin de saison fructifications noirâtres', 5, 73);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (8, 'Jaunissement des nervures des feuilles', 5, 27);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (9, 'Au départ les taches ont une taille d''une tête d''épingle puis s''agrandissent jusqu''à devenir de grosses lésions brun foncé à noires.', 5, 7);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (10, 'Tiges et racines brunissent', 5, 14);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (11, 'Pénètre généralement la plante par les jeunes feuilles du collet. Premiers symptômes sont la chlorose et la déformation des jeunes feuilles.', 5, 119);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (12, 'Petites taches brunes noirâtres  bien délimitées apparaissent de façon dispersées sur les feuilles. Entouré d''un liseré jaune pâle, le centre des taches devient nécrotique et se désagrège.', 5, 137);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (13, 'Petites taches brunes noirâtres  bien délimitées apparaissent de façon dispersées sur les feuilles. Entouré d''un liseré jaune pâle, le centre des taches devient nécrotique et se désagrège.', 5, 136);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (14, 'Pustules oranges visibles', 5, 180);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (15, 'Taches claires se forment sur jeunes feuilles, entourées de tissus de couleur brun rougeâtre, avec l''évolution de la maladie les taches deviennent plus foncées', 7, 39);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (16, 'Taches claires se forment sur jeunes feuilles, entourées de tissus de couleur brun rougeâtre, avec l''évolution de la maladie les taches deviennent plus foncées', 7, 40);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (17, 'Champignon sur la tige', 23, 76);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (18, 'Pustules blanchâtres, passe d''une apparence poudreuse à une couleur grise ou brune', 6, 29);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (19, 'Taches brunes suivant les nervures de la feuille', 17, 63);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (20, 'Feutrage blanc envahit la surface de la feuille ensuite le feutrage devient gris et se parsème de points noirs', 17, 74);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (21, 'Pustules orangées sous les feuilles, halos chlorotiques autour des pustules', 17, 144);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (22, 'Taches irrégulières avec le centre plus clair et un contour brun', 17, 157);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (23, 'Taches blanchâtres sur la tige, sclérotes observables, blanchissement au niveau des silliques', 14, 160);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (24, ' ', 20, 32);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (25, 'Se développe souvent sur les fruits. Feutrage gris ou brunâtre.', 21, 32);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (26, 'Au niveau des feuilles, brulures sous forme de petites lésions ovales et brunes entourées d''un halo brunâtre, stries noires sur la tige', 23, 49);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (27, 'Taches d''abord petites et humides, prenant progressivement une forme circulaire à ovale. Motifs concentriques sont visibles sur les taches. Les tisssus centraux, une fois nécrosés, s''éclaircissent.', 10, 8);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (28, ' ', 10, 74);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (29, ' ', 10, 159);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (30, 'Taches vertes claires avec des acervoles blanches, taches beiges sur tiges, taches grisâtres et liégeuses sur silliques', 14, 56);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (31, ' ', 19, 130);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (32, ' ', 21, 166);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (33, 'Décoloration du feuillage, devient jaunâtre, peut apparaître des taches blanches, duveteuse', 22, 34);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (34, 'Apparition d''une sorte de moisissure ou d''un léger duvet blanc', 16, 29);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (35, 'Taches brunâtres à grisâtres, feuille apparait complétement dessechées', 23, 83);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (36, 'Taches grisâtres au centre et un halo poupre visible sur les feuilles', 23, 88);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (37, 'Tache foncées avec point noir au centre et entourée d''un halo jaunâtre pouvant se nécroser', 27, 63);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (38, 'Taches nécrotiques brunes de taille variable, avec des anneaux concentrique sur les taches importantes.', 31, 9);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (39, 'Plages gris clair à gris brun avec ponctuations noires assez grosses', 31, 48);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (40, 'Plaques argentées couvertes de fines ponctuations noires', 31, 82);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (41, ' ', 24, 32);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (42, 'Taches brunes-noirâtres sur le centre', 29, 137);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (43, 'Taches brunes-noirâtres sur le centre', 29, 136);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (44, 'Foyers circulaires à végétation chétive', 31, 78);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (45, 'Foyers circulaires à végétation chétive', 31, 79);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (46, 'Développement tumeurs charbonneuses grisâtres au niveau des tiges et/ou épis', 23, 181);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (47, 'Décoloration du feuillage, nuance jaune, située au 2/3 supérieur de la feuille', 26, 118);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (48, 'Taches rectangulaires brunes entourée d''un halo jaune observable de deux cotés du limbe', 27, 148);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (49, 'Renflements brunâtres sur lesquels se forment des cônes rougeâtres.', 29, 81);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (50, 'Pourritures brunes à partir d''une blessure. Cercles concentrique. Momification.', 29, 104);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (51, 'Taches circulaires brunes de petites tailles puis larges nécroses noires', 29, 128);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (52, 'Présence de boursoufflures et de zones contaminées. Attaques exceptionnelles.', 29, 174);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (53, 'Croûtes liégeuses noirâtres plus ou moins crevassées.', 29, 183);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (54, 'Taches claires à bord violacé au niveau de l''épis, des glumes précédant le blanchissement de l''épillet et le dessèchement complet de l''épi', 27, 76);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (55, 'Provoque des pourritures sèches.', 31, 76);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (56, 'Des taches brunâtres se forment par endroit sur les feuilles, elles finissent par brunir totalement et à tomber.', 31, 125);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (58, 'Sclérotes noires sur l''épiderme, nécroses brunes à la base des tiges, tubercules aériens', 31, 150);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (59, 'Apparition de mosaïques limitées par les nervures sans déformation de feuillage.', 31, 132);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (60, 'Taches nécrotiques noires au niveau des nervures à la face intérieure des feuilles. Les feuilles sont cassantes et se dess!chent en restant attachées à la plante.', 31, 133);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (61, 'Le champignon attaque le bois qui est en contact avec la terre et l''eau.', 31, 117);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (62, 'Taches foncée et des nécroses en formes et dimensions irrégulières, de couleur grise dans le centre, les bords des feuilles brunissent et dessèchent', 31, 135);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (63, 'Taches liégeuses superficielles (gale plate), cratères (gale en pustule)', 31, 170);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (57, 'Sclérotes noires sur l''épiderme, nécroses brunes à la base des tiges, tubercules aériens', 31, 150);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (64, 'A la suite de l''infection les fleurs et les feuilles flétrissent et noircissent. La pointe herbacée des jeunes rameau infectés se recourbe en forme de crosse. Un chancre peut se développer sur l''écorce.', 32, 72);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (65, 'Taches violettes. Jusque 5 mm.', 32, 30);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (66, 'Pourritures brunes à partir d''une blessure. Cercles concentrique. Momification.', 32, 104);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (67, 'Pourriture circulaire ou interne. En atmosphère humide, appartition de masses blanchâtres.', 32, 109);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (68, 'Couleur gris sale, les fueilles se couvrent d''un feutrage blanchâtre, les bourgeons ont un aspect ébouriffé', 32, 131);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (69, 'Taches translucides puis brun olivâtre.', 32, 182);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (70, 'C''est un genre de champignons ascomycètes de la famille des Pleosporaceae.', 33, 62);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (71, 'C''est un genre de champignons basidiomycètes de la famille des Pucciniaceae. Des plantes affaiblies ou en fin de vie, ou poussant dans un milieu inadapté, y sont généralement beaucoup plus sensibles.', 33, 140);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (72, 'Les symptômes apparaissent sur les feuilles et les gaines sous la forme de taches ovales verdâtres entourées d''un bord brun foncé. Le centre des taches s''éclaircit, se déssèche pour devenir blanc, sec et cassant pendant que la lisière brune se renforce.', 33, 158);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (73, 'Cette bactérie phytopathogène est responsable de diverses maladies des plantes.', 33, 184);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (74, 'Le mycélium peut coloniser la surface des feuilles presque compltètement, notamment la face supérieure des feuilles. Les fructifications des cléistothèces sont brun foncés, globuleuses avec des asques filamenteuses.', 33, 29);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (75, 'Se présente soit sous forme de petites taches blanches, claires au centre avec une bordure brune, large et bien définie, soit sous forme de taches plus larges, ovales, verdâtres à brunâtres, assez humides.', 33, 101);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (76, 'Pustules brunes-orangées localisées le long des nervures, provoquant un dessèchement prématuré des feuilles', 2, 141);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (77, 'Tache arrondies, grisâtres, liseré rougeâtre (peut provoquer un dessèchement', 5, 43);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (78, 'Tache angulaires brunes claires avec bordures plus foncées pouvant entrainer un dessèchement', 5, 147);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (80, 'Flétrissement du feuillage, flétrissement du collet et/ou pourriture sèche brunâtre des racines', 5, 150);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (81, 'Flétrissement par plages, racines présentent pourritures sèches violacées', 5, 152);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (82, 'Pustules brunes-orangées dispersées sur la feuille', 6, 143);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (83, ' ', 13, 142);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (84, 'Taches grises cendrées, points noires, nécrose au niveau du collet', 14, 120);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (85, ' ', 21, 46);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (86, 'Pustules blanchâtres, passe d''une apparence poudreuse à une couleur grise ou brune', 27, 29);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (87, 'Taches de couleur beige avec des cercles concentriques, les taches sont souvent entournées d''un halo jaune', 37, 32);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (88, 'Lésions foliaires verdâtres, évoluant vers une couleur brun pâle entourée d''une bordure brun foncée pouvant provoquer un jaunissement et un dessèchement complet du limbe', 27, 157);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (89, ' ', 32, 168);
INSERT INTO public.cult_ram2cult (id, symptome, culture_id, ram_id) VALUES (79, 'Flétrissement du feuillage, flétrissement du collet et/ou pourriture sèche brunâtre des racines', 5, 150);


--
-- Name: cult_ram2cult_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.cult_ram2cult_id_seq', 89, true);


--
-- Name: cult_ram_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.cult_ram_id_seq', 185, true);


--
-- Data for Name: cult_ramphoto; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (2, 164, 36);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (3, 185, 116);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (4, 186, 6);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (5, 188, 13);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (6, 189, 12);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (7, 190, 11);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (8, 191, 1);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (9, 192, 4);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (10, 195, 44);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (11, 199, 52);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (12, 201, 69);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (13, 203, 99);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (14, 204, 6);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (15, 206, 122);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (16, 207, 139);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (17, 208, 155);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (18, 209, 156);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (19, 210, 165);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (20, 212, 37);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (21, 213, 38);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (22, 214, 58);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (23, 216, 65);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (24, 217, 124);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (25, 218, 149);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (26, 219, 60);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (27, 220, 138);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (28, 225, 45);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (29, 226, 97);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (30, 227, 100);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (31, 227, 100);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (32, 228, 15);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (33, 229, 154);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (34, 230, 15);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (35, 230, 15);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (36, 231, 2);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (37, 232, 35);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (38, 233, 163);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (39, 234, 100);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (40, 235, 87);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (41, 236, 17);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (42, 237, 67);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (43, 238, 106);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (44, 240, 107);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (45, 240, 107);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (46, 241, 108);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (47, 241, 108);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (48, 243, 41);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (49, 244, 114);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (50, 244, 20);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (51, 246, 33);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (52, 247, 154);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (53, 247, 154);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (54, 248, 153);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (55, 249, 18);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (56, 250, 162);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (57, 251, 19);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (58, 252, 26);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (59, 253, 108);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (60, 254, 17);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (61, 259, 86);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (62, 260, 85);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (63, 261, 42);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (64, 262, 53);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (65, 262, 55);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (66, 263, 126);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (67, 264, 84);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (68, 266, 94);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (69, 267, 169);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (70, 268, 96);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (71, 269, 112);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (72, 270, 115);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (73, 272, 172);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (74, 273, 3);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (75, 274, 68);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (76, 275, 22);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (77, 276, 185);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (78, 277, 167);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (79, 278, 54);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (80, 279, 23);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (81, 282, 75);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (82, 283, 25);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (83, 284, 103);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (84, 284, 103);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (85, 285, 25);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (86, 285, 25);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (87, 286, 103);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (88, 287, 171);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (89, 288, 173);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (90, 289, 102);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (91, 289, 102);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (92, 290, 5);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (93, 292, 121);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (94, 293, 129);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (95, 294, 70);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (96, 295, 70);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (97, 296, 7);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (98, 297, 29);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (99, 298, 49);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (100, 299, 29);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (101, 300, 30);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (102, 301, 32);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (103, 302, 174);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (104, 303, 46);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (105, 304, 40);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (106, 304, 39);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (107, 305, 57);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (108, 306, 62);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (109, 307, 63);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (110, 308, 76);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (111, 309, 83);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (112, 310, 88);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (113, 311, 119);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (114, 312, 34);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (115, 313, 104);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (116, 314, 130);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (117, 316, 74);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (118, 317, 101);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (119, 318, 137);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (120, 318, 136);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (121, 319, 14);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (122, 320, 135);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (123, 321, 137);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (124, 321, 136);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (125, 322, 142);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (126, 323, 140);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (127, 324, 150);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (128, 325, 147);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (129, 326, 157);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (130, 327, 27);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (131, 328, 158);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (132, 329, 141);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (133, 330, 144);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (134, 332, 183);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (135, 333, 184);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (136, 335, 43);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (137, 336, 76);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (138, 337, 63);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (139, 338, 120);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (140, 339, 148);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (141, 340, 157);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (142, 342, 150);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (143, 343, 152);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (144, 344, 168);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (145, 344, 128);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (146, 345, 182);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (147, 346, 166);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (148, 347, 29);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (149, 348, 32);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (150, 348, 32);
INSERT INTO public.cult_ramphoto (id, ramillu_id, ram_id) VALUES (151, 349, 104);


--
-- Name: cult_ramphoto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.cult_ramphoto_id_seq', 151, true);


--
-- PostgreSQL database dump complete
--

