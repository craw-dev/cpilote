INSERT INTO cult_categorie (type,nom) VALUES ('MALADIE','Champignon');
INSERT INTO cult_categorie (type,nom) VALUES ('MALADIE','Bactérie');
INSERT INTO cult_categorie (type,nom) VALUES ('MALADIE','Virus');
INSERT INTO cult_categorie (type,nom) VALUES ('RAVAGEUR','Insecte');
INSERT INTO cult_categorie (type,nom) VALUES ('RAVAGEUR','Mammifère');
INSERT INTO cult_categorie (type,nom) VALUES ('RAVAGEUR','Oiseau');
INSERT INTO cult_categorie (type,nom) VALUES ('RAVAGEUR','Némathode');
INSERT INTO cult_categorie (type,nom) VALUES ('RAVAGEUR','Mollusque');