# -*- coding: utf-8 -*-
"""
Created on 19/08/2020
@author: p.houben
"""
from django.urls import path
from . import views


app_name = "cult"

# urlpatterns = [
#     path('', views.CPListView.as_view(), name='cp_list'),
#     path('centrepilote/<pk>', views.CPDetailView.as_view(), name='cp_detail'),
# ]
