# -*- coding: utf-8 -*-
"""
cult.admin description
"""
from django.contrib import admin
from .models import Categorie, RamPhoto, Culture, Ram, Ram2Cult

admin.AdminSite.site_header = "Gestion des cultures"

class RamPhotoAdmin(admin.TabularInline):
    search_fields = ['photo']
    model = RamPhoto
    extra = 0


class RamAdmin(admin.ModelAdmin):
    list_display = ('code', 'nom', 'categorie', 'description')
    ordering = ('code',)
    search_fields = ('nom', 'culture__nom', 'code')
    list_filter = ('categorie',)    
    inlines = [RamPhotoAdmin]
    
    def ram_code(self, obj):
        return obj.ram.code

    ram_code.admin_order_field = 'ram__code'    


class CultAdmin(admin.ModelAdmin):
    list_display = ('code', 'nom', 'description')
    ordering = ('code',)
    search_fields = ('code', 'nom', 'description')  
    filter_vertical = ('ram',)


class Ram2CultAdmin(admin.ModelAdmin):
    list_display = ('ram', 'culture', 'symptome')
    list_filter = ('culture',)
    search_fields = ('ram', 'culture', 'symptome')


admin.site.register(Categorie)
admin.site.register(Ram, RamAdmin)
admin.site.register(Culture, CultAdmin)
admin.site.register(Ram2Cult, Ram2CultAdmin)

