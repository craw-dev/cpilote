# -*- coding: utf-8 -*-
"""
msg.models description
"""
from ckeditor.fields import RichTextField
from filer.fields.image import FilerImageField

from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse
# from cms.admin.placeholderadmin import PlaceholderAdminMixin

from cms.models.fields import PlaceholderField
from accounts.models import ListeEnvoi, CentrePilote
# from cult.models import Culture, Ram

__author__ = "openHBP"
__email__ = "p.houben@cra.wallonie.be"
__copyright__ = "Copyright 2020, Patrick Houben"
__license__ = "GPLv3"
__date__ = "2020-10-13"
__version__ = "1.0.3"
__statut__ = "Development"


# Keep for migration history reason (presence in initial_0001)
class MyPlaceholderField(PlaceholderField):
    pass

class Categorie(models.Model):
    """
    Catégories de message
    event: Evénement
    comm: Communication
    avert: Avertissement
    """
    code = models.CharField(max_length=5, default="avert")
    nom = models.CharField(max_length=15, default="avertissement")
    icone = models.CharField(max_length=30, blank=True, null=True)
    couleur = models.CharField(max_length=7, blank=True, null=True)

    class Meta:
        app_label = "msg"
        ordering = ["nom"]

    def __str__(self):
        return "{0}".format(self.nom)


class Msg(models.Model):
    """
    Message à envoyer aux abonnés
    """
    MSG_STATUTS = (
        ("draft", _("Brouillon")),
        ("publie", _("Publié")),
        ("envoye", _("Envoyé")),
        # ("archive", _("archivé")),
    )

    centrepilote = models.ForeignKey(CentrePilote, on_delete=models.CASCADE)
    msg_created = models.DateTimeField(_("Créé le"), default=timezone.now)
    msg_date = models.DateTimeField(_("Envoyé le"), default=timezone.now)
    title = models.CharField(_("Sujet de l'E-mail"), max_length=255,
        help_text = "Le sujet est toujours précédé de 3 items: nom centre pilote - nom liste d'envoi - date d'envoi. Il est donc inutile de les ajouter au sujet.")
    listeenvoi = models.ForeignKey(ListeEnvoi, on_delete=models.CASCADE, verbose_name=_("Liste d'envoi"))
    summary_img = FilerImageField(
        related_name="summary_img", on_delete=models.PROTECT, 
        null=True, blank=True, verbose_name=_("Image d'accroche"),
        help_text=_("Sans image, le logo du centre pilote sera affiché.")
        )
    summary_txt = RichTextField(_("Bref résumé"),
        blank=True, null=True,
        help_text=_("Sans résumé, les différents sous-titres du message seront affichés.")
        )
    # RichTextField(_("Détail"), blank=True, null=True)
    statut = models.CharField(
        _("Statut"),
        max_length=10,
        choices=MSG_STATUTS,
        default="draft",
        help_text=_("Remettre en publié pour pouvoir réenvoyer l'avertissement."),
    )
    msg_nextdate = models.DateField(_("Date du prochain avis"), blank=True, null=True,
        help_text=_("Ne rien saisir pour afficher 'Dernier avis de la saison' dans l'email d'avertissement."))
    display_nextdate = models.BooleanField(_("Afficher la date du prochain avis"), default=False,
        help_text=_("Cochez cette case pour afficher la date prévue du prochain avis dans l'email d'avertissement."))

    class Meta:
        app_label = "msg"
        verbose_name = _("Avertissement")
        #ordering = ["-id"] # pour que ça fonctionne avec prev et next btn
        ordering = ["-msg_date"]

    def get_msg_date(self):
        if self.msg_date is None:
            msg_date = self.msg_created.strftime('%d.%m.%Y')
        else:
            msg_date = self.msg_date.strftime('%d.%m.%Y')
        return msg_date

    def get_title_stamp_ext(self):
        if self.id > 156:
            return self.get_title()
        else:
            return self.title

    def get_statut(self):
        """
        Return statut text
        """
        statut_title = self.get_statut_display().capitalize()
        if self.msg_date is not None:
            statut_date = self.msg_date.strftime('%d.%m.%Y')
        else:
            statut_date = self.msg_created.strftime('%d.%m.%Y')
        retstr = f"{statut_title} le {statut_date}"
        if self.statut == "draft":
            retstr = f"{statut_title} du {statut_date}"
        return retstr

    def display_placeholder(self):
        """
        Flag pour changement de version en octobre avec msg_id = 164
        return True or False
        """
        if int(self.id) > 163:
            return True
        else:
            return False

    def get_img200(self):
        if self.summary_img is not None:
            options = {'size': (200, 150)}
            summary_img_url = self.summary_img.file.get_thumbnail(options).url
            return summary_img_url
        else:
            return None

    def get_img300(self):
        if self.summary_img is not None:
            options = {'size': (300, 225), 'crop': True}
            summary_img_url = self.summary_img.file.get_thumbnail(options).url
            return summary_img_url
        else:
            return None

    def get_img400(self):
        if self.summary_img is not None:
            # if self.summary_img.width > self.summary_img.height:
            options = {'size': (400, 300), 'crop': True}
            # else:
            #     options = {'size': (300, 400), 'crop': True, 'upscale': True}
            summary_img_url = self.summary_img.file.get_thumbnail(options).url
            return summary_img_url
        else:
            return None

    def get_absolute_url(self):
        return reverse('msg:msg_consult', args=[str(self.id)])

    def get_title(self):
        return f"{self.centrepilote} {self.listeenvoi} {self.get_msg_date()}"

    def __str__(self):
        return self.get_title()

    # def __str__(self):
    #     return "{}".format(self.title)


class MsgDet(models.Model):
    """
    Détail du message
    """
    msg = models.ForeignKey(Msg, on_delete=models.CASCADE, verbose_name=_("Titre"))
    title = models.CharField(_("Sous-titre"), max_length=255)
    short_text2 = PlaceholderField("msgdet_slot")
    short_text = RichTextField(_("Détail"), blank=True, null=True)
    categorie = models.ForeignKey(
        Categorie, on_delete=models.CASCADE, help_text=_("Avertissement/Communication/Événement"),
        verbose_name=_("Categorie")
    )
    # culture = models.ManyToManyField(Culture, blank=True, verbose_name=_("Cultures"))
    # ram = models.ManyToManyField(
    #     Ram, blank=True, verbose_name=_("Ravageurs et maladies"))
    class Meta:
        app_label = "msg"
        verbose_name = _("Détail Avertissement")
        ordering = ["categorie", "title"]

    def get_absolute_url(self):
        return reverse('msg:msgdet_consult', args=[str(self.msg_id), str(self.id)])

    def get_cultures(self):
        return ", ".join(i.nom for i in self.culture.all())

    def get_rams(self):
        return ", ".join(i.nom for i in self.ram.all())

    def get_title(self):
        msg_date = self.msg.get_msg_date()
        return f"Détail Avertissement du {msg_date}"

    def __str__(self):
        return self.get_title()
