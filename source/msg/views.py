# -*- coding: utf-8 -*-
"""
msg.views description
"""
__author__ = 'hbp'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2017, Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2017-10-17'
__version__ = '1.2'
__status__ = 'Development'

import os
import logging

from next_prev import next_in_order, prev_in_order

# from braces.views import LoginRequiredMixin
from django.http import HttpResponse
from django.db.models import Q
from django.urls import resolve, reverse
from django.views import generic
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib import messages
from django.utils import translation, timezone
from django.utils.translation import ugettext_lazy as _
from cms.utils.copy_plugins import copy_plugins_to
from cms.models.pluginmodel import CMSPlugin

from config import settings
from .models import Msg, MsgDet, Categorie
from .forms import MsgForm, MsgDetForm, MsgDetFormSet, MsgSearch
from accounts.models import Inscription, CentrePilote, ListeEnvoi

from .utils import SendEmailImages, get_email_list, rm_last_digit, msg_clean


LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")
LOG_INFO = logging.getLogger("msg_info_send")


def user_is_manager(user):
    """
    L'utilisateur est-il gestionnaire d'un cp?
    Oui => return true
    """
    if user.is_authenticated and user.is_manager:
        return True
    else:
        return False


class MsgListView(generic.ListView):
    model = Msg
    form = MsgSearch
    listeenvoi_code = ''
    listeenvoi_nom = ''
    listeenvoi = None
    url_name = ''
    totalrec = 0

    def __init__(self, listeenvoi_code='NADA'):
        super().__init__()
        self.url_name_list = 'msg:msg'
        if listeenvoi_code != 'NADA':
            self.listeenvoi = ListeEnvoi.objects.get(code=listeenvoi_code)
            self.url_name_list = f'msg:{listeenvoi_code}'
            self.listeenvoi_nom = self.listeenvoi.nom

    def get_queryset(self):
        qs = Msg.objects.exclude(statut__in=('archive','draft'))
        if user_is_manager(self.request.user):
            cp_others = CentrePilote.objects.exclude(id=self.request.user.manage_cp.id).values_list('id', flat=True)
            qs = Msg.objects.exclude(centrepilote__id__in=cp_others, statut__in=('archive','draft'))
        if self.listeenvoi is not None:
            qs = qs.filter(listeenvoi=self.listeenvoi)

        self.totalrec = qs.count
        self.form = MsgSearch(self.request.GET, queryset=qs)
        qs = self.form.get_queryset(self.request.GET)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form
        context['url_name_list'] = self.url_name_list
        context['listeenvoi'] = self.listeenvoi_nom
        context['totalrec'] = self.totalrec
        context['title'] = _("Avertissements")
        
        return context


class MsgDetailView(generic.DetailView):
    model = Msg

    def build_next_prev(self):
        """
        create first, prev, next, last context variables
        based on appropriate qs (same as ListView)
        Msg = self.object.__class__
        """
        context = {}
        
        if self.request.user.is_authenticated and user_is_manager(self.request.user):
            cp_others = CentrePilote.objects.exclude(id=self.request.user.manage_cp.id).values_list('id', flat=True)
            qs = self.model.objects.exclude(centrepilote__id__in=cp_others, statut__in=('archive','draft'))
        else:
            qs = self.model.objects.exclude(statut__in=('archive','draft'))

        if "msg" not in self.request.path:
            qs = qs.filter(listeenvoi=self.object.listeenvoi)

        last_obj = qs.first()
        context['last'] = last_obj
        # LOG_DEBUG.debug(f"last_obj.id={last_obj.id}")
        # LOG_DEBUG.debug(f"self.object.id={self.object.id}")
        if self.object.id == last_obj.id:
            context['next'] = last_obj
            # LOG_DEBUG.debug(f"next={context['next']}")
        else:
            context['next'] = prev_in_order(self.object, qs=qs)
            # LOG_DEBUG.debug(f"prev_in_order={context['next']}")

        first_obj = qs.last()
        context['first'] = first_obj
        # LOG_DEBUG.debug(f"first_obj.id={first_obj.id}")
        
        if self.object.id == first_obj.id:
            context['prev'] = first_obj
            # LOG_DEBUG.debug(f"prev={context['prev']}")
        else:
            context['prev'] = next_in_order(self.object, qs=qs)
            # LOG_DEBUG.debug(f"next_in_order={context['prev']}")
        return context
    
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ctx = self.build_next_prev()
        context.update(ctx)
        context['msg_id'] = self.object.id
        context['msg_date'] = self.object.msg_date
        context['msgdet_count'] = self.object.msgdet_set.count()
        context['title'] = f"{self.object.get_title()}"
        context['breadcrumb_display'] = context['title']
        context['categorie'] = Categorie.objects.all()

        root_url = self.request.path_info
        lpos = root_url.rfind('/', 0) + 1
        context['url_name_list'] = str(root_url[:lpos-1])

        # if 'msg' in self.request.path:
        #     context['url_name_list'] = 'msg:msg'
        # else:
        #     context['url_name_list'] = f'{self.object.listeenvoi.code}:{self.object.listeenvoi.code}'           

        if user_is_manager(self.request.user):
            confirm_msg = ""
            confirm_url = ""
            context['btn_type'] = "btn-success2"
            if "publier_confirm" in self.request.path_info:
                confirm_msg = _("Cliquer sur confirmer pour publier le message qui sera alors visible par tous sur cette plateforme.")
                confirm_url = reverse("msg:publierok", args=[str(self.object.id)])
            if "envoyer_confirm" in self.request.path_info:
                listeenvoi_nom = str(self.object.listeenvoi.nom)
                confirm_msg = _("Cliquer sur confirmer pour envoyer le message aux personnes inscrite à la liste d'envoi ")
                confirm_url = reverse("msg:envoyerok", args=[str(self.object.id)])
            if "copier_confirm" in self.request.path_info:
                confirm_msg = _("Cliquer sur confirmer pour copier le message ci-dessous. COPIE sera ajouté au titre.")
                confirm_url = reverse("msg:copierok", args=[str(self.object.id)])
                context['btn_type'] = "btn-info2"
            if "del_confirm" in self.request.path_info:
                confirm_msg = _("Cliquer sur confirmer pour supprimer ce message dans son intégralité.")
                confirm_url = reverse("msg:supprimerok", args=[str(self.object.id)])
                context['btn_type'] = "btn-warning2"                
            context['confirm_msg'] = confirm_msg
            context['confirm_url'] = confirm_url                
        return context


@login_required
@user_passes_test(user_is_manager)
def msg_delete(request, msg_id):
    msg = Msg.objects.get(id=msg_id)
    msg.delete()
    messages.success(request, "Message supprimé.")
    # url_path = rm_last_digit(request.path.replace('/del/', ''))
    return redirect("msg:msg_list")

# def msg_publier(request, msg_id):
#     msg = Msg.objects.get(id=msg_id)
#     result = msg_clean(from_id=msg.id)
#     msg.statut = 'publie'
#     msg.msg_date = timezone.now()
#     msg.save()
#     messages.success(request, "Message publié.")
#     return redirect(msg.get_absolute_url())

# class MsgDelView(LoginRequiredMixin, generic.DeleteView):
#     model = Msg
#     template_name = "odk/xform_detail.html"
#     confirm_message = "Supprimer ce formulaire?"
#     confirm_url = reverse_lazy("odk:xform_list")
#     success_url = reverse_lazy("odk:xform_list")


@login_required
@user_passes_test(user_is_manager)
def msg_copier(request, msg_id):
    """
    Copier message
    """
    msg = Msg.objects.get(id=msg_id)
    msgdet = msg.msgdet_set.all()
    url_name = 'msg'
    if 'msg' not in request.path:
        url_name = msgcopy.listeenvoi.code

    msgcopy = Msg.objects.create(centrepilote=msg.centrepilote,
                                 listeenvoi=msg.listeenvoi,
                                 title='{0} COPIE!'.format(msg.title),
                                 summary_img=msg.summary_img,
                                 summary_txt=msg.summary_txt,
                                 )
    for i in msgdet:
        msgdetcopy = MsgDet.objects.get(pk=i.id)
        msgdetcopy.pk = None
        msgdetcopy.msg = msgcopy
        msgdetcopy.save()
        # https://stackoverflow.com/questions/42207006/how-to-copy-a-placeholderfield-in-django-cms
        plugins = CMSPlugin.objects.filter(placeholder_id=i.short_text2.id)
        copy_plugins_to(plugins, msgdetcopy.short_text2, no_signals=True)

    context = {
        'msg': msgcopy,
        'msg_id': msgcopy.id,
        'msgdet': msgcopy.msgdet_set.all(),
        'url_name_list': f'msg:{url_name}'
    }
    return redirect(msgcopy.get_absolute_url())
    # return render(request, 'msg/msg_detail.html', context)

@login_required
@user_passes_test(user_is_manager)
def msg_new(request):
    """
    Ajout nouveau message
    """
    data = request.POST or None
    
    # msg_nextdate = timezone.localdate()
    # msg_nextdate = timezone.now().date()
    # msg_nextdate = datetime.date.today()
    # print(msg_nextdate)
    form = MsgForm(data, user=request.user)
    formset = MsgDetFormSet(data, queryset=MsgDet.objects.filter(id=0))
    url_path_list = request.path.replace('/new', '')
    title = 'Avertissement > Nouveau'
    if 'cereales' in request.path:
        title = 'Avertissement céréales > Nouveau'
    if 'oleagineux' in request.path:
        title = 'Avertissement oléagineux > Nouveau'
    if 'proteagineux' in request.path:
        title = 'Avertissement proteagineux > Nouveau'              

    if request.method == 'POST':
        if form.is_valid():
            msg = form.save(commit=False)
            msg.centrepilote = request.user.manage_cp
            msg.save()
            if formset.is_valid():
                for frm in formset:
                    # Save only for line where sub-title has been filled in
                    if frm.cleaned_data.get("title"):
                        msgdet = frm.save(commit=False)
                        msgdet.msg = msg
                        msgdet.save()

            return redirect(msg.get_absolute_url())
    context = {
        'title': title,
        'form': form,
        'formset': formset,
        'url_path_list': url_path_list
        }
    return render(request, 'msg/msg_form.html', context)

@login_required
@user_passes_test(user_is_manager)
def msg_update(request, msg_id):
    """
    Pour editer un msg
    """
    msg = Msg.objects.get(id=msg_id)
    url_name = 'msg'
    if 'msg' not in request.path:
        url_name = msg.listeenvoi.code

    data = request.POST or None
    form = MsgForm(data, instance=msg, user=request.user)
    formset = MsgDetFormSet(data=data, queryset=msg.msgdet_set.all())
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            if formset.is_valid():
                for frm in formset:
                    # Save only for line where sub-title has been filled in
                    if frm.cleaned_data.get("title"):
                        msgdet = frm.save(commit=False)
                        msgdet.msg = msg
                        msgdet.save()
                return redirect(msg.get_absolute_url())

    context = {
        'title': 'Message > Modifier',
        'form': form,
        'formset': formset,
        'msg_id': msg_id,
        'url_name_list': f'msg:{url_name}'
        }
    return render(request, 'msg/msg_form.html', context)

@login_required
@user_passes_test(user_is_manager)
def msg_publier(request, msg_id):
    msg = Msg.objects.get(id=msg_id)
    result = msg_clean(from_id=msg.id)
    msg.statut = 'publie'
    msg.msg_date = timezone.now()
    msg.save()
    messages.success(request, "Message publié.")
    return redirect(msg.get_absolute_url())


@login_required
@user_passes_test(user_is_manager)
def msg_envoyer(request, msg_id):
    """
    envoyer un msg
    """
    template_name = 'msg/email/email.html'
    msg = Msg.objects.get(pk=msg_id)
    myemail = SendEmailImages(subject=f"{msg.get_title_stamp_ext()}: {msg.title}")
    myemail.process_msgdet(request=request, msg=msg, template_name=template_name)
    
    recipients = get_email_list(msg.listeenvoi)
    blocnr = 0
    blocnr_list = []
    email_warning = False
    
    for bloc_recipients in recipients:
        blocnr += 1
        myemail.bcc = bloc_recipients
        emailstatus = myemail.send()
        # LOG_INFO.info('emailstatusDEBUG: {0} | blocnr {1} | bloc_recipients: {2}'.format(emailstatus, blocnr, bloc_recipients))
        if emailstatus:
            # All OK
            blocnr_list.append(blocnr)
            LOG_INFO.info('emailstatus: {0} | blocnr {1} | bloc_recipients: {2}'.format(emailstatus, blocnr, bloc_recipients),
                extra={'user': request.user})
        else:
            # Problem
            email_warning = True
            blocnr_list.append(blocnr)
            LOG.warning('emailstatus: {0} | blocnr {1} | bloc_recipients: {2}'.format(emailstatus, blocnr, bloc_recipients),
                extra={'user': request.user})                

    # Add a message with the return of the sends email.
    if email_warning:
        msg_txt = "Erreur lors de l'envoi pour les blocs {0}! Contactez {1} pour qu'il consulte les 'warning' logs.".format(blocnr_list, settings.ADMINS[1])
        messages.error(request, msg_txt)
    else:
        msg.statut = 'envoye'
        # msg.msg_date = timezone.now()
        msg.save()
        messages.success(request, "Message envoyé!")            

    return redirect(msg.get_absolute_url())


@login_required
@user_passes_test(user_is_manager)
def preview_email(request, msg_id):
    template_name = 'msg/email/email.html'
    msg = Msg.objects.get(pk=msg_id)
    myemail = SendEmailImages(subject=msg.get_title_stamp_ext())
    context = myemail.process_msgdet(request=request, msg=msg, template_name=template_name)
    # if settings.DEBUG:
    #     template_name = 'msg/email/email_tmp_after.html'
    return render(request, template_name, context)


def msgdet_consult(request, msg_id, msgdet_id):
    """
    Consulter détail de message (used in email only)
    """
    try:
        msg = Msg.objects.get(id=msg_id)
    except Msg.DoesNotExist:
        return render(request, 'msg/wrong_url.html', {'title': 'Adresse obsolète'})
    
    try:
        msgdet = MsgDet.objects.get(id=msgdet_id)
    except MsgDet.DoesNotExist:
        return redirect(msg.get_absolute_url())
    
    context = {
        'user': request.user,
        'msg': msg,
        'msgdet': msgdet,
        'title': msgdet.get_title(),
        'breadcrumb_display': msgdet.get_title(),
    }
    return render(request, 'msg/msgdet_consult.html', context)
