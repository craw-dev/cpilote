from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.urls import path, re_path
from . import views


@apphook_pool.register
class MsgGlobal(CMSApp):
    app_name = "msg"  # must match the application namespace
    name = "Messages"

    def get_urls(self, page=None, language=None, **kwargs):
        return [
            path("", views.MsgListView.as_view(), name="msg"),
            "msg.urls",
            ]


@apphook_pool.register
class MsgCereales(CMSApp):
    app_name = "msg"  # must match the application namespace
    name = "Messages Cereales"

    def get_urls(self, page=None, language=None, **kwargs):
        return [
            path("", views.MsgListView.as_view(listeenvoi_code='cepicop_cereales'), name="cepicop_cereales"),
            # path("", views.MsgListView.as_view(listeenvoi_code='cepicop_cereales'), name="cepicop_cereales"),
            "msg.urls",
            ]


@apphook_pool.register
class MsgOleagineux(CMSApp):
    app_name = "msg"  # must match the application namespace
    name = "Messages Oleagineux"

    def get_urls(self, page=None, language=None, **kwargs):
        return [
            path("", views.MsgListView.as_view(listeenvoi_code='cepicop_oleagineux'), name="cepicop_oleagineux"),
            "msg.urls",
            ]


@apphook_pool.register
class MsgVegemar(CMSApp):
    app_name = "msg"  # must match the application namespace
    name = "Messages Vegemar"

    def get_urls(self, page=None, language=None, **kwargs):
        return [
            path("", views.MsgListView.as_view(listeenvoi_code='vegemar_newsletter'), name="vegemar_newsletter"),
            "msg.urls",
            ]
