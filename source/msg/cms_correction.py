# 
# openHBP correction created on 8 March 2022
# Doc: https://djangocms.readthedocs.io/en/2.2/extending_cms/placeholders.html
# Issue: 
# https://github.com/django-cms/django-cms/issues/5178#issuecomment-669904244
# https://github.com/django-cms/django-cms/issues/5410
# Stackoverflow: https://stackoverflow.com/questions/55361102/django-cms-placeholderfield
# Based on open issue 5178 (19 April 2016)
# Does not work!
#
from cms.models.placeholdermodel import Placeholder
from cms.models.fields import PlaceholderField


class CorrPlaceholder(Placeholder):
    """
    Attributes:
        is_static       Set to "True" for static placeholders by the template tag
        is_editable     If False the content of the placeholder is not editable in the frontend
    
    Adapt generic Placeholder to get rid of error when non Admin
    """

    def has_change_permission(self, user):
        """
        Returns True if user has permission
        to change all models attached to this placeholder.
        """
        from cms.utils.permissions import get_model_permission_codename

        attached_models = self._get_attached_models()
        # openHBP, instead of returning is_superuser, return True!
        if not attached_models:
            # technically if placeholder is not attached to anything,
            # user should not be able to change it but if is superuser
            # then we "should" allow it.
            # return user.is_superuser
            return True

        attached_objects = self._get_attached_objects()

        for obj in attached_objects:
            try:
                perm = obj.has_placeholder_change_permission(user)
            except AttributeError:
                model = type(obj)
                change_perm = get_model_permission_codename(model, 'change')
                perm = user.has_perm(change_perm)

            if not perm:
                return False
        return True


class CorrPlaceholderField(PlaceholderField):
    
    def _get_new_placeholder(self, instance):
        print('*****************************************')
        return CorrPlaceholder.objects.create(slot=self._get_placeholder_slot(instance), default_width=self.default_width)        