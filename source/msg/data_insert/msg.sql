﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: msg_categorie; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.msg_categorie (id, code, nom, icone, couleur) VALUES (1, 'avert', 'Avertissement', 'glyphicon-bullhorn', '#d68433');
INSERT INTO public.msg_categorie (id, code, nom, icone, couleur) VALUES (3, 'event', 'Événement', 'glyphicon-calendar', '#5f8cb2');
INSERT INTO public.msg_categorie (id, code, nom, icone, couleur) VALUES (2, 'comm', 'Communication', 'glyphicon-lamp', '#709d17');


--
-- Name: msg_categorie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.msg_categorie_id_seq', 3, true);


--
-- Data for Name: msg_msg; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (2, '2017-12-22 11:07:03+01', 'Service messagerie et avertissements', 'draft', 6, '2017-12-22 11:07:03+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (1, '2017-12-21 16:27:14+01', 'CADCO  - Actualité  - céréales du 14 novembre 2017 ( C28 )', 'publie', 2, '2017-12-21 16:27:14+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (3, '2018-01-15 10:13:35+01', 'CADCO - Actualité - céréales du 24 octobre 2017 (C25) 1/1', 'publie', 2, '2018-01-15 10:13:35+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (5, '2018-01-15 11:06:56+01', 'CADCO - Actualité - céréales du 07 novembre 2017 (C27) ', 'draft', 2, '2018-01-15 11:06:56+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (4, '2018-01-15 10:55:09+01', 'CADCO - Actualité - céréales du 31 octobre 2017 (C26)', 'publie', 2, '2018-01-15 10:55:09+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (6, '2018-01-15 14:12:01+01', 'semaine n° 37 du 11/09/2017', 'publie', 6, '2018-01-15 14:12:01+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (7, '2018-01-18 11:35:12+01', 'A vos agendas', 'publie', 2, '2018-01-18 11:35:12+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (8, '2018-02-20 09:57:32+01', 'Service messagerie et avertissements', 'publie', 2, '2018-02-20 09:57:32+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (9, '2018-03-13 12:08:59+01', 'CADCO - Actualité - céréales du 13 mars 2018 ', 'publie', 2, '2018-03-13 12:08:59+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (10, '2018-03-13 12:40:42+01', 'Colza : Situation au 13 mars 2018', 'publie', 2, '2018-03-13 12:40:42+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (11, '2018-03-20 12:26:36+01', 'CADCO - Actualité - céréales du 20 mars 2018 (C02)', 'publie', 2, '2018-03-20 12:26:36+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (12, '2018-03-27 13:40:57+02', 'CADCO - Actualité - céréales du 27 mars 2018 (C03)', 'publie', 2, '2018-03-27 13:40:57+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (13, '2018-03-27 13:49:28+02', 'Colza, situation au 27 mars 2018', 'publie', 2, '2018-03-27 13:49:28+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (14, '2018-04-03 12:08:35+02', 'CADCO - Actualité - céréales du 03 avril 2018 (C04)', 'publie', 2, '2018-04-03 12:08:35+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (15, '2018-04-03 13:03:35+02', 'Colza, situation au 03 avril 2018', 'publie', 2, '2018-04-03 13:03:35+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (16, '2018-04-10 12:39:13+02', 'CADCO - Actualité - céréales du 10 avril 2018 (C05)', 'publie', 2, '2018-04-10 12:39:13+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (17, '2018-04-10 13:19:03+02', 'Colza, situation au 10 avril 2018', 'publie', 2, '2018-04-10 13:19:03+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (18, '2018-04-17 12:22:32+02', 'CADCO - Actualité - céréales du 17 avril 2018 (C06)', 'publie', 2, '2018-04-17 12:22:32+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (19, '2018-04-17 12:59:51+02', 'Colza, situation au 17 avril 2018 (C05)', 'publie', 2, '2018-04-17 12:59:51+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (20, '2018-04-24 12:36:36+02', 'CADCO - Actualité - céréales du 24 avril 2018 (C07)', 'publie', 2, '2018-04-24 12:36:36+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (21, '2018-04-24 14:25:38+02', 'Colza, situation au 24 avril 2018 (C06)', 'publie', 2, '2018-04-24 14:25:38+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (22, '2018-04-25 18:29:13+02', 'Colza, situation au 25 avril 2018 (C07)', 'publie', 2, '2018-04-25 18:29:13+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (23, '2018-05-01 11:29:36+02', 'CADCO - Actualité - céréales du 01 mai 2018 (C08)', 'publie', 2, '2018-05-01 11:29:36+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (24, '2018-05-01 16:56:28+02', 'Colza, situation au 01 mai 2018 (C08)', 'publie', 2, '2018-05-01 16:56:28+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (25, '2018-05-08 09:52:28+02', 'Colza, situation au 08 mai 2018 (C09)', 'publie', 2, '2018-05-08 09:52:28+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (26, '2018-05-08 11:42:09+02', 'CADCO - Actualité - céréales du 08 mai 2018 (C09)', 'publie', 2, '2018-05-08 11:42:09+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (27, '2018-05-15 11:09:08+02', 'CADCO - Actualité - céréales du 15 mai 2018 (C10)', 'publie', 2, '2018-05-15 11:09:08+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (28, '2018-05-15 12:46:53+02', 'Colza : Floraison terminée ou presque', 'publie', 2, '2018-05-15 12:46:53+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (29, '2018-05-22 11:34:59+02', 'CADCO - Actualité - céréales du 22 mai 2018 (C11)', 'publie', 2, '2018-05-22 11:34:59+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (30, '2018-05-24 07:52:08+02', 'CADCO - Actualité - céréales du 24 mai 2018 (C12)', 'publie', 2, '2018-05-24 07:52:08+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (31, '2018-05-29 11:55:08+02', 'CADCO - Actualité - céréales du 29 mai 2018 (C13)', 'publie', 2, '2018-05-29 11:55:08+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (38, '2018-06-19 08:05:11.090037+02', 'CADCO - Actualité - céréales du 19 juin 2018 (C15)', 'publie', 2, '2018-06-18 22:47:45+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (37, '2018-06-05 17:22:55.316962+02', 'CADCO - Actualité - céréales du 5 juin 2018 (C14)', 'publie', 2, '2018-06-05 16:58:05+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (49, '2018-10-09 12:33:25.286017+02', 'CADCO - Actualité - céréales du 09 octobre 2018 (C22)', 'publie', 2, '2018-10-09 10:48:24+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (39, '2018-07-20 07:51:00.640084+02', 'CADCO - Actualité - céréales du 20 juillet 2018 (C16)', 'publie', 2, '2018-07-19 16:06:10+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (50, '2018-10-09 12:48:46.201035+02', 'Colza : les altises toujours actives et arrivée des premiers charançons du bourgeon terminal', 'publie', 2, '2018-10-09 10:55:27+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (40, '2018-09-04 12:55:58.638351+02', 'Copie de CADCO - Actualité - céréales du 04 septembre 2018 (C17)', 'publie', 2, '2018-09-04 11:44:47+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (41, '2018-09-11 09:07:27.589196+02', 'CADCO - Actualité - céréales du 11 septembre 2018 (C18)', 'publie', 2, '2018-09-10 20:44:39+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (46, '2018-09-25 11:37:40.067778+02', 'CADCO - Actualité - céréales du 25 septembre 2018 (C20)', 'publie', 2, '2018-09-25 08:05:40+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (42, '2018-09-11 12:35:22.242036+02', 'Le colza : Premières arrivées d’altises', 'publie', 2, '2018-09-11 12:29:05+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (45, '2018-09-25 12:01:41.121441+02', 'Le colza : de la pluie et de la fraîcheur automnale', 'publie', 2, '2018-09-25 08:03:10+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (47, '2018-10-02 11:45:50.897753+02', 'CADCO - Actualité - céréales du 02 octobre 2018 (C21)', 'publie', 2, '2018-10-02 11:18:48+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (43, '2018-09-18 11:47:03.467232+02', 'CADCO - Actualité - céréales du 18 septembre 2018 (C19)', 'publie', 2, '2018-09-18 08:34:39+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (44, '2018-09-18 12:38:44.15627+02', 'Colza : du soleil et des altises ', 'publie', 2, '2018-09-18 12:11:01+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (51, '2018-10-16 12:34:33.892945+02', 'CADCO - Actualité - céréales du 16 octobre 2018 (C23)', 'publie', 2, '2018-10-09 12:50:58+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (48, '2018-10-02 12:30:51.459631+02', 'Le colza : les grosses altises toujours présentes !', 'publie', 2, '2018-10-02 11:57:23+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (53, '2018-10-23 12:36:16.091891+02', 'CADCO - Actualité - céréales du 23 octobre 2018 (C24)', 'publie', 2, '2018-10-23 09:14:25+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (62, '2018-12-18 15:51:28.44111+01', 'CADCO - Actualité - céréales du 18 décembre 2018 (C29)', 'publie', 2, '2018-12-18 15:33:51+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (54, '2018-10-24 11:10:25.369202+02', 'Le colza et les conditions automnales ', 'publie', 2, '2018-10-23 09:58:45+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (52, '2018-10-16 12:36:46+02', 'Un peu de tout en insectes et en stades de développement', 'publie', 2, '2018-10-16 11:33:31+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (58, '2018-11-06 12:45:27.363795+01', 'Le colza : les insectes sont toujours présents', 'publie', 2, '2018-11-06 12:39:40+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (56, '2018-11-06 12:32:57+01', 'CADCO - Actualité - céréales du 06 novembre 2018 (C26)', 'publie', 2, '2018-11-06 10:32:56+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (55, '2018-10-30 12:35:27.95832+01', 'CADCO - Actualité - céréales du 30 octobre 2018 (C25)', 'publie', 2, '2018-10-29 20:45:56+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (61, '2018-11-20 13:06:25.890818+01', 'CADCO - Actualité - céréales du 20 novembre 2018 (C28)', 'publie', 2, '2018-11-20 09:48:27+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (60, '2018-11-13 13:35:58.861448+01', 'CADCO - Actualité - céréales du 13 novembre 2018 (C27)', 'publie', 2, '2018-11-13 10:03:30+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (64, '2019-02-22 14:59:39.874357+01', 'CADCO - Actualité - céréales du 22 février 2019 (C01)', 'publie', 2, '2019-02-22 13:59:51+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (67, '2019-02-26 12:36:00.92598+01', 'Le colza : Douceur record en février et arrivée des premiers insectes', 'publie', 2, '2019-02-26 12:28:56+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (71, '2019-02-26 13:50:06.98457+01', 'CADCO - Actualité - céréales du 26 février 2019 (C02)', 'publie', 2, '2019-02-26 13:24:05+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (74, '2019-03-05 12:34:59.251999+01', 'CADCO - Actualité - céréales du 05 mars 2019 (C03)', 'publie', 2, '2019-03-04 07:49:26+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (75, '2019-03-12 12:56:28+01', 'CADCO - Actualité - céréales du 12 mars 2019 (C04)', 'publie', 2, '2019-03-12 12:19:57+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (76, '2019-03-22 21:09:28.027654+01', 'Le colza : Faible présence d’insectes après la tempête', 'publie', 2, '2019-03-12 13:16:49+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (79, '2019-03-22 21:11:22.023719+01', 'Le colza : Vol de méligèthes', 'publie', 2, '2019-03-22 15:24:51+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (80, '2019-03-22 17:51:21.082911+01', 'Test envoi double', 'publie', 2, '2019-03-22 17:29:45+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (77, '2019-03-22 21:05:02.934008+01', 'CADCO - Actualité - céréales du 19 mars 2019 (C05)', 'publie', 2, '2019-03-19 07:35:24+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (90, '2019-04-23 12:51:59.202042+02', 'CADCO - Actualité - céréales du 23 avril 2019 (C10)', 'publie', 2, '2019-04-22 21:46:46+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (91, '2019-04-23 12:32:11+02', 'Le colza : Arrivée des charançons des siliques ', 'publie', 2, '2019-04-23 12:28:16+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (101, NULL, 'CePiCOP - Actualité - céréales du xxx 2019 (C19)', 'draft', 2, '2019-06-10 20:31:42+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (92, '2019-04-30 12:48:58.266184+02', 'CADCO - Actualité - céréales du 30 avril 2019 (C11)', 'publie', 2, '2019-04-29 21:42:16+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (82, '2019-03-26 15:46:54.968447+01', 'CADCO - Actualité - céréales du 26 mars 2019 (C06)', 'publie', 2, '2019-03-25 14:36:16+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (83, '2019-03-26 16:01:32.300609+01', 'Présence de méligèthes dans les inflorescences', 'publie', 2, '2019-03-26 12:51:30+01', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (100, '2019-06-11 13:30:35.266225+02', 'CePiCOP - Actualité - céréales du 11 juin 2019 (C18)', 'publie', 2, '2019-06-05 11:13:12+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (93, '2019-04-30 13:03:41.569251+02', 'Le colza : Pourquoi ne fleurit-il pas bien partout ?', 'publie', 2, '2019-04-30 12:58:31+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (84, '2019-04-01 15:49:20.857988+02', 'Toujours sous la menace des méligèthes', 'publie', 2, '2019-04-01 14:44:02+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (94, '2019-05-07 13:15:44.082644+02', 'CePiCOP - Actualité - céréales du 07 mai 2019 (C12)', 'publie', 2, '2019-05-06 10:36:33+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (85, '2019-04-02 12:21:49.963688+02', 'CADCO - Actualité - céréales du 02 avril 2019 (C07)', 'publie', 2, '2019-04-01 16:23:51+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (102, '2019-06-20 12:38:44.906429+02', 'CePiCOP : Agenda des visites d''essais', 'publie', 2, '2019-06-20 11:28:05+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (95, '2019-05-14 12:44:03.576117+02', 'CePiCOP - Actualité - céréales du 14 mai 2019 (C13)', 'publie', 2, '2019-05-13 11:04:04+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (87, '2019-04-09 16:24:50.690343+02', 'Le colza : Vers le début de la floraison', 'publie', 2, '2019-04-09 11:58:57+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (86, '2019-04-09 17:06:20+02', 'CADCO - Actualité - céréales du 09 avril 2019 (C08)', 'publie', 2, '2019-04-08 17:31:40+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (96, '2019-05-21 13:19:40.225939+02', 'CePiCOP - Actualité - céréales du 21 mai 2019 (C14)', 'publie', 2, '2019-05-21 11:59:59+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (89, '2019-04-16 12:14:37.779178+02', 'Le colza commence à fleurir', 'publie', 2, '2019-04-16 11:16:14+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (88, '2019-04-16 13:08:57.082747+02', 'CADCO - Actualité - céréales du 16 avril 2019 (C09)', 'publie', 2, '2019-04-16 09:02:16+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (97, '2019-05-28 12:42:11.917388+02', 'CePiCOP - Actualité - céréales du 28 mai 2019 (C15)', 'publie', 2, '2019-05-28 11:15:14+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (98, '2019-05-31 22:08:36.294274+02', 'CePiCOP - Actualité - céréales du 31 mai 2019 (C16)', 'publie', 2, '2019-05-31 16:18:55+02', 2);
INSERT INTO public.msg_msg (id, msg_date, title, statut, centrepilote_id, msg_created, liste_envois_id) VALUES (99, '2019-06-04 12:41:53.021797+02', 'CePiCOP - Actualité - céréales du 04 juin 2019 (C17)', 'publie', 2, '2019-06-04 08:42:11+02', 2);


--
-- Name: msg_msg_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.msg_msg_id_seq', 102, true);


--
-- Data for Name: msg_msgdet; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (1, 'Jaunisse nanisante de l’orge: fin de saison', 'left', 300, 225, '<p>Les analyses virologiques pratiqu&eacute;es sur les pucerons collect&eacute;s dans les champs montrent qu&rsquo;une proportion assez faible d&rsquo;entre eux sont porteurs du virus.</p>

<p>De nos observations il ressort qu&rsquo;il n&rsquo;y a pas de risque pour :</p>

<ul>
	<li>les escourgeons trait&eacute;s &agrave; partir de la mi-octobre. L&rsquo;infestation y est les plus souvent nulle, rarement elle atteint un maximum de 8 % de plantes occup&eacute;es ce qui est peu (compte tenu de la proportion de virulif&egrave;res).</li>
	<li>les parcelles issues de semences trait&eacute;es avec un n&eacute;onicotino&iuml;de (Argento, Gaucho duo)</li>
	<li>les froments dont l&rsquo;infestation est faible. Le maximum observ&eacute; est de 5 % de plantes occup&eacute;es ce qui</li>
	<li>est peu.</li>
	<li>les deux &nbsp;<u>vari&eacute;t&eacute;s d&rsquo;escourgeon tol&eacute;rantes &agrave; la jaunisse nanisante de l&rsquo;orge</u>: <strong>RAFAELA </strong>et <strong>DOMINO </strong>qui ne justifient aucun traitement insecticide, m&ecirc;me si la pression de la jaunisse est forte.</li>
</ul>

<p>Dans les rares cas ou l&rsquo;infestation d&eacute;passerait 10 % des plantes, il n&rsquo;y aurait pas d&rsquo;urgence &agrave; traiter, mais il conviendrait d&rsquo;&eacute;viter de se laisser surprendre par les pluies dans des terres difficiles d&rsquo;acc&egrave;s. Dans nos r&eacute;seaux d&rsquo;observation, ce cas de figure ne se rencontre que dans un seul champ d&rsquo;escourgeon (12% de plantes occup&eacute;es) n&rsquo;ayant pas &eacute;t&eacute; trait&eacute; &agrave; la mi-octobre.</p>
', 1, NULL, 1, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (4, 'Jaunisse nanisante de l’orge : statu quo', 'left', 300, 225, '<p style="text-align:justify">Bien que les observations de ce lundi 23 octobre r&eacute;v&egrave;lent la pr&eacute;sence de pucerons ail&eacute;s dans les champs non trait&eacute;s du r&eacute;seau, l&rsquo;infestation n&rsquo;a gu&egrave;re &eacute;volu&eacute; et si situe entre 1 et 14% des plantes. Les champs les plus infest&eacute;s sont les suivants : Jandrain (14%), Mortroux (14%), Pailhe (12%), Staves (11%).</p>

<p style="text-align:justify">On en ignore toujours la proportion de virulif&egrave;res des pucerons en cours d&rsquo;analyse, information attendue pour ce jeudi.</p>

<p style="text-align:justify">Le conseil de la semaine derni&egrave;re reste d&rsquo;application :</p>

<p style="text-align:justify">Dans les champs o&ugrave; la proportion de plantes porteuses de pucerons atteint ou d&eacute;passe 10 %, &nbsp;il est conseill&eacute; de profiter du passage herbicide, et d&rsquo;appliquer un insecticide. L&agrave; o&ugrave; les niveaux d&rsquo;infestation sont moindres, il vaut mieux attendre encore quelques jours le r&eacute;sultat des analyses virologiques. Ci-apr&egrave;s le lien vers les listes des produits phyto autoris&eacute;s en c&eacute;r&eacute;ales : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a> &nbsp;Pour acc&eacute;der &agrave; un dossier la &laquo; jaunisse nanisante &raquo; : <a href="http://www.cadcoasbl.be/p09_biblio/art0001/JNO.pdf">ici</a></p>

<p style="text-align:justify">Nos observations de la semaine prochaine auront pour objectifs :</p>

<ul>
	<li style="text-align:justify">De contr&ocirc;ler la r&eacute;-infestation dans les champs d&#39;escourgeon trait&eacute;s</li>
	<li style="text-align:justify">De contr&ocirc;ler l&rsquo;infestation dans les champs de froment</li>
</ul>

<p style="text-align:justify">Deux <u>vari&eacute;t&eacute;s d&rsquo;escourgeon sont tol&eacute;rantes &agrave; la jaunisse nanisante de l&rsquo;orge</u> : <strong>RAFAELA</strong> et <strong>DOMINO</strong>. Ces vari&eacute;t&eacute;s ne justifient aucun traitement insecticide, m&ecirc;me si la pression de la jaunisse est forte.</p>

<p style="text-align:justify">Le prochain avertissement est pr&eacute;vu pour le mardi 31 octobre en d&eacute;but d&rsquo;apr&egrave;s-midi.</p>
', 1, NULL, 3, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (5, 'Le CRA-W recherche des agriculteurs partenaires', 'left', 300, 225, '<p style="text-align:justify">Cet automne, le CRA-W d&eacute;marre un projet de recherche intitul&eacute; RESIST.&nbsp; Ce projet s&rsquo;int&eacute;ressera, pendant minimum 3 ans, &agrave; la r&eacute;sistance de la septoriose et du vulpin aux produits de protection des plantes.&nbsp; Il poursuit 3 objectifs :</p>

<ul>
	<li style="text-align:justify">r&eacute;aliser un monitoring de la r&eacute;sistance en R&eacute;gion Wallonne,</li>
	<li style="text-align:justify">identifier les pratiques &agrave; risques,</li>
	<li style="text-align:justify">mettre au point des guides de bonnes pratiques.</li>
</ul>

<p style="text-align:justify">Afin de mener au mieux ce projet, des pr&eacute;l&egrave;vements de vulpins et de septoriose devraient &ecirc;tre effectu&eacute;s, d&egrave;s cet automne, dans des parcelles agricoles.</p>

<p style="text-align:justify">Nous recherchons, sur tout le territoire de la Wallonie, des agriculteurs aux pratiques contrast&eacute;es (cf. ci-dessous) qui travaillent des terres dans lesquelles des vulpins sont r&eacute;guli&egrave;rement observ&eacute;s.&nbsp; Les agriculteurs devront &eacute;galement disposer d&rsquo;historiques pr&eacute;cis (rotation, produits phytos, amendements, travail du sol,&hellip;) pour les parcelles concern&eacute;es.</p>

<p style="text-align:justify">Les pratiques cibl&eacute;es sont :</p>

<ul>
	<li style="text-align:justify">fermes pratiquant le labour et ne r&eacute;duisant pas trop les doses de produits de protection des plantes ;</li>
	<li style="text-align:justify">fermes pratiquant la r&eacute;duction de dose de produits de protection des plantes ;</li>
	<li style="text-align:justify">fermes pratiquant le non labour ;</li>
	<li style="text-align:justify">fermes en agriculture biologique.</li>
</ul>

<p style="text-align:justify">Il s&rsquo;agira d&rsquo;une recherche collaborative, &eacute;tudiant les effets de vos pratiques sur la s&eacute;lection de r&eacute;sistance du vulpin et de la septoriose aux produits de protection des plantes.&nbsp; Vous b&eacute;n&eacute;ficierez des r&eacute;sultats du monitoring dans vos parcelles et aiderez l&rsquo;ensemble de la profession.</p>

<p style="text-align:justify"><strong>Si vous &ecirc;tes int&eacute;ress&eacute;s,</strong> contactez<strong> </strong>Fran&ccedil;ois Henriet<strong> : </strong>081 62 52 62 ; <a href="mailto:f.henriet@cra.wallonie.be">f.henriet@cra.wallonie.be</a></p>
', 2, NULL, 3, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (3, 'Caractéristiques du harcèlement', 'right', 300, 225, '<p>Le harc&egrave;lement peut prendre diff&eacute;rentes formes (point pr&eacute;c&eacute;dent) mais il pr&eacute;sente toujours les m&ecirc;mes caract&eacute;ristiques(2).</p>

<ul>
	<li>
	<p><strong>R&eacute;p&eacute;tition</strong> dans le temps : agression &agrave; long terme et &agrave; caract&egrave;re r&eacute;p&eacute;titif. Elle peut durer des mois, voire plusieurs ann&eacute;es</p>
	</li>
	<li>
	<p>La <strong>loi du plus fort</strong>, intimidation, domination : agression des plus forts contre les plus faibles, des plus nombreux contre ceux qui sont seuls ou des plus &acirc;g&eacute;s contre des plus jeunes.</p>
	</li>
	<li>
	<p>L&rsquo;<strong>intention</strong> : volont&eacute; r&eacute;elle de nuire, de faire mal, de blesser de la part du harceleur, m&ecirc;me si la plupart du temps, il dira qu&rsquo;il s&rsquo;agit simplement d&rsquo;un jeu. Le harceleur &agrave; toujours besoin d&rsquo;un public pour pouvoir r&eacute;ellement profiter de la situation.</p>
	</li>
</ul>
', 1, 26, 2, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (8, 'Assemblée sectorielle grandes cultures et pomme de terre', 'left', 300, 225, '<p>Quand? 14/11/2017, 18h30</p>

<p>O&ugrave;?&nbsp;Maison de la ruralit&eacute;</p>

<p>Chauss&eacute;e de Namur 47, 5030 Gembloux (SOCOPRO)</p>
', 3, NULL, 4, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (9, 'Jaunisse nanisante de l’orge : rester attentif', 'left', 300, 225, '<p style="text-align:justify">Les premiers froids n&rsquo;ont pas eu d&rsquo;impact sur les pucerons. Les observations de ce lundi 06 novembre r&eacute;v&egrave;lent la situation suivante :</p>

<p style="text-align:justify"><u>En escourgeon</u></p>

<ul>
	<li style="text-align:justify">de&nbsp; 0 &agrave; 24 % de plantes occup&eacute;es dans les champs trait&eacute;s &agrave; la mi-octobre</li>
	<li style="text-align:justify">de 0 &agrave; 16 % de plantes occup&eacute;es dans les champs non trait&eacute;s</li>
</ul>

<p style="margin-left:35.45pt; margin-right:0cm; text-align:justify"><u>En froment</u></p>

<ul>
	<li style="text-align:justify">de 1 &agrave; 17 % de plantes occup&eacute;es dans les champs non trait&eacute;s</li>
</ul>

<p style="text-align:justify">Les analyses virologiques pratiqu&eacute;es sur les pucerons collect&eacute;s dans les champs montrent qu&rsquo;une proportion assez faible d&rsquo;entre eux sont porteurs du virus</p>

<p style="text-align:justify">Les pr&eacute;visions m&eacute;t&eacute;orologiques sont toujours &agrave; la douceur pour toute la semaine &agrave; venir.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">Dans cette situation, notre conseil est plus que jamais de <u>v&eacute;rifier le niveau d&rsquo;infestation</u> de ses champs.</p>

<p style="text-align:justify">- Traiter l&agrave; o&ugrave; il n&rsquo;y a pas de puceron n&rsquo;a aucun sens.</p>

<p style="margin-left:42.55pt; margin-right:0cm; text-align:justify">- L&agrave; o&ugrave; les pucerons sont pr&eacute;sents en faible nombre (&lt; 10% plantes occup&eacute;es), un traitement insecticide n&rsquo;est pas requis.</p>

<p style="margin-left:42.55pt; margin-right:0cm; text-align:justify">- Si les niveaux d&rsquo;infestation d&eacute;passent 10 %,&nbsp; il n&rsquo;y a pas d&rsquo;urgence &agrave; traiter, mais il convient d&rsquo;&eacute;viter de se laisser surprendre par les pluies dans des terres difficiles d&rsquo;acc&egrave;s.</p>

<p style="text-align:justify">Ci-apr&egrave;s le lien vers les listes des produits phyto autoris&eacute;s en c&eacute;r&eacute;ales : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a>&nbsp; Pour acc&eacute;der &agrave; un dossier la &laquo; Jaunisse nanisante &raquo; : <a href="http://www.cadcoasbl.be/p09_biblio/art0001/JNO.pdf">ici</a></p>

<p style="text-align:justify">Pas de risque pour :</p>

<ul>
	<li style="text-align:justify">les parcelles sem&eacute;es avec un insecticide en enrobage de semence.</li>
	<li style="text-align:justify">les deux <u>vari&eacute;t&eacute;s d&rsquo;escourgeon tol&eacute;rantes &agrave; la jaunisse nanisante de l&rsquo;orge</u> : <strong>RAFAELA</strong> et <strong>DOMINO</strong> qui ne justifient aucun traitement insecticide, m&ecirc;me si la pression de la jaunisse est forte.</li>
</ul>

<p style="text-align:justify">Le prochain avertissement est pr&eacute;vu pour le mardi 14 novembre en d&eacute;but d&rsquo;apr&egrave;s-midi.</p>

<p><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, M. De Proft</p>
', 1, NULL, 5, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (6, 'Jaunisse nanisante de l’orge : les froments sous surveillance', 'left', 300, 225, '<p style="text-align:justify">Les observations de ce lundi 30 octobre dans le r&eacute;seau &laquo; escourgeon &raquo; indiquent une infestation qui se situe entre 0 et 26% des plantes dans les parcelles n&rsquo;ayant re&ccedil;u aucun traitement insecticide. Les champs les plus infest&eacute;s sont les suivants : Nivelles (26%), Jandrain (18%), Mortroux (11%), Pailhe (8%), Stave (8%).</p>

<p style="text-align:justify">Dans les parcelles trait&eacute;es apr&egrave;s la mi-octobre, le niveau d&rsquo;infestation est nul ou quasi nul.</p>

<p style="text-align:justify">Neuf parcelles de froment ont &eacute;galement &eacute;t&eacute; visit&eacute;es. L&rsquo;infestation y est fonction de la date de semis. Les semis d&rsquo;avant la mi-octobre sont infest&eacute;s entre 0 et 10% selon les champs.</p>

<p style="text-align:justify">Par ailleurs, les analyses virologiques pratiqu&eacute;es sur 213 pucerons r&eacute;v&egrave;lent que 2% d&rsquo;entre eux sont porteurs du virus de la jaunisse nanisante.&nbsp; Jusqu&rsquo;&agrave; pr&eacute;sent, et quel que soit le niveau de colonisation des emblavures par les pucerons, l&rsquo;&eacute;pid&eacute;mie n&rsquo;en est encore qu&rsquo;&agrave; un niveau assez bas. Dans les parcelles sem&eacute;es le plus t&ocirc;t, il faut n&eacute;anmoins commencer &agrave; se m&eacute;fier du temps qui passe.</p>

<p style="text-align:justify">C&rsquo;est pourquoi, en escourgeon, il est recommand&eacute; de traiter toute emblavure qui n&rsquo;aurait encore re&ccedil;u aucun traitement insecticide. Ce traitement n&rsquo;a pas de caract&egrave;re d&rsquo;urgence, mais devrait intervenir avant que les conditions ne permettent plus l&rsquo;acc&egrave;s au champ. Le m&ecirc;me conseil peut &ecirc;tre appliqu&eacute; aux parcelles de froment infest&eacute;es &agrave; plus de 10% des plantes.</p>

<p style="text-align:justify">Des vols de pucerons ne sont pas termin&eacute;s, et pourraient encore conduire &agrave; l&rsquo;infestation des parcelles sem&eacute;es r&eacute;cemment. C&rsquo;est pourquoi nos observations se poursuivent. C&rsquo;est aussi pourquoi nous recommandons &agrave; tous les c&eacute;r&eacute;aliers de ne pas remiser leur pulv&eacute;risateur &laquo; &agrave; la Toussaint &raquo;, comme il &eacute;tait encore recommand&eacute; il y a quelques ann&eacute;es d&rsquo;ici. Le r&eacute;chauffement climatique se marque de fa&ccedil;on sensible sur la dynamique de la jaunisse nanisante, et il convient &eacute;videmment d&rsquo;en tenir compte. Nous poursuivons donc les observations et l&rsquo;&eacute;mission d&rsquo;avis hebdomadaires le temps qu&rsquo;il faudra.</p>

<p style="text-align:justify">Dans les terres d&rsquo;abord difficile, il est important d&rsquo;&ecirc;tre attentif aux pr&eacute;visions m&eacute;t&eacute;orologiques. Si des pluies importantes &eacute;taient annonc&eacute;es, il serait prudent d&rsquo;envisager un traitement de &laquo; fin de saison &raquo; si le niveau d&eacute;passe 10% des plantes. Se laisser surprendre par les pluies pourrait, comme en 2015-16, emp&ecirc;cher d&rsquo;intervenir avant le mois de mars, et laisser &agrave; la jaunisse la possibilit&eacute; de prendre trop d&rsquo;expansion en cas d&rsquo;hiver tr&egrave;s doux.</p>

<p style="text-align:justify">Vous trouverez au lien suivant :</p>

<p style="text-align:justify">les listes &laquo; insecticides autoris&eacute;s pour lutter contre les pucerons vecteurs de jaunisse nanisante de l&rsquo;orge en c&eacute;r&eacute;ales &raquo; : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">insecticides</a> &nbsp;</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Le prochain avertissement est pr&eacute;vu pour le 07/11/17.</p>
', 1, 351, 4, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (7, 'Mouche des semis : aidez-nous en signalant les  dégâts', 'right', 300, 225, '<p><em><strong><u>Photo</u></strong><strong> :</strong> Prise de vue le 27/10/2017. Aspect d&rsquo;un champ de froment sem&eacute; en direct le 12/10/2017 &agrave; Vezin (Namur), suite &agrave; l&rsquo;arrachage de betterave le 13/09/17. Pr&eacute;sence de larves et de pupes.</em></p>

<p style="text-align:justify">Plus encore que l&rsquo;an dernier, des d&eacute;g&acirc;ts de mouche des semis sont signal&eacute;s dans diff&eacute;rentes r&eacute;gions du pays. Ces d&eacute;g&acirc;ts parfois graves se manifestent par des d&eacute;fauts de lev&eacute;es, ou bien par des attaques de plantules : sectionnement de la tige et jaunissement de la plus jeune feuille.</p>

<p style="text-align:justify">Ces d&eacute;g&acirc;ts concernent exclusivement les froments succ&eacute;dant &agrave; des betteraves ou &agrave; des chicor&eacute;es arrach&eacute;es t&ocirc;t.</p>

<p style="text-align:justify">Les r&eacute;sidus de ces cultures pourrissant sur le sol attirent les mouches adultes, qui y pondent abondamment. Les larves entament leur phase alimentaire sur ces r&eacute;sidus, mais s&rsquo;en prennent &eacute;galement aux froments fra&icirc;chement sem&eacute;s, dont une proportion importante de plantules peuvent &ecirc;tre d&eacute;truites d&egrave;s avant la lev&eacute;e.</p>

<p style="text-align:justify">Au stade actuel, o&ugrave; la pr&eacute;sence de pupes t&eacute;moigne de la fin de la phase alimentaire de ces mouches, il ne faut plus craindre d&rsquo;attaque en cas de re-semis, m&ecirc;me sans protection insecticide.</p>

<p style="text-align:justify">Afin de nous permettre de mieux cerner les facteurs conduisant &agrave; une attaque de cet insecte, nous lan&ccedil;ons un appel &agrave; signalement de tout d&eacute;g&acirc;t de mouche des semis, via ce <a href="https://goo.gl/forms/sVOHIVVheJpd9ICg1">formulaire</a> qui ne prend que deux minutes &agrave; remplir.&nbsp; Nous vous remercions d&rsquo;avance de votre aide.</p>
', 2, 163, 4, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (10, 'Matière sèche du maïs ensilage', 'left', 300, 225, '<p>Les conditions climatiques de la semaine derni&egrave;re ont permis d&rsquo;atteindre de bonnes progressions des taux de mati&egrave;re s&egrave;che du ma&iuml;s ensilage de l&rsquo;ordre de 2 &agrave; 3 % partout en Wallonie (sauf Ardenne).</p>

<p><strong>Au Centre du Pays</strong>, les maturit&eacute;s se situent d&eacute;j&agrave; entre 30 et 35 % de MS en r&eacute;gions limoneuses. Le stade optimal de r&eacute;colte des vari&eacute;t&eacute;s pr&eacute;coces est atteint d&egrave;s ce lundi 11 septembre partout en zones limoneuses et en Campine hennuy&egrave;re. D&egrave;s que les conditions m&eacute;t&eacute;orologiques s&rsquo;am&eacute;lioreront, les r&eacute;coltes de ces vari&eacute;t&eacute;s ne devront donc plus trainer, sous peine de sur-maturit&eacute; entrainant des pertes de qualit&eacute; importantes ! En vari&eacute;t&eacute;s plus tardives, les ensilages des meilleures situations peuvent aussi d&eacute;buter cette semaine au gr&eacute; de la m&eacute;t&eacute;o. Mais dans la plupart des cas, ces vari&eacute;t&eacute;s n&rsquo;atteindront pas le stade optimal de r&eacute;colte avant la prochaine semaine, soit au plus t&ocirc;t le 18 septembre.</p>

<p><strong>Au sud du sillon Sambre-et-Meuse</strong>, la maturit&eacute; en Condroz a bien progress&eacute;, si bien que cette r&eacute;gion a r&eacute;sorb&eacute; son retard par rapport &agrave; la Famenne. Par contre, vu la m&eacute;t&eacute;o automnale attendue ces prochains jours, le stade optimal de r&eacute;colte des vari&eacute;t&eacute;s pr&eacute;coces ne devrait &ecirc;tre atteint que dans 10 jours au plus t&ocirc;t. Les r&eacute;coltes de ces vari&eacute;t&eacute;s ne devront d&egrave;s lors s&rsquo;envisager qu&rsquo;&agrave; partir du 20 septembre en Condroz, Famenne, r&eacute;gion herbag&egrave;re li&eacute;geoise et r&eacute;gion jurassique. Toutefois, pour la r&eacute;gion jurassique, les vari&eacute;t&eacute;s pr&eacute;coces sem&eacute;es au 20 avril peuvent s&rsquo;ensiler d&egrave;s cette semaine au gr&eacute; de la m&eacute;t&eacute;o ! Pour ces m&ecirc;mes r&eacute;gions, les vari&eacute;t&eacute;s plus tardives ne se situent qu&rsquo;entre 24 et 30 % de mati&egrave;re s&egrave;che. Vu la m&eacute;t&eacute;o annonc&eacute;e, le stade optimal de r&eacute;colte ne sera atteint qu&rsquo;&agrave; partir du 25 septembre dans les meilleures situations, voire d&eacute;but octobre dans les cas moins favorables, notamment les parcelles sem&eacute;es plus tard en mai.</p>

<p><u>En conclusion</u>, la m&eacute;t&eacute;o automnale de ces prochains jours va donc perturber l&rsquo;organisation des chantiers de r&eacute;colte des ma&iuml;s arrivant pourtant &agrave; maturit&eacute; en Basse et Moyenne Belgique tandis qu&rsquo;elle risque de retarder la maturit&eacute; au sud du sillon Sambre-et-Meuse. D&rsquo;autant plus que les temp&eacute;ratures minimales risquent d&rsquo;encore baisser &agrave; partir du prochain week-end. D&egrave;s lors, la situation au sud du pays n&eacute;cessitera un suivi minutieux dont les r&eacute;sultats seront publi&eacute;s lors des prochains communiqu&eacute;s.</p>

<p><strong>En Ardenne</strong>, les suivis vont &eacute;galement d&eacute;buter.</p>
', 1, 352, 6, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (11, 'Sélectivité de nouvelles associations sur les principaux nouveaux hybrides', 'right', 300, 225, '<p>Cet essai a permis de comparer la s&eacute;lectivit&eacute; du Samson extra 60 OD &agrave; celle du Samson 4 SC seul ou en associations sur 30 hybrides. Les 4 associations sont parfaitement s&eacute;lectives sur tous les hybrides. Les doubles doses de Samson 4 SC ou de Samson Extra 60 OD induisent des marbrures assez prononc&eacute;es sur les vari&eacute;t&eacute;s Ajaxx, Asteri CS, DKC2960. Cependant, les d&eacute;formations et d&eacute;colorations observ&eacute;es lors du premier relev&eacute; n&#39;ont eu aucune incidence sur la croissance du ma&iuml;s. En effet, une visite ult&eacute;rieure (le 20 juillet) n&#39;a mis en &eacute;vidence aucune diff&eacute;rence de taille entre les plantes trait&eacute;es et les t&eacute;moins.<br />
&nbsp;</p>

<p>Vulgarisation</p>

<ul>
	<li>Divers articles ont &eacute;t&eacute; publi&eacute;s dans la presse agricole (cfr n&deg; sp&eacute;ciaux Sillon belge mars 2007, techniques culturales et autre).</li>
	<li>Une consultance t&eacute;l&eacute;phonique a &eacute;t&eacute; assur&eacute;e.</li>
	<li>Centre s&rsquo;est d&eacute;plac&eacute; dans diverses exploitations pour assurer des recommandations.</li>
	<li>Un colloque &laquo; conseil de printemps &raquo; a &eacute;t&eacute; organis&eacute; le 1er mars et a accueilli +/- 115 personnes.</li>
</ul>
', 2, 24, 6, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (13, 'Séance d''information colza', 'left', 300, 225, '<p><strong>Quand ?</strong> Lundi 29/01/2018, 14h30 &agrave; 17h00</p>

<p><strong>O&ugrave; ?</strong>&nbsp;&nbsp;Auditoire de biologie v&eacute;g&eacute;tale, avenue Mar&eacute;chal juin, 4 &agrave; Gembloux</p>

<p><strong>Phytolicence ?</strong> <span style="color:#ff0000">Inscription de 13h30 &agrave; 14h30</span>.<strong> </strong>S&eacute;ance agr&eacute;&eacute;e comme module de formation pour la phytolicence.</p>

<p>Format pdf : <a href="https://www.cadcoasbl.be/p10_agenda/180129%20colza.pdf" target="_blank">Invitation.pdf</a></p>
', 3, 356, 7, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (18, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, dont r&eacute;cemment remis &agrave; jour : les <strong>traitements herbicides</strong>, &hellip;&nbsp; <a href="http://cadcoasbl.be/p09_biblio.html#art0002"><strong>ici</strong></a></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:right"><strong><u>Coordonnateur du CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 2, NULL, 9, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (15, 'Présentation du Livre Blanc céréales', 'left', 300, 225, '<p>Les deux s&eacute;ances sont <span style="color:#ff0000">agr&eacute;&eacute;es comme module de formation phytolicence</span>.</p>

<p><span style="color:#ff0000">Inscriptions ouvertes</span>, munissez-vous de votre carte d&#39;identit&eacute; :</p>

<ul>
	<li><span style="color:#ff0000">d&egrave;s 8h00 </span>pour la s&eacute;ance de 9h00.</li>
	<li><span style="color:#ff0000">d&egrave;s 13h00 </span>pour la s&eacute;ance de 14h00.</li>
</ul>

<p><strong>Quand ?</strong>&nbsp;Mercredi 21/02/2018, 9h00 <strong><u>ou </u></strong>14h00</p>

<p><strong>O&ugrave; ?</strong> Gembloux, Espace Senghor Avenue de la Facult&eacute; d&rsquo;Agronomie, 11 &agrave; Gembloux</p>

<p>Format pdf : <a href="https://www.cadcoasbl.be/p10_agenda/1802LB.pdf" target="_blank">invitation.pdf</a></p>
', 3, NULL, 8, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (14, 'Erreur message précédent', 'left', 300, 225, '<p>Toutes nos excuses pour le message pr&eacute;c&eacute;dent titr&eacute; &quot;CADCO - Actualit&eacute; - c&eacute;r&eacute;ales du 31 octobre 2017 (C26)&quot;.</p>

<p>Il s&#39;agit bien d&#39;une erreur de manipulation de notre part lors des tests d&#39;envois de courriels avec une nouvelle mise en page.</p>

<p>Pour ceux qui ne parviennent pas &agrave; ouvrir les liens propos&eacute;s &agrave; cause de leur anti-virus, il s&#39;agit d&#39;un ajout automatique li&eacute; &agrave; notre fournisseur de service d&#39;envoi de courriels. Nous cherchons une solution &agrave; ce probl&egrave;me. Sinon, rendez-vous sur le site&nbsp;<a href="http://r.bh.d.sendibt3.com/tr/cl/WFH4E0lR3exTz7FyCHa8JhMnDuh4pfjE2feHJAjnx50bhSmcH8vHSTv3LgpwbQ_0EFVnkipjgLSkOu2HzThcJnSe5nOpy7ixsSXcebUAJpwDkgJ-8OWC4KhHvxlBDEJGjJy_xh-13vVIgkDimIC6xiPejyXtg0TFBuAVRxeL3Gej?u=0e4a5076-dd8a-4167-b13f-705489658fb2" target="_blank">cadcoasbl.be</a>.</p>
', 2, NULL, 7, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (17, 'A la veille d’une nouvelle saison', 'left', 300, 225, '<p style="text-align:justify">Comme c&rsquo;est g&eacute;n&eacute;ralement le cas &agrave; la sortie de l&rsquo;hiver, les principales maladies en froment et en orge &eacute;taient bien pr&eacute;sentes sur les feuilles ces derni&egrave;res semaines. Les temp&eacute;ratures des 15 derniers jours ont acc&eacute;l&eacute;r&eacute; la senescence de ces vieilles feuilles symptomatiques. Il en r&eacute;sulte que les nouvelles feuilles qui apparaissent sont saines et les parcelles sont, de mani&egrave;re g&eacute;n&eacute;rale, dans un tr&egrave;s bon &eacute;tat sanitaire.</p>

<p style="text-align:justify">En froment, les quelques foyers de rouille brune qui ont pu &ecirc;tre observ&eacute;s ont disparu et cette maladie ne devrait pas poser de probl&egrave;me avant la fin du printemps. Les foyers de rouille jaune ont &eacute;galement disparu pour le moment, il faut donc attendre et rester attentif &agrave; l&rsquo;apparition &eacute;ventuelle de nouveaux foyers.</p>

<p style="text-align:justify">En orge, des petits d&eacute;g&acirc;ts de jaunisse (maladie virale transmise par puceron) ont pu &ecirc;tre observ&eacute;s mais ont &eacute;t&eacute; globalement bien trait&eacute;s. <em>A priori</em>, il ne reste pas de foyers actuellement et il ne devrait pas y avoir d&rsquo;infections secondaires.</p>

<p style="text-align:justify">Comme l&rsquo;ann&eacute;e derni&egrave;re, la mosa&iuml;que (maladie virale transmise par un microorganisme du sol) est parfois observ&eacute;e. Bien que tr&egrave;s ponctuelle, cette maladie doit &ecirc;tre bien surveill&eacute;e car elle pourrait s&rsquo;av&eacute;rer &ecirc;tre le plus gros probl&egrave;me en culture d&rsquo;orge cette ann&eacute;e. Le seul moyen de lutte contre la mosa&iuml;que est l&rsquo;utilisation de vari&eacute;t&eacute;s r&eacute;sistantes.</p>

<p style="text-align:justify">L&rsquo;exp&eacute;rience des derni&egrave;res ann&eacute;es nous a r&eacute;v&eacute;l&eacute; plusieurs &eacute;l&eacute;ments importants :</p>

<ul>
	<li style="text-align:justify">Ce sont r&eacute;ellement les conditions printani&egrave;res qui d&eacute;terminent le d&eacute;veloppement ult&eacute;rieur de la rouille jaune.</li>
	<li style="text-align:justify">La r&eacute;sistance &agrave; la rouille jaune ne s&rsquo;exprime pas toujours sur les plantes jeunes. Des sympt&ocirc;mes de rouille peuvent donc &ecirc;tre observ&eacute;s sur des plantules de vari&eacute;t&eacute;s r&eacute;sistantes mais disparaitront sur les plantes plus &acirc;g&eacute;es.</li>
	<li style="text-align:justify">Les traitements h&acirc;tifs avant le stade 31 (premier n&oelig;ud) n&rsquo;ont pas &eacute;t&eacute; utiles. Il faut donc <strong>attendre le stade 31</strong> avant d&rsquo;&eacute;valuer l&rsquo;int&eacute;r&ecirc;t ou non d&rsquo;un traitement, m&ecirc;me sur les vari&eacute;t&eacute;s sensibles. Nous vous informerons en temps utile de l&rsquo;&eacute;volution de la situation. Nous profitons de ce message pour vous rappeler l&rsquo;importance de suivre l&rsquo;&eacute;volution de l&rsquo;&eacute;tat phytosanitaire de vos parcelles afin d&rsquo;adapter nos conseils aux situations particuli&egrave;res.</li>
</ul>

<p style="text-align:justify"><u>Quelle est la strat&eacute;gie du r&eacute;seau CADCO </u>?</p>

<p style="text-align:justify">1&deg;) Nos observations de terrain : Nous suivrons les champs de notre r&eacute;seau en temps utile et vous informerons de l&rsquo;&eacute;volution de la situation, ainsi que des options possibles en cas de n&eacute;cessit&eacute; d&rsquo;agir.</p>

<p style="text-align:justify">2&deg;) L&rsquo;information vari&eacute;tale, un atout &agrave; bien utiliser. Nous remettrons en temps utile &agrave; votre disposition l&rsquo;information ad hoc.</p>

<p style="text-align:justify">3&deg;) Votre observation de terrain : Une observation cibl&eacute;e de votre champ est primordiale pour savoir au mieux &agrave; quel type de sc&eacute;nario d&eacute;tect&eacute; dans le r&eacute;seau CADCO peut se r&eacute;f&eacute;rer votre champ. Afin de vous faciliter la t&acirc;che, nous vous indiquerons comme par le pass&eacute; les p&eacute;riodes critiques o&ugrave; il sera opportun d&rsquo;aller voir l&rsquo;&eacute;tat des maladies dans son champ.</p>

<p style="text-align:justify">4&deg;) Intervenir ? En fonction de la situation : Vari&eacute;t&eacute;, pr&eacute;c&eacute;dent, pression en maladies constat&eacute;e, conditions m&eacute;t&eacute;o annonc&eacute;es, &hellip; une intervention pourra &ecirc;tre propos&eacute;e. A ce stade nous vous rappelons le conseil g&eacute;n&eacute;ral suivant : une seule SDHI/saison et au plus t&ocirc;t &agrave; la derni&egrave;re feuille ou &agrave; l&rsquo;&eacute;piaison.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte en collaboration avec C. Bataille</p>

<p style="text-align:right"><strong><u>Coordonnateur du CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 9, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (12, 'Présentation du Livre Blanc', 'right', 300, 225, '<p><strong>Quand ?</strong>&nbsp;Mercredi 21/02/2018, 9h00 <strong><u>ou </u></strong>14h00</p>

<p><strong>O&ugrave; ?</strong> Gembloux, Espace Senghor Avenue de la Facult&eacute; d&rsquo;Agronomie, 11 &agrave; Gembloux</p>

<p><strong>Phytolicence ?</strong> Demande d&#39;agr&eacute;ment comme module phytolicence en cours</p>

<p>Format pdf : <a href="https://www.cadcoasbl.be/p10_agenda/1802LB.pdf" target="_blank">invitation.pdf</a></p>
', 3, 355, 7, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (89, 'Recherche échantillons d''orge brassicole wallonne', 'left', 300, 225, '<p style="text-align:justify">Le Centre wallon de Recherches agronomiques (CRA-W) recherche &agrave; se procurer de l&#39;orge brassicole (printemps ou hiver ; conventionnelle ou bio) wallonne des quatre coins de la Wallonie pour des analyses ult&eacute;rieures dans le cadre d&#39;une &eacute;valuation de la qualit&eacute; de ces orges brassicoles et l&rsquo;accompagnement du d&eacute;veloppement de cette fili&egrave;re. Dans ce cadre, le CRA-W cherche des agriculteurs et stockeurs pour lui fournir des &eacute;chantillons de 10 kg d&rsquo;orge brassicole (printemps ou hiver) ou d&rsquo;orge fourrag&egrave;re de printemps de la r&eacute;colte 2018. Si, vous souhaitez participer &agrave; cette initiative et fournir des &eacute;chantillons au CRA-W pour<span style="background-color:white"> que ce projet puisse &ecirc;tre men&eacute; &agrave; bien, contactez Bruno Godin (<a href="mailto:b.godin@cra.wallonie.be">b.godin@cra.wallonie.be</a> ; 081 620 350 ; B&acirc;timent Henseval / Chauss&eacute;e de Namur, 24</span> / 5030 Gembloux)<span style="background-color:white">.</span></p>
', 2, NULL, 40, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (16, 'Jaunisse nanisante de l’orge : constat sortie d’hiver', 'left', 300, 225, '<p style="text-align:justify">Les premiers beaux jours seront mis &agrave; profit pour des observations visant &agrave; d&eacute;terminer si des pucerons ont surv&eacute;cu &agrave; l&rsquo;hiver dans les c&eacute;r&eacute;ales.</p>

<p style="text-align:justify">Sur la vari&eacute;t&eacute; d&rsquo;escourgeon Rafaela et Domino, tol&eacute;rantes au virus de la jaunisse nanisante, la pr&eacute;sence de pucerons ne pr&eacute;sente aucun risque.</p>

<p style="text-align:justify"><span style="color:black">Le prochain avertissement est pr&eacute;vu pour le 20 mars.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>

<p style="text-align:right"><strong><u>Coordonnateur du CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 9, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (77, 'Recherche échantillons d''orge brassicole wallonne', 'left', 300, 225, '<p style="text-align:justify">Le Centre wallon de Recherches agronomiques (CRA-W) recherche &agrave; se procurer de l&#39;orge brassicole (printemps ou hiver ; conventionnelle ou bio) wallonne des quatre coins de la Wallonie pour des analyses ult&eacute;rieures dans le cadre d&#39;une &eacute;valuation de la qualit&eacute; de ces orges brassicoles et l&rsquo;accompagnement du d&eacute;veloppement de cette fili&egrave;re. Dans ce cadre, le CRA-W cherche des agriculteurs et stockeurs pour lui fournir des &eacute;chantillons de 10 kg d&rsquo;orge brassicole (printemps ou hiver) ou d&rsquo;orge fourrag&egrave;re de printemps de la r&eacute;colte 2018. Si, vous souhaitez participer &agrave; cette initiative et fournir des &eacute;chantillons au CRA-W pour<span style="background-color:white"> que ce projet puisse &ecirc;tre men&eacute; &agrave; bien, contactez Bruno Godin (</span><a href="mailto:b.godin@cra.wallonie.be" target="_blank"><span style="background-color:white">b.godin@cra.wallonie.be</span></a><span style="background-color:white"> ; 081 620 350 ; B&acirc;timent Henseval / Chauss&eacute;e de Namur, 24</span> / 5030 Gembloux)<span style="background-color:white">.</span></p>
', 2, NULL, 38, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (19, 'Fumures de référence', 'left', 300, 225, '<p>Quelques conseils pour l&rsquo;application de la premi&egrave;re fraction :</p>

<ul>
	<li>attendre une franche reprise de la v&eacute;g&eacute;tation&nbsp; et que les sols soient ressuy&eacute;s pour &eacute;viter des pertes d&rsquo;engrais par d&eacute;nitrification</li>
	<li>ne pas exag&eacute;rer la dose car les cultures sont en bon &eacute;tat</li>
</ul>

<p><strong><u><span style="color:black">EN ESCOURGEON</span></u></strong></p>

<p><strong><span style="color:black">La fumure de r&eacute;f&eacute;rence est :</span></strong></p>

<p style="margin-left:18.0pt; margin-right:0cm"><span style="color:black">En vari&eacute;t&eacute; <strong>hybride</strong>, un total de 175 unit&eacute;s d&rsquo;N r&eacute;parties en :</span></p>

<ul>
	<li><span style="color:black">Fraction du tallage (1&egrave;re fraction) : 25 unit&eacute;s d&rsquo;N</span></li>
	<li><span style="color:black">Fraction du redressement (2&egrave;me fraction) : 75 unit&eacute;s d&rsquo;N</span></li>
	<li><span style="color:black">Fraction de la derni&egrave;re feuille (3&egrave;me fraction) : 75 unit&eacute;s d&rsquo;N</span></li>
</ul>

<p style="margin-left:18.0pt; margin-right:0cm"><span style="color:black">En vari&eacute;t&eacute; <strong>lign&eacute;e</strong>, un total de 160 unit&eacute;s d&rsquo;N r&eacute;parties en :</span></p>

<ul>
	<li><span style="color:black">Fraction du tallage (1&egrave;re fraction) : 55 N</span></li>
	<li><span style="color:black">Fraction du redressement (2&egrave;me fraction) : 55 N</span></li>
	<li><span style="color:black">Fraction de la derni&egrave;re feuille (3&egrave;me fraction) : 50N</span></li>
</ul>

<p><strong><u><span style="color:black">EN FROMENT </span></u></strong></p>

<p style="text-align:justify"><strong>Le sch&eacute;ma de fumure en 3 fractions est recommand&eacute; et il sera privil&eacute;gi&eacute; dans la majorit&eacute; des situations culturales</strong>. Le sch&eacute;ma de fumure en 2 fractions sera, par contre, privil&eacute;gi&eacute; dans les situations o&ugrave; les cultures pr&eacute;sentent au moins trois talles au d&eacute;but mars (semis pr&eacute;coces ou semis d&rsquo;octobre) et lorsque de l&rsquo;azote est disponible en quantit&eacute; suffisante (apports fr&eacute;quents de mati&egrave;re organique, pr&eacute;c&eacute;dent l&eacute;gumineuses, pr&eacute;c&eacute;dent pomme de terre, anciennes prairies).</p>

<p>&nbsp;</p>

<p><u><span style="color:black">En <strong>trois </strong>fractions</span></u><span style="color:black"> un total de 185 unit&eacute;s d&rsquo;N r&eacute;parties en :</span></p>

<p><span style="color:black">Fraction du tallage (1&egrave;re fraction) : 60 N</span></p>

<p><span style="color:black">Fraction du redressement (2&egrave;me fraction) : 60 N</span></p>

<p><span style="color:black">Fraction de la derni&egrave;re feuille (3&egrave;me fraction) : 65 N</span></p>

<p><u><span style="color:black">En <strong>deux </strong>fractions</span></u><span style="color:black"> un total de 185 unit&eacute;s d&rsquo;N r&eacute;parties en : </span></p>

<p><span style="color:black">Fraction interm&eacute;diaire &laquo; tallage - redressement&raquo; : 90 N</span></p>

<p><span style="color:black">Fraction de la derni&egrave;re feuille (2&egrave;me fraction) : 95 N</span></p>

<p><strong><u><span style="color:black">EN EPEAUTRE</span></u></strong><strong><span style="color:black">&nbsp;&nbsp; la fumure de r&eacute;f&eacute;rence est</span></strong></p>

<ul>
	<li><u><span style="color:black">R&eacute;gion limoneuse</span></u><span style="color:black"> :</span></li>
</ul>

<p style="margin-left:35.45pt; margin-right:0cm"><span style="color:black">Total 135 kg N/ha : 75 &ndash; 60 &ndash; 0</span></p>

<p style="margin-left:35.45pt; margin-right:0cm"><span style="color:black">Total 150 kg N/ha : 90 &ndash; 60 &ndash; 0</span></p>

<p style="margin-left:106.35pt; margin-right:0cm"><span style="color:black">+&nbsp; 30 kg N/ha &agrave; DF si on vise un suppl&eacute;ment prot&eacute;ine</span></p>

<ul>
	<li><u><span style="color:black">R&eacute;gion froide (Ardenne)</span></u><span style="color:black"> :</span></li>
</ul>

<p style="margin-left:35.45pt; margin-right:0cm"><span style="color:black">Total 105 kg N/ha : 60 &ndash; 45 &ndash; 0 </span></p>

<p style="margin-left:35.45pt; margin-right:0cm"><span style="color:black">Total 105 kg N/ha : 75 &ndash; 30 &ndash; 0</span></p>

<p style="margin-left:35.45pt; margin-right:0cm"><span style="color:black">Total 120 kg N/ha : 75 &ndash;45 &ndash; 0</span></p>

<p style="margin-left:106.35pt; margin-right:0cm"><span style="color:black">+&nbsp; 30 kg N/ha &agrave; DF si on vise un suppl&eacute;ment prot&eacute;ine</span></p>

<p style="text-align:justify"><strong><span style="color:black">Ces fumures de r&eacute;f&eacute;rence doivent &ecirc;tre modul&eacute;es en fonction des conditions particuli&egrave;res de la parcelle et de l&rsquo;&eacute;tat de la culture au moment de l&rsquo;application</span></strong><span style="color:black">. Plus d&rsquo;informations et pour d&eacute;terminer la dose optimale pour chaque situation, rendez-vous sur le site </span><a href="http://www.livre-blanc-cereales.be">www.livre-blanc-cereales.be</a><span style="color:black"> dans les rubriques &laquo; Th&eacute;matiques &ndash; Fumure &raquo; ou &laquo; Outils &raquo;.</span></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson et R. Meza</p>

<p style="text-align:right"><strong><u>Coordonnateur du CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 9, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (20, 'Le colza : Retour de la douceur et déjà les premières sorties d’insectes', 'left', 300, 225, '<p style="text-align:justify">Apr&egrave;s une fin f&eacute;vrier marqu&eacute;e par un gel intense accentu&eacute; par le vent polaire, le colza d&rsquo;hiver a perdu une partie de ses feuilles qui ont gel&eacute;. La culture red&eacute;marre en profitant des temp&eacute;ratures douces et de l&rsquo;absence de gel nocturne.</p>

<p style="text-align:justify">Contrairement aux pr&eacute;visions m&eacute;t&eacute;o annon&ccedil;ant de fortes pluies, la journ&eacute;e du dimanche 11 mars, tr&egrave;s douce et ensoleill&eacute;e, a &eacute;t&eacute; favorable aux vols d&rsquo;insectes. Les premi&egrave;res apparitions de m&eacute;lig&egrave;thes et de charan&ccedil;ons de la tige du chou ont &eacute;t&eacute; observ&eacute;es dans les pi&egrave;ges install&eacute;s dans le cadre du r&eacute;seau de pi&eacute;geage remis en place au printemps dans 30 champs de colza en R&eacute;gion<br />
wallonne.</p>

<p style="text-align:justify">Les premiers charan&ccedil;ons de la tige du chou ont &eacute;t&eacute; pi&eacute;g&eacute;s &agrave; Anth&eacute;e, Bois-de-Villers, Clermont, Den&eacute;e, Foy, Gembloux, Lesve, Morialm&eacute;, Soye et Stave. Les premiers m&eacute;lig&egrave;thes ont &eacute;t&eacute; pi&eacute;g&eacute;s &agrave; Anth&eacute;e, Bois-de-Villers, Clermont, Den&eacute;e, Foy, Gembloux, Leignon, Lesve, Morialm&eacute;, Rhisnes et Sauveni&egrave;re.</p>

<p style="text-align:justify">Le colza d&rsquo;hiver n&rsquo;est actuellement pas &agrave; un stade sensible vis-&agrave;-vis de ces insectes, d&rsquo;autant plus que les temp&eacute;ratures annonc&eacute;es pour les prochains jours vont baisser. En cours de montaison, c&rsquo;est essentiellement le charan&ccedil;on de la tige qui sera &agrave; surveiller. Les m&eacute;lig&egrave;thes ne trouvent pas encore de boutons floraux visibles ; ils sont donc inoffensifs.</p>

<p style="text-align:justify">La surveillance du colza et des insectes ravageurs sera assur&eacute;e au cours des prochaines semaines et un point de la situation sera r&eacute;guli&egrave;rement communiqu&eacute;.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>
', 1, NULL, 10, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (22, 'Jaunisse nanisante de l’orge : constat sortie d’hiver', 'left', 300, 225, '<p style="text-align:justify">L&rsquo;hiver, d&rsquo;abord marqu&eacute; par de fortes pluies battantes, et ensuite par des p&eacute;riodes de froid assez s&eacute;v&egrave;re, n&rsquo;a pas &eacute;t&eacute; favorable &agrave; la survie des pucerons. Par ailleurs, parmi les pucerons collect&eacute;s au champ au cours de l&rsquo;automne, une faible proportion s&rsquo;&eacute;tait r&eacute;v&eacute;l&eacute;e porteuse du virus. Le risque d&rsquo;extension post-hivernale de la virose est donc tr&egrave;s faible.</p>

<p style="text-align:justify">Jusqu&rsquo;&agrave; pr&eacute;sent, les conditions m&eacute;t&eacute;orologiques n&rsquo;ont pas permis de mesurer correctement l&rsquo;&eacute;ventuelle infestation r&eacute;siduaire. Ce sera chose faite d&egrave;s les premiers beaux jours et fera l&rsquo;objet d&rsquo;un prochain avertissement.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>
', 1, NULL, 11, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (21, 'Plate-forme de diffusion des avertissements, améliorations en cours', 'left', 300, 225, '<p>Nous mettons en &oelig;uvre plusieurs am&eacute;liorations de la plateforme qui nous sert &agrave; notamment diffuser les avertissements et pouvons conna&icirc;tre certains disfonctionnements &agrave; cause de ces changements.</p>

<p>N&#39;h&eacute;sitez pas &agrave; rapporter, le cas &eacute;ch&eacute;ant, tout probl&egrave;me que vous pourriez rencontrer</p>

<p>&agrave; <span style="color:#0000cd">cadcoasbl@cadcoasbl.be</span></p>

<p>nous y regarderons au plus vite.</p>

<p>&nbsp;</p>

<p><span style="color:#00b050">Le prochain avertissement est pr&eacute;vu pour le 27 mars</span></p>
', 2, NULL, 11, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (80, 'Recherche échantillons d''orge brassicole wallonne', 'left', 300, 225, '<p style="text-align:justify">Le Centre wallon de Recherches agronomiques (CRA-W) recherche &agrave; se procurer de l&#39;orge brassicole (printemps ou hiver ; conventionnelle ou bio) wallonne des quatre coins de la Wallonie pour des analyses ult&eacute;rieures dans le cadre d&#39;une &eacute;valuation de la qualit&eacute; de ces orges brassicoles et l&rsquo;accompagnement du d&eacute;veloppement de cette fili&egrave;re. Dans ce cadre, le CRA-W cherche des agriculteurs et stockeurs pour lui fournir des &eacute;chantillons de 10 kg d&rsquo;orge brassicole (printemps ou hiver) ou d&rsquo;orge fourrag&egrave;re de printemps de la r&eacute;colte 2018. Si, vous souhaitez participer &agrave; cette initiative et fournir des &eacute;chantillons au CRA-W pour<span style="background-color:white"> que ce projet puisse &ecirc;tre men&eacute; &agrave; bien, contactez Bruno Godin (</span><a href="mailto:b.godin@cra.wallonie.be" target="_blank"><span style="background-color:white">b.godin@cra.wallonie.be</span></a><span style="background-color:white"> ; 081 620 350 ; B&acirc;timent Henseval / Chauss&eacute;e de Namur, 24</span> / 5030 Gembloux)<span style="background-color:white">.</span></p>
', 2, NULL, 39, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (23, 'Désherbage printanier', 'left', 300, 225, '<p style="text-align:justify"><strong><u>Conseils pour le d&eacute;sherbage printanier</u></strong></p>

<p><u>L&rsquo;observation,&hellip; base du bon choix</u></p>

<p style="text-align:justify">Avant tout traitement, il est indispensable de v&eacute;rifier l&rsquo;&eacute;tat de la culture et d&rsquo;identifier les adventices en pr&eacute;sence.</p>

<p style="text-align:justify">Afin d&rsquo;&eacute;viter tout effet phytotoxique, la c&eacute;r&eacute;ale ne doit pas &ecirc;tre d&eacute;chauss&eacute;e et atteindre le stade d&eacute;but tallage (la premi&egrave;re talle doit &ecirc;tre visible).</p>

<p style="text-align:justify">Toujours pour &eacute;viter ces probl&egrave;mes de phytotoxicit&eacute;, il convient de ne pas traiter en p&eacute;riode de gel, m&ecirc;me nocturne.</p>

<p style="text-align:justify">Les produits &agrave; utiliser devraient &ecirc;tre d&eacute;termin&eacute;s en fonction des adventices pr&eacute;sentes dans la parcelle.</p>

<p style="text-align:justify">La dose d&rsquo;emploi des produits choisis peut, quant &agrave; elle, &ecirc;tre raisonn&eacute;e selon le niveau d&rsquo;infestation et le stade de d&eacute;veloppement des adventices.</p>

<p><strong><u>Froment et &eacute;peautre</u></strong> :</p>

<p style="text-align:justify">Il y a de fortes chances que les adventices soient bien d&eacute;velopp&eacute;es dans les premiers semis.</p>

<p style="text-align:justify">Dans ce cas, il ne faut pas trop attendre et pr&eacute;f&eacute;rer l&rsquo;utilisation de produits &agrave; p&eacute;n&eacute;tration foliaire, tant contre les gramin&eacute;es que contre les dicotyl&eacute;es.</p>

<p style="text-align:justify">Sur des semis plus tardifs, en pr&eacute;sence d&rsquo;adventices peu d&eacute;velopp&eacute;es, l&rsquo;emploi de produits racinaires pourrait s&rsquo;av&eacute;rer suffisant.</p>

<p style="text-align:justify"><strong><u>Escourgeon</u></strong> :</p>

<p style="text-align:justify">Si un rattrapage de printemps est n&eacute;cessaire contre les gramin&eacute;es, les produits &agrave; base de pinoxaden (AXEO, AXIAL) ou de fenoxaprop (FOXTROT) constituent la seule solution.</p>

<p style="text-align:justify">En effet, ces anti-gramin&eacute;es sp&eacute;cifiques devraient &ecirc;tre plus efficaces, sur des gramin&eacute;es probablement forts d&eacute;velopp&eacute;es, que les produits &agrave; base de chlortoluron.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson et R. Meza et F. Henriet</p>

<p style="text-align:justify">Vous trouverez <a href="http://cadcoasbl.be/p09_biblio.html#art0002"><strong>ici</strong></a> les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, dont r&eacute;cemment remis &agrave; jour : les <strong>traitements herbicides</strong>, &hellip;&nbsp;</p>
', 1, NULL, 11, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (25, 'Lutte contre les maladies, enseignements du passé', 'left', 300, 225, '<p style="text-align:justify">Les conditions climatiques des derni&egrave;res semaines et celles annonc&eacute;es prochainement ne favorisent pas le d&eacute;veloppement des cultures, ni celui des maladies. Le d&eacute;veloppement du bl&eacute; est en retard par rapport aux derni&egrave;res ann&eacute;es et il est encore trop t&ocirc;t pour observer ses parcelles.</p>

<p style="text-align:justify">Pour le froment, la question du premier traitement ne doit pas se poser avant le stade 32 (2&egrave;me n&oelig;ud), sauf dans certains &eacute;pisodes rares d&rsquo;&eacute;pid&eacute;mie de rouille jaune au stade 31 (montaison). Dans 75% des essais du r&eacute;seau d&rsquo;essais fongicides wallons, un traitement &agrave; la montaison a abouti &agrave; une perte nette. Il faut atteindre des prix du bl&eacute; de 260&euro;/T pour que ce traitement soit rentable, et ce seulement dans la moiti&eacute; des essais ! Avec un prix du bl&eacute; inf&eacute;rieur &agrave; 150&euro;/T, la r&egrave;gle est simple : <strong>au stade 31, traitement seulement si pr&eacute;sence de foyers actifs de rouille jaune sur une vari&eacute;t&eacute; sensible.</strong></p>

<p style="text-align:justify">Ces cinq derni&egrave;res ann&eacute;es, le traitement a souvent pu &ecirc;tre report&eacute; au stade 39 (derni&egrave;re feuille) si la parcelle respectait deux conditions :</p>

<ul>
	<li style="text-align:justify">La parcelle est emblav&eacute;e avec une vari&eacute;t&eacute; tol&eacute;rante &agrave; la septoriose</li>
	<li style="text-align:justify">Si la vari&eacute;t&eacute; emblav&eacute;e est peu tol&eacute;rante &agrave; la rouille jaune, il n&rsquo;y a pas encore de sympt&ocirc;mes de rouille jaune visibles au stade 32.</li>
</ul>

<p style="text-align:justify">Rappelons &eacute;galement les enseignements de 2017 : avec une faible pression des maladies au stade 32 (2&egrave;me n&oelig;ud), le report du traitement au stade 39 (derni&egrave;re feuille) a permis d&rsquo;&eacute;tablir un programme en un seul passage qui maximisait le rendement net dans la plupart des parcelles. Commencer t&ocirc;t n&rsquo;est donc pas syst&eacute;matiquement synonyme de r&eacute;ussite et un programme en un seul passage est une possibilit&eacute; r&eacute;elle !&nbsp;</p>

<p style="text-align:justify">Pour l&rsquo;escourgeon, un traitement au stade 39 (derni&egrave;re feuille) est souvent bien valoris&eacute;. Le programme pourra &ecirc;tre adapt&eacute; suivant la pression en maladies, &agrave; la lumi&egrave;re des avis du CADCO. Nous vous tiendrons bien s&ucirc;r inform&eacute;s des pressions observ&eacute;es dans nos parcelles d&rsquo;essais.&nbsp;</p>

<p style="text-align:justify">Pour voir la sensibilit&eacute; d&rsquo;une vari&eacute;t&eacute; &agrave; tel ou telle maladie, consulter <a href="http://www.livre-blanc-cereales.be/thematiques/varietes/froment/fiches-varietes/"><strong>ici</strong></a></p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 12, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (24, 'Jaunisse nanisante de l’orge : constat sortie d’hiver', 'left', 300, 225, '<p style="text-align:justify">De notre r&eacute;seau d&rsquo;observation il ressort qu&rsquo;en escourgeon et froment, l&#39;hiver a eu un effet insecticide : les pucerons n&rsquo;y ont pas surv&eacute;cu.</p>

<p style="text-align:justify">N&rsquo;h&eacute;sitez pas &agrave; nous transmettre toute observation ou toute information que vous jugeriez utile.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>
', 1, NULL, 12, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (26, 'Plate-forme de diffusion des avertissements, améliorations en cours', 'left', 300, 225, '<p style="text-align:justify">Nous mettons en &oelig;uvre plusieurs am&eacute;liorations de la plateforme qui nous sert &agrave; notamment diffuser les avertissements et pouvons conna&icirc;tre certains disfonctionnements &agrave; cause de ces changements.</p>

<p style="text-align:justify">Nous vous demandons de rapporter, le cas &eacute;ch&eacute;ant, tout probl&egrave;me que vous pourriez rencontrer &agrave; cadcoasbl@cadcoasbl.be nous y regarderons au plus vite. D&eacute;sol&eacute; pour ces d&eacute;sagr&eacute;ments.</p>
', 2, NULL, 12, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (27, 'Le colza : Les charançons de la tige sont à surveiller au cours de la montaison', 'left', 300, 225, '<p style="text-align:justify">Le colza d&rsquo;hiver pr&eacute;sente des d&eacute;veloppements divers selon les parcelles. Le gel tardif et le froid en mars avec des sols toujours froids, ont retard&eacute; la croissance du colza. La montaison est maintenant bien engag&eacute;e.</p>

<p style="text-align:justify">Le retour de temp&eacute;ratures douces et du soleil bien g&eacute;n&eacute;reux au cours du dernier week-end ont favoris&eacute; la sortie des insectes. De nombreux m&eacute;lig&egrave;thes ont &eacute;t&eacute; captur&eacute;s partout dans les bassins jaunes, en nombre variable (de 1 &agrave; 313 adultes) mais la majorit&eacute; des champs de colza d&rsquo;hiver ne sont pas encore arriv&eacute;s au stade de sensibilit&eacute; vis-&agrave;-vis des m&eacute;lig&egrave;thes. Peu d&rsquo;insectes ont &eacute;t&eacute; retrouv&eacute;s<br />
sur les plantes.</p>

<p style="text-align:justify">En cours de montaison, c&rsquo;est surtout le charan&ccedil;on de la tige qu&rsquo;il faut surveiller. Quelques individus ont &eacute;galement &eacute;t&eacute; pi&eacute;g&eacute;s dans 24 champs sur 29 du r&eacute;seau d&rsquo;observations. D&rsquo;apr&egrave;s leur d&eacute;termination, il s&rsquo;agit surtout de charan&ccedil;on de la tige du chou avec le bout des pattes rousses. Le charan&ccedil;on de la tige du colza, avec le bout des pattes noires, le plus dangereux des charan&ccedil;ons de la tige &agrave; cause des piq&ucirc;res dans la tige, n&rsquo;a &eacute;t&eacute; observ&eacute; que dans 1 champ sur 5.</p>

<p style="text-align:justify"><br />
Le bassin jaune surmont&eacute; gr&acirc;ce &agrave; un support et install&eacute; dans la culture, est le moyen le plus facile de suivre l&rsquo;&eacute;volution dans ses champs, en plus des observations faites dans le cadre du r&eacute;seau d&rsquo;observations.</p>

<p style="text-align:justify">La pluie et le vent annonc&eacute;s pour les prochains jours vont sans doute &agrave; nouveau ralentir l&rsquo;activit&eacute; des insectes. La surveillance de ces insectes ravageurs se poursuivra lorsque les conditions climatiques se radouciront avec l&rsquo;arriv&eacute;e du printemps.</p>

<p style="text-align:justify"><br />
Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p style="text-align:justify"><br />
Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</p>
', 1, NULL, 13, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (29, 'Le colza : La culture change et toujours peu d’insectes jusqu’à présent', 'left', 300, 225, '<p style="text-align:justify">Les gel&eacute;es nocturnes sont termin&eacute;es, les temp&eacute;ratures sont plus douces et les journ&eacute;es rallongent, ce qui permet au colza d&rsquo;hiver de se d&eacute;velopper. Comme pr&eacute;vu, les fr&eacute;quentes averses accompagn&eacute;es ou non de vent au cours de la semaine &eacute;coul&eacute;e, ont contrari&eacute; les vols d&rsquo;insectes. Tr&egrave;s peu de m&eacute;lig&egrave;thes et de charan&ccedil;ons ont &eacute;t&eacute; captur&eacute;s, voire aucun dans certains bassins.</p>

<p style="text-align:justify">La m&eacute;t&eacute;o nous pr&eacute;dit un net r&eacute;chauffement pour la fin de semaine avec des temp&eacute;ratures pouvant atteindre 20&deg;C le week-end prochain, ce qui pourrait favoriser les vols d&rsquo;insectes.</p>

<p style="text-align:justify">Si le pi&egrave;ge, par sa couleur jaune, est tr&egrave;s attractif, il faut en plus de l&rsquo;observation des insectes pi&eacute;g&eacute;s, regarder les plantes pour voir la pr&eacute;sence d&rsquo;insectes ravageurs (m&eacute;lig&egrave;thes et charan&ccedil;ons de la tige).</p>

<p style="text-align:justify">Les colzas les plus d&eacute;velopp&eacute;s pr&eacute;sentent actuellement des boutons d&eacute;gag&eacute;s des feuilles et sont donc vuln&eacute;rables aux attaques de m&eacute;lig&egrave;thes &agrave; la recherche de pollen dans les boutons.</p>

<p style="text-align:justify">Si, au stade boutons accol&eacute;s, le nombre moyen de m&eacute;lig&egrave;thes d&eacute;passe 3 &agrave; 4 par plante, un traitement insecticide sera envisag&eacute;.</p>

<p style="text-align:justify">Si le colza a souffert du gel et est plus faible ou retard&eacute; au niveau de son d&eacute;veloppement, il faut &eacute;galement observer la pr&eacute;sence de m&eacute;lig&egrave;thes. Dans ce cas, un traitement est envisag&eacute; s&rsquo;il y a 1 m&eacute;lig&egrave;the par plante en moyenne, en comptant une quarantaine de plantes.</p>

<p style="text-align:justify">Il est inutile de traiter en l&rsquo;absence d&rsquo;insectes ou si les seuils ne sont pas atteints. Une v&eacute;rification dans chaque parcelle de colza est indispensable. C&rsquo;est le meilleur moyen de garder des solutions efficaces contre les insectes ravageurs, car le nombre de modes d&rsquo;action des produits autoris&eacute;s va se r&eacute;duire.</p>

<p style="text-align:justify">La surveillance continue et sera renforc&eacute;e lors des hausses de temp&eacute;rature annonc&eacute;es.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p style="text-align:right">Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</p>
', 1, NULL, 15, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (28, 'Maladies en céréales', 'left', 300, 225, '<p style="text-align:justify"><em><u>Thyphula</u></em></p>

<p>Photo : Scl&eacute;rotes caract&eacute;ristiques de Typhula.</p>

<p>Des d&eacute;g&acirc;ts occasionn&eacute;s par <em>Typhula</em> nous ont &eacute;t&eacute; signal&eacute;s la semaine derni&egrave;re. <em>Typhula</em> est un genre de champignon inf&eacute;od&eacute; au sol qui infecte le collet et les gaines foliaires des c&eacute;r&eacute;ales d&rsquo;hiver, en particulier l&rsquo;orge. Apr&egrave;s l&rsquo;attaque, un jaunissement ou un d&eacute;p&eacute;rissement de la jeune plante est observable &agrave; la reprise de la v&eacute;g&eacute;tation. Les plantes touch&eacute;es s&rsquo;arrachent facilement, leurs racines secondaires &eacute;tant d&eacute;truites. Le plateau de tallage prend un aspect filandreux et une pourriture humide se d&eacute;veloppe. Des structures caract&eacute;ristiques ais&eacute;ment reconnaissables appel&eacute;es scl&eacute;rotes sont visibles au niveau des gaines foliaires (photo 1). Celles-ci sont arrondies de couleur brun-rouge et d&rsquo;une taille pouvant varier de 0.5 &agrave; 5 mm. Les sympt&ocirc;mes se manifestent au champ sous forme de grandes plages de plantes ou par lignes de semis, particuli&egrave;rement sur des sols l&eacute;gers. A l&rsquo;instar d&rsquo;autres champignons pr&eacute;sents dans le sol comme les <em>Pythium</em> spp., <em>Typhula</em> est un parasite de faiblesse qui s&rsquo;attaque en hiver &agrave; des plantes physiologiquement faibles, principalement dans les cas de semis pr&eacute;coces et sur sols l&eacute;gers. Il infecte les plantes en automne et se d&eacute;veloppe lorsque les temp&eacute;ratures sont basses, apr&egrave;s une couverture neigeuse et une humidit&eacute; importante du sol. Les sympt&ocirc;mes n&rsquo;apparaissent que lors de la reprise de v&eacute;g&eacute;tation et sont dus au faible renouvellement racinaire des plantes infect&eacute;es. Cette maladie est rarement &agrave; l&rsquo;origine de d&eacute;g&acirc;ts significatifs. Dans les situations o&ugrave; une attaque de <em>Typhula</em> est rep&eacute;r&eacute;e, les d&eacute;g&acirc;ts ont d&eacute;j&agrave; &eacute;t&eacute; caus&eacute;s et <strong>aucune intervention n&rsquo;est possible</strong>.</p>

<p style="text-align:justify"><u>Autres maladies</u></p>

<p style="text-align:justify">La pr&eacute;sence de sympt&ocirc;mes d&rsquo;autres maladies (septoriose et o&iuml;dium majoritairement) dans le bas de plantes &agrave; la sortie de l&rsquo;hiver est fr&eacute;quente &agrave; ce moment de l&rsquo;ann&eacute;e et ne pr&eacute;sage pas de la pression en maladie au printemps. Les conditions climatiques &agrave; venir d&eacute;termineront le d&eacute;veloppement ult&eacute;rieur de ces maladies sur les nouvelles feuilles. Les cultures sont encore peu avanc&eacute;es par rapport aux derni&egrave;res ann&eacute;es, il est encore trop t&ocirc;t pour r&eacute;aliser les premi&egrave;res observations.</p>

<p style="text-align:justify"><strong>Un point sur la situation phytosanitaire sera r&eacute;alis&eacute; le 10 avril prochain.</strong></p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>

<p style="text-align:right"><strong><u>Coordonnateur du CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, 357, 14, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (31, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, dont r&eacute;cemment remis &agrave; jour : une mise &agrave; jour du 09 avril de la liste des r&eacute;gulateurs de croissance autoris&eacute;s est disponible via ce lien : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a></p>

<p style="text-align:justify"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 16, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (32, 'Maladies en escourgeon ', 'left', 300, 225, '<p>&nbsp;</p>

<p style="text-align:center"><img align="" filer_id="359" height="162" src="/filer/canonical/1523357731/359/" thumb_option="" title="" width="756" /></p>

<p><strong><u>R&eacute;sum&eacute;</u></strong></p>

<p style="text-align:justify">La majorit&eacute; des parcelles est actuellement au stade 1er n&oelig;ud. L&rsquo;helminthosporiose et la rhynchosporiose sont observ&eacute;es dans quelques champs mais le niveau d&rsquo;infection ne n&eacute;cessite pas de traitement. Seule la pr&eacute;sence de la rouille naine sur certaines parcelles peut &ecirc;tre pr&eacute;occupante.</p>

<p><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify">Ce lundi 09 avril, 31% des parcelles du r&eacute;seau ont atteint le stade ph&eacute;nologique &laquo; &eacute;pi 1cm &raquo; (BBCH 30) et un quart des parcelles est au stade &laquo; 2&egrave;me n&oelig;ud &raquo; (BBCH 32). Plus de la moiti&eacute; des parcelles sont au stade &laquo; 1er n&oelig;ud &raquo; (BBCH 31).</p>

<p style="text-align:center"><img align="" filer_id="358" height="" src="/filer/canonical/1523357550/358/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u>&nbsp;</p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a><strong> </strong>est observ&eacute;e &agrave; Ath, B&eacute;clers et Tinlot, soit dans 9 des 13 parcelles du r&eacute;seau CADCO. Dans ces parcelles, elle est visible sur la F-2 &agrave; des fr&eacute;quences inf&eacute;rieures &agrave; 10%, m&ecirc;me sur vari&eacute;t&eacute;s sensibles. Le pourcentage de surface foliaire touch&eacute; est tr&egrave;s faible dans tous les cas.</p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rhynchosporiose</strong></a><span style="color:black"> est pr&eacute;sente dans 12 des 13 parcelles, mais la proportion de plantes touch&eacute;es est faible (maximum 8% des plantes observ&eacute;es et seulement sur les F-2 ou dans le fond de v&eacute;g&eacute;tation). Cette maladie n&rsquo;est donc pas pr&eacute;occupante pour l&rsquo;instant.</span></p>

<p style="text-align:justify">La <a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille naine</strong></a> est pr&eacute;sente dans le Hainaut &agrave; Ath et B&eacute;clers :&nbsp; plus de 50% des F-2 sont touch&eacute;es sur les vari&eacute;t&eacute;s sensibles ou moyennement tol&eacute;rantes. Dans la r&eacute;gion de Li&egrave;ge, cette maladie a &eacute;t&eacute; observ&eacute;e sur les F-2 de vari&eacute;t&eacute;s sensibles (10 &agrave; 35%) &agrave; Tinlot et Kemexhe ou dans le fond de v&eacute;g&eacute;tation &agrave; Milmort sur vari&eacute;t&eacute; tol&eacute;rante. Cette maladie n&rsquo;a pas &eacute;t&eacute; observ&eacute;e dans nos parcelles &agrave; Assesse et Nam&ecirc;che.</p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>o&iuml;dium</strong></a><span style="color:black"> est observ&eacute; dans 10 des 13 parcelles du r&eacute;seau, mais seulement dans le fond de v&eacute;g&eacute;tation ou sur 3 &agrave; 4% des F-2. Cette maladie n&rsquo;est donc pas pr&eacute;occupante &agrave; ce stade.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p><strong><u>Recommandations</u></strong></p>

<p style="text-align:justify">L&rsquo;&eacute;tat sanitaire des parcelles du r&eacute;seau d&rsquo;observation est en g&eacute;n&eacute;ral assez bon. Seule la pression en rouille naine dans certaines parcelles, en particulier dans le Hainaut, pourrait &ecirc;tre pr&eacute;occupante et n&eacute;cessiter un traitement au stade 31. En effet, rappelons que pour lutter contre les maladies fongiques de l&rsquo;escourgeon, <span style="color:black">un traitement unique au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH 39) est la solution g&eacute;n&eacute;ralement la plus adapt&eacute;e.&nbsp; Pour les parcelles ayant atteint le stade 1er n&oelig;ud (BBCH 31), un traitement peut n&eacute;anmoins &ecirc;tre envisag&eacute; si la vari&eacute;t&eacute; emblav&eacute;e est fortement sensible &agrave; une maladie pr&eacute;sente dans le champ au-dessus d&rsquo;un seuil de nuisibilit&eacute;. Dans le cas de la rouille naine, ce seuil est atteint si plus de 10% des trois derni&egrave;res feuilles sont atteintes pour les vari&eacute;t&eacute;s sensibles et plus de 50% des trois derni&egrave;res feuilles sont atteintes pour les vari&eacute;t&eacute;s tol&eacute;rantes.&nbsp; </span></p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>

<p style="text-align:justify">&nbsp;</p>
', 1, NULL, 16, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (30, 'Point phytotechnique en escourgeon et froment', 'left', 300, 225, '<p>&nbsp;</p>

<p><strong><u>EN ESCOURGEON</u></strong></p>

<p><u>R&eacute;gulateur</u></p>

<p style="text-align:justify"><span style="color:black">Le d&eacute;veloppement des cultures d&rsquo;escourgeon est un peu en retard par rapport aux ann&eacute;es pr&eacute;c&eacute;dentes, m&ecirc;me si les plus avanc&eacute;s sont au stade 2 n&oelig;uds.</span></p>

<p style="text-align:justify"><span style="color:black">Des traitements r&eacute;gulateurs sont d&eacute;j&agrave;&nbsp; r&eacute;glementairement possibles, il faut cependant ne pas perdre de vue que les traitements les plus efficaces sont ceux effectu&eacute;s plus tard dans le d&eacute;veloppement de la culture (stade derni&egrave;re feuille).&nbsp; Un traitement &agrave; ce stade pr&eacute;coce implique souvent de devoir revenir plus tard au stade derni&egrave;re feuille. Compte tenu du prix des c&eacute;r&eacute;ales et du retard de d&eacute;veloppement de la culture, un programme de deux traitements &laquo; r&eacute;gulateur &raquo; n&rsquo;est pas conseill&eacute;.</span></p>

<p>&nbsp;</p>

<p><strong><u>EN FROMENT</u></strong></p>

<p><u>Fumure et r&eacute;gulateur</u></p>

<p style="text-align:justify">Seuls les semis de d&eacute;but octobre&nbsp; et les vari&eacute;t&eacute;s les plus&nbsp; pr&eacute;coces&nbsp; sont au stade &laquo; &eacute;pi &agrave; 1 cm &raquo;, les autres n&rsquo;atteignent pas encore ce stade.</p>

<p style="text-align:justify">Lorsque&nbsp; la culture sera bien au stade redressement, il y a lieu de penser &agrave; apporter la deuxi&egrave;me fraction de la fumure azot&eacute;e&nbsp; et &agrave; appliquer le traitement r&eacute;gulateur.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, Bernard Bodson et Benjamin Dumont</span></p>
', 1, NULL, 16, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (33, 'Le colza : Températures exceptionnellement élevées et arrivée massive de méligèthes et de charançons de la tige', 'left', 300, 225, '<p style="text-align:justify">La remont&eacute;e nette des temp&eacute;ratures et la tomb&eacute;e du vent lors du week-end printanier du 7 et 8 avril 2018 ont &eacute;t&eacute; b&eacute;n&eacute;fiques &agrave; la croissance du colza d&rsquo;hiver. Les boutons floraux sont maintenant bien visibles partout, quelle que soit la taille du colza. Les temp&eacute;ratures exceptionnelles atteintes notamment le dimanche 8 avril ont &eacute;galement &eacute;t&eacute; favorables aux vols massifs de m&eacute;lig&egrave;thes surtout et de charan&ccedil;ons de la tige.</p>

<p style="text-align:justify">De nombreuses captures d&rsquo;insectes ont &eacute;t&eacute; r&eacute;alis&eacute;es dans les bassins et de nombreux m&eacute;lig&egrave;thes ont &eacute;t&eacute; d&eacute;nombr&eacute;s sur les plantes. Dans 20 champs du r&eacute;seau d&rsquo;observation, le seuil d&rsquo;intervention contre les m&eacute;lig&egrave;thes &eacute;tait d&eacute;pass&eacute;, allant de 3-4 m&eacute;lig&egrave;thes par plante jusqu&rsquo;&agrave; 28 m&eacute;lig&egrave;thes par plante, soit plus de 1.000 m&eacute;lig&egrave;thes pour 40 plantes !</p>

<p style="text-align:justify">Dans quelques champs, l&rsquo;ajout, lors du semis, d&rsquo;une vari&eacute;t&eacute; plus pr&eacute;coce au d&eacute;but de la floraison &agrave; raison de 2 &agrave; 3 plantes/m&sup2;, permet d&rsquo;attirer les m&eacute;lig&egrave;thes sur les premi&egrave;res fleurs. Cependant, lorsque les m&eacute;lig&egrave;thes sont tr&egrave;s nombreux, cela peut ne plus suffire. Il faut bien &eacute;valuer le nombre de m&eacute;lig&egrave;thes pr&eacute;sents dans la vari&eacute;t&eacute; principale et voir si le seuil d&rsquo;intervention (3-4 m&eacute;lig&egrave;thes par plante) est atteint ou d&eacute;pass&eacute;.</p>

<p style="text-align:justify">Les stades du colza d&rsquo;hiver &eacute;voluent rapidement et peuvent &eacute;galement varier fortement au sein d&rsquo;une m&ecirc;me parcelle, notamment pour les parcelles touch&eacute;es par des d&eacute;g&acirc;ts de gel. Les plantes les plus petites avec les plus petits boutons floraux sont les plus fragiles vis-&agrave;-vis des attaques de m&eacute;lig&egrave;thes qui recherchent le pollen dans les boutons floraux avant la floraison du colza.</p>

<p style="text-align:justify">Il est donc vivement conseill&eacute; de bien observer la situation actuelle dans chaque champ de colza avant une &eacute;ventuelle intervention insecticide.</p>

<p style="text-align:justify">Dans le cadre de la lutte int&eacute;gr&eacute;e, il faut varier les mol&eacute;cules utilis&eacute;es pour &eacute;viter l&rsquo;apparition d&rsquo;insectes r&eacute;sistants. Plusieurs pyr&eacute;thrino&iuml;des (cyhalothrine, deltam&eacute;thrine,&hellip;) largement utilis&eacute;s dans le pass&eacute; ne sont plus efficaces sur les m&eacute;lig&egrave;thes, m&ecirc;me s&rsquo;ils restent autoris&eacute;s. Plusieurs mati&egrave;res actives de diff&eacute;rentes familles chimiques sont disponibles pour lutter contre les m&eacute;lig&egrave;thes. A noter que le Gouvernement wallon a d&eacute;cid&eacute; le 22 mars 2018 d&rsquo;interdire l&rsquo;usage des n&eacute;onicotino&iuml;des &agrave; partir du 1er juin 2018. En colza, cela concerne le thiacloprid (BISCAYA) et l&rsquo;ac&eacute;tamiprid (ANTILOP, INSYST/EXXODUS, GAZELLE, MOSPILAN). Les autres n&eacute;onicotino&iuml;des ayant &eacute;t&eacute; utilis&eacute;s pour la d&eacute;sinfection des semences de colza (CRUISER OSR, ELADO, CHINOOK) sont d&eacute;j&agrave; interdits au niveau europ&eacute;en depuis 2013 (moratoire europ&eacute;en).</p>

<p style="text-align:justify">Lorsque la floraison du colza d&rsquo;hiver d&eacute;marrera, les m&eacute;lig&egrave;thes deviendront inoffensifs car ils trouveront le pollen libre sur les fleurs.</p>

<p style="text-align:right"><br />
Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>
', 1, NULL, 17, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (34, 'Situation maladies en escourgeon : pression faible', 'left', 300, 225, '<p><strong>Dates </strong>: <span style="color:#726056">Observations du lundi 16 avril 2018 ; Semis du 25 septembre au 29 &nbsp;septembre 2017</span></p>

<p><strong>Vari&eacute;t&eacute;s </strong>: <span style="color:#726056">Hedwig, Quadriga, Rafaela, Tonic, Verity</span></p>

<p><strong>R&eacute;seau </strong>: <span style="color:#726056">13 parcelles</span><span style="color:#726056"> r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath,</span> <span style="color:#726056">B&eacute;clers), Li&egrave;ge (Kemexhe, Milmort, Tinlot) et Namur (Assesse, Nam&egrave;che)</span></p>

<p><span style="color:#726056">--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p>

<p><strong><u>R&eacute;sum&eacute;</u></strong></p>

<p style="text-align:justify">Cette semaine, la pression en maladie est faible sur l&rsquo;ensemble des parcelles du r&eacute;seau CADCO qui ont pour la plupart atteint le stade 2&egrave;me n&oelig;ud (BBCH32).</p>

<p style="text-align:justify"><span style="color:#726056">--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p>

<p><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify">Pr&egrave;s de 70% des parcelles ont atteint le stade 2&egrave;me n&oelig;ud (BBCH32). Deux parcelles sont encore au stade 1er n&oelig;ud (BBCH31) et deux autres au stade &eacute;pi 1 cm (BBCH30).</p>

<p style="text-align:center"><img align="" filer_id="360" height="430" src="/filer/canonical/1523961374/360/" thumb_option="" title="" width="756" /></p>

<p>&nbsp;</p>

<p><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a><strong> </strong>est pr&eacute;sente un peu partout mais reste cantonn&eacute;e dans le bas des plantes. Seules quelques parcelles montrent des sympt&ocirc;mes sur les F-2 du moment (moins de 5% des plantes dans tous les cas).</p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rhynchosporiose</strong></a><span style="color:black"> est elle aussi pr&eacute;sente uniquement dans le fond de v&eacute;g&eacute;tation et &agrave; des fr&eacute;quences tr&egrave;s faibles. Maximum 10% des plantes sont touch&eacute;es dans les vari&eacute;t&eacute;s sensibles et r&eacute;sistantes.</span></p>

<p>La <a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille naine</strong></a> est pr&eacute;sente dans l&rsquo;ensemble des parcelles du r&eacute;seau except&eacute; celles d&rsquo;Assesse et de Nam&egrave;che dans le Namurois. Sur les vari&eacute;t&eacute;s sensibles, entre 25 et 60% des F-2 du moment sont touch&eacute;es. Sur la vari&eacute;t&eacute; r&eacute;sistante Hedwig &agrave; Milmort, seules 5% des F-2 pr&eacute;sentent des sympt&ocirc;mes de rouille naine.</p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>o&iuml;dium</strong></a><span style="color:black"> est observ&eacute;e sur les F-2 de quelques plantes dans les parcelles de Tinlot (Rafaela) et Milmort (Hedwig). Pour le reste, cette maladie est seulement pr&eacute;sente dans le fond de v&eacute;g&eacute;tation.</span></p>

<p><u><strong><span style="background-color:#00ff00">Recommandations</span></strong></u></p>

<p style="text-align:justify">La pression phytosanitaire est assez faible actuellement et le temps chaud et sec annonc&eacute; pour cette semaine ne devrait pas &ecirc;tre favorable aux maladies. Il n&rsquo;y a donc pas de raison de s&rsquo;inqui&eacute;ter pour l&rsquo;instant.</p>

<p style="text-align:justify">Seule la pression en rouille naine dans certaines parcelles pourrait &ecirc;tre pr&eacute;occupante et n&eacute;cessiter un traitement au stade 31. En effet, rappelons que pour lutter contre les maladies fongiques de l&rsquo;escourgeon, <span style="color:black">un traitement unique au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH 39) est la solution g&eacute;n&eacute;ralement la plus adapt&eacute;e.&nbsp; Pour les parcelles ayant atteint le stade 1er n&oelig;ud (BBCH 31), un traitement peut n&eacute;anmoins &ecirc;tre envisag&eacute; si la vari&eacute;t&eacute; emblav&eacute;e est fortement sensible &agrave; une maladie pr&eacute;sente dans le champ au-dessus d&rsquo;un seuil de nuisibilit&eacute;. Dans le cas de la rouille naine, ce seuil est atteint si plus de 10% des trois derni&egrave;res feuilles sont atteintes pour les vari&eacute;t&eacute;s sensibles et plus de 50% des trois derni&egrave;res feuilles sont atteintes pour les vari&eacute;t&eacute;s tol&eacute;rantes.</span></p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 18, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (35, 'Point phytotechnique en froment', 'left', 300, 225, '<p><strong><u>Fumure et r&eacute;gulateur</u></strong></p>

<p style="text-align:justify">Les semis de d&eacute;but octobre sont ou approchent du stade premier n&oelig;ud. Ceux de novembre sont ou approchent du stade &eacute;pi &agrave; 1cm.</p>

<p style="text-align:justify">Lorsque&nbsp; la culture sera bien au stade redressement, il y a lieu de penser &agrave; apporter la deuxi&egrave;me fraction de la fumure azot&eacute;e&nbsp; et &agrave; appliquer le traitement r&eacute;gulateur.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, Bernard Bodson et Benjamin Dumont</span></p>
', 1, NULL, 18, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (36, 'Situation maladies en froment : pression faible', 'left', 300, 225, '<p><u>Dates </u>: <span style="color:#726056">Observations du lundi 16 avril 2018 ; Semis du 18 octobre au 27 &nbsp;octobre 2017</span></p>

<p><u>Vari&eacute;t&eacute;s </u>: <span style="color:#726056">Albert, Anapolis, Bergamo, Edgar, Gedser, Graham, KWS Smart, Ragnar, Reflection, Sacramento</span></p>

<p><u>R&eacute;seau </u>: <span style="color:#726056">25 parcelles</span><span style="color:#726056"> r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath,</span> <span style="color:#726056">Ellignies-Sainte-Anne), Li&egrave;ge (Eben Emael, Mortroux, Pailhe, Les Waleffes) et Namur (Assesse, Franc-Waret)</span></p>

<p><span style="color:#726056">--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p>

<p><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">Cette semaine, la pression en maladie est faible sur l&rsquo;ensemble des parcelles du r&eacute;seau CADCO qui sont soit au stade &eacute;pi 1cm (BBCH30), soit au stade 1er n&oelig;ud (BBCH31).</p>

<p>--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>

<p style="text-align:center"><img align="" filer_id="361" height="150" original_image="false" src="/filer/canonical/1523961968/361/" thumb_option="" title="" width="742" /></p>

<p><u>Pression en maladies</u></p>

<p style="text-align:justify"><span style="color:black">La </span><strong>septoriose</strong> est observ&eacute;e dans la quasi-totalit&eacute; des essais &agrave; des fr&eacute;quences tr&egrave;s variables mais uniquement sur les feuilles les plus &acirc;g&eacute;es. Elle n&rsquo;atteint jamais un &eacute;tage foliaire sup&eacute;rieur &agrave; la F-2 du moment.</p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium </strong><span style="color:black">est peu pr&eacute;sent dans les parcelles. Lorsque cette maladie est observ&eacute;e, elle reste cantonn&eacute;e dans le fond de v&eacute;g&eacute;tation. Seules 7 parcelles montrent des sympt&ocirc;mes sur les F-2 du moment &agrave; des fr&eacute;quences faibles (moins de 20%).</span></p>

<p style="text-align:justify"><span style="color:black">La </span><strong>rouille jaune</strong> est pr&eacute;sente dans 6 parcelles : &agrave; Eben Emael, Mortroux et Les Waleffes sur les F-2 ou F-3 du moment des vari&eacute;t&eacute;s tol&eacute;rantes. Elle est &eacute;galement observ&eacute;e sur 10% des F-2 de la vari&eacute;t&eacute; sensible Reflection &agrave; Les Waleffes.</p>

<p style="text-align:justify"><span style="color:black">La </span><strong>rouille brune</strong><span style="color:black"> n&rsquo;est pas observ&eacute;e dans les parcelles d&rsquo;essai du r&eacute;seau CADCO.</span></p>

<p><strong><span style="background-color:#00ff00">Recommandations</span></strong></p>

<p style="text-align:justify">Les froments sont encore au stade &eacute;pi 1 cm ou 1er n&oelig;ud.&nbsp; La pression des maladies est faible et ne devrait &eacute;voluer vu les conditions climatiques chaudes et s&egrave;ches annonc&eacute;es. Ces conditions permettent d&rsquo;&eacute;carter l&rsquo;id&eacute;e d&rsquo;un quelconque traitement actuellement. Rappelons que la recommandation du CADCO est la suivante : aucun traitement ne doit &ecirc;tre envisag&eacute; avant le stade 31. Au stade 31, un traitement pourrait &eacute;ventuellement &ecirc;tre envisag&eacute; seulement dans les parcelles emblav&eacute;es avec une vari&eacute;t&eacute; <strong><u>tr&egrave;s sensible</u></strong> &agrave; la rouille jaune <strong><u>et</u></strong> qui pr&eacute;senteraient des gros <strong><u>foyers actifs</u></strong> de rouille jaune. Ce cas de figure est assez rare et nous ne sommes pas confront&eacute;s &agrave; cette situation dans notre r&eacute;seau d&rsquo;observations.</p>

<p style="text-align:justify">Les vari&eacute;t&eacute;s suivantes sont consid&eacute;r&eacute;es comme sensibles &agrave; la rouille jaune : Atomic, Benchmark, Complice, Expert, Lyrik, Manitou, Nemo, Reflection, RGT Reform et RGT Texaco.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 18, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (37, 'Produits autorisés en céréales, actualisation de listes', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, dont r&eacute;cemment remis &agrave; jour : une mise &agrave; jour de ce mois de la liste des fongicides, r&eacute;gulateurs de croissance,&hellip; &nbsp;autoris&eacute;s est disponible via ce lien : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a></p>
', 1, NULL, 18, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (38, 'Le colza : De nombreux méligèthes et bientôt la floraison', 'left', 300, 225, '<p style="text-align:justify"><u>Situation au 17 avril 2018</u></p>

<p style="text-align:justify">En quelques jours, la culture de colza d&rsquo;hiver s&rsquo;est d&eacute;velopp&eacute;e rapidement, aussi bien en taille qu&rsquo;en stade.&nbsp; Les boutons floraux grossissent et se s&eacute;parent sur la hampe principale ; les ramifications secondaires se d&eacute;veloppent.&nbsp; Les premi&egrave;res fleurs jaunes sont visibles dans quelques champs.</p>

<p style="text-align:justify">L&rsquo;arriv&eacute;e en masse de m&eacute;lig&egrave;thes et de charan&ccedil;ons de la tige (du chou principalement) sur les plantes, au cours de la derni&egrave;re d&eacute;cade, a n&eacute;cessit&eacute; ou n&eacute;cessite encore une lutte contre ces insectes.&nbsp; En g&eacute;n&eacute;ral, on trouve des m&eacute;lig&egrave;thes sur chaque plante.&nbsp; Leur nombre varie d&rsquo;une plante &agrave; l&rsquo;autre et aussi d&rsquo;un champ &agrave; l&rsquo;autre.&nbsp; Au stade &laquo; boutons &eacute;cart&eacute;s &raquo;, le seuil d&rsquo;intervention est de 2 &agrave; 3 m&eacute;lig&egrave;thes par plante si le colza est faible, et de 7 &agrave; 8 m&eacute;lig&egrave;thes par plante si le colza est en bon &eacute;tat.&nbsp; Le seuil d&rsquo;intervention est d&eacute;pass&eacute; dans plusieurs champs suivis dans le cadre du r&eacute;seau d&rsquo;observations.</p>

<p style="text-align:justify">On peut actuellement d&eacute;j&agrave; observer des boutons floraux qui se d&eacute;ss&egrave;chent, ce qui est une cons&eacute;quence de l&rsquo;attaque de petits boutons par les m&eacute;lig&egrave;thes il y a quelques jours.&nbsp; Si la culture est vigoureuse, la compensation pourra se faire durant la floraison.&nbsp; Par contre, si la culture est faible, la r&eacute;cup&eacute;ration sera plus difficile.</p>

<p style="text-align:justify">Les insecticides utilis&eacute;s sur m&eacute;lig&egrave;thes pr&eacute;sentant des modes d&rsquo;action diff&eacute;rents (par contact, syst&eacute;mique, ingestion) montrent des efficacit&eacute;s diff&eacute;rentes.&nbsp; Dans certains cas, l&rsquo;inefficacit&eacute; du premier traitement &eacute;valu&eacute; apr&egrave;s quelques jours a &eacute;t&eacute; constat&eacute;e.&nbsp; S&rsquo;il fallait r&eacute;intervenir avec un insecticide, il faut veiller &agrave; alterner les modes d&rsquo;action.&nbsp; R&eacute;sistance des insectes ou vols d&rsquo;insectes suivants sont des facteurs explicatifs de la forte pr&eacute;sence de m&eacute;lig&egrave;thes cette ann&eacute;e.</p>

<p style="text-align:justify">L&rsquo;arriv&eacute;e de la floraison du colza sera acc&eacute;l&eacute;r&eacute;e cette semaine gr&acirc;ce aux temp&eacute;ratures estivales annonc&eacute;es pour les prochains jours.&nbsp; D&rsquo;autres insectes pollinisateurs (bourdons et abeilles) seront &eacute;galement actifs.&nbsp; Les pulv&eacute;risations en dehors des heures de butinage permettront de prot&eacute;ger ces insectes b&eacute;n&eacute;fiques pour la pollinisation.</p>

<p style="text-align:justify">A partir de la floraison de nombreuses plantes de colza, les m&eacute;lig&egrave;thes ne seront plus nuisibles, m&ecirc;me s&rsquo;ils restent nombreux.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP</p>

<p style="text-align:right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Michel De Proft, Expert scientifique CRA-W</p>
', 1, NULL, 19, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (39, 'Avertissement, précaution d''usage', 'left', 300, 225, '<p style="text-align:justify">Nous mettons en &oelig;uvre plusieurs am&eacute;liorations de la plateforme qui nous sert &agrave; notamment diffuser les avertissements et pouvons conna&icirc;tre certains dysfonctionnements &agrave; cause de ces changements. Nous vous demandons de rapporter, le cas &eacute;ch&eacute;ant, tout probl&egrave;me que vous pourriez rencontrer &agrave; <a href="mailto:cadcoasbl@cadcoasbl.be" target="_blank">cadcoasbl@cadcoasbl.be</a> afin d&rsquo;y rem&eacute;dier au plus vite. <span style="color:red">Au cas o&ugrave; vous auriez un probl&egrave;me de lecture, les avis se trouvent sur le Site CADCO : </span><a href="http://www.cadcoasbl.be/p01_avertissement.html" target="_blank">ici</a></p>
', 2, NULL, 20, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (40, 'Quelques attaques de mouche grise en froment', 'left', 300, 225, '<p style="text-align:justify">Des attaques de mouche grise se manifestent actuellement dans certains champs par le dess&egrave;chement d&rsquo;une proportion variable de pousses. Les plantes touch&eacute;es pr&eacute;sentent des talles dont la plus jeune feuille jaunit ou s&egrave;che, et se d&eacute;tache sans r&eacute;sistance lorsqu&rsquo;on la tire. Une feuille ainsi pr&eacute;lev&eacute;e apparait rong&eacute;e &agrave; sa base, o&ugrave; l&rsquo;on peut trouver un petit asticot blanch&acirc;tre de 2 &agrave; 8 mm de long.</p>

<p style="text-align:justify">Ces attaques sont assez rares et presque partout insignifiantes. Toutefois, dans certaines parcelles sem&eacute;es tard, le d&eacute;g&acirc;t peut localement &ecirc;tre plus important. La migration des larves dans les sols a partiellement r&eacute;ussi, malgr&eacute; une premi&egrave;re partie d&rsquo;hiver tr&egrave;s pluvieuse entra&icirc;nant la compaction des sols. Le gel survenu en fin d&rsquo;hiver, malgr&eacute; sa tardivit&eacute;, a donc &eacute;t&eacute; favorable &agrave; l&rsquo;insecte.</p>

<p style="text-align:justify">Il ne sert &agrave; rien d&rsquo;intervenir de quelle que fa&ccedil;on que ce soit. A moins d&rsquo;attaques tr&egrave;s importantes, les pertes de talles seront compens&eacute;es par le tallage.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, M. De Proft</p>
', 1, NULL, 20, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (41, 'Criocères en céréales de printemps', 'left', 300, 225, '<p style="text-align:justify">De fortes pontes de crioc&egrave;res sont signal&eacute;es en avoine et autres c&eacute;r&eacute;ales de printemps. Actuellement, elles ne requi&egrave;rent aucune intervention, mais lorsque les jeunes larves entameront leur phase alimentaire, il est possible que ces champs tr&egrave;s attaqu&eacute;s devront &ecirc;tre trait&eacute;s. Dans l&rsquo;imm&eacute;diat, l&rsquo;heure est &agrave; l&rsquo;observation !</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, M. De Proft</p>
', 1, NULL, 20, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (42, 'Stades phénologiques des céréales', 'left', 300, 225, '<p>Les &eacute;chelles compl&egrave;tes sont reprises <a href="http://www.cadcoasbl.be/p09_biblio.html#art00015">ici</a></p>
', 2, 362, 20, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (51, 'Listes des produits autorisés en céréales, actualisation du 01/05', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remis &agrave; jour : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a></p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u><em> </em>: </strong>X. Bertel (081/62.56.85)</p>
', 2, NULL, 23, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (43, 'Point phytotechnique en escourgeon et froment', 'left', 300, 225, '<p><strong><u>EN ESCOURGEON</u></strong></p>

<ul>
	<li style="text-align:justify"><u>Derni&egrave;re fraction de la fumure azot&eacute;e</u> :<u> </u>Les escourgeons sont majoritairement au stade derni&egrave;re feuille, moment o&ugrave; la derni&egrave;re fraction de la fumure azot&eacute;e doit &ecirc;tre appliqu&eacute;e. Les observations effectu&eacute;es dans les essais et dans quelques parcelles indiquent qu&rsquo;il n&rsquo;y a pas lieu de modifier la dose recommand&eacute;e pour cette fraction dans la fumure de r&eacute;f&eacute;rence (25-75-75). Cette dose de r&eacute;f&eacute;rence doit &ecirc;tre modul&eacute;e en fonction des param&egrave;tres culturaux de la parcelle et de l&rsquo;&eacute;tat de la culture (voir les recommandations et le calcul de la dose &agrave; la parcelle sur www.cereales.be).</li>
</ul>

<ul>
	<li style="text-align:justify"><u>R&eacute;gulateur de croissance</u> : Le r&eacute;gulateur de croissance &agrave; base d&rsquo;&eacute;th&eacute;phon destin&eacute; &agrave; prot&eacute;ger la culture vis-&agrave;-vis des risques de verse doit &ecirc;tre appliqu&eacute; au stade &laquo; derni&egrave;re feuille &raquo; en association avec le traitement fongicide de derni&egrave;re feuille.</li>
</ul>

<p><strong><u>EN FROMENT</u></strong></p>

<ul>
	<li style="text-align:justify">Les&nbsp; tous derniers semis de froment&nbsp;&nbsp; sont au stade redressement et sont aptes &agrave; recevoir la fraction azot&eacute;e de redressement et le traitement r&eacute;gulateur &agrave; base de chlorm&eacute;quat.</li>
</ul>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, Bernard Bodson et Benjamin Dumont</span></p>
', 1, NULL, 20, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (45, 'Situation maladies en froment, épeautre', 'left', 300, 225, '<p><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">Le temps sec et chaud de la semaine &eacute;coul&eacute;e a favoris&eacute; le d&eacute;veloppement des plantes plus que celui des maladies. La pression en maladies est relativement faible m&ecirc;me si la pr&eacute;sence de rouille jaune est signal&eacute;e &agrave; diff&eacute;rents endroits. L&rsquo;observation des parcelles est tr&egrave;s importante afin de d&eacute;tecter la pr&eacute;sence &eacute;ventuelle de foyers actifs de rouille jaune et de juger de la pression des autres maladies.</p>

<p><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify">La majorit&eacute; des parcelles a atteint le stade 1er n&oelig;ud (BBCH31) et un tiers est d&eacute;j&agrave; au stade 2&egrave;me n&oelig;ud (BBCH32).</p>

<p><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><span style="color:black">La </span><strong>septoriose</strong> est pr&eacute;sente dans l&rsquo;ensemble des parcelles du r&eacute;seau o&ugrave; elle est observable dans le fond de v&eacute;g&eacute;tation ou sur les F-2. Sur celles-ci, maximum 15% des plantes sont touch&eacute;es &agrave; des gravit&eacute;s inf&eacute;rieures au pourcent. D&rsquo;apr&egrave;s le mod&egrave;le de pr&eacute;vision &eacute;pid&eacute;miologique PROCULTURE, il n&rsquo;y a pas eu de p&eacute;riode d&rsquo;infection r&eacute;cemment.</p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium </strong><span style="color:black">est observable dans le fond de v&eacute;g&eacute;tation de 7 des 29 parcelles du r&eacute;seau et sur les F-2 des 4 parcelles &agrave; Mortroux o&ugrave; un tiers des talles est touch&eacute;. Les seuils de nuisibilit&eacute; ne sont pas atteints.</span></p>

<p style="text-align:justify"><span style="color:black">La </span><strong>rouille jaune</strong> est signal&eacute;e dans 7 parcelles &eacute;galement. Dans 3 parcelles au stade 31, les F-2 sont touch&eacute;es. De gros foyers actifs sont observ&eacute;s sur KWS Smart &agrave; Eben Emael. Des sympt&ocirc;mes sont &eacute;galement observ&eacute;s sur Porthus, Sacramento et Reflection &agrave; Eghez&eacute;e.</p>

<p style="text-align:justify"><span style="color:black">La </span><strong>rouille brune</strong><span style="color:black"> n&rsquo;est pas signal&eacute;e dans les parcelles observ&eacute;es.</span></p>

<p><strong><u>Recommandations</u></strong></p>

<p style="text-align:justify">L&rsquo;observation des parcelles est primordiale pour le moment. Si des foyers actifs de rouille jaune sont observ&eacute;s sur une vari&eacute;t&eacute; sensible (cote - ou - - dans le Livre Blanc) au stade 31, un traitement peut &ecirc;tre envisag&eacute;. Un traitement au stade 32 est envisageable si une pr&eacute;sence significative (plus de 10% des plantes) de sympt&ocirc;mes de rouille jaune est pr&eacute;sente sur une vari&eacute;t&eacute; peu tol&eacute;rante (cote =, - ou - - dans le Livre Blanc).</p>

<p style="text-align:justify">Dans les parcelles indemnes de rouille jaune et o&ugrave; la pression des autres maladies est faible, le traitement peut &ecirc;tre postpos&eacute; au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39). C&rsquo;est le cas dans la plupart de nos parcelles d&rsquo;essai pour le moment. Faire l&rsquo;impasse sur le traitement au stade 32 permet d&rsquo;&eacute;viter deux inconv&eacute;nients (l&rsquo;obligation d&rsquo;effectuer un second traitement 3-4 semaines apr&egrave;s le premier et comme les bl&eacute;s sont en g&eacute;n&eacute;ral au stade &eacute;piaison 3-4 semaines apr&egrave;s le stade 32, la derni&egrave;re feuille n&rsquo;est donc pas prot&eacute;g&eacute;e d&egrave;s sa sortie). Au contraire, faire l&rsquo;impasse donne la possibilit&eacute; de ne traiter qu&rsquo;une fois sur la saison si les conditions le permettent et la derni&egrave;re feuille est prot&eacute;g&eacute;e d&egrave;s sa sortie. Au besoin, un traitement relais au stade floraison permet de contr&ocirc;ler les maladies plus tardives.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, 364, 20, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (46, 'Le colza d’hiver : fleurira, fleurira pas ?', 'left', 300, 225, '<p style="text-align:justify">La m&eacute;t&eacute;o estivale de la semaine &eacute;coul&eacute;e a acc&eacute;l&eacute;r&eacute; le d&eacute;veloppement du colza d&rsquo;hiver arrivant &agrave; la floraison dans la plupart des champs.&nbsp; Les premi&egrave;res siliques apparaissent.&nbsp; A partir de ce stade, le risque li&eacute; aux m&eacute;lig&egrave;thes est &eacute;cart&eacute;.</p>

<p style="text-align:justify">Dans plusieurs champs de colza avec ou sans fleurs, des avortements de boutons floraux sont visibles, dans des proportions faibles &agrave; tr&egrave;s &eacute;lev&eacute;es, ce qui est inqui&eacute;tant car de nombreuses siliques sont manquantes ou risquent de ne pas se former.&nbsp; Les d&eacute;g&acirc;ts de m&eacute;lig&egrave;thes sur boutons floraux sont certainement responsables d&rsquo;une partie de ces avortements, mais d&rsquo;autres facteurs non encore bien identifi&eacute;s sont en jeu et la question d&rsquo;&eacute;ventuels effets li&eacute;s &agrave; des m&eacute;langes de produits phytopharmaceutiques est pos&eacute;e.</p>

<p style="text-align:justify"><strong>Pour nous aider &agrave; comprendre ce ph&eacute;nom&egrave;ne, nous sollicitons votre aide : si vous observez de tels avortements de fleurs, pourriez-vous prendre contact par e-mail (</strong><a href="mailto:cepicop@gmail.com"><strong>cepicop@gmail.com</strong></a><strong> ou </strong><a href="mailto:appo.gembloux@uliege.be"><strong>appo.gembloux@uliege.be</strong></a><strong> )</strong><strong> ou par t&eacute;l&eacute;phone &agrave; l&rsquo;un des num&eacute;ros d&rsquo;appel suivants : GSM : 0497/53.84.47 ou 0476/76.05.32).</strong></p>

<p style="text-align:justify">A partir de la floraison, avant la chute des premiers p&eacute;tales, la protection contre la principale maladie du colza, le scl&eacute;rotinia, est obligatoirement r&eacute;alis&eacute;e pr&eacute;ventivement.&nbsp; A l&rsquo;heure actuelle, il n&rsquo;existe aucune vari&eacute;t&eacute; r&eacute;sistante, ni aucun traitement curatif lorsque les premiers sympt&ocirc;mes apparaissent : pourriture sur les feuilles au contact des p&eacute;tales humides et infect&eacute;s par les spores de scl&eacute;rotinia.&nbsp; Appliquer un fongicide avant la chute des premiers p&eacute;tales est le meilleur positionnement du traitement permettant de prot&eacute;ger la culture de colza.&nbsp;</p>

<p style="text-align:justify">C&ocirc;t&eacute; insectes ravageurs, depuis la chute r&eacute;cente des temp&eacute;ratures de ce lundi, on observe leur moindre pr&eacute;sence.&nbsp; Au cours de la floraison et de la formation des siliques, le charan&ccedil;on des siliques sera surveill&eacute;.&nbsp; Il est tr&egrave;s rarement observ&eacute; actuellement.</p>

<p style="text-align:justify">Il n&rsquo;y a, &agrave; pr&eacute;sent, pas de raison d&rsquo;ajouter un insecticide au traitement fongicide. &nbsp;</p>

<p style="text-align:justify">M&ecirc;me s&rsquo;il n&rsquo;y a qu&rsquo;un fongicide &agrave; appliquer, le traitement sera r&eacute;alis&eacute; en dehors des heures de butinage des insectes pollinisateurs, abeilles et autres butineurs tr&egrave;s attir&eacute;s par le colza en fleur.</p>

<p style="margin-left:106.2pt; margin-right:0cm; text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP</p>

<p style="text-align:right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Michel De Proft, Expert scientifique CRA-W</p>
', 1, NULL, 21, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (47, 'Colza : dégâts physiologiques exceptionnels', 'left', 300, 225, '<p style="text-align:justify">Depuis le milieu de la semaine derni&egrave;re, des avortements de boutons floraux sont observ&eacute;s dans de nombreux champs de colza. Dans les cas les plus graves, c&rsquo;est l&rsquo;enti&egrave;ret&eacute; de l&rsquo;inflorescence qui est touch&eacute;e et quelquefois m&ecirc;me les inflorescences secondaires. M&ecirc;me si les m&eacute;lig&egrave;thes sont nombreux, de tels d&eacute;g&acirc;ts, observ&eacute;s dans toutes les r&eacute;gions de Wallonie, n&rsquo;ont jamais &eacute;t&eacute; observ&eacute;s auparavant. Tout indique qu&rsquo;il s&rsquo;agit d&rsquo;une cause physiologique. Gel tardif ? Chaleurs soudaines ? Le ou les facteurs pr&eacute;cis ne sont pas encore tr&egrave;s clairs.</p>

<p style="text-align:justify">Dans les pays voisins, et particuli&egrave;rement en Allemagne, on incrimine surtout la brusque vague de chaleur du 18 au 22 avril et les &eacute;carts de temp&eacute;rature de plus de 20&deg;C entre le jour et la nuit.</p>

<p style="text-align:justify">Nous pensons que cette vague de chaleur a conduit aux d&eacute;g&acirc;ts observ&eacute;s surtout parce qu&rsquo;elle est survenue alors que <strong><u>les sols &eacute;taient encore tr&egrave;s froids</u></strong>. Le syst&egrave;me racinaire n&rsquo;a pas pu suivre la &laquo; demande &raquo; des tissus les plus jeunes en forte croissance et ce stress s&rsquo;est traduit par des avortements de boutons floraux.</p>

<p style="text-align:justify">Quel que soit le niveau actuel des d&eacute;g&acirc;ts, il faut attendre quelques jours pour pouvoir se faire une id&eacute;e pr&eacute;cise des possibilit&eacute;s de r&eacute;cup&eacute;ration.</p>

<p style="text-align:justify">Dans cette situation o&ugrave; chaque bouton floral compte, une attention particuli&egrave;re est recommand&eacute;e quant &agrave; la pr&eacute;sence de m&eacute;lig&egrave;thes.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="margin-left:106.2pt; margin-right:0cm; text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP</p>

<p style="text-align:right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Michel De Proft, Expert scientifique CRA-W</p>
', 1, NULL, 22, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (49, 'Stades phénologiques des céréales', 'left', 300, 225, '<p style="text-align:center"><img align="" filer_id="367" height="368" src="/filer/canonical/1525168289/367/" thumb_option="" title="" width="861" /></p>
', 2, NULL, 23, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (52, 'Floraisons contrastées, de bonnes à mauvaises', 'left', 300, 225, '<p style="text-align:justify">Alors que le colza devrait conna&icirc;tre &agrave; cette &eacute;poque une floraison nous offrant de belles surfaces bien jaunes, la situation est tr&egrave;s contrast&eacute;e cette ann&eacute;e.</p>

<p style="text-align:justify">En effet, &agrave; c&ocirc;t&eacute; de champs dont la floraison se d&eacute;roule normalement, de nombreux champs connaissent d&rsquo;importantes difficult&eacute;s au niveau de la formation des fleurs et des siliques qui contiendront les futures graines de colza.&nbsp; Les diff&eacute;rents stress subis par le colza (gels tardifs en mars 2018, arriv&eacute;e massive de m&eacute;lig&egrave;thes, vague de chaleur intense en avril,&hellip;) ont des cons&eacute;quences sur la fertilit&eacute; de la plante et, sans doute, sur le rendement final de la culture.</p>

<p style="text-align:justify">Le retour de temp&eacute;ratures conformes aux normales saisonni&egrave;res et les pluies r&eacute;centes devraient permettre au colza de former des siliques, au fur et &agrave; mesure de la f&eacute;condation des fleurs.&nbsp; Si tel n&rsquo;est pas le cas, si le taux de boutons floraux avort&eacute;s est important et que des signes d&rsquo;am&eacute;lioration ne sont pas constat&eacute;s &agrave; la fin de la premi&egrave;re semaine du mois de mai, il est conseill&eacute; de faire constater les d&eacute;g&acirc;ts par la Commission de constat des d&eacute;g&acirc;ts, avant un &eacute;ventuel retournement de la culture.</p>

<p style="text-align:justify">La culture de colza d&rsquo;hiver nous a souvent &eacute;tonn&eacute;s de son potentiel de r&eacute;cup&eacute;ration.&nbsp; Cette situation in&eacute;dite nous apportera encore de nouveaux enseignements li&eacute;s aux brusques variations climatiques et &agrave; ses cons&eacute;quences sur les cultures.</p>

<p style="margin-left:106.2pt; margin-right:0cm; text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP</p>

<p style="text-align:right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Michel De Proft, Expert scientifique CRA-W</p>
', 1, NULL, 24, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (53, 'Colza : Produira, produira pas de graines', 'left', 300, 225, '<p style="text-align:justify">La situation en colza d&rsquo;hiver reste contrast&eacute;e.&nbsp; La floraison en cours doit inciter chacun &agrave; aller voir ses champs de colza d&rsquo;hiver et &agrave; observer les plantes &agrave; l&rsquo;int&eacute;rieur de chaque champ de colza, qu&rsquo;il soit conventionnel ou bio, pour &eacute;valuer le taux de r&eacute;ussite de la f&eacute;condation et de la formation des siliques.</p>

<p style="text-align:justify">Dans les champs les plus avanc&eacute;s, la floraison se termine car elle a &eacute;t&eacute; acc&eacute;l&eacute;r&eacute;e par le retour de temp&eacute;ratures nettement sup&eacute;rieures aux normales saisonni&egrave;res.</p>

<p style="text-align:justify">Les siliques manquantes sont nombreuses dans les parcelles o&ugrave; de nombreux boutons floraux ont avort&eacute;.&nbsp; Une certaine r&eacute;cup&eacute;ration est observ&eacute;e pour les derniers &eacute;tages de fleurs, depuis quelques jours.</p>

<p style="text-align:justify">Dans les parcelles probl&eacute;matiques o&ugrave; aucune fleur n&rsquo;est apparue jusqu&rsquo;&agrave; pr&eacute;sent, il est indispensable de v&eacute;rifier l&rsquo;existence de siliques.</p>

<p style="text-align:justify">Si des probl&egrave;mes importants sont observ&eacute;s (absence totale ou partielle de siliques form&eacute;es), il est vivement conseill&eacute; de faire constater les d&eacute;g&acirc;ts en colza, par la Commission de constat des d&eacute;g&acirc;ts de la commune o&ugrave; se trouvent les parcelles cultiv&eacute;es en colza, avant un retournement &eacute;ventuel de la culture.&nbsp; L&rsquo;impact sera tr&egrave;s important sur les rendements finaux de la culture.</p>

<p style="text-align:justify">C&ocirc;t&eacute; insectes, les m&eacute;lig&egrave;thes adultes sont actuellement peu nombreux et de tr&egrave;s rares charan&ccedil;ons des siliques sont observ&eacute;s dans les parcelles du r&eacute;seau d&rsquo;observations.&nbsp;</p>

<p style="text-align:justify">Les journ&eacute;es actuelles tr&egrave;s ensoleill&eacute;es et tr&egrave;s chaudes sont tr&egrave;s favorables &agrave; l&rsquo;activit&eacute; des insectes pollinisateurs qui profitent de la floraison du colza d&rsquo;hiver.</p>

<p style="margin-left:106.2pt; margin-right:0cm; text-align:right"><em>Christine Cartrysse, APPO, Centre Pilote CePiCOP</em></p>

<p style="text-align:right"><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Michel De Proft, Expert scientifique CRA-W</em></p>

<p style="text-align:justify"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNE-D&eacute;veloppement et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 25, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (50, 'Situation maladies en froment, épeautre', 'left', 300, 225, '<p style="text-align:center"><img align="" filer_id="368" height="263" src="/filer/canonical/1525168540/368/" thumb_option="" title="" width="1065" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute;</strong></u> : La pression des maladies foliaires est &agrave; nouveau faible cette semaine.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u> : Une large majorit&eacute; des parcelles du r&eacute;seau ont atteint le stade 32. Quatre parcelles sont encore au stade 31.</p>

<p style="text-align:center"><img align="" filer_id="369" height="222" src="/filer/canonical/1525168651/369/" thumb_option="" title="" width="998" /></p>

<p><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>septoriose</strong></a> est observ&eacute;e &agrave; des fr&eacute;quences variables suivant les r&eacute;gions. Elle infecte moins de 15% des F-2 dans le Hainaut et est seulement pr&eacute;sente dans le bas des plantes dans la province de Namur et &agrave; Mortroux. Dans le reste de la province de Li&egrave;ge, elle touche jusqu&rsquo;&agrave; 50% des F-2. La gravit&eacute; de la maladie est faible dans tous les cas, ne d&eacute;passant pas les 5% de surface foliaire symptomatique.</p>

<p style="text-align:justify">D&rsquo;apr&egrave;s le mod&egrave;le &eacute;pid&eacute;miologique PROCULTURE, les pluies contaminatrices de ces derniers jours ont permis &agrave; la septoriose de gagner un &eacute;tage foliaire sup&eacute;rieur et des infections sont donc potentiellement en incubation sur les futures F3 d&eacute;finitives.</p>

<p style="text-align:justify"><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">L&rsquo;<strong>o&iuml;dium</strong></a><strong> </strong><span style="color:black">est signal&eacute; dans le fond de v&eacute;g&eacute;tation uniquement en province de Li&egrave;ge. Les autres parcelles du r&eacute;seau en sont indemnes.</span></p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p09_biblio/art0006/0704RJSymptome.pdf"><strong>rouille jaune</strong></a> est observ&eacute;e dans 11 parcelles. Les F-2 de la vari&eacute;t&eacute; sensible Reflection montrent des sympt&ocirc;mes &agrave; Ath, Ellignies-Saintes-Anne, Pailhe, Les Waleffes et Nivelles. Ailleurs, ce sont des vari&eacute;t&eacute;s tol&eacute;rantes qui pr&eacute;sentent quelques pustules sur leurs F-2 ou F-3.</p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille brune</strong></a><span style="color:black"> n&rsquo;est pas signal&eacute;e dans le r&eacute;seau d&rsquo;observation.</span></p>

<p>&nbsp;</p>

<p><strong><u>Recommandations</u></strong></p>

<p style="text-align:justify">Pour les quelques parcelles encore au stade 31, seule la pr&eacute;sence de foyers actifs de rouille jaune sur une vari&eacute;t&eacute; sensible peut amener &agrave; envisager un traitement. Dans tous les autres cas, il est pr&eacute;f&eacute;rable de r&eacute;&eacute;valuer la situation lorsque la parcelle est au stade 32.</p>

<p style="text-align:justify">Pour les parcelles au stade 32, un traitement est envisageable si une pr&eacute;sence significative (plus de 10% des plantes) de sympt&ocirc;mes de rouille jaune est observ&eacute;e sur une vari&eacute;t&eacute; peu tol&eacute;rante (cote =, - ou - - dans le Livre Blanc). Un traitement au stade 32 peut aussi &ecirc;tre envisag&eacute; sur une vari&eacute;t&eacute; peu tol&eacute;rante &agrave; la septoriose si plus de 20% des F-2 pr&eacute;sentent des sympt&ocirc;mes de cette maladie. Dans les parcelles indemnes de rouille jaune et o&ugrave; la pression des autres maladies est faible, le traitement peut &ecirc;tre postpos&eacute; au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39). C&rsquo;est le cas dans la plupart de nos parcelles d&rsquo;essai pour le moment. Faire l&rsquo;impasse sur le traitement au stade 32 permet d&rsquo;&eacute;viter deux inconv&eacute;nients (l&rsquo;obligation d&rsquo;effectuer un second traitement 3-4 semaines apr&egrave;s le premier et comme les bl&eacute;s sont en g&eacute;n&eacute;ral au stade &eacute;piaison 3-4 semaines apr&egrave;s le stade 32, la derni&egrave;re feuille n&rsquo;est donc pas prot&eacute;g&eacute;e d&egrave;s sa sortie). Au contraire, faire l&rsquo;impasse donne la possibilit&eacute; de ne traiter qu&rsquo;une fois sur la saison si les conditions le permettent et la derni&egrave;re feuille est prot&eacute;g&eacute;e d&egrave;s sa sortie. Au besoin, un traitement relais au stade floraison permet de contr&ocirc;ler les maladies plus tardives.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 23, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (48, 'Situation maladies en escourgeon', 'left', 300, 500, '<p style="text-align:center"><img align="" filer_id="366" height="223" src="/filer/canonical/1525168019/366/" thumb_option="" title="" width="1025" /></p>

<p style="text-align:justify"><strong><u>R&eacute;sum&eacute;</u></strong> : L&rsquo;helminthosporiose progresse, surtout dans le Hainaut. Partout ailleurs, c&rsquo;est la rouille naine qui gagne les &eacute;tages foliaires sup&eacute;rieurs.</p>

<p style="text-align:justify"><strong><u>Avancement des cultures</u></strong> : Toutes les parcelles ont atteint ou d&eacute;pass&eacute; le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39), le stade cl&eacute; de la protection fongicide en escourgeon.</p>

<p style="text-align:center"><img align="" filer_id="365" height="261" src="/filer/canonical/1525167880/365/" thumb_option="" title="" width="1025" /></p>

<p style="text-align:justify"><strong><u>Pression en maladies</u></strong></p>

<p style="text-align:justify">Pour rappel, la pression maladie rapport&eacute;e ici concerne des parcelles non trait&eacute;es.</p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a> est pr&eacute;sente de mani&egrave;re anecdotique dans le bas des plantes en provinces de Li&egrave;ge et de Namur. Dans le Hainaut, de 2 &agrave; 10% des F-1 ou des F-2 sont touch&eacute;es. La s&eacute;v&eacute;rit&eacute; des sympt&ocirc;mes reste cependant tr&egrave;s faible.</p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rhynchosporiose</strong></a><span style="color:black"> n&rsquo;est signal&eacute;e que dans 3 parcelles : 5% des F-3 sont touch&eacute;es &agrave; B&eacute;clers (sur Verity) et Pailhe (sur Quadriga) et 35% des F-3 sont touch&eacute;es sur Tonic &agrave; Pailhe.</span></p>

<p style="text-align:justify">La <a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille naine</strong></a> est observ&eacute;e sur les F-1 dans 11 parcelles du r&eacute;seau et est observ&eacute;e &agrave; des fr&eacute;quences variant entre 10 et 65%.&nbsp;</p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>o&iuml;dium</strong></a><span style="color:black"> est observ&eacute; sur 30% des F-2 &agrave; Pailhe. Dans les autres parcelles, cette maladie n&rsquo;est pas observ&eacute;e.</span></p>

<p>&nbsp;</p>

<p><u><strong>Recommandations</strong></u></p>

<p style="text-align:justify">La p&eacute;riode s&rsquo;&eacute;talant entre le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39) et le stade sortie des barbes (BBCH49) est l&rsquo;intervalle pivot pour la protection fongicide des escourgeons. Un traitement permettra de lutter contre les maladies d&eacute;j&agrave; pr&eacute;sentes mais aussi de pr&eacute;venir l&rsquo;apparition de la ramulariose. Le traitement &agrave; ce stade doit &ecirc;tre complet et r&eacute;manent. Il est conseill&eacute; d&rsquo;utiliser les sp&eacute;cialit&eacute;s &agrave; base de carboxamides en m&eacute;lange avec une triazole et/ou une strobilurine. Il est &eacute;galement conseill&eacute; d&rsquo;appliquer du chlorothalonil car c&rsquo;est le dernier produit encore r&eacute;ellement efficace contre la ramulariose.</p>

<p style="text-align:right">&nbsp;<strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 23, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (54, 'Froment : Fumure azotée, fraction de dernière feuille', 'left', 300, 225, '<p style="text-align:justify">A partir du moment o&ugrave; la derni&egrave;re feuille pointe (BBCH 37), il y a lieu d&rsquo;appliquer la derni&egrave;re fraction de la fumure azot&eacute;e.</p>

<p style="text-align:justify">La dose&nbsp; de r&eacute;f&eacute;rence &agrave; moduler selon les conditions culturales de la parcelle, les doses d&eacute;j&agrave; appliqu&eacute;es et l&rsquo;&eacute;tat de la culture&nbsp; d&eacute;finie lors dans le Livre blanc 2018 est 65 kg N/ha&nbsp; pour la modulation voir le site internet <a href="http://www.cereales.be">www.cereales.be</a></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">Les observations dans les essais fumure et dans des parcelles de r&eacute;f&eacute;rence indiquent qu&rsquo;il n&rsquo;y a pas lieu de corriger la dose de r&eacute;f&eacute;rence.</p>

<p>L&rsquo;application peut &ecirc;tre r&eacute;alis&eacute;e sous forme solide ou liquide.</p>

<p style="text-align:justify">Si on choisit de l&rsquo;appliquer sous forme liquide, il convient d&rsquo;&ecirc;tre prudent afin d&rsquo;&eacute;viter d&rsquo;occasionner des br&ucirc;lures importantes aux feuilles sup&eacute;rieures de la culture. Les pr&eacute;cautions suivantes doivent &ecirc;tre prises :</p>

<ul>
	<li>Ne pas appliquer en plein soleil et lorsqu&rsquo;il y a des vents du nord et de l&rsquo;est</li>
	<li>Utiliser des jets &agrave; grosses gouttes (rain drop) ou des jets filets qui permettront &agrave; l&rsquo;engrais liquide d&rsquo;atteindre le sol</li>
	<li>Il est souhaitable que la culture re&ccedil;oive des pr&eacute;cipitations m&ecirc;me l&eacute;g&egrave;res dans les jours qui suivent l&rsquo;application</li>
	<li>Ne pas appliquer en m&eacute;lange de l&rsquo;engrais liquide avec des produits phytosanitaires.</li>
</ul>

<p style="margin-left:18.0pt; margin-right:0cm; text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, Bernard Bodson et Benjamin Dumont</span></p>
', 1, NULL, 26, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (55, 'Listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remis &agrave; jour : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a></p>
', 2, NULL, 26, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (67, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remis &agrave; jour : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a></p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 2, NULL, 29, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (56, 'Ravageurs des céréales : cécidomyie orange sur le point d’émerger', 'left', 300, 225, '<p style="text-align:justify">Selon le mod&egrave;le pr&eacute;dictif des &eacute;mergences de c&eacute;cidomyie orange, ces derni&egrave;res pourraient commencer au cours des prochains jours. Les vagues d&rsquo;&eacute;mergence r&eacute;pondent &agrave; des pluies &laquo; inductrices de nymphose &raquo;. La premi&egrave;re vague, qui devrait se produire tout prochainement, r&eacute;pond aux tr&egrave;s faibles pluies du 10 avril. Elle ne devrait donner lieu qu&rsquo;&agrave; tr&egrave;s peu d&rsquo;&eacute;mergences. La deuxi&egrave;me vague, r&eacute;pondant aux pluies du 14 et 15 avril pourrait &ecirc;tre plus abondante, l&agrave; o&ugrave; les pluies ont &eacute;t&eacute; les plus g&eacute;n&eacute;reuses. Cette vague devrait survenir d&rsquo;ici une dizaine de jours. La vague la plus importante devrait survenir d&rsquo;ici trois bonnes semaines, cons&eacute;quence d&rsquo;une pluie inductrice de la fin avril.</p>

<p style="text-align:justify">Les &eacute;mergences d&eacute;marrent, mais sur la pointe des pieds. De toute mani&egrave;re, elles n&rsquo;ont aucune cons&eacute;quence tant que les &eacute;pis sont prot&eacute;g&eacute;s par les gaines. Dans les jours qui viennent, les avertissements pourront pr&eacute;ciser les risques, &agrave; la lumi&egrave;res des observations effectu&eacute;es sur le terrain.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, M. De Proft</p>
', 1, NULL, 26, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (57, 'Maladies en escourgeon : rappel des recommandations', 'left', 300, 225, '<p style="text-align:justify">Toutes les parcelles ayant d&eacute;pass&eacute; le stade derni&egrave;re feuille &eacute;tal&eacute;e, il n&rsquo;y a plus d&rsquo;observation syst&eacute;matique pr&eacute;vue dans notre r&eacute;seau. La p&eacute;riode s&rsquo;&eacute;talant entre le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39) et le stade sortie des barbes (BBCH49) est l&rsquo;intervalle pivot pour la protection fongicide des escourgeons. Un traitement &agrave; ce stade permet de lutter contre les maladies d&eacute;j&agrave; pr&eacute;sentes mais aussi de pr&eacute;venir l&rsquo;apparition de la ramulariose.&nbsp; Il est conseill&eacute; d&rsquo;utiliser les sp&eacute;cialit&eacute;s &agrave; base de carboxamides en m&eacute;lange avec une triazole et/ou une strobilurine pour couvrir le spectre de maladies et &ecirc;tre r&eacute;manent. Il est &eacute;galement conseill&eacute; d&rsquo;appliquer du chlorothalonil car c&rsquo;est le dernier produit encore r&eacute;ellement efficace contre la ramulariose.</p>

<p style="text-align:right">&nbsp;<strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 26, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (58, 'Situation maladies en froment, épeautre', 'left', 300, 225, '<p style="text-align:center"><img align="" filer_id="370" height="467" src="/filer/canonical/1525773279/370/" thumb_option="" title="" width="878" /></p>

<p><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">Selon les dates de semis et les localit&eacute;s, les parcelles sont actuellement majoritairement au stade 2&egrave;me n&oelig;ud ou au stade derni&egrave;re feuille pointante. La pression en maladie varie d&rsquo;une emblavure &agrave; l&rsquo;autre et une observation de ses parcelles &agrave; ce stade nous para&icirc;t indispensable pour adapter votre strat&eacute;gie.</p>

<p>&nbsp;</p>

<p><strong><u>Pression en maladies</u></strong></p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>septoriose</strong></a> est pr&eacute;sente dans le fond de v&eacute;g&eacute;tation de l&rsquo;ensemble des parcelles. Elle touche les F4 dans 11 emblavures o&ugrave; elle est pr&eacute;sente sur maximum 25% des talles avec des niveaux de gravit&eacute; faibles (moins de 4% de surface foliaire symptomatique).</p>

<p style="text-align:justify">Ces derni&egrave;res semaines, les conditions m&eacute;t&eacute;orologiques n&rsquo;ont pas &eacute;t&eacute; similaires partout en Wallonie. Selon Proculture, le Hainaut et la r&eacute;gion d&rsquo;Alleur ont &eacute;t&eacute; &eacute;pargn&eacute;es par les pluies contaminatrices et aucune infection n&rsquo;est en incubation sur les F3 pour le moment. Ailleurs, des infections peuvent &ecirc;tre en incubation sur les F3 et seront exprim&eacute;es dans deux semaines. D&rsquo;ici l&agrave;, les parcelles devraient avoir atteint le stade derni&egrave;re feuille &eacute;tal&eacute;e et une protection pourra alors &ecirc;tre envisag&eacute;e &agrave; la lumi&egrave;re de nos prochains avertissements. Pour le moment, les seuils de nuisibilit&eacute; (repris dans la partie recommandations de cet avis) ne sont pas atteints dans les parcelles de notre r&eacute;seau.</p>

<p style="text-align:justify"><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">L&rsquo;<strong>o&iuml;dium</strong></a><strong> </strong>est pr&eacute;sent dans 10 des 29 parcelles du r&eacute;seau mais jamais &agrave; des niveaux inqui&eacute;tants. Seules quatre parcelles pr&eacute;sentent des sympt&ocirc;mes d&rsquo;o&iuml;dium sur maximum 15% des F4. Pour le reste, la maladie reste cantonn&eacute;e dans le bas des plantes.</p>

<p style="text-align:justify">En ce qui concerne la <a href="http://www.cadcoasbl.be/p09_biblio/art0006/0704RJSymptome.pdf" target="_blank"><strong>rouille jaune</strong></a><span style="color:black">, </span>55% des F3 sont infect&eacute;es dans une parcelle de Reflection &agrave; Pailhe. Ailleurs, 5 &agrave; 15% des F4 ou F5 des vari&eacute;t&eacute;s sensibles montrent des pustules. Des foyers ont &eacute;t&eacute; signal&eacute;s &agrave; Nivelles, Jandrain, Mortroux, le Hainaut et le Namurois sur la vari&eacute;t&eacute; sensible Reflection qui semble particuli&egrave;rement touch&eacute;e par cette maladie. Sur les vari&eacute;t&eacute;s tol&eacute;rantes, la rouille jaune n&rsquo;est pas signal&eacute;e dans le r&eacute;seau &agrave; l&rsquo;exception de Mortroux et Eben Emael o&ugrave; quelques pustules sont visibles sur les F4 ou F5 (surtout sur Anapolis et Bergamo).</p>

<p style="text-align:justify">Quelques pustules de <a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille brune</strong></a><strong>,</strong> ont &eacute;t&eacute; d&eacute;tect&eacute;es sur respectivement 5 et 15% des F5 d&rsquo;Edgar et Graham &agrave; Mortroux. La rouille brune n&rsquo;est pas signal&eacute;e dans les autres parcelles du r&eacute;seau. Les temp&eacute;ratures plus fraiches annonc&eacute;es prochainement ne devraient pas permettre son d&eacute;veloppement. Les parcelles doivent donc &ecirc;tre surveill&eacute;es mais la situation n&rsquo;est pas alarmante.</p>

<p style="text-align:justify">&nbsp;</p>

<p><strong><u>Recommandations</u></strong></p>

<p style="text-align:justify">Pour les parcelles au stade 32, un traitement est envisageable si une pr&eacute;sence significative (plus de 10% des plantes) de sympt&ocirc;mes de rouille jaune est observ&eacute;e sur une vari&eacute;t&eacute; peu tol&eacute;rante (cote =, - ou - - dans le Livre Blanc). Un traitement au stade 32 peut aussi &ecirc;tre envisag&eacute; sur une vari&eacute;t&eacute; peu tol&eacute;rante &agrave; la septoriose si plus de 20% des F-2 pr&eacute;sentent des sympt&ocirc;mes de cette maladie. Dans les parcelles indemnes de rouille jaune et o&ugrave; la pression des autres maladies est faible, le traitement peut &ecirc;tre postpos&eacute; au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39). C&rsquo;est le cas dans toutes nos parcelles d&rsquo;essai pour le moment. Faire l&rsquo;impasse sur le traitement au stade 32 permet d&rsquo;&eacute;viter deux inconv&eacute;nients : premi&egrave;rement, cela &eacute;vite de devoir effectuer un second traitement 3-4 semaines apr&egrave;s le premier et d&rsquo;autre part, comme les bl&eacute;s sont en g&eacute;n&eacute;ral au stade &eacute;piaison 3-4 semaines apr&egrave;s le stade 32, la derni&egrave;re feuille n&rsquo;est pas prot&eacute;g&eacute;e d&egrave;s sa sortie. Au contraire, faire l&rsquo;impasse donne la possibilit&eacute; de ne traiter qu&rsquo;une fois sur la saison si les conditions le permettent et la derni&egrave;re feuille est prot&eacute;g&eacute;e d&egrave;s sa sortie. Au besoin, un traitement relais au stade floraison permet de contr&ocirc;ler les maladies plus tardives.</p>

<p style="text-align:justify">Dans les parcelles au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39) et dans lesquelles aucun traitement n&rsquo;a &eacute;t&eacute; effectu&eacute;, un traitement devrait &ecirc;tre envisag&eacute; si des sympt&ocirc;mes de maladies foliaires sont visibles sur une des trois derni&egrave;res feuilles et cela peu importe la vari&eacute;t&eacute;.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 26, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (59, 'Stades phénologiques des céréales', 'left', 300, 225, '<p style="text-align:center"><img align="" filer_id="367" height="" original_image="false" src="/filer/canonical/1525168289/367/" thumb_option="" title="" width="" /></p>
', 1, NULL, 26, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (61, 'Formation "cécidomyie orange"', 'left', 300, 225, '<p style="text-align:center"><img align="" filer_id="375" height="" src="/filer/canonical/1526378132/375/" thumb_option="" title="" width="" /></p>
', 1, NULL, 27, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (62, 'Ravageurs des céréales : fortes émergences de cécidomyie orange attendues dans quelques jours', 'left', 300, 225, '<p style="text-align:justify">Les &eacute;mergences de c&eacute;cidomyie orange suivent de deux &agrave; quatre semaines (en fonction des temp&eacute;ratures) les pluies qui induisent leur nymphose. Jusqu&rsquo;&agrave; pr&eacute;sent, trois petites pluies (entre le 10 et le 23 avril) ont donn&eacute; lieu &agrave; de petites vagues d&rsquo;&eacute;mergence au cours des derniers jours. Les vols cr&eacute;pusculaires, m&ecirc;me dans les froments commen&ccedil;ant &agrave; &eacute;pier, sont n&eacute;gligeables et ne requi&egrave;rent aucune intervention.</p>

<p style="text-align:justify">En revanche, la prochaine vague d&rsquo;&eacute;mergence, correspondant aux fortes pluies de la toute fin avril, devrait &ecirc;tre importante. D&rsquo;apr&egrave;s le mod&egrave;le pr&eacute;visionnel d&eacute;velopp&eacute; au CRA-W, ces &eacute;mergences devraient se produire vers la fin du week-end dans les r&eacute;gions les plus chaudes (Hainaut occidental) et deux &agrave; trois jours plus tard dans les stations les plus froides.</p>

<p style="text-align:justify">Lorsque les &eacute;mergences d&eacute;buteront, il faudra appr&eacute;cier le stade du froment : avant l&rsquo;&eacute;clatement des gaines, le froment ne court aucun risque, de m&ecirc;me qu&rsquo;apr&egrave;s la fin de la floraison. En revanche, si des &eacute;mergences massives se produisent entre ces deux stades, des d&eacute;g&acirc;ts importants peuvent &ecirc;tre produits en quelques heures.</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">Nous surveillons de pr&egrave;s les pi&egrave;ges &agrave; ph&eacute;romones install&eacute;s au champ, et nous invitons donc chacun &agrave; la vigilance. C&rsquo;est au cr&eacute;puscule, en d&eacute;rangeant les insectes &agrave; l&rsquo;aide d&rsquo;une baguette pass&eacute;e &agrave; l&rsquo;horizontale dans les &eacute;pis, qu&rsquo;on peut appr&eacute;cier l&rsquo;abondance des c&eacute;cidomyies. Si <strong>dans un champ de froment de vari&eacute;t&eacute; sensible</strong>, cette technique provoque l&rsquo;envol de plus de 20 insectes/ m&sup2;, un traitement insecticide &agrave; l&rsquo;aide d&rsquo;un pyr&eacute;thrino&iuml;de est recommand&eacute;, surtout si les pr&eacute;visions m&eacute;t&eacute;orologiques annoncent des soir&eacute;es calmes et douces.</p>

<p>Voir ici la <a href="http://www.cadcoasbl.be/p09_biblio/art0017/vari%C3%A9t%C3%A9sR%C3%A9sistantes17.pdf" target="_blank">liste des vari&eacute;t&eacute;s r&eacute;sistantes</a>.</p>

<p style="text-align:justify">En cas de d&eacute;clenchement brutal des vols avant la fin de la semaine, un avis sp&eacute;cifique serait &eacute;mis.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle et M. De Proft</p>
', 1, NULL, 27, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (63, 'Produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remis &agrave; jour : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a></p>
', 2, NULL, 27, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (60, 'Situation maladies en froment, épeautre', 'left', 300, 225, '<p><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">Deux-tiers des parcelles du r&eacute;seau d&rsquo;observation sont au stade derni&egrave;re feuille pointante. D&rsquo;autres ont atteint le stade derni&egrave;re feuille &eacute;tal&eacute;e et 4 parcelles sont d&eacute;j&agrave; au stade &eacute;piaison. La situation sanitaire est globalement peu pr&eacute;occupante pour le moment. Les rouilles jaune et brunes sont tout de m&ecirc;me &agrave; surveiller attentivement. Le stade derni&egrave;re feuille &eacute;tal&eacute;e est le moment o&ugrave; la protection compl&egrave;te est recommand&eacute;e si aucun traitement n&rsquo;a &eacute;t&eacute; r&eacute;alis&eacute; auparavant, de mani&egrave;re &agrave; prot&eacute;ger les feuilles dont d&eacute;pend le rendement.</p>

<p style="text-align:center"><img align="" filer_id="372" height="" src="/filer/canonical/1526376227/372/" thumb_option="" title="" width="" /></p>

<p><strong><u>Pression en maladies</u></strong></p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>septoriose</strong></a> est pr&eacute;sente dans le fond de v&eacute;g&eacute;tation de la plupart des parcelles et touche les F-2 de la moiti&eacute; de celles-ci. Sa fr&eacute;quence ne d&eacute;passe pas 25% pour les vari&eacute;t&eacute;s sensibles et 5% pour les vari&eacute;t&eacute;s tol&eacute;rantes. Cette maladie est assez discr&egrave;te cette ann&eacute;e.</p>

<p style="text-align:justify">Selon le mod&egrave;le de pr&eacute;vision &eacute;pid&eacute;miologique PROCULTURE, la septoriose n&rsquo;a pas &eacute;volu&eacute; de la m&ecirc;me mani&egrave;re dans les diff&eacute;rentes r&eacute;gions de Wallonie (Tableau 2). Dans les parcelles encore non trait&eacute;es, cette maladie n&rsquo;est pas pr&eacute;sente sur le dernier &eacute;tage foliaire. Sur l&rsquo;avant derni&egrave;re feuille d&eacute;finitive, le taux d&rsquo;infection en incubation est soit nul, soit tr&egrave;s faible (l&rsquo;infection repr&eacute;sentera moins de 5% de surface n&eacute;cros&eacute;e lorsqu&rsquo;elle sera exprim&eacute;e).</p>

<p style="text-align:center"><img align="" filer_id="373" height="" src="/filer/canonical/1526376783/373/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">La <strong><a href="http://www.cadcoasbl.be/p09_biblio.html#art00012">fusariose</a></strong> du feuillage est signal&eacute;e sur la vari&eacute;t&eacute; Edgar dans le Brabant Wallon. Les sympt&ocirc;mes de cette maladie ressemblent &agrave; ceux de la septoriose &agrave; la diff&eacute;rence qu&rsquo;aucune pycnide (point noir au centre des taches) ne se forme. Maladie &agrave; surveiller car les feuilles peuvent &ecirc;tre une source d&rsquo;inoculum pour la fusariose de l&rsquo;&eacute;pi.</p>

<p style="text-align:center"><img align="" filer_id="374" height="" src="/filer/canonical/1526377200/374/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">L&rsquo;<strong>o&iuml;dium</strong></a><strong> </strong>n&rsquo;est observ&eacute; que sur le bas des plantes dans 4 parcelles du r&eacute;seau &agrave; des fr&eacute;quences et incidences tr&egrave;s faible.</p>

<p style="text-align:justify">La <a href="http://www.cadcoasbl.be/p09_biblio/art0006/0704RJSymptome.pdf" target="_blank"><strong>rouille jaune</strong></a><span style="color:black">, </span>est visible dans la moiti&eacute; des parcelles du r&eacute;seau. Elle est pr&eacute;sente &agrave; des fr&eacute;quences variant de 5% des F-2 sur les vari&eacute;t&eacute;s tol&eacute;rantes &agrave; 100% des F-1 dans les cas les plus graves sur vari&eacute;t&eacute; sensible.</p>

<p style="text-align:justify">Des pustules de <a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille brune</strong></a> sont visibles sur vari&eacute;t&eacute; sensible &agrave; Ath et Ellignies Sainte-Anne (F-2 de Gedser), &agrave; Mortroux (F-1 de Graham), &agrave; Les Waleffes (F-2 de Graham). A Mortroux, des vari&eacute;t&eacute;s tol&eacute;rantes (Sacramento, Reflection) ou moyennement r&eacute;sistante (Edgar) sont &eacute;galement touch&eacute;es. Dans tous les cas, cela ne concerne que quelques pustules sur maximum 5% des plantes.</p>

<p>&nbsp;</p>

<p><u><strong>Recommandations</strong></u></p>

<p style="text-align:justify">Si un traitement a &eacute;t&eacute; r&eacute;alis&eacute; au stade 32, il est recommand&eacute; d&rsquo;attendre et de r&eacute;&eacute;valuer la situation maximum 4 semaines apr&egrave;s le traitement. Au besoin, un traitement sera r&eacute;alis&eacute; afin de prot&eacute;ger les derniers &eacute;tages folaires. Pour les parcelles au stade 37, il est recommand&eacute; d&rsquo;attendre la sortie compl&egrave;te de la derni&egrave;re feuille afin d&rsquo;envisager un traitement de mani&egrave;re &agrave; prot&eacute;ger l&rsquo;enti&egrave;ret&eacute; de la derni&egrave;re feuille. Au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39), un traitement devrait &ecirc;tre envisag&eacute; si une maladie est pr&eacute;sente sur un des trois derniers &eacute;tages foliaires, peu importe la vari&eacute;t&eacute;. Ce traitement devra &ecirc;tre complet et assurer une bonne protection contre l&rsquo;ensemble des maladies (triazole et carboxamide). Si tel n&rsquo;est pas le cas, la protection peut &ecirc;tre report&eacute;e <strong><u>avec vigilance</u></strong>.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 27, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (64, 'Colza : Floraison terminée ou presque', 'left', 300, 225, '<p style="text-align:justify">Les champs de colza d&rsquo;hiver les plus avanc&eacute;s sont d&eacute;fleuris.&nbsp; La floraison y aura &eacute;t&eacute; rapide, suite aux journ&eacute;es chaudes.&nbsp; Les siliques continuent &agrave; grandir, mais leur nombre est d&eacute;finitif.</p>

<p style="text-align:justify">En revanche, dans les champs de colza ayant subi de nombreux avortements de boutons floraux, on peut voir des p&eacute;doncules sans silique. Ces champs continuent de fleurir, et ceci permettra aux plantes un certain rattrapage, par la formation de nouvelles siliques dont le nombre s&rsquo;accro&icirc;t encore.&nbsp; Leur maturit&eacute; sera plus tardive.</p>

<p style="text-align:justify">Dans les p&eacute;tioles des feuilles du bas, on retrouve des larves d&rsquo;altises r&eacute;sultant de pontes &agrave; l&rsquo;automne.&nbsp; Des tiges d&eacute;form&eacute;es indiquent la pr&eacute;sence de larves de charan&ccedil;ons de la tige, dont les adultes ont &eacute;t&eacute; observ&eacute;s au d&eacute;but du printemps.&nbsp; Par ailleurs, des larves de m&eacute;lig&egrave;thes sont pr&eacute;sentes dans les fleurs, de m&ecirc;me que les premi&egrave;res larves de c&eacute;cidomyies des siliques dans quelques siliques perfor&eacute;es par le charan&ccedil;on des siliques.</p>

<p style="text-align:justify">Le printemps 2018, outre ses extr&ecirc;mes climatiques, aura donc aussi donn&eacute; lieu &agrave; une forte pression des insectes ravageurs en colza d&rsquo;hiver.</p>

<p style="margin-left:106.2pt; margin-right:0cm; text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP</p>

<p style="text-align:right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Michel De Proft, Expert scientifique CRA-W</p>

<p style="text-align:justify">Le r&eacute;seau d&rsquo;observations et d&rsquo;avertissement mis en place chaque ann&eacute;e en R&eacute;gion wallonne, permet de suivre de pr&egrave;s l&rsquo;&eacute;volution de plusieurs champs de colza d&rsquo;hiver r&eacute;partis dans les principales r&eacute;gions de production et d&rsquo;alerter au mieux les producteurs de colza.</p>

<p style="text-align:justify">Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNE-D&eacute;veloppement et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</p>
', 1, NULL, 28, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (90, 'A l''agenda', 'left', 300, 225, '<p><span style="color:red">A destination des producteurs de c&eacute;r&eacute;ales,</span></p>

<p><span style="color:red">les quatre soir&eacute;es ci-apr&egrave;s octroient des points phytolicence</span></p>

<p>&nbsp;</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td>
			<p>Jeudi<br />
			13/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">&nbsp; Gembloux<br />
			&nbsp;&nbsp; 19h30</span></p>
			</td>
			<td>
			<p><strong>Soir&eacute;e C&eacute;r&eacute;ales</strong> Livre Blanc en l&#39;espace Senghor &agrave; l&#39;avenue de la Facult&eacute;, 11</p>

			<p><a href="http://www.cadcoasbl.be/p10_agenda.html">Plus d&rsquo;information ici</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mardi<br />
			18/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">Braine-le-Comte </span><span style="color:#ff0000">(sur inscription)<br />
			18h00</span></p>
			</td>
			<td>
			<p><strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. Salle Scaubecq, chemin de Mariemont, 7 (CARAH)</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mercredi<br />
			19/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">Kain<br />
			18h00</span></p>
			</td>
			<td>
			<p><strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. A la Ferme du Reposoir, chemin du Ruisseau 4 &agrave; 7540 Kain (CARAH)</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Jeudi<br />
			20/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">Ath (sur inscription)<br />
			18h00</span></p>
			</td>
			<td>
			<p><strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. Salle L.Delm&eacute;e, 11 rue P.Pastur (CARAH)</p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 3, NULL, 40, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (66, 'Ravageurs des céréales : émergences de cécidomyie orange', 'left', 300, 225, '<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="color:#726056">Observations du vendredi 18 mai 2018 </span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="color:#726056">R&eacute;seaux : 20 parcelles r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath,</span> <span style="color:#726056">Melles), Li&egrave;ge (Eben-Emael, Fexhe-slins, Les-Waleffes, Pailhe), Brabant Wallon (Jandrain) et Namur (Anth&eacute;e, Assesse, Biesmer&eacute;e, Ciney, Clermont, Foy notre Dame, Franc-Waret, Gembloux, Hanret, Meux, Rhisnes, Saint-G&eacute;rard, Stave)</span></p>

<p style="text-align:justify"><strong><u>Pucerons et crioc&egrave;res</u></strong></p>

<p style="text-align:justify">Les populations de ces deux insectes sont faibles et ne n&eacute;cessitent aucune intervention : de 0 &agrave; 8 pucerons par 100 talles et de 0 &agrave; 48 larves de crioc&egrave;res pour 100 talles.</p>

<p style="text-align:justify"><strong><u>C&eacute;cidomyie orange</u></strong> : Jusqu&rsquo;&agrave; pr&eacute;sent, les &eacute;mergences de c&eacute;cidomyie orange, r&eacute;pondant aux petites pluies &eacute;parses d&rsquo;avril, ont &eacute;t&eacute; tr&egrave;s limit&eacute;es. En revanche, les fortes pluies des deux derniers jours d&rsquo;avril ont vraisemblablement induit la nymphose d&rsquo;une forte proportion de la population de c&eacute;cidomyie orange, et une grosse vague d&rsquo;&eacute;mergence est attendue d&rsquo;ici dans les tout prochains jours.</p>

<p style="text-align:center"><strong>Emergences massives + d&eacute;but d&rsquo;&eacute;piaison = sc&eacute;nario &agrave; risque</strong></p>

<p style="text-align:justify">La m&eacute;t&eacute;o pr&eacute;voit une semaine douce et plut&ocirc;t humide ; le vent devrait &ecirc;tre faible. Ces conditions conviennent &agrave; l&rsquo;activit&eacute; de la c&eacute;cidomyie orange. Tous les &eacute;l&eacute;ments du risque semblent donc r&eacute;unis pour que le froment soit soumis &agrave; des attaques importantes de ce ravageur dans les prochains jours.</p>

<p style="text-align:justify">Dans ces conditions, il est vivement recommand&eacute; de se tenir pr&ecirc;t &agrave; effectuer un traitement insecticide aussit&ocirc;t que des vols significatifs auront &eacute;t&eacute; observ&eacute;s. Selon les r&eacute;gions, leur d&eacute;clenchement peut varier de deux &agrave; trois jours.</p>

<p style="text-align:justify">L&rsquo;observation de la c&eacute;cidomyie est impossible en pleine journ&eacute;e. Il faut aller au cr&eacute;puscule, passer une baguette tenue horizontalement &agrave; hauteur des &eacute;pis. Cette op&eacute;ration d&eacute;range les femelles occup&eacute;es &agrave; pondre, et on peut en estimer le nombre. Si plus d&rsquo;une vingtaine d&rsquo;insectes s&rsquo;envolent / m&sup2;, un traitement insecticide est recommand&eacute;.</p>

<p>Voir ici la <a href="http://www.cadcoasbl.be/p09_biblio/art0017/vari%C3%A9t%C3%A9sR%C3%A9sistantes17.pdf" target="_blank">liste des vari&eacute;t&eacute;s r&eacute;sistantes</a></p>

<p style="text-align:center"><img align="" filer_id="376" height="540" src="/filer/canonical/1526982362/376/" thumb_option="" title="" width="527" /></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle et M. De Proft</p>
', 1, NULL, 29, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (82, 'Phytolicence : formations continues', 'left', 300, 225, '<p style="text-align:justify">Dans le cadre de la Foire agricole de Libramont, le Comit&eacute; r&eacute;gional PHYTO organise 2 formations continues reconnues pour la phytolicence. <a href="http://www.crphyto.be/sites/default/files/kcfinder/files/Formations%20Foire%20agricole%20Libramont%202018%281%29.pdf">Informations et inscriptions ici</a></p>

<ul>
	<li style="text-align:justify">Formation continue du <strong>vendredi 27/07</strong> (de 10 &agrave; 12h) est un tout nouveau module, le 1er module de formation enti&egrave;rement bas&eacute; sur des exercices pratiques &agrave; destination du public agricole.</li>
	<li style="text-align:justify">Formation du <strong>samedi 28/07</strong> (10 &agrave; 12h) matin a pour th&egrave;me la lutte int&eacute;gr&eacute;e.</li>
</ul>
', 3, NULL, 39, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (65, 'Situation maladies en froment, épeautre', 'left', 300, 225, '<p style="text-align:center"><img align="" filer_id="380" height="" original_image="false" src="/filer/canonical/1526983291/380/" thumb_option="" title="" width="" /></p>

<p><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p>Cette semaine, la septoriose a tr&egrave;s peu progress&eacute; dans les parcelles qui ont presque toutes d&eacute;pass&eacute; le stade derni&egrave;re feuille &eacute;tal&eacute;e. La rouille jaune et la rouille brune sont &agrave; surveiller, particuli&egrave;rement sur les vari&eacute;t&eacute;s r&eacute;put&eacute;es sensibles.</p>

<p style="text-align:center"><img align="" filer_id="378" height="" src="/filer/canonical/1526982985/378/" thumb_option="" title="" width="" /></p>

<p><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>septoriose</strong></a> est peu observ&eacute;e cette semaine. Lorsqu&rsquo;elle est pr&eacute;sente, elle touche moins de 20% des F3 d&eacute;finitives (12 parcelles concern&eacute;es) &agrave; 2 exceptions pr&egrave;s : &agrave; Les Waleffes o&ugrave; elle touche 5% des F2 d&eacute;finitives (Graham) et Ath o&ugrave; 30% des F3 d&eacute;finitives (Reflection) pr&eacute;sentent des sympt&ocirc;mes. Dans les autres situations, elle est observ&eacute;e uniquement dans le bas des plantes</p>

<p style="text-align:justify">Les conditions climatiques de cette semaine n&rsquo;ont pas &eacute;t&eacute; favorables au d&eacute;veloppement de la septoriose et aucune pluie contaminatrice n&rsquo;a &eacute;t&eacute; observ&eacute;e dans le r&eacute;seau. Le mod&egrave;le de pr&eacute;vision &eacute;pid&eacute;miologique PROCULTURE pr&eacute;voit l&rsquo;expression des sympt&ocirc;mes dus aux infections latentes d&eacute;but juin. Dans tous les cas, moins de 5% de la surface foliaire est concern&eacute;e.</p>

<p style="text-align:center"><img align="" filer_id="377" height="" src="/filer/canonical/1526982840/377/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">La <strong><u><span style="color:#0070c0">fusariose</span></u></strong> du feuillage est signal&eacute;e sur la vari&eacute;t&eacute; Edgar dans le Brabant Wallon et la province de Li&egrave;ge. Les sympt&ocirc;mes de cette maladie ressemblent &agrave; ceux de la septoriose &agrave; la diff&eacute;rence qu&rsquo;aucune pycnide (point noir au centre des taches) ne se forme. Maladie &agrave; surveiller car les feuilles peuvent &ecirc;tre une source d&rsquo;inoculum pour la fusariose de l&rsquo;&eacute;pi. Sur les feuilles, les sympt&ocirc;mes sont plut&ocirc;t caus&eacute;s par des champignons du genre <em>Microdochium</em> qui ne produisent pas de mycotoxine lorsqu&rsquo;ils infectent les &eacute;pis, au contraire des <em>Fusarium </em>spp</p>

<p style="text-align:center"><img align="" filer_id="374" height="335" src="/filer/canonical/1526377200/374/" thumb_option="" title="" width="484" /></p>

<p style="text-align:justify"><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">L&rsquo;<strong>o&iuml;dium</strong></a><strong> </strong>est parfois observ&eacute; dans les parcelles du r&eacute;seau. Il est pr&eacute;sent &agrave; faible fr&eacute;quence &agrave; Ath, Eben-Emael et Mortroux. &Agrave; Ciney (Anapolis), Assesse (Reflection) et Franc-Waret (Reflection), par contre, l&rsquo;ensemble des plantes observ&eacute;es sont colonis&eacute;es par l&rsquo;o&iuml;dium.</p>

<p style="text-align:justify">La <a href="http://www.cadcoasbl.be/p09_biblio/art0006/0704RJSymptome.pdf" target="_blank"><strong>rouille jaune</strong></a> est observ&eacute;e dans 16 de nos 31 parcelles d&rsquo;observation. La pression occasionn&eacute;e par cette maladie d&eacute;pend fortement de la vari&eacute;t&eacute;. Elle est pr&eacute;sente sur les derni&egrave;res ou avant derni&egrave;res feuilles d&eacute;finitives de toutes nos parcelles emblav&eacute;es par la vari&eacute;t&eacute; sensible Reflection. Ailleurs, les vari&eacute;t&eacute;s plus tol&eacute;rantes portent des pustules sur les feuilles plus basses et &agrave; des fr&eacute;quences relativement faibles. &Agrave; Eben Emael, 75% des avant derni&egrave;res feuilles d&eacute;finitives de la vari&eacute;t&eacute; r&eacute;sistante Anapolis sont touch&eacute;es. C&rsquo;est la seule situation o&ugrave; une vari&eacute;t&eacute; r&eacute;put&eacute;e tr&egrave;s tol&eacute;rante est fortement infect&eacute;e par la rouille jaune.</p>

<p style="text-align:justify">La <a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille brune</strong></a> est pr&eacute;sente dans 9 parcelles d&rsquo;observation. Dans 6 parcelles, elle touche moins de 20% des F2 ou F3 d&eacute;finitives. &Agrave; Les Waleffes, elle touche 45% des F2 de la vari&eacute;t&eacute; Graham et 35% des F3 de la vari&eacute;t&eacute; Edgar. Enfin &agrave; Assesse, la rouille brune touche 60% des F2 de la vari&eacute;t&eacute; Reflection pourtant r&eacute;put&eacute;e tol&eacute;rante.</p>

<p style="text-align:justify">&nbsp;</p>

<p><strong><u>Recommandations</u></strong></p>

<p style="text-align:justify">Pour les parcelles au stade 39 qui n&rsquo;ont pas encore &eacute;t&eacute; trait&eacute;es, un traitement doit &ecirc;tre envisag&eacute; si la pr&eacute;sence d&rsquo;une maladie foliaire est d&eacute;tect&eacute;e sur une des 3 derni&egrave;res feuilles, peu importe la vari&eacute;t&eacute;. Si la pression en maladie reste faible au sein de la parcelle, le traitement peut &ecirc;tre postpos&eacute;, <strong>avec vigilance</strong>.</p>

<p style="text-align:justify">Pour les parcelles au stade 51-59 (&eacute;piaison), un traitement doit &eacute;galement &ecirc;tre envisag&eacute; si la parcelle n&rsquo;a pas encore &eacute;t&eacute; trait&eacute;e et si la pr&eacute;sence d&rsquo;une maladie foliaire est d&eacute;tect&eacute;e sur une des 3 derni&egrave;res feuilles, peu importe la vari&eacute;t&eacute;. Si la pression en maladie reste faible au sein de la parcelle, le traitement peut encore &ecirc;tre postpos&eacute;, <strong>avec vigilance</strong>.</p>

<p style="text-align:justify">Pour les parcelles qui ont &eacute;t&eacute; trait&eacute;es au stade 32, la protection apport&eacute;e par ce traitement se dissipe au bout de 3 &agrave; 4 semaines et un traitement relais devrait &ecirc;tre envisag&eacute; pour prot&eacute;ger les derniers &eacute;tages foliaires.</p>

<p style="text-align:justify">Le traitement r&eacute;alis&eacute; une fois le stade 39 atteint (au stade 39 ou apr&egrave;s selon les trois possibilit&eacute;s d&eacute;taill&eacute;es ci-dessus) doit &ecirc;tre complet et assurer une bonne protection contre la septoriose (m&eacute;lange de triazoles ou triazole associ&eacute;e au carboxamide), la rouille brune (strobilurine si vari&eacute;t&eacute; sensible) et la fusariose de l&rsquo;&eacute;pi (prothioconazole, tebuconazole et metconazole). Le premier cit&eacute; est efficace contre <em>Fusarium </em>spp. et <em>Microdochium </em>spp., les deux suivants sont efficaces contre <em>Fusarium </em>spp. Le risque de fusariose de l&rsquo;&eacute;pi est fortement li&eacute; aux pr&eacute;visions de fortes pluies lors de l&rsquo;&eacute;piaison et de la floraison, ce qui n&rsquo;est pas annonc&eacute; cette semaine. Enfin, si un traitement au stade 39 a d&eacute;j&agrave; &eacute;t&eacute; r&eacute;alis&eacute;, la protection des feuilles est encore bien pr&eacute;sente et il n&rsquo;est donc pas n&eacute;cessaire de traiter &agrave; nouveau.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 29, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (69, 'Avertissement Spécial cécidomyie orange', 'left', 300, 225, '<p style="text-align:justify">L&rsquo;infestation de c&eacute;cidomyie orange est tr&egrave;s &eacute;lev&eacute;e. Les vols observ&eacute;s ce soir, 23/05, sont impressionnants. Les pr&eacute;visions m&eacute;t&eacute;o des heures et des jours qui viennent sont exactement celles qui conviennent &agrave; l&rsquo;insecte.</p>

<p style="text-align:justify">La situation est dangereuse et justifie une <strong><u>intervention insecticide imm&eacute;diate</u></strong> dans tout champ de froment ayant atteint le d&eacute;but de l&rsquo;&eacute;piaison. Les vari&eacute;t&eacute;s r&eacute;sistantes ne permettent pas aux larves de c&eacute;cidomyie orange de se d&eacute;velopper dans les &eacute;pis, mais elles peuvent n&eacute;anmoins souffrir des attaques avort&eacute;es. C&rsquo;est pourquoi, au vu des tr&egrave;s haut niveaux d&rsquo;attaque, nous recommandons le traitement insecticide, <strong><u>m&ecirc;me des vari&eacute;t&eacute;s r&eacute;sistantes</u></strong>.</p>

<p style="text-align:justify">Id&eacute;alement, les traitements insecticides devraient se faire plut&ocirc;t le soir ou le matin dans la ros&eacute;e. Un seul traitement devrait suffire pour couvrir toute la p&eacute;riode d&rsquo;attaque. &raquo;</p>

<p style="text-align:center"><img align="" filer_id="381" height="" src="/filer/canonical/1527142193/381/" thumb_option="" title="" width="" /></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 30, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (70, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remis &agrave; jour : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a>&nbsp; &hellip; voir liste insecticides autoris&eacute;s contre c&eacute;cidomyies</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 30, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (72, 'Cécidomyie orange : ravageur de l’année', 'left', 300, 225, '<p style="text-align:justify">Des vols extr&ecirc;mement abondants de c&eacute;cidomyie orange ont &eacute;t&eacute; observ&eacute;s au cours de la semaine &eacute;coul&eacute;e. Malgr&eacute; l&rsquo;appel &agrave; la vigilance &eacute;mis le 22/04, les observations faites en soir&eacute;e, et m&ecirc;me t&ocirc;t le matin, &eacute;taient telles que nous avons jug&eacute; utile d&rsquo;envoyer un avis &laquo; sp&eacute;cial c&eacute;cidomyie orange &raquo; le 25/04, conseillant de traiter imm&eacute;diatement tout froment ayant atteint le d&eacute;but de l&rsquo;&eacute;piaison, y compris les vari&eacute;t&eacute;s r&eacute;sistantes.</p>

<p style="text-align:justify"><u>Pourquoi un avis &agrave; ce point inqui&eacute;tant ?</u></p>

<p style="text-align:justify">Parce que tous les &eacute;l&eacute;ments &eacute;taient r&eacute;unis pour conduire &agrave; de tr&egrave;s gros d&eacute;g&acirc;ts : forte population de c&eacute;cidomyies, co&iuml;ncidence parfaite des vols avec la phase sensible de la plupart des bl&eacute;s, conditions m&eacute;t&eacute;o id&eacute;ales pour les vols et les pontes. Dans pareilles conditions, on peut perdre plus de 10 % de rendement en une seule nuit.</p>

<p style="text-align:justify"><u>Pourquoi avoir recommand&eacute; le traitement, m&ecirc;me des vari&eacute;t&eacute;s r&eacute;sistantes ?</u></p>

<p style="text-align:justify">Parce que ces vari&eacute;t&eacute;s sont attaqu&eacute;es autant que les sensibles et que, m&ecirc;me si les jeunes larves meurent tr&egrave;s peu de temps apr&egrave;s avoir commenc&eacute; &agrave; s&rsquo;alimenter, ces attaques avort&eacute;es co&ucirc;tent &agrave; la plante. Des essais ont montr&eacute; qu&rsquo;en cas de forte pression, les pertes de rendement provoqu&eacute;es sur vari&eacute;t&eacute;s r&eacute;sistantes pouvaient &ecirc;tre tr&egrave;s s&eacute;rieuses (jusqu&rsquo;&agrave; 8-10%).</p>

<p style="text-align:justify"><u>Les traitements effectu&eacute;s couvriront-ils toute la p&eacute;riode d&rsquo;attaque ?</u></p>

<p style="text-align:justify">Oui. Un traitement appliqu&eacute; &agrave; l&rsquo;&eacute;clatement des gaines (= d&eacute;but de la phase sensible du bl&eacute;) prot&egrave;gera des attaques jusqu&rsquo;&agrave; la fin de la floraison (fin de la phase sensible du bl&eacute;).</p>

<p style="text-align:justify"><u>Les champs les plus tardifs, arrivant seulement &agrave; l&rsquo;&eacute;clatement des gaines peuvent-ils encore subir des attaques dommageables ? Cette question revient &agrave; demander si la c&eacute;cidomyie est encore pr&eacute;sente en grande abondance, ou si de nouvelles &eacute;mergences sont encore attendues.</u></p>

<p style="text-align:justify">Vraisemblablement pas. Toutefois, les conditions m&eacute;t&eacute;orologiques sont tellement favorables &agrave; l&rsquo;insecte ces derniers jours qu&rsquo;il pourrait bien demeurer pr&eacute;sent et nuisible plus longtemps qu&rsquo;&agrave; l&rsquo;accoutum&eacute;e. Les bl&eacute;s tardifs m&eacute;ritent donc d&rsquo;&ecirc;tre observ&eacute;s au cr&eacute;puscule. Si en effleurant les &eacute;pis &agrave; l&rsquo;aide d&rsquo;une baguette tenue horizontalement, on provoque l&rsquo;envol de plus de 20 c&eacute;cidomyies, un traitement insecticide est recommand&eacute;.</p>

<p style="text-align:justify"><u>Peut-on mesurer l&rsquo;attaque subie par un bl&eacute;, et donc la &laquo; production &raquo; de c&eacute;cidomyies retournant au sol ?</u></p>

<p style="text-align:justify">Oui, c&rsquo;est possible et m&ecirc;me assez facile, par extraction des larves pr&eacute;sentes dans les &eacute;pis. D&rsquo;ici trois &agrave; quatre semaines, cette op&eacute;ration sera r&eacute;alisable. Des indications pr&eacute;cises quant &agrave; la technique &agrave; utiliser seront donn&eacute;es dans un prochain avertissement.</p>

<p style="text-align:justify"><strong><u>Pucerons et crioc&egrave;res</u></strong></p>

<p style="text-align:justify">En froment et &eacute;peautre : les populations de pucerons sont faibles partout dans le r&eacute;seau, de 0 &agrave; 26 pucerons/100 talles. Pour les crioc&egrave;res (<em>lema</em>), les niveaux sont de 0 &agrave; 29 larves de crioc&egrave;res/ 100 talles.</p>

<p style="text-align:justify">Bien que plus abondants qu&rsquo;&agrave; l&rsquo;habitude, les crioc&egrave;res ne justifient pas d&rsquo;intervention. A noter que le traitement appliqu&eacute; contre la c&eacute;cidomyie a &eacute;videmment un effet sur les autres ravageurs.&nbsp;</p>

<p style="text-align:justify">Nos observations continuent. Le prochain avertissement sera celui du 05 juin.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 31, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (73, 'Produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remis &agrave; jour : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a>&nbsp; &hellip; voir liste insecticides autoris&eacute;s contre c&eacute;cidomyies</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 31, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (68, 'Vos terres sont-elles infestées de vulpin résistant ?', 'left', 300, 225, '<p style="text-align:justify">La r&eacute;sistance des vulpins aux herbicides est un ph&eacute;nom&egrave;ne largement r&eacute;pandu en Europe, notamment en France et au Royaume-Uni et cela n&#39;est pas sans poser certains probl&egrave;mes. Les agriculteurs anglais, par exemple, sont parfois contraints &agrave; mettre en &oelig;uvre des programmes herbicides &agrave; 3 passages incluant en moyenne 6 substances actives diff&eacute;rentes.</p>

<p style="text-align:justify">Actuellement, le ph&eacute;nom&egrave;ne en Belgique semble &ecirc;tre en expansion et certaines zones g&eacute;ographiques comme les Polders, le Tournaisis et la r&eacute;gion de Fosses-la-Ville sont particuli&egrave;rement touch&eacute;es. Il n&#39;est pas rare de retrouver un peu partout dans nos campagnes des taches de vulpins d&eacute;passant des c&eacute;r&eacute;ales. Cela est-il d&ucirc; &agrave; traitement mal positionn&eacute;, &agrave; des conditions climatiques non optimales au moment du traitement, &agrave; un mauvais recouvrement, ou bien &agrave; la pr&eacute;sence de vulpins r&eacute;sistants ?</p>

<p style="text-align:justify">Cette ann&eacute;e, l&rsquo;Unit&eacute; Protection des Plantes et Ecotoxicologie du CRA-W effectue une enqu&ecirc;te afin d&#39;&eacute;valuer la proportion de vulpins r&eacute;sistants en Wallonie et de d&eacute;terminer les pratiques permettant un meilleur contr&ocirc;le de ces r&eacute;sistances. Dans le cadre de cette enqu&ecirc;te, le CRA-W vous propose de <strong>tester gratuitement la r&eacute;sistance des vulpins pr&eacute;sents dans vos terres</strong>. Il vous est simplement demand&eacute; de r&eacute;colter les semences de vulpin &agrave; maturit&eacute; (fin juin &ndash; d&eacute;but juillet) ainsi que de nous communiquer quelques informations culturales sur la parcelle. Les vulpins pr&eacute;lev&eacute;s seront test&eacute;s en serres durant l&rsquo;hiver et les r&eacute;sultats vous seront communiqu&eacute;s. Ce type de renseignement peut vous aider &agrave; mieux appr&eacute;hender votre d&eacute;sherbage des c&eacute;r&eacute;ales et la lutte contre le vulpin en particulier.</p>

<p style="text-align:justify">Int&eacute;ress&eacute; ? Merci de prendre contact avec Pierre Hellin via l&rsquo;adresse <a href="mailto:p.hellin@cra.wallonie.be">p.hellin@cra.wallonie.be</a> ou via le num&eacute;ro de t&eacute;l&eacute;phone 081.62.52.62 pour recevoir plus de d&eacute;tail sur la proc&eacute;dure.</p>

<p style="text-align:right"><strong><u>Chercheur au CRA-W</u></strong><strong><em> </em></strong><strong>: </strong>P. Hellin, <a href="mailto:p.hellin@cra.wallonie.be">p.hellin@cra.wallonie.be</a></p>
', 2, NULL, 29, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (83, 'Nouvelle édition du carnet de champ, un outil agronomique et de traçabilité', 'left', 300, 225, '<p><strong><span style="color:#92d050">La nouvelle &eacute;dition est disponible !!</span></strong></p>

<p style="text-align:justify">Ce <strong>carnet </strong>(format de poche) comprend 44 pages dont, une &eacute;ph&eacute;m&eacute;ride, des renseignements sur des asbl &oelig;uvrant pour la Profession (R&eacute;quasud, CGFC, CRP, &hellip;) des tableaux (38) permettant de collationner, par parcelles, les interventions men&eacute;es. Des carnets sont disponibles dans les d&eacute;p&ocirc;ts habituels ou sur demande via ce <a href="https://docs.google.com/forms/d/e/1FAIpQLSf12vXXyzgeNW6QK7-bzMoyskHzilKw9dZB7E1G1wIkjI86Xw/viewform">formulaire</a></p>
', 2, NULL, 39, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (99, 'Jaunisse nanisante : Vers une saison à faible pression ?', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Un &eacute;l&eacute;ment de plus invitant &agrave; penser que la pression de cette ann&eacute;e pourrait &ecirc;tre faible : Actuellement, on trouve tr&egrave;s rarement des pucerons dans les repousses. Une petite trentaine a pu &ecirc;tre collect&eacute;e et sera prochainement analys&eacute;e pour en mesurer le pouvoir virulif&egrave;re. Les r&eacute;sultats vous seront communiqu&eacute;s par la suite.</span></p>

<p style="text-align:justify"><span style="color:black">Pour les nouvelles emblavures, une infestation par des vols lointains est toujours possible,&hellip; &agrave; voir en saison.</span></p>

<p style="text-align:justify"><span style="color:black">Notre surveillance hebdomadaire reprendra d&egrave;s la lev&eacute;e jusqu&rsquo;&agrave; l&rsquo;entr&eacute;e effective de l&rsquo;hiver.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 43, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (71, 'Situation maladies en froment et épeautre', 'left', 300, 225, '<p style="text-align:justify">Observer avant de d&eacute;cider : il est recommand&eacute; d&rsquo;observer vos terres avant de d&eacute;cider d&rsquo;un &eacute;ventuel traitement.</p>

<p style="text-align:justify">Dans notre r&eacute;seau de parcelles d&rsquo;essais non trait&eacute;es, les observations syst&eacute;matiques sont termin&eacute;es. La plupart des parcelles sont aux stades &eacute;piaison ou floraison. La septoriose aura &eacute;t&eacute; globalement discr&egrave;te cette ann&eacute;e. La rouille jaune aura &eacute;t&eacute; tr&egrave;s probl&eacute;matique sur les vari&eacute;t&eacute;s sensibles (Reflection, &hellip;) alors qu&rsquo;elle n&rsquo;a pas &eacute;t&eacute; fortement observ&eacute;e sur les vari&eacute;t&eacute;s r&eacute;sistantes. La rouille brune est &eacute;galement assez bien pr&eacute;sente cette ann&eacute;e sur vari&eacute;t&eacute;s sensibles.</p>

<p style="text-align:justify"><u>Conseil</u> :</p>

<ul>
	<li style="text-align:justify">Pour les parcelles au stade 51-59 (&eacute;piaison), un traitement doit &ecirc;tre envisag&eacute; si la parcelle n&rsquo;a pas encore &eacute;t&eacute; trait&eacute;e et si la pr&eacute;sence d&rsquo;une maladie foliaire est d&eacute;tect&eacute;e sur une des 3 derni&egrave;res feuilles, peu importe la vari&eacute;t&eacute;. Si la pression en maladie reste faible au sein de la parcelle, le traitement peut encore &ecirc;tre postpos&eacute;, avec vigilance.</li>
</ul>

<ul>
	<li style="text-align:justify">Pour les parcelles qui ont &eacute;t&eacute; trait&eacute;es au stade 32, la protection apport&eacute;e par ce traitement se dissipe au bout de 3 &agrave; 4 semaines et un traitement relais devrait &ecirc;tre envisag&eacute; pour prot&eacute;ger les derniers &eacute;tages foliaires. Le traitement r&eacute;alis&eacute; au stade 39 ou apr&egrave;s le stade 39 doit &ecirc;tre complet et assurer une bonne protection contre la septoriose (m&eacute;lange de triazoles ou triazole associ&eacute;e au carboxamide) et la rouille brune (strobilurine si vari&eacute;t&eacute; sensible). Lorsqu&rsquo;il est r&eacute;alis&eacute; apr&egrave;s le stade 39, il peut en outre prendre en compte le risque li&eacute; &agrave; la fusariose de l&rsquo;&eacute;pi.</li>
</ul>

<p style="text-align:justify"><u>Fusariose de l&rsquo;&eacute;pi</u></p>

<p style="text-align:justify">La sensibilit&eacute; vari&eacute;tale est le premier param&egrave;tre &agrave; prendre en compte. Les situations &agrave; risque sont les cultures de froment de vari&eacute;t&eacute;s sensibles dans lesquelles le travail du sol a &eacute;t&eacute; r&eacute;duit et les froments (vari&eacute;t&eacute; sensible) apr&egrave;s froment ou ma&iuml;s, particuli&egrave;rement lorsque les cannes sont encore apparentes dans la parcelle. Le temps humide (orage,&hellip;) favorise le d&eacute;veloppement de la maladie. C&rsquo;est au stade floraison (au plus tard entre le d&eacute;but et la mi-floraison, stade 61 &agrave; 65) que l&rsquo;on peut intervenir si n&eacute;cessaire contre la fusariose de l&rsquo;&eacute;pi. L&rsquo;utilisation de prothioconazole est le plus indiqu&eacute; pour lutter contre les deux &laquo; types &raquo; de pathog&egrave;nes de la fusariose. Le t&eacute;buconazole et le metconazole sont, quant &agrave; eux, utiles uniquement contre les <em>Fusarium spp</em>.</p>

<p style="text-align:justify">A savoir que les &eacute;peautres sont g&eacute;n&eacute;ralement moins sensibles &agrave; la fusariose de l&rsquo;&eacute;pi que les froments.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, NULL, 31, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (44, 'Situation maladies en escourgeon ', 'left', 800, 400, '<p><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">La pression en maladie est relativement faible mais les maladies sont bien pr&eacute;sentes. Il faut donc bien surveiller ses parcelles. Si le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39) est atteint, une protection devrait donc &ecirc;tre envisag&eacute;e.</p>

<p><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify">Le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39) est atteint dans la plupart des parcelles et proche de l&rsquo;&ecirc;tre dans les autres.&nbsp;</p>

<p><strong><u>Pression en maladies</u></strong></p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a>, bien que pr&eacute;sente, est souvent peu pr&eacute;occupante. En effet, elle est observ&eacute;e dans 11 des 13 parcelles du r&eacute;seau mais ne touche les F-2 que dans quatre parcelles seulement. Dans deux de celles-ci, &agrave; Pailhe et Kemexhe, la fr&eacute;quence d&rsquo;infection n&rsquo;atteint que maximum 5% sur Quadriga et Tonic (vari&eacute;t&eacute;s r&eacute;sistantes, au stade 37 et 39 respectivement), et la gravit&eacute; des sympt&ocirc;mes est &eacute;galement tr&egrave;s faible, ne d&eacute;passant pas le pourcent.</p>

<p style="text-align:justify">La situation des parcelles &agrave; Assesse et &agrave; Nam&ecirc;che est diff&eacute;rente. Respectivement emblav&eacute;es avec Quadriga et Tonic et toutes deux au stade 33, 15% des F-1 sont touch&eacute;es pour la premi&egrave;re et 35% des F-2 pour la deuxi&egrave;me.&nbsp;</p>

<p style="text-align:justify"><span style="color:black">La </span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rhynchosporiose</strong></a><span style="color:black"> est pr&eacute;sente sur le bas des plantes dans 10 parcelles du r&eacute;seau. Sa fr&eacute;quence ne d&eacute;passe cependant pas 10% des plantes. Les sensibilit&eacute;s vari&eacute;tales ne sont pas marqu&eacute;es. </span></p>

<p style="text-align:justify">La <a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille naine</strong></a> est pr&eacute;sente partout sauf &agrave; Assesse et Nam&ecirc;che. Elle est pr&eacute;sente sur moins de 50% des plantes dans tous les cas et ne touche que les F-2 au maximum.</p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>o&iuml;dium</strong></a><span style="color:black"> n&rsquo;est pas signal&eacute; &agrave; l&rsquo;exception de 5 parcelles &agrave; Pailhe, Milmort et Kemexhe. 30% des F-2 du Rafaela sont touch&eacute;es &agrave; Pailhe, dans les autres cas seul le bas des plantes pr&eacute;sente des sympt&ocirc;mes d&rsquo;o&iuml;dium.</span></p>

<p><strong><u>Recommandations</u></strong></p>

<p style="text-align:justify">M&ecirc;me si la pression en maladie est faible actuellement, la protection fongicide devrait &ecirc;tre envisag&eacute;e dans les parcelles ayant atteint le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39). Ce stade est atteint dans la plupart des parcelles. En effet, les r&eacute;sultats des derni&egrave;res ann&eacute;es montrent qu&rsquo;un traitement unique au stade derni&egrave;re feuille s&rsquo;av&egrave;re indispensable quelle que soit la vari&eacute;t&eacute;. Dans ce cas, l&rsquo;utilisation de chlorothalonil permettra de lutter contre une &eacute;ventuelle future pression de ramulariose qui n&rsquo;est pas encore pr&eacute;sente.</p>

<p style="text-align:justify">Pour les parcelles qui ont &eacute;t&eacute; trait&eacute;es au stade 31, il est souhaitable de respecter un d&eacute;lai de trois semaines avant l&rsquo;application du deuxi&egrave;me traitement. De cette mani&egrave;re, le premier traitement est mieux rentabilis&eacute; et la protection est &eacute;tal&eacute;e sur une plus longue p&eacute;riode.</p>

<p style="text-align:right"><strong><em><u>Coordination</u></em></strong><em><u> <strong>scientifique</strong></u></em><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo;, A. Legr&egrave;ve, M. Delitte</p>
', 1, 363, 20, NULL);
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (74, 'Cécidomyie orange : au tour des parasitoïdes !', 'left', 300, 225, '<p style="text-align:justify">Apr&egrave;s une derni&egrave;re semaine de mai marqu&eacute;e par des vols extr&ecirc;mement abondants de c&eacute;cidomyie orange, c&rsquo;est maintenant au tour de <em>Macroglenes penetrans</em> de se faire remarquer. Ce minuscule micro-hym&eacute;nopt&egrave;re pond ses &oelig;ufs dans les &oelig;ufs de c&eacute;cidomyie orange. &OElig;uf dans &oelig;uf, puis larve dans larve, <em>M. penetrans</em> va parasiter la c&eacute;cidomyie orange jusqu&rsquo;au printemps prochain.</p>

<p style="text-align:justify">Ce parasitisme n&rsquo;emp&ecirc;che &eacute;videmment pas la larve de c&eacute;cidomyie orange de grandir aux d&eacute;pends du grain de bl&eacute; et de provoquer des pertes de rendement. En revanche, en parasitant une population (quelquefois jusqu&rsquo;&agrave; 95 % et plus !), ce petit parasito&iuml;de constitue un frein tr&egrave;s efficace au d&eacute;veloppement de la population de c&eacute;cidomyie orange pour l&rsquo;an prochain, et pour les ann&eacute;es suivantes.</p>

<p style="text-align:justify">D&eacute;sormais, tout traitement insecticide est donc &agrave; &eacute;viter. Nos observations se poursuivent n&eacute;anmoins avec comme objet principal de surveiller l&rsquo;&eacute;volution des populations de pucerons.</p>

<p style="text-align:justify">Nos observations continuent. Le prochain avertissement sera celui du 12 juin.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 37, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (75, 'Ravageurs d’été : fin de saison', 'left', 300, 225, '<p style="text-align:justify"><strong><u>Pucerons et crioc&egrave;res</u></strong></p>

<p style="text-align:justify"><strong>Observations du</strong> : 18 juin 2017</p>

<p style="text-align:justify"><strong>R&eacute;seau d&rsquo;observations de 17 champs</strong> : Anth&eacute;e, Assesse, Ath, Biesmer&eacute;e, Ciney, Clermont, Foy ND, Franc-Waret, &nbsp;Gembloux, Hanret, Jandrain, Melles, Meux, Nivelles, Rhisnes, Stave, Saint-G&eacute;rard.</p>

<p>Les observations de ce printemps en c&eacute;r&eacute;ales ont r&eacute;v&eacute;l&eacute; de faibles niveaux d&rsquo;infestation des pucerons. Les populations de pucerons vont de 0 &agrave; 62 pucerons par 100 talles ce qui est fort peu.</p>

<p style="text-align:justify">Les populations de crioc&egrave;res (<em>lema</em>) bien que parfois plus pr&eacute;sentes sont demeur&eacute;es sans risque.</p>

<p style="text-align:justify"><strong><u>Mesurer l&rsquo;attaque de c&eacute;cidomyie orange subie, c&rsquo;est mesurer le risque pour la saison &agrave; venir</u></strong></p>

<p style="text-align:justify">Les fortes attaques de c&eacute;cidomyie orange subies voici 10-15 jours, se concr&eacute;tisent actuellement par la pr&eacute;sence de petites larves orange vif situ&eacute;es &agrave; la base des grains, dans les &eacute;pillets (face ventrale ou face dorsale du grain). Ces larves ne peuvent &ecirc;tre touch&eacute;es par aucun insecticide, et plus aucune intervention ne se justifie dans la culture.</p>

<p style="text-align:justify">Les larves les plus &acirc;g&eacute;es auront bient&ocirc;t termin&eacute; leur phase alimentaire, stade &agrave; partir duquel une pluie sera n&eacute;cessaire pour qu&rsquo;elles quittent les &eacute;pis et regagnent le sol o&ugrave; elles resteront au moins jusqu&rsquo;au printemps prochain.</p>

<p style="text-align:justify">Les pr&eacute;visions m&eacute;t&eacute;orologiques indiquent que le temps devrait rester sec jusqu&rsquo;&agrave; la fin du mois. Si ceci se confirme, les larves de c&eacute;cidomyie orange les plus jeunes devraient avoir fini leur phase alimentaire et &ecirc;tre pr&ecirc;tes quitter les &eacute;pis, sans que les plus &acirc;g&eacute;es n&rsquo;aient commenc&eacute; &agrave; tomber : toutes les larves devraient quitter les &eacute;pis en une fois, lorsque la premi&egrave;re pluie se pr&eacute;sentera.</p>

<p style="text-align:justify">Cette situation peut &ecirc;tre mise &agrave; profit pour <strong>mesurer pr&eacute;cis&eacute;ment l&rsquo;attaque subie</strong>.</p>

<p style="text-align:justify"><strong>A quoi peut servir cette mesure ?</strong></p>

<p style="text-align:justify">D&rsquo;une part, &agrave; estimer la perte subie dans ses propres champs, mais surtout, &agrave; estimer la quantit&eacute; de larves qui retournent au sol et qui constitueront la menace pour la saison prochaine. Une forte production de larves observ&eacute;e dans une exploitation pourrait par exemple pousser le c&eacute;r&eacute;alier concern&eacute; &agrave; opter pour des vari&eacute;t&eacute;s de bl&eacute; r&eacute;sistantes &agrave; la c&eacute;cidomyie orange plut&ocirc;t que pour des sensibles, ou &agrave; cultiver de l&rsquo;&eacute;peautre ou de l&rsquo;escourgeon plut&ocirc;t que du bl&eacute;, les c&eacute;r&eacute;ales &agrave; grains v&ecirc;tus &eacute;tant beaucoup moins sensibles &agrave; cet insecte.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong>Pratiquement : </strong></p>

<p style="text-align:justify">L&rsquo;id&eacute;al serait de r&eacute;colter 50 tiges (10 prises de 5 tiges, en respectant une distance de 5m minimum entre deux prises), <strong><u>le plus tard possible en saison</u>, mais <u>imp&eacute;rativement avant le retour des pluies</u></strong><strong>.</strong></p>

<p style="text-align:justify">Ces petites bottes &eacute;tiquet&eacute;es (1. Localit&eacute;, 2. Date du pr&eacute;l&egrave;vement, 3. Vari&eacute;t&eacute;, 4. Date du traitement insecticide &eacute;ventuel, 5. Coordonn&eacute;es du demandeur) peuvent &ecirc;tre achemin&eacute;es au 11 rue du Bordia &agrave; 5030 Gembloux (B&acirc;timent Carson), o&ugrave; aura lieu l&rsquo;extraction et le comptage des larves. <u>Ne pas mettre ces bottes dans des sacs en plastique</u>. Ce service est ouvert &agrave; quiconque au tarif de 10 &euro; / &eacute;chantillon.</p>

<p style="text-align:justify">Cet avis termine la saison d&rsquo;avertissements.</p>

<p style="text-align:justify">Nous profitons de l&rsquo;occasion pour remercier toutes les &eacute;quipes ayant contribu&eacute;es au bon d&eacute;roulement de cette saison d&rsquo;avertissements.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 38, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (76, 'Visite de champ d’essais orge de brasserie', 'left', 300, 225, '<p style="text-align:justify">Agriculteurs, n&eacute;gociants, malteurs, brasseurs, scientifiques &hellip;. Vous &ecirc;tes nombreux &agrave; vous &ecirc;tre investis dans la fili&egrave;re orge de brasserie cette ann&eacute;e, c&rsquo;est pourquoi nous vous proposons une visite des champs d&rsquo;essais orge de brasserie r&eacute;alis&eacute;s par le CARAH (le centre pour l&rsquo;agronomie et l&rsquo;agro-industrie de la Province de Hainaut).</p>

<p style="text-align:justify">La visite aura lieu le <strong>vendredi 22 juin au Moulin de la Hunelle</strong>, Rue d&#39;Ath 90, 7950 Chi&egrave;vres. L&rsquo;accueil est pr&eacute;vu &agrave; 9h30 afin de d&eacute;marrer la visite &agrave; 10h00. La visite sera suivie d&rsquo;un &eacute;change autour du verre de l&rsquo;amiti&eacute;. Afin de planifier au mieux cette journ&eacute;e, inscription souhait&eacute;e <a href="mailto:helene.louppe@collegedesproducteurs.be">helene.louppe@collegedesproducteurs.be</a></p>
', 3, NULL, 38, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (78, 'Visites d''essais céréales, oléagineux, protéagineux', 'left', 300, 225, '<p><strong><span style="background-color:white">Voir toutes les visites d&rsquo;essais cliquez </span>&nbsp;</strong><a href="http://www.cadcoasbl.be/p10_agenda.html"><strong><span style="background-color:white">ici</span></strong></a></p>
', 3, NULL, 38, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (79, 'Sécheresse exceptionnelle : à quelque chose, malheur est bon…', 'left', 300, 225, '<p style="text-align:justify"><strong><u><span style="color:black">Limaces</span></u></strong></p>

<p style="text-align:justify"><span style="color:black">Au c&oelig;ur de cet &eacute;t&eacute; chaud et tr&egrave;s sec, les limaces traversent une p&eacute;riode critique : apr&egrave;s les r&eacute;coltes de colza et de c&eacute;r&eacute;ales, elles n&rsquo;ont plus de ressources alimentaires et doivent rester abrit&eacute;es dans les petits creux am&eacute;nag&eacute;s &agrave; faibles profondeur dans le sol. Dans de telles conditions, un passage rapide et superficiel &agrave; l&rsquo;aide d&rsquo;un outil &agrave; dents expose toute la population du champ, &oelig;ufs, larves et adultes, au soleil et &agrave; la s&eacute;cheresse. Cette pratique constitue sans aucun doute la plus efficace et la moins ch&egrave;re des techniques de destruction des limaces. Elle est particuli&egrave;rement recommand&eacute;e apr&egrave;s des cultures &agrave; couvert dense comme le colza. Elle est aussi recommand&eacute;e dans les terres destin&eacute;es &agrave; &ecirc;tre ensemenc&eacute;es en colza ou autres cultures tr&egrave;s vuln&eacute;rables aux limaces.</span></p>

<p style="text-align:justify"><strong><u><span style="color:black">Adventices et repousses</span></u></strong></p>

<p style="text-align:justify"><span style="color:black">Le d&eacute;chaumage permet aussi de provoquer la germination des grains perdus &agrave; la r&eacute;colte et des semences d&rsquo;adventices. Ces plantules peuvent d&egrave;s lors &ecirc;tre d&eacute;truites m&eacute;caniquement en interculture</span><span style="color:#1f497d">,</span><span style="color:black"> et c&rsquo;est autant de mauvaises herbes en moins dans la culture suivante.</span></p>

<p style="text-align:justify"><strong><u><span style="color:black">C&eacute;cidomyies</span></u></strong></p>

<p style="text-align:justify"><span style="color:black">Les pluies re&ccedil;ues vers le 4-5 juillet ont d&eacute;clench&eacute; un d&eacute;but de migration de la c&eacute;cidomyie orange vers le sol. Toutefois, il est probable que cette ann&eacute;e de grande s&eacute;cheresse soit tr&egrave;s n&eacute;faste &agrave; cet insecte. En effet, divers observateurs ont signal&eacute; des larves mortes, soit dans les &eacute;pis, soit au sol apr&egrave;s le passage de la moissonneuse.</span></p>

<p style="text-align:justify"><span style="color:black">La c&eacute;cidomyie &eacute;questre, quant &agrave; elle, devrait voir ses populations r&eacute;gresser tr&egrave;s fortement. En effet, cet insecte est connu pour ne pas bien supporter la s&eacute;cheresse.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 39, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (84, 'Mouche grise des céréales', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Une tourn&eacute;e de pr&eacute;l&egrave;vement d&rsquo;&eacute;chantillons de terre est en cours dans une centaine de sites o&ugrave; des attaques de mouche grise ont d&eacute;j&agrave; &eacute;t&eacute; observ&eacute;es par le pass&eacute;. Ces pr&eacute;l&egrave;vements ne concernent que la betterave, un couvert favorable aux pontes et pris comme couvert de r&eacute;f&eacute;rence.</span></p>

<p style="text-align:justify"><span style="color:black">Les analyses suivront et les r&eacute;sultats vous seront communiqu&eacute;s d&egrave;s que possible.</span></p>

<p style="text-align:justify"><span style="color:black">Pour rappel, la mouche grise pond ses &oelig;ufs &agrave; m&ecirc;me le sol, de pr&eacute;f&eacute;rence dans des endroits frais et ombrag&eacute;s. Les couverts les plus attractifs sont ceux qui offrent &agrave; la fois de l&rsquo;ombre et peu d&rsquo;encombrement au sol, tels que la betterave.</span></p>

<p style="text-align:justify"><span style="color:black">Cet insecte pond en fin d&rsquo;&eacute;t&eacute;. Nous ne savons pas encore si la vague de chaleur et la s&eacute;cheresse de cet &eacute;t&eacute; ont &eacute;t&eacute; favorables ou d&eacute;favorable &agrave; la mouche grise. Cette information sera connue dans quelques jours et sera transmise via les avertissements du CADCO.</span></p>

<p style="text-align:justify"><span style="color:black">La mouche grise ne menace que les froments sem&eacute;s &agrave; partir de novembre. En effet, les bl&eacute;s sem&eacute;s t&ocirc;t auront atteint le tallage au moment des attaques, et r&eacute;agiront &agrave; la perte de talle par du tallage de compensation.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 40, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (86, 'Jaunisse nanisante de l’orge : vers quel genre d’automne ?', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Plusieurs &eacute;l&eacute;ments donnent &agrave; penser que cet automne ne verra vraisemblablement pas une forte pression de jaunisse nanisante : l&rsquo;an&eacute;antissement des populations de pucerons et la raret&eacute; des sympt&ocirc;mes de jaunisse dans les c&eacute;r&eacute;ales en fin d&rsquo;hiver, la raret&eacute; des pucerons dans les c&eacute;r&eacute;ales en cours de saison, la raret&eacute; actuelle des pucerons dans les ma&iuml;s, et la pr&eacute;cocit&eacute; des r&eacute;coltes de ma&iuml;s. Ce dernier &eacute;l&eacute;ment, en provoquant un vide des plantes h&ocirc;tes principales (c&eacute;r&eacute;ales et ma&iuml;s) dans les campagnes, devrait entra&icirc;ner une rupture importante dans le cycle du virus de la jaunisse nanisante.</span></p>

<p style="text-align:justify"><span style="color:black">Les observations, les analyses et les avertissements se feront n&eacute;anmoins avec la m&ecirc;me attention qu&rsquo;&agrave; l&rsquo;habitude, l&rsquo;&eacute;ventualit&eacute; de vols de pucerons d&rsquo;origine lointaine n&rsquo;&eacute;tant jamais exclue.</span></p>

<p style="text-align:center"><strong><u>Interdiction de l&rsquo;utilisation de produits contenant des n&eacute;onicotino&iuml;des</u></strong></p>

<p style="text-align:justify"><span style="color:black">En Wallonie, l&rsquo;utilisation de semences de c&eacute;r&eacute;ales trait&eacute;es avec des insecticides n&eacute;onicotino&iuml;des n&rsquo;est plus autoris&eacute;e depuis le 1er juin 2018. Il s&rsquo;agit de l&rsquo;ARGENTO (9855P/B), du GAUCHO DUO (10399P/B) et du NUPRID 600 FS (10477P/B).</span></p>

<p style="text-align:justify"><span style="color:black">Le CADCO n&rsquo;a jamais recommand&eacute; l&rsquo;utilisation de semences trait&eacute;es avec ces produits. En effet, leur co&ucirc;t &eacute;tait &eacute;lev&eacute;, et des alternatives fiables existent depuis longtemps :</span></p>

<p style="text-align:justify"><span style="color:black">- pulv&eacute;risations &eacute;ventuelles d&rsquo;insecticides pyr&eacute;thrino&iuml;des sur base des observations et avertissements du CADCO.</span></p>

<p style="text-align:justify"><span style="color:black">- utilisation de vari&eacute;t&eacute;s tol&eacute;rantes au virus : Rafaela ou Domino.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 40, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (85, 'Produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb (r&eacute;cemment remis &agrave; jour :&nbsp; Traitements de semences, insecticides,...) <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002"><strong>Cliquez ici</strong></a></p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 1, NULL, 40, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (87, 'Carnet de champ, un outil agronomique de traçabilité', 'left', 300, 225, '<p style="text-align:center"><strong><span style="color:#92d050">La nouvelle &eacute;dition est disponible !!</span></strong></p>

<p style="text-align:justify">Ce <strong>carnet </strong>(format de poche) comprend 44 pages dont, une &eacute;ph&eacute;m&eacute;ride, des renseignements sur des asbl &oelig;uvrant pour la Profession (R&eacute;quasud, SPW, &hellip;) des tableaux (38) permettant de collationner, par parcelles, les interventions men&eacute;es. Des carnets sont disponibles dans les d&eacute;p&ocirc;ts habituels ou sur demande &agrave; <a href="mailto:cadcoasbl@cadcoasbl.be">cadcoasbl@cadcoasbl.be</a></p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 2, NULL, 40, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (98, 'Situation au 11 septembre 2018', 'left', 300, 225, '<p>Suite &agrave; la s&eacute;cheresse estivale, les dates de semis du colza d&rsquo;hiver se sont &eacute;chelonn&eacute;es sur trois semaines, dans l&rsquo;attente de pluies facilitant le travail du sol et la germination du colza et des repousses de c&eacute;r&eacute;ales.</p>

<p>Les stades actuels du colza varient selon les dates de semis et de lev&eacute;e. Les plus avanc&eacute;s sont &agrave; 3 feuilles ; les moins avanc&eacute;s sont en cours de lev&eacute;e.</p>

<p>Avec une m&eacute;t&eacute;o actuellement tr&egrave;s douce et ensoleill&eacute;e, les premi&egrave;res altises (puces de terre) ont d&eacute;j&agrave; &eacute;t&eacute; pi&eacute;g&eacute;es dans les bassins jaunes mi-enterr&eacute;s. Ces insectes se nourrissent en provoquant des morsures bien visibles sur cotyl&eacute;dons et jeunes feuilles. Il faut donc bien surveiller de pr&egrave;s les cultures de colza quel que soit le stade car les bonnes temp&eacute;ratures sont favorables aux insectes.</p>

<p>Il n&rsquo;y a plus aucune protection insecticide des semences de colza, depuis 5 ans. La seule protection insecticide se d&eacute;roulera en v&eacute;g&eacute;tation lorsque plus de 3 plantes sur 10 pr&eacute;sentent des morsures.</p>

<p>Apr&egrave;s la phase d&rsquo;alimentation, ce sont les pontes &agrave; la base des plantes qui seront &agrave; craindre lorsque les temp&eacute;ratures seront plus fra&icirc;ches.</p>

<p>Quelques morsures de limaces sont aussi localement observ&eacute;es, malgr&eacute; la s&eacute;cheresse. Les colzas les plus petits sont les plus vuln&eacute;rables.</p>

<p style="text-align:right"><br />
Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p style="text-align:right"><br />
Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</p>
', 1, NULL, 42, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (94, 'Froment : Variétés résistantes à la cécidomyie orange', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Une mise &agrave; jour de la liste des vari&eacute;t&eacute;s r&eacute;sistantes &agrave; la c&eacute;cidomyie orange est disponible</span> <a href="http://www.cadcoasbl.be/p09_biblio.html#art00017">ici</a></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 41, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (92, 'Mouche grise : niveaux de ponte élevé dans l’est', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Les pr&eacute;l&egrave;vements d&rsquo;&eacute;chantillons de sol sont en cours, et les analyses ont livr&eacute; leurs premiers r&eacute;sultats : quelques niveaux de ponte &eacute;lev&eacute;s sont observ&eacute;s dans l&rsquo;est. </span></p>

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; height:384px; width:660px">
	<tbody>
		<tr>
			<td style="height:15.75pt; text-align:center; width:120pt">Temploux</td>
			<td style="text-align:center; width:60pt">240</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Isnes</td>
			<td style="text-align:center">250</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center"><span style="color:#ff0000">Lonz&eacute;e</span></td>
			<td style="text-align:center"><span style="color:#ff0000">320</span></td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Saint-Denis</td>
			<td style="text-align:center">290</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Aische-en-Refail</td>
			<td style="text-align:center">300</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Walhain St-Paul</td>
			<td style="text-align:center">240</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Perwez</td>
			<td style="text-align:center">70</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Ramillies-offus</td>
			<td style="text-align:center">40</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Taviers</td>
			<td style="text-align:center">300</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Hannut</td>
			<td style="text-align:center">70</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Jodoigne</td>
			<td style="text-align:center">140</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center"><span style="color:#ff0000">Bomel</span></td>
			<td style="text-align:center"><span style="color:#ff0000">850</span></td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Avennes</td>
			<td style="text-align:center">190</td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center"><span style="color:#ff0000">Moxhe</span></td>
			<td style="text-align:center"><span style="color:#ff0000">320</span></td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center"><span style="color:#ff0000">Burdinne</span></td>
			<td style="text-align:center"><span style="color:#ff0000">360</span></td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center"><span style="color:#ff0000">Avin</span></td>
			<td style="text-align:center"><span style="color:#ff0000">920</span></td>
		</tr>
		<tr>
			<td style="height:15.75pt; text-align:center">Ciplet</td>
			<td style="text-align:center">230</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><span style="color:black">Comme toujours, les r&eacute;sultats d&rsquo;analyses sont disparates. Ceci provient d&rsquo;une part du fait que le niveau des pontes est lui-m&ecirc;me irr&eacute;gulier : la mouche grise &laquo; a ses pr&eacute;f&eacute;rences &raquo; et, d&rsquo;autre part, du fait de la petite taille des &eacute;chantillons (10 dm&sup2;) dont le niveau de repr&eacute;sentativit&eacute; est donc assez faible. Ceci conduit &agrave; des r&eacute;sultats qui, individuellement, ne sont gu&egrave;re significatifs, mais qui dans leur ensemble permettent de percevoir la hauteur du risque dans une r&eacute;gion.</span></p>

<p style="text-align:justify"><span style="color:black">Parmi les r&eacute;sultats disponibles, le niveau moyen est assez &eacute;lev&eacute; (autour de 300 &oelig;ufs / m&sup2;), et certains r&eacute;sultats sont franchement &eacute;lev&eacute;s (8-900 &oelig;ufs / m&sup2;). Dans une semaine, nous disposerons de tous les r&eacute;sultats et nous pourrons dire si cette menace vaut pour l&rsquo;ensemble du pays.</span></p>

<p style="text-align:justify"><span style="color:black">M&ecirc;me en cas de niveaux &eacute;lev&eacute;s de ponte, tous les froments ne sont pas menac&eacute;s. La mouche grise ne menace que les froments sem&eacute;s tard (&agrave; partir de novembre) succ&eacute;dant &agrave; des betteraves et &agrave; d&rsquo;autres cultures laissant un couvert ombrag&eacute; et sans trop d&rsquo;encombrement au sol.</span></p>

<p style="text-align:justify"><span style="color:black">Les traitements de semences ne sont pas efficaces sur semis pr&eacute;coces : leur persistance est trop courte pour prot&eacute;ger la culture d&rsquo;attaques survenant &agrave; la sortie de l&rsquo;hiver. De plus, les bl&eacute;s sem&eacute;s t&ocirc;t ont atteint le tallage au moment de l&rsquo;attaque et peuvent r&eacute;agir aux attaques de mouche grise par du tallage de compensation.</span></p>

<p style="text-align:justify"><span style="color:black">Les semis d&rsquo;octobre peuvent &ecirc;tre effectu&eacute;s sans pr&eacute;caution particuli&egrave;re, sinon de veiller &agrave; <strong><u>soigner le travail du sol pour &eacute;viter de laisser des creux en profondeur</u></strong>. Attention cette ann&eacute;e aux mottes tr&egrave;s s&egrave;ches, qui devraient &ecirc;tre fragment&eacute;es avant un &eacute;ventuel labour.</span></p>

<p><span style="color:black">Voir la liste des traitements de semences autoris&eacute;es </span><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">ici</a><span style="color:black">.</span></p>

<p>&nbsp;
<p style="text-align:justify"><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 18 septembre.</span></p>
</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 41, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (97, 'Prolonger votre phytolicence', 'left', 300, 225, '<p>Nouveau d&eacute;pliant du SPF &agrave; propos de la prolongation de la phytolicence.</p>

<p>Assurez-vous de faire le n&eacute;cessaire &agrave; temps pour prolonger votre phytolicence : <a href="https://fytoweb.be/fr/guides/phytolicence/depliant-prolongation-phytolicence">Acc&eacute;der au d&eacute;pliant</a></p>
', 2, NULL, 41, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (96, 'A l''agenda', 'left', 300, 225, '<p><span style="color:red">A destination des producteurs de c&eacute;r&eacute;ales, les quatre soir&eacute;es ci-apr&egrave;s octroient des points phytolicence</span></p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td>
			<p>Jeudi<br />
			13/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">&nbsp; Gembloux<br />
			&nbsp;&nbsp; 19h30</span></p>
			</td>
			<td>
			<p><strong>Soir&eacute;e C&eacute;r&eacute;ales</strong> Livre Blanc en l&#39;espace Senghor &agrave; l&#39;avenue de la Facult&eacute;, 11</p>

			<p><a href="http://www.cadcoasbl.be/p10_agenda.html">Plus d&rsquo;information ici</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mardi<br />
			18/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">Braine-le-Comte </span><br />
			<span style="color:#ff0000">18h00</span></p>
			</td>
			<td>
			<p><strong><span style="color:#92d050">sur inscription </span><span style="color:#92d050">: 068/274417.</span><span style="color:#92d050"> </span></strong> <strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. Salle Scaubecq, chemin de Mariemont, 7 (CARAH) <a href="http://www.cadcoasbl.be/p10_agenda/1809CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mercredi<br />
			19/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">Kain<br />
			18h00</span></p>
			</td>
			<td>
			<p><strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. A la Ferme du Reposoir, chemin du Ruisseau 4 &agrave; 7540 Kain (CARAH) <a href="http://www.cadcoasbl.be/p10_agenda/1809CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Jeudi<br />
			20/09/2018</p>
			</td>
			<td>
			<p style="text-align:center"><span style="color:#ff0000">Ath<br />
			18h00</span></p>
			</td>
			<td>
			<p><strong><span style="color:#92d050">sur inscription </span><span style="color:#92d050">: 068/274417.</span><span style="color:#92d050"> </span></strong> <strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. Salle L.Delm&eacute;e, 11 rue P.Pastur (CARAH) <a href="http://www.cadcoasbl.be/p10_agenda/1809CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 3, NULL, 41, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (102, 'A l''agenda', 'left', 300, 225, '<p><span style="color:red">A destination des producteurs de c&eacute;r&eacute;ales, les trois soir&eacute;es ci-apr&egrave;s octroient des points phytolicence</span></p>

<table border="1" class="Table" style="border:1pt solid windowtext; height:287px; width:771px">
	<tbody>
		<tr>
			<td>
			<p style="text-align:center">Mardi<br />
			18/09/2018</p>
			</td>
			<td style="width:78.05pt">
			<p style="text-align:center"><span style="color:red">Braine-le-Comte<br />
			18h00</span></p>
			</td>
			<td style="width:394.65pt">
			<p><strong><span style="color:#92d050">sur inscription </span></strong><strong><span style="color:#92d050">: 068/274417.</span></strong><strong> </strong><strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. Salle Scaubecq, chemin de Mariemont, 7 (CARAH) <a href="http://www.cadcoasbl.be/p10_agenda/1809CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p style="text-align:center">Mercredi<br />
			19/09/2018</p>
			</td>
			<td style="width:78.05pt">
			<p style="text-align:center"><span style="color:red">Kain<br />
			18h00</span></p>
			</td>
			<td style="width:394.65pt">
			<p><strong>Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. A la Ferme du Reposoir, chemin du Ruisseau 4 &agrave; 7540 Kain (CARAH) <a href="http://www.cadcoasbl.be/p10_agenda/1809CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p style="text-align:center">Jeudi<br />
			20/09/2018</p>
			</td>
			<td style="width:78.05pt">
			<p style="text-align:center"><span style="color:red">Ath<br />
			18h00</span></p>
			</td>
			<td style="width:394.65pt">
			<p><strong><span style="color:#92d050">sur inscription </span></strong><strong><span style="color:#92d050">: 068/274417</span></strong>.<strong> Soir&eacute;e d&#39;information</strong> vari&eacute;t&eacute;s de c&eacute;r&eacute;ales : froment conventionnel et bio, escourgeon et orge brassicole. Salle L.Delm&eacute;e, 11 rue P.Pastur (CARAH) <a href="http://www.cadcoasbl.be/p10_agenda/1809CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/62.56.85), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 3, NULL, 43, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (103, 'Escourgeon : des variétés  tolérantes aux virus', 'left', 300, 225, '<p><span style="color:black">La vari&eacute;t&eacute; est le premier des leviers en lutte int&eacute;gr&eacute;e. L&rsquo;assortiment de vari&eacute;t&eacute;s tol&eacute;rantes en escourgeon se d&eacute;veloppe, il y a :</span></p>

<ul>
	<li><u><span style="color:black">Les vari&eacute;t&eacute;s tol&eacute;rantes au virus de la jaunisse nanisante de l&rsquo;orge</span></u><span style="color:black"> : <strong>RAFAELA</strong>, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>. D&egrave;s lors, m&ecirc;me si des pucerons y &eacute;taient observ&eacute;s en grands nombres, ces vari&eacute;t&eacute;s ne justifieraient aucun traitement insecticide</span></li>
</ul>

<ul>
	<li><u><span style="color:black">Les vari&eacute;t&eacute;s tol&eacute;rantes au virus de la mosa&iuml;que virale de l&rsquo;orge de type 1 et 2 </span></u><span style="color:black">: <strong>KWS KEEPER </strong>et<strong> HEDWIG</strong></span></li>
</ul>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; Phytotechnie &raquo;, B. Bodson et R. Blanchard</p>
', 1, NULL, 43, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (101, 'Arrachages précoces suivis de beau temps : attention à la mouche des semis ', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Des d&eacute;g&acirc;ts de mouche des semis (<em>Delia Platura</em> Meigen) peuvent parfois s&#39;observer dans des froments sem&eacute;s t&ocirc;t en automne. Les pontes de cet insecte se concentrent dans les feuilles broy&eacute;es de betteraves ou de chicor&eacute;es en d&eacute;composition sur le sol. Elles peuvent &ecirc;tre tr&egrave;s abondantes lorsque l&rsquo;arrachage est suivi de quelques jours de beau temps.</span></p>

<p style="text-align:justify"><span style="color:black">Les jeunes larves commencent par se nourrir des feuilles en putr&eacute;faction, puis attaquent le froment d&egrave;s la germination. Ceci conduit &agrave; des d&eacute;fauts de lev&eacute;e quelquefois graves. Une attaque se produisant apr&egrave;s la lev&eacute;e conduit &agrave; un jaunissement de la plus jeune feuille, puis &agrave; la disparition de plantules.</span></p>

<p style="text-align:justify"><strong><span style="color:black">Ces d&eacute;g&acirc;ts peuvent &ecirc;tre limit&eacute;s en enfouissant rapidement les feuilles apr&egrave;s l&rsquo;arrachage.</span></strong></p>

<p style="text-align:justify"><span style="color:black">Un traitement de semences effectu&eacute; contre la mouche grise ma&icirc;trise &eacute;galement la mouche des semis.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 43, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (104, 'Froment : variétés recommandées', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Sur base des r&eacute;sultats observ&eacute;s aux travers d&rsquo;un r&eacute;seau d&rsquo;essais vari&eacute;t&eacute;s de 2016 &agrave; 2018. Deux groupes de vari&eacute;t&eacute;s vous sont propos&eacute;s :</span></p>

<ul>
	<li style="text-align:justify"><strong><span style="color:black">Vari&eacute;t&eacute;s recommand&eacute;es en production int&eacute;gr&eacute;e</span></strong><span style="color:black"> = vari&eacute;t&eacute;s r&eacute;pondant aux crit&egrave;res de la production int&eacute;gr&eacute;e : bon comportement face &agrave; la rouille jaune, &agrave; la septoriose et &agrave; la verse. Ces vari&eacute;t&eacute;s sont susceptibles de permettre de diminuer la protection phytopharmaceutique et donc le co&ucirc;t de production. </span></li>
</ul>

<p style="text-align:justify"><span style="color:black">ALCIDES / CHEVIGNON / EDGAR / JOHNSON / </span><span style="color:#92d050">KWS SMART </span><span style="color:black">/ KWS TALENT / LIMABEL / MENTOR / OLYMPUS / PORTHUS / </span><span style="color:#92d050">SAFARI</span></p>

<ul>
	<li style="text-align:justify"><strong><span style="color:black">Vari&eacute;t&eacute;s &agrave; surveillance renforc&eacute;e</span></strong><span style="color:black"> = vari&eacute;t&eacute;s &agrave; potentiel de rendement &eacute;lev&eacute; si la protection est ad&eacute;quate. Ces vari&eacute;t&eacute;s ont l&rsquo;une ou l&rsquo;autre faiblesse agronomique &agrave; surveiller.</span></li>
</ul>

<p style="text-align:justify"><span style="color:black">ALBERT / ANAPOLIS / BERGAMO / GRAHAM / HENRIK / </span><span style="color:#92d050">KWS DORSET </span><span style="color:black">/ RGT REFORM</span></p>

<p style="text-align:justify"><span style="color:black">--</span></p>

<p style="text-align:justify">Par ailleurs, <span style="color:#92d050">KWS SMART / SAFARI / KWS DORSET </span>sont r&eacute;sistantes &agrave; la c&eacute;cidomyie orange.</p>

<p style="margin-left:18.0pt; margin-right:0cm; text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; Phytotechnie &raquo;, B. Bodson et R. Blanchard</p>
', 1, NULL, 43, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (100, 'Mouche grise : niveaux de ponte élevé dans l’est', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Les pr&eacute;l&egrave;vements d&rsquo;&eacute;chantillons de sol sont en cours, et les analyses continuent de livrer leurs r&eacute;sultats&nbsp;: quelques niveaux de ponte &eacute;lev&eacute;s (</span><span style="color:red">8</span><span style="color:black"> sur 52) sont observ&eacute;s essentiellement dans l&rsquo;est (</span><span style="color:red">6</span><span style="color:black">). Les derniers &eacute;chantillons seront analys&eacute;s pour&nbsp; le prochain avertissement.</span></p>

<table border="1" cellpadding="1" cellspacing="1" style="height:338px; width:500px">
	<tbody>
		<tr>
			<td style="text-align:center"><span style="color:black">Bramesnil</span></td>
			<td style="text-align:center">0</td>
			<td style="text-align:center"><span style="color:black">Qu&eacute;vy</span></td>
			<td style="text-align:center">100</td>
			<td style="text-align:center"><span style="color:black">Loupoigne</span></td>
			<td style="text-align:center">60</td>
			<td style="text-align:center"><strong><span style="color:black">Bomel</span></strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>850</strong></span></td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Maubray</span></td>
			<td style="text-align:center">0</td>
			<td style="text-align:center"><strong><span style="color:black">Givry</span></strong></td>
			<td style="text-align:center">70</td>
			<td style="text-align:center"><span style="color:black">Saint-Amand</span></td>
			<td style="text-align:center">0</td>
			<td style="text-align:center"><strong><span style="color:black">Mal&egrave;ves-St-Marie</span></strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>400</strong></span></td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Mont-St-Aubert</span></td>
			<td style="text-align:center">10</td>
			<td style="text-align:center"><span style="color:black">Anderlues</span></td>
			<td style="text-align:center">90</td>
			<td style="text-align:center"><span style="color:black">Temploux</span></td>
			<td style="text-align:center">240</td>
			<td style="text-align:center"><span style="color:black">Perwez</span></td>
			<td style="text-align:center">250</td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Mont-St-Aubert</span></td>
			<td style="text-align:center">30</td>
			<td style="text-align:center"><span style="color:black">Leers-et-fosteau</span></td>
			<td style="text-align:center">150</td>
			<td style="text-align:center"><span style="color:black">Isnes</span></td>
			<td style="text-align:center">250</td>
			<td style="text-align:center"><span style="color:black">Avennes</span></td>
			<td style="text-align:center">190</td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">H&eacute;rinnes</span></td>
			<td style="text-align:center">140</td>
			<td style="text-align:center"><span style="color:black">Solre sur Sambre</span></td>
			<td style="text-align:center">180</td>
			<td style="text-align:center"><strong><span style="color:black">Lonz&eacute;e</span> </strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>320</strong></span></td>
			<td style="text-align:center"><strong><span style="color:black">Moxhe</span></strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>320</strong></span></td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Sint-Denijs</span></td>
			<td style="text-align:center">10</td>
			<td style="text-align:center"><span style="color:black">Grand-Leez</span></td>
			<td style="text-align:center">170</td>
			<td style="text-align:center"><span style="color:black">Saint-Denis</span></td>
			<td style="text-align:center">290</td>
			<td style="text-align:center"><strong><span style="color:black">Burdinne</span></strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>360</strong></span></td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Pecq</span></td>
			<td style="text-align:center">30</td>
			<td style="text-align:center"><span style="color:black">Walhain</span></td>
			<td style="text-align:center">210</td>
			<td style="text-align:center"><span style="color:black">Aische-en-Refail</span></td>
			<td style="text-align:center">300</td>
			<td style="text-align:center"><strong><span style="color:black">Avin</span></strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>920</strong></span></td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Esquelmes</span></td>
			<td style="text-align:center">0</td>
			<td style="text-align:center"><strong><span style="color:black">Nil-St-Vincent</span></strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>480</strong></span></td>
			<td style="text-align:center"><span style="color:black">Perwez</span></td>
			<td style="text-align:center">70</td>
			<td style="text-align:center"><span style="color:black">Ciplet</span></td>
			<td style="text-align:center">230</td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Thieu</span></td>
			<td style="text-align:center">20</td>
			<td style="text-align:center"><span style="color:black">Marbais</span></td>
			<td style="text-align:center">60</td>
			<td style="text-align:center"><span style="color:black">Ramillies-offus</span></td>
			<td style="text-align:center">40</td>
			<td style="text-align:center"><span style="color:black">Walhain St-Paul</span></td>
			<td style="text-align:center">240</td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Quievrain</span></td>
			<td style="text-align:center">30</td>
			<td style="text-align:center"><span style="color:black">Marbais</span></td>
			<td style="text-align:center">30</td>
			<td style="text-align:center"><span style="color:black">Taviers</span></td>
			<td style="text-align:center">300</td>
			<td style="text-align:center"><span style="color:black">Thynes</span></td>
			<td style="text-align:center">60</td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Wih&eacute;ries</span></td>
			<td style="text-align:center">10</td>
			<td style="text-align:center"><span style="color:black">Saint-Amand</span></td>
			<td style="text-align:center">180</td>
			<td style="text-align:center"><strong><span style="color:black">Thisnes (Hannut)</span></strong></td>
			<td style="text-align:center"><span style="color:#ff0000"><strong>460</strong></span></td>
			<td style="text-align:center"><span style="color:black">Braibant</span></td>
			<td style="text-align:center">0</td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Erquennes</span></td>
			<td style="text-align:center">80</td>
			<td style="text-align:center"><span style="color:black">Frasnes-Lez-Gosselies</span></td>
			<td style="text-align:center">10</td>
			<td style="text-align:center"><span style="color:black">Hannut</span></td>
			<td style="text-align:center">70</td>
			<td style="text-align:center"><span style="color:black">Leignon</span></td>
			<td style="text-align:center">50</td>
		</tr>
		<tr>
			<td style="text-align:center"><span style="color:black">Sars-la-Bruyere</span></td>
			<td style="text-align:center">20</td>
			<td style="text-align:center"><span style="color:black">Thines</span></td>
			<td style="text-align:center">20</td>
			<td style="text-align:center"><span style="color:black">Jodoigne</span></td>
			<td style="text-align:center">140</td>
			<td style="text-align:center"><span style="color:black">Schaltin</span></td>
			<td style="text-align:center">60</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><span style="color:black">Comme toujours, les r&eacute;sultats d&rsquo;analyses sont disparates. Ceci provient d&rsquo;une part du fait que le niveau des pontes est lui-m&ecirc;me irr&eacute;gulier : la mouche grise &laquo; a ses pr&eacute;f&eacute;rences &raquo; et, d&rsquo;autre part, du fait de la petite taille des &eacute;chantillons (10 dm&sup2;) dont le niveau de repr&eacute;sentativit&eacute; est donc assez faible. Ceci conduit &agrave; des r&eacute;sultats qui, individuellement, ne sont gu&egrave;re significatifs, mais qui dans leur ensemble permettent de percevoir la hauteur du risque dans une r&eacute;gion.</span></p>

<p style="text-align:justify"><span style="color:black">Parmi les r&eacute;sultats disponibles, le niveau moyen est assez &eacute;lev&eacute; (autour de 300 &oelig;ufs / m&sup2;), et certains r&eacute;sultats sont franchement &eacute;lev&eacute;s (8-900 &oelig;ufs / m&sup2;). Les r&eacute;sultats complets seront disponibles pour le prochain avertissement.</span></p>

<p style="text-align:justify"><strong><span style="color:black">M&ecirc;me en cas de niveaux &eacute;lev&eacute;s de ponte, tous les froments ne sont pas menac&eacute;s</span></strong><span style="color:black">. La mouche grise ne menace que les froments sem&eacute;s tard (&agrave; partir de novembre) succ&eacute;dant &agrave; des betteraves et &agrave; d&rsquo;autres cultures laissant un couvert ombrag&eacute; et sans trop d&rsquo;encombrement au sol.</span></p>

<p style="text-align:justify"><span style="color:black">Les traitements de semences ne sont pas efficaces sur semis pr&eacute;coces : leur persistance est trop courte pour prot&eacute;ger la culture d&rsquo;attaques survenant &agrave; la sortie de l&rsquo;hiver. De plus, les bl&eacute;s sem&eacute;s t&ocirc;t ont atteint le tallage au moment de l&rsquo;attaque et peuvent r&eacute;agir aux attaques de mouche grise par du tallage de compensation.</span></p>

<p style="text-align:justify"><span style="color:black">Les semis d&rsquo;octobre peuvent &ecirc;tre effectu&eacute;s sans pr&eacute;caution particuli&egrave;re, sinon de veiller &agrave; <strong><u>soigner le travail du sol pour &eacute;viter de laisser des creux en profondeur</u></strong>. Attention cette ann&eacute;e aux mottes tr&egrave;s s&egrave;ches, qui devraient &ecirc;tre fragment&eacute;es avant un &eacute;ventuel labour.</span></p>

<p style="text-align:justify"><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 25 septembre.</span></p>

<p><span style="color:black">Voir la liste des traitements de semences autoris&eacute;es </span><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">ici</a></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 43, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (105, 'Dérogation épandage des engrais minéraux et organiques ', 'left', 300, 225, '<p style="text-align:justify"><u>Vu les conditions de s&eacute;cheresse de cette ann&eacute;e, une d&eacute;rogation a &eacute;t&eacute; accord&eacute;e par le Ministre</u> :</p>

<p style="text-align:justify">Azote min&eacute;ral autoris&eacute; jusqu&rsquo;au 30 septembre</p>

<p style="text-align:justify">Engrais de ferme &agrave; action rapide autoris&eacute; jusqu&rsquo;au 30 septembre &agrave; condition d&rsquo;implanter un couvert (avant le 15 septembre), ou une culture d&rsquo;hiver ou max 80kg d&rsquo;azote organique.</p>

<p style="text-align:justify">Engrais de ferme &agrave; action lente : situation inchang&eacute;e par rapport &agrave; la l&eacute;gislation en vigueur.</p>

<p style="text-align:right">[Plus d&#39;information : <a href="http://www.protecteau.be">Protect&rsquo;eau</a> 081 62 73 13]</p>
', 2, NULL, 43, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (112, 'Mouche grise : niveaux de ponte élevé principalement dans l’est', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Les analyses des pr&eacute;l&egrave;vements d&rsquo;&eacute;chantillons de sol sont maintenant termin&eacute;es : Le constat reste le m&ecirc;me. Quelques niveaux de ponte &eacute;lev&eacute;s (</span><span style="color:red">8</span><span style="color:black"> sur 67) sont observ&eacute;s essentiellement dans l&rsquo;est (</span><span style="color:red">6</span><span style="color:black">).</span></p>

<p style="text-align:justify"><strong><span style="color:black">M&ecirc;me en cas de niveaux &eacute;lev&eacute;s de ponte, tous les froments ne sont pas menac&eacute;s</span></strong><span style="color:black">. La mouche grise ne menace que les froments sem&eacute;s tard (&agrave; partir de novembre) succ&eacute;dant &agrave; des betteraves et &agrave; d&rsquo;autres cultures laissant un couvert ombrag&eacute; et sans trop d&rsquo;encombrement au sol.</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Les traitements de semences ne sont pas efficaces sur semis pr&eacute;coces : leur persistance est trop courte pour prot&eacute;ger la culture d&rsquo;attaques survenant &agrave; la sortie de l&rsquo;hiver. De plus, les bl&eacute;s sem&eacute;s t&ocirc;t ont atteint le tallage au moment de l&rsquo;attaque et peuvent r&eacute;agir aux attaques de mouche grise par du tallage de compensation.</span></p>

<p style="text-align:justify"><span style="color:black">Les semis d&rsquo;octobre peuvent &ecirc;tre effectu&eacute;s sans pr&eacute;caution particuli&egrave;re, sinon de veiller &agrave; <strong>soigner le travail du sol pour &eacute;viter de laisser des creux en profondeur</strong>. </span></p>

<p style="text-align:justify"><span style="color:black">Voir la liste des traitements de semences autoris&eacute;es</span> <a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">ici</a></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 46, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (106, 'Situation au 18 septembre 2018', 'left', 600, 500, '<p style="text-align:justify">L&#39;&eacute;t&eacute; continue avec ses temp&eacute;ratures &eacute;lev&eacute;es et sa forte luminosit&eacute;. En revanche, l&#39;absence de pluies laisse toujours les sols tr&egrave;s secs. Selon les champs, les stades peuvent varier fortement : certains n&rsquo;ont pas encore lev&eacute;, faute d&rsquo;eau, tandis que d&rsquo;autres ayant b&eacute;n&eacute;fici&eacute; de quelques pluies sont d&eacute;j&agrave; &agrave; cinq feuilles. La s&eacute;cheresse induit &eacute;galement des stades tr&egrave;s disparates au sein m&ecirc;me des champs.</p>

<p>Ce temps tr&egrave;s sec profite aux altises, dont les vols sont abondants et menacent les champs les plus r&eacute;cemment sem&eacute;s. Selon les sites, on peut observer des taux de morsures allant de 0 &agrave; quasi 100 %. Dans les champs trait&eacute;s une premi&egrave;re fois aux environs du 10-11 septembre, les nouvelles feuilles ne comportent pas, ou bien tr&egrave;s peu de morsures. Les traitements insecticides ont donc prot&eacute;g&eacute; la culture. Toutefois, il faut rester tr&egrave;s vigilant, parce que les conditions de chaleur et surtout de forte luminosit&eacute; sont d&eacute;favorables &agrave; la persistance d&rsquo;efficacit&eacute; des insecticides. En cas de reprise des attaques et de fortes captures dans les bassins jaunes, il faudra &ecirc;tre pr&ecirc;t &agrave; appliquer un nouveau traitement.</p>

<p style="text-align:justify">Un traitement insecticide doit &eacute;viter les heures chaudes et lumineuses. Il doit aussi &eacute;viter les vents secs. Traiter tard, quand l&rsquo;humidit&eacute; de l&rsquo;air s&rsquo;accro&icirc;t, ou bien t&ocirc;t le matin dans la ros&eacute;e est une mesure tr&egrave;s importante pour donner au traitement toute ses chances de s&rsquo;av&eacute;rer efficace.</p>

<p style="text-align:justify">Le colza en cours de lev&eacute;e est le plus expos&eacute; aux d&eacute;g&acirc;ts d&#39;altises. Il faut bien prot&eacute;ger les jeunes colzas, avec un insecticide, lorsque plus de 3 plantes sur 10 sont touch&eacute;es. Les colzas &agrave; 4 feuilles n&#39;ont plus &agrave; craindre les d&eacute;g&acirc;ts de morsures des grosses altises ; cependant, ils ne sont pas &agrave; l&#39;abri de futures attaques par les larves.</p>

<p style="text-align:justify">La surveillance continue car les belles journ&eacute;es estivales sont encore annonc&eacute;es pour les prochains jours.</p>

<p style="text-align:right"><br />
Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>
', 1, 382, 44, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (122, 'CePiCOP, communiqué du 02 octobre 2018 (C14)', 'left', 350, 200, '<p style="text-align:justify">&nbsp;L&rsquo;automne est arriv&eacute; avec ses temp&eacute;ratures fra&icirc;ches, quelques averses, les premi&egrave;res gel&eacute;es et encore de belles journ&eacute;es ensoleill&eacute;es.</p>

<p style="text-align:justify">Les stades sont tr&egrave;s contrast&eacute;s entre les colzas ayant eu un bon d&eacute;part (de 6 &agrave; 8 feuilles actuellement) et les colzas ayant eu une lev&eacute;e difficile (de cotyl&eacute;dons &agrave; quelques feuilles).</p>

<p style="text-align:justify">Les grosses altises continuent &agrave; &ecirc;tre pi&eacute;g&eacute;es dans les bassins, m&ecirc;me dans les champs ayant d&eacute;j&agrave; &eacute;t&eacute; trait&eacute;s avec un insecticide il y a 2 semaines.&nbsp; Les captures cumul&eacute;es en une semaine peuvent atteindre 40 &agrave; 424 individus, dans certains champs.</p>

<p style="text-align:justify">Lors des belles journ&eacute;es avec des temp&eacute;ratures proches de 20&deg;C, les altises (puces de terre) sont bien visibles sur les plantes.</p>

<p style="text-align:justify">Les petits colzas sont toujours tr&egrave;s vuln&eacute;rables vis-&agrave;-vis des morsures d&rsquo;altises.</p>

<p style="text-align:justify">Il est donc important de continuer &agrave; observer la pr&eacute;sence des altises aussi bien dans les pi&egrave;ges que sur les plantes car les prochains jours leur seront sans doute favorables.</p>

<p style="text-align:justify">Avec l&rsquo;arriv&eacute;e des gel&eacute;es et nuits fra&icirc;ches, il se pourrait que les altises passent aux pontes &agrave; la base des plantes de colza.&nbsp; Le danger est li&eacute; aux larves qui peuvent se cacher en p&eacute;n&eacute;trant dans les p&eacute;tioles &agrave; la base des feuilles et faire des d&eacute;g&acirc;ts au c&oelig;ur des plantes pendant l&rsquo;automne et l&rsquo;hiver.&nbsp; Emp&ecirc;cher la ponte des adultes est possible par l&rsquo;&eacute;limination actuelle des altises adultes pr&eacute;sentes en culture de colza.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>
', 1, 386, 48, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (113, 'Dérogation épandage en prairie ', 'left', 300, 225, '<p style="text-align:justify"><u>Vu les conditions de s&eacute;cheresse de cette ann&eacute;e, une d&eacute;rogation a &eacute;t&eacute; accord&eacute;e par le Ministre</u> :</p>

<p style="text-align:justify">Suite aux conditions de s&eacute;cheresse exceptionnelles, une DEROGATION prolonge jusqu&rsquo;au 20 octobre inclus la p&eacute;riode autoris&eacute;e d&rsquo;&eacute;pandage d&rsquo;engrais de ferme &agrave; action rapide sur PRAIRIES, si les conditions d&rsquo;&eacute;pandages sont respect&eacute;es.</p>

<p style="text-align:right">[Plus d&#39;information : <a href="http://www.protecteau.be">Protect&rsquo;eau</a> 081 62 73 13]</p>
', 2, NULL, 46, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (114, 'Phytolicence : Suivre combien de formations ?', 'left', 300, 225, '<p style="text-align:justify"><strong>Le nombre de formations &agrave; suivre </strong>d&eacute;pend du type de phytolicence(s) dont vous &ecirc;tes titulaire :</p>

<p style="text-align:justify">NP : 2 formations ; &nbsp;P1 : 3 formations ; P2 : 4 formations ; P3 : 6 formations</p>

<p style="text-align:justify"><strong>Vous disposez de plusieurs phytolicences</strong> ? Chaque formation suivie compte pour toutes vos licences. Exemple: si vous disposez d&rsquo;une P2 et d&rsquo;une NP vous devez suivre au moins deux activit&eacute;s de formation pour prolonger votre phytolicence NP et au moins quatre pour que les deux licences soient prolong&eacute;es.</p>

<p style="text-align:right">&nbsp;</p>
', 2, NULL, 46, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (111, 'Jaunisse nanisante : Actuellement peu de pucerons et pas de virus', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Actuellement, les pucerons sont tr&egrave;s peu abondants dans les repousses, au point qu&rsquo;il est difficile d&rsquo;en collecter en vue d&rsquo;analyse virologique. Jusqu&rsquo;&agrave; pr&eacute;sent, une trentaine de pr&eacute;l&egrave;vements se sont tous r&eacute;v&eacute;l&eacute;s n&eacute;gatifs au virus de la jaunisse. C&rsquo;est un &eacute;l&eacute;ment de plus indiquant que la saison &laquo; jaunisse nanisante &raquo; d&eacute;marre avec une pression tr&egrave;s faible. </span></p>

<p style="text-align:justify"><span style="color:black">Il ne faut cependant pas crier trop t&ocirc;t victoire : des vols de pucerons d&rsquo;origine lointaine sont toujours possible au cours de l&rsquo;automne, et l&rsquo;infestation des jeunes emblavures doit &ecirc;tre surveill&eacute;e jusqu&rsquo;&agrave; l&rsquo;entr&eacute;e de l&rsquo;hiver. Le CADCO &eacute;mettra des avis hebdomadaires bas&eacute;s sur les observations effectu&eacute;es dans un r&eacute;seau d&rsquo;une vingtaine de champs distribu&eacute;s sur toute la r&eacute;gion c&eacute;r&eacute;ali&egrave;re.</span></p>

<p style="text-align:right"><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 02 octobre.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 46, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (109, 'Escourgeon : Densités de semis recommandées', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">En conditions normales, </span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Pour les <strong>vari&eacute;t&eacute;s lign&eacute;es</strong> : la densit&eacute; recommand&eacute;e est de <strong>170 &agrave; 200 grains/m&sup2;</strong> soit&nbsp; 70 &agrave; 110 kg/ha.</span></li>
	<li style="text-align:justify"><span style="color:black">Pour les <strong>vari&eacute;t&eacute;s hybrides</strong> elle est de <strong>125 &agrave; 170 grains/m&sup2;</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
</ul>

<p style="text-align:justify"><span style="color:black">En cas de mauvaises conditions de semis (m&eacute;t&eacute;o, terre froide, mal pr&eacute;par&eacute;e) ou de semis tardif, la densit&eacute; de semis des lign&eacute;es peut &ecirc;tre augment&eacute;e sans jamais d&eacute;passer les 250 grains/m&sup2; soit&nbsp; 100 &agrave; 140 kg/ha.</span></p>

<p style="text-align:justify"><span style="color:black">En cas de semis en conditions trop d&eacute;favorables,&nbsp; de semis trop tardif (apr&egrave;s le 10/10) il est pr&eacute;f&eacute;rable de remplacer l&rsquo;orge d&rsquo;hiver par du froment, de l&rsquo;&eacute;peautre, de l&rsquo;orge de printemps ou des pois prot&eacute;agineux.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; Phytotechnie &raquo;, B. Bodson et R. Blanchard</p>
', 1, NULL, 46, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (115, 'Phytolicence : Consultez le nombre de formations suivies en ligne', 'left', 300, 225, '<p style="text-align:justify">Un mode d&rsquo;emploi est accessible au <a href="http://www.crphyto.be/comment-se-connecter-son-compte-en-ligne">lien suivant</a></p>

<p style="text-align:justify">Un souci : veuillez contacter le SPF Sant&eacute; publique au <a href="mailto:phytolicence@sante.belgique.be">phytolicence@sante.belgique.be</a> ou au 02/5249797</p>
', 2, NULL, 46, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (107, 'Situation au 25 septembre 2018', 'left', 350, 200, '<p>D&#39;importantes pluies tant attendues sont arriv&eacute;es en m&ecirc;me temps que le d&eacute;but de l&#39;automne.&nbsp; Les temp&eacute;ratures sont maintenant devenues fra&icirc;ches aussi bien en journ&eacute;e que la nuit.</p>

<p>Tandis que les champs de colza les plus avanc&eacute;s couvrent d&eacute;j&agrave; le sol, d&#39;autres colzas lev&eacute;s tardivement &agrave; cause de la s&eacute;cheresse pr&eacute;sentent un retard de d&eacute;veloppement.</p>

<p>Les grosses altises ont &eacute;t&eacute; tr&egrave;s pr&eacute;sentes et actives jusqu&#39;&agrave; l&#39;arriv&eacute;e des pluies.&nbsp; Elles ont &eacute;t&eacute; pi&eacute;g&eacute;es massivement dans les bassins.&nbsp; Les morsures sur les plantes sont toujours bien visibles.&nbsp; Dans les colzas pr&eacute;sentant plus de 4 feuilles et ayant re&ccedil;u un traitement insecticide, il n&#39;y a plus de morsures sur les jeunes feuilles.&nbsp; Les captures d&#39;altises sont nettement plus faibles actuellement.</p>

<p>L&#39;annonce de temp&eacute;ratures saisonni&egrave;res avec un bon ensoleillement doit maintenir la surveillance des altises.<br />
<br />
Pour les plus petits colzas, avec le retour de l&#39;humidit&eacute; du sol suite aux r&eacute;centes pluies abondantes, il faut bien surveiller les d&eacute;g&acirc;ts de limace, surtout en bord de champs, car les petites plantes sont vuln&eacute;rables.</p>

<p>&nbsp;</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>
', 1, 385, 45, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (116, 'Recommandations : désherbage d’automne', 'left', 300, 225, '<p><u><span style="color:black">En orge d&rsquo;hiver</span></u></p>

<p style="text-align:justify"><span style="color:black">A l&rsquo;heure des premiers semis, il est bon de se rappeler que c&rsquo;est durant l&rsquo;automne que se g&egrave;re les adventices. En effet, c&rsquo;est &agrave; ce moment que la majorit&eacute; des mauvaises herbes va germer et cro&icirc;tre.</span></p>

<p style="text-align:justify"><span style="color:black">Jeunes et peu d&eacute;velopp&eacute;es, les adventices sont facilement et &eacute;conomiquement limit&eacute;es. En revanche, au printemps, les adventices ayant pass&eacute; l&rsquo;hiver sont trop d&eacute;velopp&eacute;es et la culture, g&eacute;n&eacute;ralement dense et vigoureuse, diminue l&rsquo;efficacit&eacute; de la lutte (effet parapluie,&hellip;). Des rattrapages printaniers sont n&eacute;anmoins possibles et quelquefois n&eacute;cessaires.</span></p>

<p style="text-align:justify"><span style="color:black">Voir la liste des traitements herbicides remises &agrave; jour r&eacute;cemment :</span></p>

<table border="1" cellpadding="1" cellspacing="1" style="width:500px">
	<tbody>
		<tr>
			<td>01 octobre 2018</td>
			<td>En pr&eacute;-&eacute;mergence de la c&eacute;r&eacute;ale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf">HerbPreC.pdf</a></td>
		</tr>
		<tr>
			<td>01 octobre 2018</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1510HerbPreEp.pdf">HerbPreEp.pdf</a></td>
		</tr>
		<tr>
			<td>01 octobre 2018</td>
			<td>De la lev&eacute;e au d&eacute;but tallage</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf">HerbLTalC.pdf</a></td>
		</tr>
		<tr>
			<td>01 octobre 2018</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalEp.pdf">HerbLTalEp.pdf</a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p style="text-align:justify">C<span style="color:black">onsulter les listes des produits phyto autoris&eacute;s en c&eacute;r&eacute;ales : </span><a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>

<p>&nbsp;</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em>&nbsp; F.Henriet</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/875870), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, NULL, 47, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (117, 'Jaunisse nanisante : la surveillance reprend dès la levée', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Nos avis hebdomadaires bas&eacute;s sur les observations effectu&eacute;es dans un r&eacute;seau d&rsquo;une vingtaine de champs distribu&eacute;s sur toute la r&eacute;gion c&eacute;r&eacute;ali&egrave;re d&eacute;buteront d&egrave;s la lev&eacute;e des premiers escourgeons.</span></p>

<p style="text-align:right"><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 09 octobre.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/875870), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, NULL, 47, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (123, 'Jaunisse nanisante : la surveillance reprend dès la levée', 'left', 300, 225, '<p><strong><span style="color:#726056"><u>Observations</u>&nbsp; du 08 octobre 2018 ; semis du 28 septembre &agrave; d&eacute;but octobre</span></strong></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 19 parcelles</u></strong> r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath, Mainvault, Wiers), Brabant wallon (Jandrain, Opain), Li&egrave;ge (Fexhe-Slins, Mortroux, Pailhe, Verlaine), Namur (Anth&eacute;e, Biesmer&eacute;e, Clermont, Corroy-le-ch&acirc;teau, Falmagne, Foy-Notre-Dame, Morville, Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Les premiers escourgeons sont lev&eacute;s. Les plus avanc&eacute;s sont au stade une feuille.</span></p>

<p style="text-align:justify"><span style="color:black">Dans 50 % des champs, il n&rsquo;y a pas de pucerons, et dans les autres il y en a actuellement peu : 1 &agrave; 1,5% de plantes colonis&eacute;es. Pour la grande majorit&eacute; ce sont des ail&eacute;s isol&eacute;s, rarement et au plus un ail&eacute; avec une larve apt&egrave;re.</span></p>

<p style="text-align:justify"><span style="color:black">M&ecirc;me s&rsquo;il n&rsquo;est pas facile d&rsquo;en trouver, des collectes de pucerons ont &eacute;t&eacute; men&eacute;es. Ils seront analys&eacute;s afin d&rsquo;estimer la proportion de pucerons porteurs du virus de la jaunisse nanisante.</span></p>

<p style="text-align:justify"><span style="color:black">Dans ces conditions, aucun traitement n&rsquo;est actuellement recommand&eacute;.</span></p>

<p style="text-align:justify"><span style="color:black">Nous vous rappelons que les traitements par pulv&eacute;risation, lorsqu&rsquo;ils sont n&eacute;cessaires, n&rsquo;ont qu&rsquo;une faible r&eacute;manence. Or les pucerons peuvent arriver tout au long de la saison. </span></p>

<p style="text-align:justify"><span style="color:black">Afin de limiter les interventions, et donc le co&ucirc;t de la protection anti-pucerons lorsqu&rsquo;elle s&rsquo;av&egrave;re utile, l&rsquo;objectif sera d&rsquo;intervenir le plus tard possible en saison. </span></p>

<p style="text-align:right"><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 16 octobre.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/875870), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, 387, 49, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (125, 'CePiCOP, communiqué du 09 octobre 2018 (C15)', 'left', 350, 200, '<p style="text-align:justify">&nbsp;L&rsquo;ann&eacute;e 2018 avec sa s&eacute;cheresse bien marqu&eacute;e est tr&egrave;s favorable aux insectes en colza.&nbsp; Les temp&eacute;ratures actuelles sup&eacute;rieures aux normales saisonni&egrave;res et l&rsquo;ensoleillement important sont des facteurs favorables &agrave; l&rsquo;activit&eacute; des grosses altises encore bien pr&eacute;sentes au cours des derniers jours.&nbsp; Le nombre de captures des altises est tr&egrave;s variable d&rsquo;un champ &agrave; l&rsquo;autre mais reste &eacute;lev&eacute; dans plusieurs cas.&nbsp; Les premi&egrave;res captures de charan&ccedil;on du bourgeon terminal ont eu lieu &agrave; la fin de la premi&egrave;re semaine d&rsquo;octobre.</p>

<p style="text-align:justify">Les r&eacute;centes pluies profitent &agrave; la culture du colza aux stades tr&egrave;s divers.</p>

<p style="text-align:justify">Pour les plus petits colzas (lev&eacute;e difficile), l&rsquo;attention doit &ecirc;tre maintenue vis-&agrave;-vis des d&eacute;g&acirc;ts de morsures d&rsquo;altises sur jeunes plantules.</p>

<p style="text-align:justify">Les prochains jours de cet &eacute;t&eacute; indien donneront l&rsquo;occasion de continuer &agrave; observer les insectes sur les plantes, sur le sol et dans les pi&egrave;ges.&nbsp; Les altises sont plus faciles &agrave; voir sur v&eacute;g&eacute;tation s&egrave;che et sur sol sec r&eacute;chauff&eacute;, dans l&rsquo;apr&egrave;s-midi.</p>

<p style="text-align:justify">Dans le cas d&rsquo;un deuxi&egrave;me traitement insecticide, il faut veiller &agrave; respecter le nombre de traitements maximal autoris&eacute; sur altises pour chaque produit agr&eacute;&eacute; de la famille des pyr&eacute;thrino&iuml;des.&nbsp; Le traitement sera fait de pr&eacute;f&eacute;rence le soir ou le matin dans la ros&eacute;e.&nbsp; Il faut &eacute;viter les heures chaudes de la journ&eacute;e.</p>

<p style="text-align:justify">Pour se fournir en pi&egrave;ges &agrave; insectes, ceux-ci sont disponibles &agrave; l&rsquo;APPO (<a href="mailto:appo.gembloux@uliege.be">appo.gembloux@uliege.be</a> ou 0497/53.84.47).&nbsp; Ils permettent de suivre la situation dans ses champs, aussi bien &agrave; l&rsquo;automne (bassin mi-enterr&eacute; dans le sol) qu&rsquo;au printemps (sur un support permettant de remonter facilement le bassin au fur et &agrave; mesure de la croissance du colza d&rsquo;hiver).</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>
', 1, 388, 50, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (126, 'Jaunisse nanisante : infestation amorcée', 'left', 150, 112, '<p><strong><span style="color:#726056"><u>Observations</u>&nbsp; du 15 octobre 2018 ; semis du 22 septembre &agrave; d&eacute;but octobre</span></strong></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 19 parcelles</u></strong> r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath, Mainvault, Wiers), Brabant wallon (Jandrain, Opain), Li&egrave;ge (Fexhe-Slins, Mortroux, Pailhe, Verlaine), Namur (Anth&eacute;e, Biesmer&eacute;e, Clermont, Corroy-le-ch&acirc;teau, Falmagne, Foy-Notre-Dame, Morville, Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p style="text-align:justify"><span style="color:black">Les deux derni&egrave;res semaines ont &eacute;t&eacute; marqu&eacute;es par une douceur extr&ecirc;me, si bien que les escourgeons les plus d&eacute;velopp&eacute;s sont d&eacute;j&agrave; au stade tallage, et que les premiers froments commencent &agrave; lever.</span></p>

<p style="text-align:justify"><span style="color:black">Cette douceur a &eacute;galement profit&eacute; aux pucerons que l&rsquo;on retrouve maintenant dans tous les champs du r&eacute;seau. Presque partout plus de 5% des plantes sont colonis&eacute;es, tandis que le champ le plus infest&eacute; est &agrave; 10 % de plantes colonis&eacute;es.</span></p>

<p style="text-align:justify"><span style="color:black">On trouve des pucerons ail&eacute;s, mais &eacute;galement des colonies comptant jusqu&rsquo;&agrave; 10 larves.</span></p>

<p style="text-align:justify"><span style="color:black">Les analyses virologiques montrent une tr&egrave;s faible proportion de pucerons porteurs du virus de la jaunisse nanisante.</span></p>

<p style="text-align:justify"><strong><span style="color:black">Dans cette situation,</span></strong><span style="color:black"> <strong>il n&rsquo;y a pas d&rsquo;urgence &agrave; traiter</strong>.&nbsp; Traiter trop t&ocirc;t, surtout sur des plantes peu d&eacute;velopp&eacute;es, est d&rsquo;ailleurs un facteur d&rsquo;&eacute;chec fr&eacute;quent de la protection contre la jaunisse nanisante. En effet, ceci ouvre la porte &agrave; la recolonisation des emblavures par les pucerons apr&egrave;s traitement. Toutefois, l&rsquo;&eacute;volution est tr&egrave;s rapide. <strong>Il faut</strong> donc <strong>se pr&eacute;parer &agrave; traiter, et ne pas se laisser surprendre par une longue p&eacute;riode de pluie sans pouvoir acc&eacute;der aux champs</strong>.</span></p>

<p style="text-align:justify"><span style="color:black">Les informations concernant la proportion de pucerons porteurs du virus de la jaunisse nanisante donne une indication, mais pas une id&eacute;e pr&eacute;cise. Aussi, <strong>les champs dont plus de 10 % des plantes sont porteuses de pucerons devraient &ecirc;tre trait&eacute;s, <u>sans urgence</u></strong> mais avant d&rsquo;en &ecirc;tre emp&ecirc;ch&eacute; par les pluies pour une p&eacute;riode ind&eacute;termin&eacute;e.</span></p>

<p style="text-align:justify"><span style="color:black">Il fait extr&ecirc;mement sec. Aussi est-il conseill&eacute; de ne pas appliquer de traitement insecticide en pleine journ&eacute;e, mais plut&ocirc;t le soir, ou bien t&ocirc;t le matin, dans la ros&eacute;e. Les insecticides pyr&eacute;thrino&iuml;des sont moins efficaces par temps doux et sec que par temps plus froid et humide. Si les conditions m&eacute;t&eacute;orologiques sont douces et s&egrave;ches lors du traitement, les pyr&eacute;thrino&iuml;des gagneraient en efficacit&eacute; en &eacute;tant accompagn&eacute;s d&rsquo;un quart de dose d&rsquo;un produit &agrave; base de pirimicarbe.</span></p>

<p style="text-align:justify"><span style="color:black">Lien vers la liste des insecticides autoris&eacute;s contre pucerons vecteurs de la jaunisse nanisante : </span><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">ici</a></p>

<p style="text-align:justify"><span style="color:black">Toutes les listes des produits phytopharmaceutiques autoris&eacute;s en c&eacute;r&eacute;ales : lien </span><a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>

<p><u><span style="color:black">Les vari&eacute;t&eacute;s tol&eacute;rantes au virus de la jaunisse nanisante de l&rsquo;orge</span></u><span style="color:black"> (&agrave; savoir <strong>RAFAELA</strong>, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) ne justifient aucun traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons qui y serait constat&eacute;.</span></p>

<p><span style="color:black">Nos prochaines observations seront celles du 22 octobre. Elles seront men&eacute;es en escourgeon et dans les premiers froments. </span><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 23 octobre.</span></p>

<p>&nbsp;</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p>&nbsp;</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (081/875870), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, 389, 51, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (127, 'CePiCOP, communiqué du 16 octobre 2018 (C16)', 'left', 350, 200, '<p style="text-align:justify">La m&eacute;t&eacute;o exceptionnelle avec des temp&eacute;ratures tr&egrave;s &eacute;lev&eacute;es pour le mois d&rsquo;octobre (10 &agrave; 12&deg;C au-dessus des normales de saison) reste favorable &agrave; l&rsquo;activit&eacute; des insectes dans le colza. Les captures de grosses altises continuent, surtout l&agrave; o&ugrave; il n&rsquo;y a pas eu de traitement insecticide. Quelques rares charan&ccedil;ons du bourgeon terminal ont &eacute;t&eacute; pi&eacute;g&eacute;s. Les premiers pucerons ont &eacute;t&eacute; observ&eacute;s dans les bassins et &agrave; la face inf&eacute;rieure des feuilles de colza.</p>

<p style="text-align:justify">Aucune vari&eacute;t&eacute; de colza n&rsquo;est r&eacute;sistante &agrave; la grosse altise ni au charan&ccedil;on du bourgeon terminal. Le risque li&eacute; &agrave; la pr&eacute;sence de larves dans les p&eacute;tioles ou le coeur de la plante &agrave; la fin de l&rsquo;automne, est r&eacute;duit par l&rsquo;&eacute;limination des insectes adultes dans la culture de colza.</p>

<p style="text-align:justify">Il existe actuellement 2 vari&eacute;t&eacute;s de colza tol&eacute;rantes au virus de la jaunisse du navet (TuYV) transmis par les pucerons : ARCHITECT et ANGELICO. Ces vari&eacute;t&eacute;s ne n&eacute;cessitent pas de traitement visant les pucerons.</p>

<p style="text-align:justify">La s&eacute;cheresse qui se prolonge, se marque toujours au niveau des colzas lev&eacute;s tardivement qui pr&eacute;sentent des stades allant de cotyl&eacute;dons &agrave; 2-3 feuilles, bien en retard par rapport au colza ayant b&eacute;n&eacute;fici&eacute; d&rsquo;eau apr&egrave;s le semis. Les plus avanc&eacute;s continuent &agrave; produire de la biomasse, en accumulant les sommes de temp&eacute;ratures depuis leur lev&eacute;e.</p>

<p style="text-align:justify">Les plus petits colzas continuent &agrave; &ecirc;tre sensibles aux attaques d&rsquo;insectes ; la surveillance doit continuer pour les prot&eacute;ger et les maintenir.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>
', 1, NULL, 52, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (128, 'Jaunisse nanisante : infestation en cours', 'left', 150, 112, '<p>&nbsp;</p>

<p><strong><span style="color:#726056"><u>Observations</u>&nbsp; du 15 octobre 2018 ; semis du 22 septembre &agrave; d&eacute;but octobre</span></strong></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 19 parcelles</u></strong> r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath, Mainvault, Wiers), Brabant wallon (Jandrain, Opain), Li&egrave;ge (Fexhe-Slins, Mortroux, Pailhe, Verlaine), Namur (Anth&eacute;e, Biesmer&eacute;e, Clermont, Corroy-le-ch&acirc;teau, Falmagne, Foy-Notre-Dame, Morville, Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Les escourgeons les plus d&eacute;velopp&eacute;s sont au stade tallage, d&rsquo;autres vont de 1 &agrave; 3 feuilles. Les premiers froments commencent &agrave; lever, les plus avanc&eacute;s sont au stade deux feuilles. </span></p>

<p style="text-align:justify"><span style="color:black">Les froments r&eacute;cemment lev&eacute;s en attestent, la douceur a &eacute;t&eacute; favorable aux vols de pucerons que l&rsquo;on retrouve dans tous les champs du r&eacute;seau. Ces conditions m&eacute;t&eacute;orologiques sont &eacute;galement favorables aux cicadelles qu&rsquo;on observe abondamment en c&eacute;r&eacute;ales. </span></p>

<p style="text-align:center"><span style="color:black"><img align="" filer_id="391" height="198" src="/filer/canonical/1540279091/391/" thumb_option="" title="Cicadelle" width="151" /><img align="" filer_id="390" height="196" src="/filer/canonical/1540279091/390/" thumb_option="" title="Cicadelle" width="161" /></span></p>

<p style="text-align:justify"><span style="color:black">Il s&rsquo;agit de petits insectes, progressant par bonds suivis de vol maladroit sur un ou deux m&egrave;tres. En c&eacute;r&eacute;ales se sont des cicadelles vertes qui sont le plus observ&eacute;es. Ces insectes sont tr&egrave;s mobiles et peuvent tr&egrave;s rapidement recoloniser des champs trait&eacute;s. Ceci ne signifie pas que les champs ne soient plus prot&eacute;g&eacute;s par l&rsquo;insecticide, mais simplement que les cicadelles progressent tr&egrave;s vite. <strong>Les cicadelles ne sont pas nuisibles</strong>. Elles sont n&eacute;anmoins surveill&eacute;es, parce qu&rsquo;elles peuvent &ecirc;tre vectrices du virus du pied ch&eacute;tif. Ce virus du bl&eacute; est pr&eacute;sent dans le Centre de la France, mais n&rsquo;a encore jamais &eacute;t&eacute; observ&eacute; en Belgique.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:center"><strong><span style="color:#726056"><u><img align="" filer_id="393" height="187" src="/filer/canonical/1540279091/393/" thumb_option="" title="Puceron ailé déposant une jeune larve " width="278" /><img align="" filer_id="397" height="187" src="/filer/canonical/1540280640/397/" thumb_option="" title="Puceron aptère " width="209" /></u></span></strong></p>

<p style="text-align:justify"><u><span style="color:black">En escourgeon</span></u><span style="color:black"> :</span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Dans les <strong>champs d&rsquo;escourgeon trait&eacute;s</strong> par pulv&eacute;risation vers le 17 &ndash; 20 octobre, les populations sont le plus souvent &agrave; z&eacute;ro. Toutefois, certains champs trait&eacute;s &agrave; un stade pr&eacute;coce (une-deux feuilles) peuvent avoir &eacute;t&eacute; recolonis&eacute;s jusqu&rsquo;&agrave; un niveau de 10 % des plantes. </span></li>
	<li style="text-align:justify"><span style="color:black">Dans <strong>les champs d&rsquo;escourgeon non trait&eacute;</strong>, les populations vont de 0 &agrave; 24% de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron. </span></li>
	<li style="text-align:justify"><span style="color:#292934">Pour rappel : </span><u><span style="color:#292934">Les <strong>vari&eacute;t&eacute;s tol&eacute;rantes</strong> au virus de la jaunisse nanisante de l&rsquo;orge</span></u><span style="color:#292934"> (&agrave; savoir <strong>RAFAELA</strong>, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) ne justifient aucun traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons qui y serait constat&eacute;.</span></li>
</ul>

<p style="text-align:justify"><u><span style="color:black">En froment, &eacute;peautre</span></u><span style="color:black"> : </span></p>

<p><span style="color:black">Les populations de pucerons vont de 3 et 8 % de plantes occup&eacute;es par au moins un puceron. </span></p>

<p style="text-align:justify"><span style="color:black">Aucun puceron analys&eacute; cette ann&eacute;e n&rsquo;est virulif&egrave;re. On peut d&egrave;s lors consid&eacute;rer qu&rsquo;au champ, la proportion de pucerons porteuse du virus de la jaunisse nanisante est tr&egrave;s faible. </span></p>

<p style="text-align:justify"><span style="color:black">Ces informations concernant la proportion de pucerons porteurs du virus de la jaunisse nanisante donne une indication, mais pas une id&eacute;e pr&eacute;cise. Aussi, <strong>les champs dont plus de 10 % des plantes sont porteuses de pucerons devraient &ecirc;tre trait&eacute;s, <u>sans urgence</u></strong> mais avant d&rsquo;en &ecirc;tre emp&ecirc;ch&eacute; par les pluies pour une p&eacute;riode ind&eacute;termin&eacute;e.</span></p>

<p style="text-align:justify"><span style="color:black">Un contr&ocirc;le de ses terres est donc utile afin de juger de l&rsquo;opportunit&eacute; d&rsquo;un traitement. &nbsp;</span></p>

<p style="text-align:justify"><span style="color:black">Il fait extr&ecirc;mement sec. Aussi est-il conseill&eacute; de ne pas appliquer de traitement insecticide en pleine journ&eacute;e, mais plut&ocirc;t le soir, ou bien t&ocirc;t le matin, dans la ros&eacute;e. Les insecticides pyr&eacute;thrino&iuml;des sont moins efficaces par temps doux et sec que par temps plus froid et humide. Si les conditions m&eacute;t&eacute;orologiques sont douces et s&egrave;ches lors du traitement, les pyr&eacute;thrino&iuml;des gagneraient en efficacit&eacute; en &eacute;tant accompagn&eacute;s d&rsquo;un quart de dose d&rsquo;un produit &agrave; base de pirimicarbe.</span></p>

<p style="text-align:justify"><span style="color:black">Lien vers la liste des insecticides autoris&eacute;s contre pucerons vecteurs de la jaunisse nanisante : </span><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">ici</a></p>

<p style="text-align:justify"><span style="color:black">Toutes les listes des produits phytopharmaceutiques autoris&eacute;s en c&eacute;r&eacute;ales : lien </span><a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>

<p style="text-align:justify"><span style="color:black">Lien vers le document : &laquo; </span>La jaunisse nanisante, un virus transmis par pucerons sous la loupe.&raquo; : <a href="http://www.cadcoasbl.be/p09_biblio/art0001/JNO.pdf">ici</a></p>

<p><span style="color:black">Nos prochaines observations seront celles du 29 octobre. Elles seront men&eacute;es en escourgeon et froment. </span><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 30 octobre.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468/383972), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, 393, 53, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (129, 'CePiCOP, communiqué du 23 octobre 2018 (C17)', 'left', 350, 200, '<p style="text-align:justify">Apr&egrave;s une semaine de douceur o&ugrave; les captures de grosses altises et de rares charan&ccedil;ons ont encore &eacute;t&eacute; observ&eacute;s, le retour d&rsquo;un temps plus automnal avec une chute marqu&eacute;e des temp&eacute;ratures va calmer leur activit&eacute;.</p>

<p style="text-align:justify">Les premi&egrave;res larves d&rsquo;altises sont visibles dans les p&eacute;tioles des feuilles les plus &acirc;g&eacute;es du colza non prot&eacute;g&eacute;.</p>

<p style="text-align:justify">Les pucerons apparus r&eacute;cemment en colza sont d&eacute;j&agrave; parasit&eacute;s.&nbsp; Il n&rsquo;y a donc pas de crainte li&eacute;e &agrave; leur pr&eacute;sence.</p>

<p style="text-align:justify">Le colza poursuit son d&eacute;veloppement avec des situations contrast&eacute;es selon les champs.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP</p>

<p style="text-align:right">Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>
', 1, NULL, 54, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (131, 'A l''agenda', 'left', 300, 225, '<ul>
	<li style="text-align:justify"><strong>Jeudi 08 novembre</strong> &agrave; 18h00 Gembloux<strong> : Assembl&eacute;e Sectorielle &quot;Grandes cultures et pomme de terre&quot;</strong> Maison de la Ruralit&eacute;, Chauss&eacute;e de Namur 47, 5030 Gembloux. Inscription : 081/24 04 30 ou&nbsp; <a href="mailto:info.socopro@collegedesproducteurs.be" target="_blank">info.socopro@collegedesproducteurs.be </a>;&nbsp; <a href="file:///C:\Users\ASBL%20CADCO\CADCO\Site%20CADCO\p10_agenda\181108%20SOCOPRO%20Invit%20AS%20GC%20et%20pdt.pdf">Programme.pdf</a></li>
</ul>

<ul>
	<li style="text-align:justify"><strong><span style="color:black">15, 16 et 17 novembre</span></strong><span style="color:black"> : </span><strong>Festival des terres nourrici&egrave;res sur la commune de Nassogne</strong> Prix d&#39;entr&eacute;e : 5 EUR. R&eacute;servation souhait&eacute;e au 0498/120139 ou chantal@festivalnourriciere.be ;&nbsp; <a href="file:///C:\Users\ASBL%20CADCO\CADCO\Site%20CADCO\p10_agenda\1811FestivalTerresNourricieres.pdf">Programme.pdf</a> ; <a href="file:///C:\Users\ASBL%20CADCO\CADCO\Site%20CADCO\p10_agenda\1811FestivalTerresNourricieres1.pdf">Compl&eacute;ment programme.pdf</a></li>
</ul>
', 3, NULL, 55, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (130, 'Jaunisse nanisante : vers la fin des vols de pucerons', 'left', 150, 112, '<p><strong><span style="color:#726056">Observations 29 octobre 2018 ; </span></strong></p>

<p><span style="color:#726056">Semis escourgeon du 22 septembre &agrave; d&eacute;but octobre ; Semis de froment du 05 octobre au 18 octobre</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 18 parcelles d&rsquo;escourgeon </u></strong>r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath, Mainvault, Wiers), Brabant wallon (Ophain BS), Li&egrave;ge (Bleret, Fexhe-Slins, Mortroux, Pailhe, Verlaine), Namur (Anth&eacute;e, Biesmer&eacute;e, Clermont, Corroy-le-ch&acirc;teau, Falmagne, </span><span style="color:#726056">Foy-Notre-Dame,</span><span style="color:#726056"> Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 13 parcelles de froment</u></strong> r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (H&eacute;rinnes, Houtaing), Brabant wallon (Jandrain, Ramilies, Vieux Genappe), Li&egrave;ge (Haccourt, Verlaine), Namur (Clermont, Corroy-le-ch&acirc;teau, Meux, Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p style="text-align:justify"><span style="color:black">Les observations de cette semaine montrent que :</span></p>

<p style="text-align:justify"><strong><u><span style="color:black">En escourgeon</span></u></strong><span style="color:black"> :</span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Dans les <strong>champs d&rsquo;escourgeon trait&eacute;s</strong> par pulv&eacute;risation vers le 17 &ndash; 20 octobre, les populations vont de 0 &agrave; 14% de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron</span><span style="color:black">. Les champs au-dessus des 10 % sont rares (1 sur 11).</span></li>
</ul>

<ul>
	<li style="text-align:justify"><span style="color:black">Dans <strong>les champs d&rsquo;escourgeon non trait&eacute;</strong>, les populations vont de 0 &agrave; 14% de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron. Plus de la moiti&eacute; des </span><span style="color:black">champs sont au-dessus des 10 % (4 sur 7).</span></li>
</ul>

<ul>
	<li style="text-align:justify"><span style="color:#292934">Pour rappel :</span><u><span style="color:#292934"> Les <strong>vari&eacute;t&eacute;s tol&eacute;rantes</strong> au virus de la jaunisse nanisante de l&rsquo;orge</span></u><span style="color:#292934"> (&agrave; savoir <strong>RAFAELA</strong>, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) ne justifient aucun traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons qui y serait constat&eacute;.</span></li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u><span style="color:black">En froment, &eacute;peautre</span></u></strong><span style="color:black"> : </span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Les populations de pucerons n&rsquo;ont pas &eacute;volu&eacute;es. <strong>Elles sont toutes inf&eacute;rieures &agrave; 10 %</strong> allant de 1 et 8 % de plantes occup&eacute;es par au moins un puceron. </span></li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Des pucerons analys&eacute;s, un seul est virulif&egrave;re. On peut d&egrave;s lors consid&eacute;rer qu&rsquo;au champ, la proportion de pucerons porteurs du virus de la jaunisse nanisante est tr&egrave;s faible.&nbsp;Toutefois, l&rsquo;&eacute;chantillonnage reste limit&eacute; et les informations concernant la proportion de pucerons porteurs du virus de la jaunisse nanisante ne donne qu&rsquo;une indication</span></p>

<table style="border:undefined; width:100%">
	<tbody>
		<tr>
			<td>
			<p style="text-align:justify"><strong><u><span style="color:black">Recommandation </span></u></strong><span style="color:black">: </span></p>

			<p style="text-align:justify"><span style="color:black"><strong>Les champs dont plus de 10 % des plantes sont porteuses de pucerons devraient &ecirc;tre trait&eacute;s avant l&rsquo;hiver.</strong></span></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><span style="color:black">Dans cette situation, notre conseil est de ne pas laisser monter les populations plus haut que 10 % de plantes occup&eacute;es.&nbsp; <strong><u>Nos observations de la semaine prochaine feront un point d&eacute;finitif sur l&rsquo;attitude &agrave; adopter avant de remiser les pulv&eacute;risateurs</u>.</strong></span></p>

<p style="text-align:justify"><span style="color:black">Un contr&ocirc;le de ses terres est donc utile afin de juger de l&rsquo;opportunit&eacute; d&rsquo;un traitement.&nbsp; </span></p>

<p style="text-align:justify"><span style="color:black">Il conseill&eacute; de ne pas appliquer de traitement insecticide en pleine journ&eacute;e, mais plut&ocirc;t le soir, ou bien t&ocirc;t le matin, dans la ros&eacute;e. Les insecticides pyr&eacute;thrino&iuml;des sont moins efficaces par temps doux et sec que par temps plus froid et humide.</span></p>

<p>&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Lien vers la liste des insecticides autoris&eacute;s contre pucerons vecteurs de la jaunisse nanisante : </span><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">ici</a></p>

<p style="text-align:justify"><span style="color:black">Toutes les listes des produits phytopharmaceutiques autoris&eacute;s en c&eacute;r&eacute;ales : lien </span><a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>

<p style="text-align:justify"><span style="color:black">Lien vers le document : &laquo; </span>La jaunisse nanisante, un virus transmis par pucerons sous la loupe.&raquo; : <a href="http://www.cadcoasbl.be/p09_biblio/art0001/JNO.pdf">ici</a></p>

<p><span style="color:black">Nos prochaines observations seront celles du 05 novembre. Elles seront men&eacute;es en froment. </span></p>

<p><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 06 novembre.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468/383972), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, 393, 55, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (133, 'A l''agenda', 'left', 300, 225, '<ul>
	<li style="text-align:justify"><strong>Jeudi 08 novembre</strong> &agrave; 18h00 Gembloux<strong> : Assembl&eacute;e Sectorielle &quot;Grandes cultures et pomme de terre&quot;</strong> Maison de la Ruralit&eacute;, Chauss&eacute;e de Namur 47, 5030 Gembloux. Inscription : 081/24 04 30 ou&nbsp; <a href="mailto:info.socopro@collegedesproducteurs.be" target="_blank">info.socopro@collegedesproducteurs.be </a>;&nbsp; <a href="file:///C:\Users\ASBL%20CADCO\CADCO\Site%20CADCO\p10_agenda\181108%20SOCOPRO%20Invit%20AS%20GC%20et%20pdt.pdf">Programme.pdf</a></li>
</ul>

<ul>
	<li style="text-align:justify"><strong><span style="color:black">15, 16 et 17 novembre</span></strong><span style="color:black"> : </span><strong>Festival des terres nourrici&egrave;res sur la commune de Nassogne</strong> Prix d&#39;entr&eacute;e : 5 EUR. R&eacute;servation souhait&eacute;e au 0498/120139 ou chantal@festivalnourriciere.be ;&nbsp; <a href="file:///C:\Users\ASBL%20CADCO\CADCO\Site%20CADCO\p10_agenda\1811FestivalTerresNourricieres.pdf">Programme.pdf</a> ; <a href="file:///C:\Users\ASBL%20CADCO\CADCO\Site%20CADCO\p10_agenda\1811FestivalTerresNourricieres1.pdf">Compl&eacute;ment programme.pdf</a></li>
</ul>
', 3, NULL, 56, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (136, 'CePiCOP, communiqué du 06 novembre 2018 (C18)', 'left', 350, 200, '<p style="text-align:justify">Apr&egrave;s une semaine avec des conditions nettement plus humides (pluies abondantes et de la neige &agrave; certains endroits) et plus froides (gel nocturne), les temp&eacute;ratures sont &agrave; nouveau tr&egrave;s douces et nettement sup&eacute;rieures aux normales saisonni&egrave;res.</p>

<p style="text-align:justify">La culture de colza d&rsquo;hiver a bien profit&eacute; de l&rsquo;eau qui a &eacute;t&eacute; d&eacute;ficitaire dans de nombreux champs.&nbsp; Les stades &eacute;voluent encore et permettront au colza d&rsquo;&ecirc;tre pr&ecirc;t pour l&rsquo;hiver.</p>

<p style="text-align:justify">C&ocirc;t&eacute; insectes, les petites larves d&rsquo;altises (blanches avec une t&ecirc;te noire) sont actuellement bien visibles &agrave; l&rsquo;int&eacute;rieur des p&eacute;tioles des feuilles, &agrave; la base des plantes qui n&rsquo;ont pas re&ccedil;u de traitement insecticide cet automne.&nbsp; Elles r&eacute;sultent de la ponte des altises adultes observ&eacute;es en abondance depuis la lev&eacute;e du colza.&nbsp; Ces larves resteront abrit&eacute;es dans les plantes durant l&rsquo;hiver et quitteront les plantes au printemps.</p>

<p style="text-align:justify">Les pucerons ail&eacute;s, noirs et verts, sont &eacute;galement visibles &agrave; la face inf&eacute;rieure des feuilles et dans les pi&egrave;ges sur&eacute;lev&eacute;s.&nbsp; Des analyses sont en cours pour v&eacute;rifier la pr&eacute;sence &eacute;ventuelle de de viroses.</p>

<p style="text-align:justify">De tr&egrave;s rares charan&ccedil;ons du bourgeon terminal ont &eacute;t&eacute; pi&eacute;g&eacute;s alors qu&rsquo;il y a encore des captures de grosses altises.</p>

<p style="text-align:justify">L&rsquo;extr&ecirc;me douceur actuelle incite &agrave; continuer &agrave; v&eacute;rifier la pr&eacute;sence des insectes ravageurs &agrave; l&rsquo;automne.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP</p>

<p style="text-align:right">Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>
', 1, NULL, 58, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (132, 'Jaunisse nanisante : Trop tôt encore pour remiser les pulvérisateurs…', 'left', 150, 112, '<p><span style="color:#726056">Observations 05 novem</span><span style="color:#726056">bre 2018</span><strong><span style="color:#726056"> : </span></strong><span style="color:#726056">Semis escourgeon du 22 septembre &agrave; d&eacute;but octobre ; Semis de froment du 05 octobre au 18 octobre</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 19 parcelles d&rsquo;escourgeon</u> </strong></span> <span style="color:#726056">r&eacute;parties dans les localit&eacute;s suivantes&nbsp;: Hainaut (Ath, Mainvault, Wiers), Brabant wallon (Jandrain, Ophain BS), Li&egrave;ge (Bleret, Fexhe-Slins, Mortroux, Pailhe, Verlaine), Namur (Anth&eacute;e, Biesmer&eacute;e, Clermont, Corroy-le-ch&acirc;teau, Falmagne, Foy-Notre-Dame, Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 15 parcelles de froment</u></strong> r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (H&eacute;rinnes (1 &agrave; 4)), Brabant wallon (Jandrain, Ramilies, Vieux Genappe), Li&egrave;ge (Haccourt, Verlaine), Namur (Clermont, Corroy-le-ch&acirc;teau, Meux, Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p style="text-align:justify"><span style="color:black">Les observations des derniers jours montrent peu d&rsquo;&eacute;volution.</span></p>

<p style="text-align:justify"><strong><u><span style="color:black">En escourgeon</span></u></strong><span style="color:black"> :</span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Dans les <strong>champs pulv&eacute;ris&eacute;s</strong> vers le 17 &ndash; 20 octobre, les populations vont de 0 &agrave; 10 % de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron</span><span style="color:black">. Les champs &agrave; 10 % sont rares (2 sur 19).</span></li>
	<li style="text-align:justify"><span style="color:black">Dans <strong>les champs non trait&eacute;s</strong>, les populations vont de 0 &agrave; 10 % de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron.</span></li>
	<li style="text-align:justify"><span style="color:#292934">Pour rappel : </span><u><span style="color:#292934">Les <strong>vari&eacute;t&eacute;s tol&eacute;rantes</strong> au virus de la jaunisse nanisante de l&rsquo;orge</span></u><span style="color:#292934"> (&agrave; savoir <strong>RAFAELA</strong>, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) ne justifient aucun traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons qui y serait constat&eacute;.</span></li>
</ul>

<p style="text-align:justify"><strong><u><span style="color:black">En froment, &eacute;peautre</span></u></strong><span style="color:black"> : </span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Les populations de pucerons n&rsquo;ont pas &eacute;volu&eacute;. <strong>Elles sont toutes inf&eacute;rieures &agrave; 10 %</strong> allant de 1 et 8 % de plantes occup&eacute;es par au moins un puceron. </span></li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Des pucerons collect&eacute;s (311), un seul s&rsquo;est r&eacute;v&eacute;l&eacute; &ecirc;tre virulif&egrave;re. On peut d&egrave;s lors consid&eacute;rer que <strong>la pression par la jaunisse nanisante est tr&egrave;s faible</strong>.</span></p>

<p style="text-align:justify"><span style="color:black">Dans cette situation d&rsquo;automne qui s&rsquo;&eacute;ternise, nous vous invitons &agrave; reporter encore le remisage des pulv&eacute;risateurs. En effet, m&ecirc;me dans l&rsquo;id&eacute;e d&rsquo;un traitement avant l&rsquo;hiver, il est conseill&eacute; d&rsquo;attendre le plus tard possible, mais sans se laisser surprendre par une p&eacute;riode de pluie qui emp&ecirc;cherait l&rsquo;acc&egrave;s aux champs pour une p&eacute;riode ind&eacute;finie.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Nos prochaines observations seront celles du 12 novembre. Elles seront men&eacute;es en escourgeons et en froment. </span><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 13 </span><span style="color:black">novembre</span><span style="color:black">.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468/383972), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, 393, 56, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (139, 'Jaunisse nanisante : la fin des vols', 'left', 150, 112, '<p><span style="color:#726056">Observations 05 novem</span><span style="color:#726056">bre 2018</span><strong><span style="color:#726056"> : </span></strong><span style="color:#726056">Semis escourgeon du 22 septembre &agrave; d&eacute;but octobre ; Semis de froment du 05 octobre au 18 octobre</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 16 parcelles d&rsquo;escourgeon</u> </strong></span><span style="color:#726056">r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath, Mainvault, Wiers), Brabant wallon (Jandrain 1 et 2, Ophain BS), Li&egrave;ge (Bleret, Fexhe-Slins, Mortroux, Pailhe, Verlaine), Namur (Anth&eacute;e, Clermont, Falmagne, Rhisnes, Thy-le-ch&acirc;teau)</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 15 parcelles de froment</u></strong> </span> <span style="color:#726056">r&eacute;parties dans les localit&eacute;s suivantes&nbsp;: Hainaut (Ath<strong>, </strong>H&eacute;rinnes (1 &agrave; 4)), Brabant wallon (Lilois,<strong> </strong>Ramilies, Vieux Genappe), Li&egrave;ge (Haccourt, Verlaine), Namur (Clermont, Corroy-le-ch&acirc;teau, Meux, Rhisnes, St-Denis)</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="color:black">Les observations de ce lundi montrent que la situation n&rsquo;&eacute;volue quasi plus et que les vols de pucerons sont quasi termin&eacute;s.</span></p>

<p style="text-align:justify"><strong><u><span style="color:black">En escourgeon</span></u></strong><span style="color:black"> :</span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Dans les <strong>champs pulv&eacute;ris&eacute;s</strong> vers le 17 &ndash; 20 octobre, les populations vont de 0 &agrave; 4 % de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron</span><span style="color:black">.</span></li>
	<li style="text-align:justify"><span style="color:black">Dans <strong>les champs non trait&eacute;s</strong>, les populations vont de 0 &agrave; 13 % de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron.</span></li>
</ul>

<p style="text-align:justify"><strong><u><span style="color:black">En froment, &eacute;peautre</span></u></strong><span style="color:black"> : </span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">Les <strong>champs pulv&eacute;ris&eacute;s</strong> vers le d&eacute;but novembre, sont indemnes de pucerons.</span></li>
	<li style="text-align:justify"><span style="color:black">Dans les autres champs, les populations de pucerons sont de 1 et 8 % de plantes occup&eacute;es par au moins un puceron. </span></li>
</ul>

<p style="text-align:justify"><span style="color:black">Des observations effectu&eacute;es en Flandre indiquent des niveaux d&rsquo;infestation nettement plus &eacute;lev&eacute;s, d&eacute;passant quelquefois les 80 % de plantes occup&eacute;es. Par ailleurs, sur les 311 pucerons collect&eacute;s, un seul s&rsquo;est r&eacute;v&eacute;l&eacute; virulif&egrave;re. </span></p>

<p style="text-align:justify"><strong><span style="color:black">Quel risque de d&eacute;g&acirc;t de jaunisse nanisante ?</span></strong></p>

<p style="text-align:justify"><span style="color:black">Un puceron virulif&egrave;re arrivant dans une emblavure donne lieu &agrave; une amorce de jaunisse. A partir de chaque amorce, une plage infect&eacute;e se d&eacute;veloppe de plante en plante, tant que la temp&eacute;rature est assez douce pour permettre l&rsquo;activit&eacute; des pucerons. On sait que les amorces de jaunisse sont rares cette ann&eacute;e. En revanche, l&rsquo;automne doux qui se prolonge est favorable &agrave; la croissance des plages infect&eacute;es. Dans ce sc&eacute;nario, on peut pronostiquer que le faci&egrave;s de la jaunisse nanisante &eacute;voluera vers un <strong><u>petit nombre de taches de jaunisse, aux dimensions d&rsquo;autant plus grandes que les temp&eacute;ratures douces se prolongeront</u></strong>.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u><span style="color:black">Conseil</span></u></strong></p>

<p style="text-align:justify"><span style="color:black">Un traitement insecticide est recommand&eacute; dans les emblavures de fin septembre-d&eacute;but octobre qui n&rsquo;auraient pas encore &eacute;t&eacute; trait&eacute;es, si le niveau de 10% des plantes occup&eacute;es par des pucerons est atteint.</span></p>

<p style="text-align:justify"><span style="color:black">Dans les emblavures plus tardives, de m&ecirc;me que dans les champs pulv&eacute;ris&eacute;s vers le 15-20 octobre, il est peu probable que la jaunisse atteigne un niveau dommageable, et un traitement insecticide devrait s&rsquo;av&eacute;rer inutile. Notre conseil pour ces champs serait d&rsquo;attendre encore quelques jours avant de d&eacute;cider d&rsquo;un &eacute;ventuel traitement pr&eacute;-hivernal.</span></p>

<p><span style="color:#292934">Pour rappel :</span><u><span style="color:#292934"> Les <strong>vari&eacute;t&eacute;s tol&eacute;rantes</strong> au virus de la jaunisse nanisante de l&rsquo;orge</span></u><span style="color:#292934"> (&agrave; savoir <strong>RAFAELA</strong>, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) ne justifient aucun traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons qui y serait constat&eacute;. </span></p>

<p style="text-align:justify"><span style="color:black">Nos prochaines observations seront celles du 19 novembre. </span><span style="color:black">Le prochain avis est pr&eacute;vu pour le mardi 20 </span><span style="color:black">novembre</span><span style="color:black">.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468/383972), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, 400, 60, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (143, 'Mesures visant à réduire l’impact des pesticides', 'left', 300, 225, '<p><strong><u>Rappel de mesures visant &agrave; r&eacute;duire l&rsquo;impact des pesticides et entr&eacute;es en vigueur cette ann&eacute;e</u></strong></p>

<ul>
	<li style="text-align:justify">Interdiction de d&eacute;buter une pulv&eacute;risation lorsque la vitesse du vent est sup&eacute;rieure &agrave; 20 km/heure.</li>
</ul>

<ul>
	<li style="text-align:justify">Interdiction de pulv&eacute;riser &agrave; moins de 50 m&egrave;tres des bords de toute parcelle qui jouxte un site d&rsquo;&eacute;tablissement (cours de r&eacute;cr&eacute;ation, &eacute;coles, internats, cr&egrave;ches et infrastructures d&#39;accueil de l&#39;enfance) durant les heures de fr&eacute;quentation de celui-ci.</li>
</ul>

<p style="text-align:justify"><u>Entrera en vigueur le 1er janvier</u></p>

<ul>
	<li style="text-align:justify">Obligation d&rsquo;utiliser un mat&eacute;riel d&rsquo;application qui r&eacute;duit la d&eacute;rive de minimum 50% :&nbsp; <a href="http://www.cadcoasbl.be/p09_biblio.html#art0004">Liste de mat&eacute;riel anti-d&eacute;rive</a></li>
</ul>
', 2, NULL, 61, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (141, 'Jaunisse nanisante : fin de saison', 'left', 150, 112, '<p><span style="color:#726056">Observations 19 novem</span><span style="color:#726056">bre 2018</span><strong><span style="color:#726056"> : </span></strong><span style="color:#726056">Semis escourgeon du 22 septembre &agrave; d&eacute;but octobre ; Semis de froment du 05 octobre au 18 octobre</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 14 parcelles d&rsquo;escourgeon</u> </strong></span><span style="color:#726056">r&eacute;parties dans les localit&eacute;s suivantes : Hainaut (Ath, Mainvault, Wiers), Brabant wallon (Jandrain), Li&egrave;ge (Bleret, Fexhe-Slins, Mortroux, Pailhe, Verlaine), Namur (Anth&eacute;e, Biesmer&eacute;e, Clermont, Rhisnes, Thy-le-ch&acirc;teau)</span></p>

<p><span style="color:#726056"><strong><u>R&eacute;seau de 16 parcelles de froment</u></strong> </span> <span style="color:#726056">r&eacute;parties dans les localit&eacute;s suivantes&nbsp;: Hainaut (Ath, H&eacute;rinnes (1 &agrave; 4)), Brabant wallon (Lilois, Ramilies, Vieux Genappe), Li&egrave;ge (Haccourt, Verlaine), Namur (Clermont, Corroy-le-ch&acirc;teau, Meux, Rhisnes, St-Denis, Thy-le-ch&acirc;teau)</span></p>

<p style="text-align:justify"><span style="color:black">Les analyses virologiques r&eacute;v&egrave;lent une proportion assez faible de pucerons porteurs du virus.</span></p>

<p style="text-align:justify"><span style="color:black">La chute actuelle des temp&eacute;ratures freine la dynamique des pucerons et, &agrave; moins d&rsquo;un retournement de situation tr&egrave;s improbable, ces derniers ne voleront plus.</span></p>

<p style="text-align:justify"><span style="color:black">Des observations de ce lundi, il ressort qu&rsquo;il n&rsquo;y a pas de risque pour : </span></p>

<ul>
	<li style="text-align:justify"><span style="color:black">les </span><strong><u><span style="color:#292934">vari&eacute;t&eacute;s tol&eacute;rantes</span></u></strong> <span style="color:black">au virus de la jaunisse nanisante de l&rsquo;orge (&agrave; savoir</span><span style="color:#292934"> <strong>RAFAELA</strong>, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) qui </span><span style="color:black">ne justifient aucun traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons qui y serait constat&eacute; (&agrave; ce jour, dans notre r&eacute;seau, le maximum observ&eacute; est de 22 %).</span></li>
	<li style="text-align:justify"><span style="color:black">les <strong>escourgeons pulv&eacute;ris&eacute;s</strong> vers le 17 &ndash; 20 octobre, les populations vont de 0 &agrave; 6 % de plantes occup&eacute;es par au moins </span><span style="color:#292934">un puceron</span><span style="color:black">. </span></li>
	<li style="text-align:justify"><span style="color:black">les froments <strong>pulv&eacute;ris&eacute;s</strong> vers le d&eacute;but novembre, qui sont indemnes de pucerons.</span></li>
	<li style="text-align:justify"><span style="color:black">les froments <strong>non trait&eacute;s</strong> dont l&rsquo;infestation est faible. Le maximum observ&eacute; est de 8 % de plantes occup&eacute;es ce qui est peu.</span></li>
</ul>

<p style="text-align:justify"><span style="color:black">Hors vari&eacute;t&eacute;s tol&eacute;rantes, dans les rares cas ou l&rsquo;infestation d&eacute;passerait 10 % des plantes,&nbsp; il n&rsquo;y aurait pas d&rsquo;urgence &agrave; traiter, mais il conviendrait d&rsquo;&eacute;viter de se laisser surprendre par les pluies dans des terres difficiles d&rsquo;acc&egrave;s. Dans notre &nbsp;r&eacute;seau d&rsquo;observation, ce cas de figure n&rsquo;a pas &eacute;t&eacute; rencontr&eacute; ce lundi. </span></p>

<p style="text-align:justify"><span style="color:black">Des observations effectu&eacute;es en Flandre occidentale indiquent des niveaux d&rsquo;infestation nettement plus &eacute;lev&eacute;s, d&eacute;passant quelquefois les 80 % de plantes occup&eacute;es. Les agriculteurs du Hainaut sont donc particuli&egrave;rement invit&eacute;s &agrave; aller visiter leurs parcelles.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><span style="color:black">Quel risque de d&eacute;g&acirc;t de jaunisse nanisante ?</span></strong></p>

<p style="text-align:justify"><span style="color:black">Un puceron virulif&egrave;re arrivant dans une emblavure donne lieu &agrave; une amorce de jaunisse. A partir de chaque amorce, une plage infect&eacute;e se d&eacute;veloppe de plante en plante, tant que la temp&eacute;rature est assez douce pour permettre l&rsquo;activit&eacute; des pucerons. On sait que les amorces de jaunisse sont rares cette ann&eacute;e. En revanche, un automne doux qui se prolonge est favorable &agrave; la croissance des plages infect&eacute;es. Dans ce sc&eacute;nario, on peut pronostiquer que le faci&egrave;s de la jaunisse nanisante &eacute;voluera vers un <strong><u>petit nombre de taches de jaunisse, aux dimensions d&rsquo;autant plus grandes que les temp&eacute;ratures douces se prolongeront</u></strong>.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u><span style="color:black">Conseil</span></u></strong></p>

<p style="text-align:justify"><span style="color:black">Un traitement insecticide est recommand&eacute; dans les emblavures de fin septembre-d&eacute;but octobre qui n&rsquo;auraient pas encore &eacute;t&eacute; trait&eacute;es, si le niveau de 10% des plantes occup&eacute;es par des pucerons est atteint.</span></p>

<p style="text-align:justify"><span style="color:black">Dans les emblavures plus tardives, de m&ecirc;me que dans les champs pulv&eacute;ris&eacute;s vers le 15-20 octobre, il est peu probable que la jaunisse atteigne un niveau dommageable, et un traitement insecticide devrait s&rsquo;av&eacute;rer inutile. Au cas o&ugrave; l&rsquo;hiver ne serait pas assez froid pour an&eacute;antir les pucerons, un traitement post-hivernal serait recommand&eacute; l&agrave; o&ugrave; des pucerons seraient encore observ&eacute;s. Ce constat fera l&rsquo;objet de nos observations &laquo; sortie d&rsquo;hiver &raquo; fin f&eacute;vrier, courant mars.</span></p>

<p style="text-align:justify"><span style="color:black">Cet avis cl&ocirc;ture la saison d&rsquo;avertissements 2018. Nous en profitons pour, d&rsquo;ores et d&eacute;j&agrave;, vous souhaiter une agr&eacute;able fin d&rsquo;ann&eacute;e.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468/383972), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, 400, 61, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (142, 'La mouche des semis ', 'left', 300, 225, '<p style="text-align:justify">En septembre dernier nous attirions votre attention sur le risque de mouche des semis en froment succ&eacute;dant aux arrachages pr&eacute;coces de chicor&eacute;e, de betteraves, et de certains l&eacute;gumes.</p>

<p style="text-align:justify">Comme l&rsquo;an dernier, des d&eacute;g&acirc;ts de mouche des semis sont signal&eacute;s dans diff&eacute;rentes r&eacute;gions du pays. Ces d&eacute;g&acirc;ts se manifestent par des d&eacute;fauts de lev&eacute;es, ou bien par des attaques de plantules : sectionnement de la tige et jaunissement de la plus jeune feuille.</p>

<p style="text-align:justify">Les r&eacute;sidus de ces cultures pourrissant sur le sol apr&egrave;s l&rsquo;arrachage attirent les mouches adultes qui, s&rsquo;il fait beau, y pondent abondamment. Les larves entament leur phase alimentaire sur ces r&eacute;sidus, mais s&rsquo;en prennent &eacute;galement aux froments fra&icirc;chement sem&eacute;s. La culture peut quelquefois &ecirc;tre compl&egrave;tement d&eacute;truite d&egrave;s avant la lev&eacute;e.</p>

<p style="text-align:justify">Au stade actuel, o&ugrave; la pr&eacute;sence de pupes brunes &agrave; noires t&eacute;moigne de la fin de la phase alimentaire de ces mouches, il ne faut plus craindre d&rsquo;attaque en cas de resemis, m&ecirc;me sans aucune protection insecticide.</p>

<p style="text-align:justify">N&rsquo;h&eacute;sitez pas &agrave; nous signaler tout d&eacute;g&acirc;t de mouche des semis, via ce <a href="https://goo.gl/forms/sVOHIVVheJpd9ICg1">formulaire</a> qui ne prend que deux minutes &agrave; remplir.&nbsp; Nous vous remercions d&rsquo;avance de votre aide.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, S. Chavalle, G. Jacquemin et M. De Proft</p>
', 1, NULL, 61, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (145, 'Enquête sur la gestion des adventices en froment d’hiver', 'left', 300, 225, '<p style="text-align:center">Vous cultivez du froment d&rsquo;hiver en syst&egrave;me conventionnel ou biologique ?</p>

<p style="text-align:center"><strong>Vos pratiques de d&eacute;sherbage nous int&eacute;ressent !</strong></p>

<p style="text-align:justify">Dans le cadre d&rsquo;un projet de recherche visant &agrave; am&eacute;liorer la lutte contre les adventices en adaptant la fumure azot&eacute;e, le CRA‑W et Gembloux Agro-Bio Tech r&eacute;alisent un &eacute;tat des lieux des pratiques de d&eacute;sherbage dans la culture de froment d&rsquo;hiver en Wallonie. Quelle est la r&eacute;partition des adventices en Wallonie ? Quel est l&rsquo;impact des adventices sur le rendement du froment ? Quelles sont les pratiques de d&eacute;sherbage les plus courantes ? Telles sont les questions auxquelles cette enqu&ecirc;te tente de r&eacute;pondre.</p>

<p>Vos r&eacute;ponses sont importantes. Elles permettront de faire &eacute;voluer les techniques de gestion des adventices. Dur&eacute;e du questionnaire : 15 minutes.</p>

<p style="text-align:justify">Retrouvez le questionnaire sur : &nbsp;<strong><a href="https://tinyurl.com/enquete-adventices">https://tinyurl.com/enquete-adventices</a></strong></p>

<p>Merci de votre participation.</p>

<p>&nbsp;</p>

<p style="text-align:right">Personne de contact : El&eacute;onore Malice&nbsp;&nbsp; <a href="mailto:e.malice@cra.wallonie.be">e.malice@cra.wallonie.be</a></p>
', 3, NULL, 62, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (155, 'Mesures effectives visant à réduire l’impact des pesticides', 'left', 300, 225, '<ul>
	<li style="text-align:justify">Interdiction de d&eacute;buter une pulv&eacute;risation lorsque la <strong>vitesse du vent</strong> est sup&eacute;rieure &agrave; 20 km/heure.</li>
</ul>

<ul>
	<li style="text-align:justify">Interdiction de pulv&eacute;riser &agrave; moins de 50 m&egrave;tres des <strong>bords de toute parcelle qui jouxte un site d&rsquo;&eacute;tablissement</strong> (cours de r&eacute;cr&eacute;ation, &eacute;coles, internats, cr&egrave;ches et infrastructures d&#39;accueil de l&#39;enfance) durant les heures de fr&eacute;quentation de celui-ci.</li>
	<li style="text-align:justify">Obligation d&rsquo;utiliser un <strong>mat&eacute;riel d&rsquo;application qui r&eacute;duit la d&eacute;rive</strong> de minimum 50%. <a href="http://www.cadcoasbl.be/p09_biblio.html#art0004">Liste de mat&eacute;riel anti-d&eacute;rive</a></li>
</ul>

<p style="margin-left:35.4pt; margin-right:0cm; text-align:justify">Les produits phyto ne comportant pas les m&ecirc;mes restrictions d&rsquo;utilisation en mati&egrave;re de <strong>zone tampon et de r&eacute;duction de la d&eacute;rive</strong>, le CADCO en collaboration avec Protect&rsquo;eau, a, pour votre facilit&eacute;, repris les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) dans les tableaux des produits au lien suivant : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>
', 2, NULL, 71, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (147, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les herbicides, fongicides &hellip; <strong> </strong></p>

<p style="text-align:justify">Nouveaut&eacute; de cette ann&eacute;e : en collaboration avec Protect&rsquo;eau, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m&egrave;tre) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces m&ecirc;mes tableaux des produits au lien suivant : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>

<p>&nbsp;</p>

<p><strong><u>Mesures effectives visant &agrave; r&eacute;duire l&rsquo;impact des pesticides</u></strong></p>

<ul>
	<li style="text-align:justify">Interdiction de d&eacute;buter une pulv&eacute;risation lorsque la <strong>vitesse du vent</strong> est sup&eacute;rieure &agrave; 20 km/heure.</li>
</ul>

<ul>
	<li style="text-align:justify">Interdiction de pulv&eacute;riser &agrave; moins de 50 m&egrave;tres des <strong>bords de toute parcelle qui jouxte un site d&rsquo;&eacute;tablissement</strong> (cours de r&eacute;cr&eacute;ation, &eacute;coles, internats, cr&egrave;ches et infrastructures d&#39;accueil de l&#39;enfance) durant les heures de fr&eacute;quentation de celui-ci.</li>
	<li style="text-align:justify">Obligation d&rsquo;utiliser un <strong>mat&eacute;riel d&rsquo;application qui r&eacute;duit la d&eacute;rive</strong> <strong>de minimum 50%</strong>. voir ici la <a href="http://www.cadcoasbl.be/p09_biblio.html#art0004">liste de mat&eacute;riel anti-d&eacute;rive</a></li>
</ul>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468 383972), visitez notre site : <a href="http://www.cadcoasbl.be/">www.cadcoasbl.be</a></p>
', 2, NULL, 64, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (146, 'Jaunisse nanisante : constat de sortie d’hiver ', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">En novembre dernier, nous annoncions un constat &laquo; sortie d&rsquo;hiver &raquo; pour fin f&eacute;vrier, courant mars. Les conditions actuelles y &eacute;tant propices, nous avons d&eacute;j&agrave; r&eacute;alis&eacute; une partie de ce constat.</span></p>

<p style="text-align:justify"><span style="color:black">L&rsquo;hiver a fait office de traitement. Ce constat de bonne augure est partiel. Nous vous renvoyons d&egrave;s lors &agrave; l&#39;avertissement de la semaine prochaine pour un constat exhaustif.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, G. Jacquemin et M. De Proft</p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468/383972), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 1, NULL, 64, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (154, 'Stade phénologique et fertilisation azotée', 'left', 300, 225, '<p style="text-align:justify">Si les conditions sont cl&eacute;mentes, les c&eacute;r&eacute;ales ne sont pas outre mesure en avance sur une ann&eacute;e habituelle.</p>

<p style="text-align:justify">Les froments sont actuellement au stade :</p>

<ul>
	<li>plein tallage pour les semis d&#39;octobre</li>
	<li>d&eacute;but tallage pour les semis de novembre</li>
	<li>2 &agrave; 3 feuilles tallage pour les semis de d&eacute;cembre</li>
</ul>

<p>Les escourgeons sont quant &agrave; eux toujours au tallage.</p>

<p>--</p>

<p><strong><em>Et demain,&hellip; </em></strong></p>

<p><strong><em>Venez au </em></strong><strong>Livre Blanc c&eacute;r&eacute;ales </strong><strong>(<em>info cf. agenda ci-dessous</em>)<em>, il y aura entre autre, l&rsquo;</em></strong><strong><em>actualisation des conseils de fumure,&hellip;</em></strong></p>

<p><strong><em>Disponible ce jeudi ou vendredi sur le Site Internet : </em></strong><a href="http://www.livre-blanc-cereales.be/thematiques/fumures/">Informations et outils &laquo; fumure Livre Blanc C&eacute;r&eacute;ales &raquo;</a></p>

<p>&nbsp;</p>

<p style="text-align:justify">A voir &eacute;galement sur le site de <strong>PROTECT&rsquo;eau</strong> :</p>

<p style="text-align:justify"><a href="https://protecteau.be/resources/shared/publications/fiches-techniques/Fertilisation/PE_6.5_FertilisationFroment(1812).pdf" target="_blank" title="Fertilisation Froment 18.12">Fertilisation du froment d&#39;hiver</a></p>

<p style="text-align:justify"><a href="https://protecteau.be/resources/shared/publications/fiches-techniques/APL/PE_8.2_AnalysesSol%20et%20reliquats(1804).pdf" target="_blank" title="Analyses de sol et reliquats azotés 18.04.18">Analyses de sol et reliquats azot&eacute;s</a></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson et R. Blanchard</p>
', 1, NULL, 71, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (183, 'Situation au 22 mars 2019', 'left', 300, 225, '<p style="text-align:justify">Les journ&eacute;es printani&egrave;res que nous connaissons actuellement avec un bon ensoleillement, des temp&eacute;ratures plus douces que la normale et l&rsquo;absence de vent, sont favorables au vol d&rsquo;insectes.</p>

<p style="text-align:justify">Il est facile actuellement d&rsquo;observer les m&eacute;lig&egrave;thes dans les pi&egrave;ges, au sommet des plantes de colza dans les boutons floraux ainsi que sur les fleurs d&rsquo;une vari&eacute;t&eacute; plus pr&eacute;coce d&eacute;j&agrave; en fleurs (ES Alicia).</p>

<p style="text-align:justify">Des charan&ccedil;ons de la tige sont encore observ&eacute;s mais en nombre tr&egrave;s r&eacute;duit.</p>

<p style="text-align:justify">Si le colza pr&eacute;sente des boutons accol&eacute;s et que son &eacute;tat de v&eacute;g&eacute;tation est bon, le seuil d&rsquo;intervention contre les m&eacute;lig&egrave;thes est de 3 &agrave; 4 m&eacute;lig&egrave;thes par plante, soit 120 &agrave; 160 m&eacute;lig&egrave;thes compt&eacute;s sur 40 plantes.&nbsp; Si le colza est faiblement d&eacute;velopp&eacute;, le seuil est d&rsquo;1 m&eacute;lig&egrave;the par plante, soit 40 m&eacute;lig&egrave;thes pour 40 plantes.</p>

<p style="text-align:justify">Le colza se d&eacute;veloppe rapidement et il faut bien &eacute;valuer le danger repr&eacute;sent&eacute; par les m&eacute;lig&egrave;thes en fonction de la taille des boutons floraux.&nbsp; Plus ils sont petits, plus ils sont sensibles aux attaques de m&eacute;lig&egrave;thes, car ils pourraient avorter (se d&eacute;ss&eacute;cher sans donner de silique).</p>

<p style="text-align:justify">Les capacit&eacute;s de compensation du colza restent importantes mais nous ne connaissons pas les conditions m&eacute;t&eacute;o &agrave; venir jusqu&rsquo;&agrave; la floraison &agrave; partir de laquelle les m&eacute;lig&egrave;thes trouvent du pollen sur les fleurs ouvertes et ne pr&eacute;sentent alors plus de danger pour la culture.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 79, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (148, 'Formation continue pour la phytolicence en céréales', 'left', 300, 225, '<p><u><span style="color:red">Conf&eacute;rences comptant dans le cadre de formations continues pour la phytolicence (**) </span>dans le cadre des c&eacute;r&eacute;ales</u> :</p>

<p><strong>Prochain rendez-vous le 27/02 &agrave; Gembloux : </strong><a href="http://www.cadcoasbl.be/p10_agenda/1902LB.pdf">Programme.pdf</a></p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td style="width:68.4pt">
			<p style="text-align:center"><span style="color:#ff0000">Mercredi 27/02/2019</span></p>
			</td>
			<td style="width:69.35pt">
			<p style="text-align:center"><span style="color:#ff0000">Gembloux<br />
			9h ou 14h00</span></p>
			</td>
			<td style="width:379.05pt">
			<p><span style="color:red">** Pr&eacute;sentation du <strong>Livre Blanc c&eacute;r&eacute;ales</strong> en l&#39;Espace Senghor (CePiCOP)</span>.</p>

			<p><strong>Deux</strong> <strong>s&eacute;ances identiques </strong>matin et apr&egrave;s-midi. <a href="http://www.cadcoasbl.be/p10_agenda/1902LB.pdf">Programme.pdf</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;De 8h30 &agrave; 9h00, durant la pause et de 12h05 &agrave; 12h45 : v&eacute;rification de la conformit&eacute; &nbsp;des buses &nbsp;des pulv&eacute;risateurs&nbsp; apport&eacute;es par les agriculteurs et conseils pour respecter la nouvelle l&eacute;gislation relative aux pulv&eacute;risations par 6 membres de l&rsquo;&eacute;quipe de Protect&rsquo;eau.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td style="width:68.4pt">
			<p style="text-align:center"><span style="color:#ff0000">Jeudi 14/03/2019</span></p>
			</td>
			<td style="width:69.35pt">
			<p style="text-align:center"><span style="color:#ff0000">Kain<br />
			9h30 et/ou 13h20</span></p>
			</td>
			<td style="width:379.05pt">
			<p><strong><span style="color:red">** deux demi-journ&eacute;e d&#39;information</span></strong> :</p>

			<p>&nbsp;9h30 l&eacute;gumes industriels&nbsp; ;</p>

			<p>&nbsp;13h20 c&eacute;r&eacute;ales et pommes de terre. A la ferme du reposoir (CARAH)</p>

			<p><strong><em>Plus d&#39;info &agrave; venir</em></strong></p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>
', 3, NULL, 64, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (156, 'A l''agenda', 'left', 300, 225, '<p><u><span style="color:red">Conf&eacute;rences comptant dans le cadre de formations continues pour la phytolicence (**) </span>dans le cadre des c&eacute;r&eacute;ales</u> :</p>

<p><strong>Prochain rendez-vous le 27/02 &agrave; Gembloux : </strong><a href="http://www.cadcoasbl.be/p10_agenda/1902LB.pdf">Programme.pdf</a></p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td style="width:68.4pt">
			<p style="text-align:center"><span style="color:#ff0000">Mercredi 27/02/2019</span></p>
			</td>
			<td style="width:69.35pt">
			<p style="text-align:center"><span style="color:#ff0000">Gembloux<br />
			9h ou 14h00</span></p>
			</td>
			<td style="width:379.05pt">
			<p><span style="color:red">** Pr&eacute;sentation du <strong>Livre Blanc c&eacute;r&eacute;ales</strong> en l&#39;Espace Senghor (CePiCOP)</span>.</p>

			<p><strong>Deux</strong> <strong>s&eacute;ances identiques </strong>matin et apr&egrave;s-midi.</p>
			</td>
		</tr>
	</tbody>
</table>

<p>La conf&eacute;rence vaut comme formation continue que doivent suivre tous les d&eacute;tenteurs d&rsquo;une phytolicence (P1, P2 et P3). Inscription de 8h30 &agrave; 9h00 et de 13h30 &agrave; 14h00. N&rsquo;oubliez pas votre carte d&rsquo;identit&eacute;.</p>

<p>De 8h30 &agrave; 9h00, durant la pause et de 12h05 &agrave; 12h45 : v&eacute;rification de la conformit&eacute; &nbsp;des buses &nbsp;des pulv&eacute;risateurs&nbsp; apport&eacute;es par les agriculteurs et conseils pour respecter la nouvelle l&eacute;gislation relative aux pulv&eacute;risations par 6 membres de l&rsquo;&eacute;quipe de Protect&rsquo;eau.</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td style="width:68.4pt">
			<p style="text-align:center"><span style="color:#ff0000">Jeudi 14/03/2019</span></p>
			</td>
			<td style="width:69.35pt">
			<p style="text-align:center"><span style="color:#ff0000">Kain<br />
			9h30 et/ou 13h20</span></p>
			</td>
			<td style="width:379.05pt">
			<p><strong><span style="color:red">** deux demi-journ&eacute;e d&#39;information</span></strong> :</p>

			<p>9h30 l&eacute;gumes industriels</p>

			<p>13h20 c&eacute;r&eacute;ales et pommes de terre. A la ferme du reposoir (CARAH)</p>

			<p><a href="http://www.cadcoasbl.be/p10_agenda/190314%20CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>
', 3, NULL, 71, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (151, 'Conditions propices à la lutte contre les adventices ?', 'left', 300, 225, '<p><span style="color:black">Les belles journ&eacute;es que nous connaissons actuellement semblent propices au d&eacute;sherbage.</span></p>

<p><span style="color:black">Il convient toutefois de se m&eacute;fier des gel&eacute;es nocturnes et des trop grandes amplitudes de temp&eacute;ratures qui pourraient affecter la s&eacute;lectivit&eacute; des herbicides.</span></p>

<p><span style="color:black">Pour &ecirc;tre efficace, le passage d&rsquo;outils m&eacute;caniques est &agrave; r&eacute;aliser sur des terres suffisamment ressuy&eacute;es et devrait &ecirc;tre suivi d&rsquo;au moins deux jours sans pluie. </span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> F. Henriet</p>
', 1, NULL, 71, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (149, 'Situation au 25 février 2019', 'left', 300, 225, '<p style="text-align:justify">Depuis plus d&rsquo;une semaine, le printemps qui arrive avant l&rsquo;heure avec des temp&eacute;ratures tr&egrave;s douces, un ensoleillement extraordinaire et un temps sec, r&eacute;veille les insectes de printemps en colza d&rsquo;hiver.</p>

<p style="text-align:justify">En effet, les premiers charan&ccedil;ons de la tige et m&eacute;lig&egrave;thes ont d&eacute;j&agrave; &eacute;t&eacute; pi&eacute;g&eacute;s dans les bassins jaunes install&eacute;s dans plusieurs champs du r&eacute;seau d&rsquo;observation en colza. La d&eacute;termination des charan&ccedil;ons indique qu&rsquo;il s&rsquo;agit essentiellement de charan&ccedil;ons de la tige du chou.</p>

<p style="text-align:justify">Les premiers charan&ccedil;ons de la tige ont &eacute;t&eacute; pi&eacute;g&eacute;s &agrave; Anth&eacute;e, Arbre, Ath, Bois-de-Villers, Den&eacute;e, Emines, Foy, Gembloux, Isnes, Jamiolle, Morialm&eacute;, Sauveni&egrave;re, Sombreffe, Stave, Stavesoul et Tilly. Le nombre de captures de charan&ccedil;ons varie entre 6 et 315 individus par bassin.</p>

<p style="text-align:justify">Les premiers m&eacute;lig&egrave;thes ont &eacute;t&eacute; pi&eacute;g&eacute;s en plus faible nombre (de 2 &agrave; 103 individus) dans les m&ecirc;mes champs sauf &agrave; Isnes et Stavesoul o&ugrave; ils &eacute;taient absents.</p>

<p style="text-align:justify">La cuvette jaune remont&eacute;e sur un support est un moyen efficace pour attirer les charan&ccedil;ons de la tige ; leur observation dans la v&eacute;g&eacute;tation est difficile car ils passent souvent inaper&ccedil;us. Les pi&egrave;ges enterr&eacute;s &agrave; l&rsquo;automne pour capturer les altises, deviennent inefficaces au printemps.</p>

<p style="text-align:justify">Le charan&ccedil;on de la tige du colza est plus gros que le charan&ccedil;on de la tige du chou ; il pr&eacute;sente des pattes noires tandis que celui du chou a les pattes rousses.</p>

<p style="text-align:justify">Les pontes d&eacute;butent de 1 &agrave; 3 semaines apr&egrave;s la reprise d&rsquo;activit&eacute; des femelles et les vols d&rsquo;envahissement des cultures. Elles sont influenc&eacute;es par les temp&eacute;ratures et par une culture en d&eacute;but de montaison, lorsque les femelles disposent de tiges en d&eacute;but d&rsquo;&eacute;longation. Dans les prochains jours, les temp&eacute;ratures vont nettement diminuer et ralentiront sans doute l&rsquo;activit&eacute; des insectes.</p>

<p style="text-align:justify">Le colza qui est actuellement au stade de reprise de v&eacute;g&eacute;tation devra &ecirc;tre surveill&eacute; de pr&egrave;s pour les charan&ccedil;ons de la tige. En ce qui concerne les m&eacute;lig&egrave;thes qui arrivent tr&egrave;s t&ocirc;t, ils restent inoffensifs en l&rsquo;absence de boutons floraux visibles dans le colza.</p>

<p style="text-align:justify">La surveillance du colza et des insectes ravageurs sera assur&eacute;e au cours des prochaines semaines et un point de la situation sera r&eacute;guli&egrave;rement communiqu&eacute;.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 67, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (187, 'Point phytotechnique en escourgeon et en froment', 'left', 300, 225, '<p><strong><u><span style="color:black">EN ESCOURGEON</span></u></strong></p>

<p>Les escourgeons sem&eacute;s d&eacute;but octobre sont au stade redressement. La deuxi&egrave;me application au stade redressement peut donc &ecirc;tre appliqu&eacute;e pour les escourgeons sem&eacute;s d&eacute;but octobre. Si le sch&eacute;ma d&rsquo;application est en deux passages, il convient alors d&rsquo;appliquer la premi&egrave;re fraction.</p>

<p>&nbsp;</p>

<p><strong><u><span style="color:black">EN FROMENT</span></u></strong></p>

<p><u><span style="color:black">Fumure, r&eacute;gulateur et d&eacute;sherbage</span></u></p>

<p>Les stades de d&eacute;veloppement des froments continuent &agrave; &eacute;voluer normalement :</p>

<ul>
	<li>Pour les semis de mi-octobre de plein &agrave; fin tallage</li>
	<li>Pour les semis de mi-novembre de 2 - 3 talles</li>
	<li>Pour les semis tardifs de mi-d&eacute;cembre de 3 &agrave; 5 feuilles (13-20)</li>
</ul>

<p>Pour les semis tardifs, la premi&egrave;re application devra &ecirc;tre envisag&eacute;e d&rsquo;ici la semaine prochaine. Lors de la semaine prochaine, la seconde application devra &eacute;galement &ecirc;tre r&eacute;alis&eacute;e pour les semis pr&eacute;coces. Celle-ci devra &ecirc;tre appliqu&eacute;e lorsque la culture atteindra le stade redressement. De plus, le traitement r&eacute;gulateur pourra &eacute;galement &ecirc;tre appliqu&eacute; &agrave; ce stade.</p>

<p>Ensuite, si le d&eacute;sherbage des cultures de froment n&rsquo;a pas d&eacute;j&agrave; &eacute;t&eacute; effectu&eacute;, il peut alors &ecirc;tre r&eacute;alis&eacute; en fonction des conditions climatiques (gel&eacute;es nocturnes !).</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard, R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 02 avril.</span></p>
', 1, NULL, 82, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (150, 'Jaunisse nanisante : constat partiel de sortie d’hiver ', 'left', 300, 225, '<table cellspacing="0" class="MsoTableLightShadingAccent1" style="border-collapse:collapse; border:none; width:529.9pt">
	<tbody>
		<tr>
			<td style="width:90.45pt">
			<p><span style="color:#6b7c71">Date(s) </span></p>
			</td>
			<td style="width:439.45pt">
			<p><span style="color:#726056">Observations du 14 au 22 f&eacute;vrier 2019</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><span style="color:black">Les observations des deux derni&egrave;res semaines dans les c&eacute;r&eacute;ales montrent que quasi tous les champs de c&eacute;r&eacute;ales sont actuellement d&eacute;barrass&eacute;s des pucerons. Quelques exceptions peuvent n&eacute;anmoins &ecirc;tre trouv&eacute;es dans l&rsquo;ouest du pays, o&ugrave; l&rsquo;on a compt&eacute; jusqu&rsquo;&agrave; 4 % de plantes colonis&eacute;es dans un champ d&rsquo;escourgeon. M&ecirc;me dans de tels champs, le risque de d&eacute;g&acirc;t est faible, du fait de la tr&egrave;s faible proportion de pucerons porteurs du virus. M&ecirc;me si l&rsquo;on ne peut pas exclure le d&eacute;veloppement de quelques taches de jaunisse nanisante, cette virose ne devrait pas porter pr&eacute;judice au rendement, et les traitements insecticides qui seraient appliqu&eacute;s n&rsquo;auraient qu&rsquo;un effet cosm&eacute;tique.</span></p>

<p style="text-align:justify"><span style="color:black">Il reste &eacute;videmment conseill&eacute; d&rsquo;aller soi-m&ecirc;me v&eacute;rifier le niveau d&rsquo;infestation de ses parcelles et de signaler au CADCO d&rsquo;&eacute;ventuelles observations discordantes. La surveillance va se poursuivre et se pr&eacute;ciser au cours des prochaines semaines.</span></p>

<p style="text-align:justify"><span style="color:black">Les </span><strong><u><span style="color:#292934">vari&eacute;t&eacute;s tol&eacute;rantes</span></u></strong> <span style="color:black">au virus de la jaunisse nanisante de l&rsquo;orge (</span><strong><span style="color:#292934">RAFAELA</span></strong><span style="color:#292934">, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) </span><span style="color:black">ne justifient JAMAIS de traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons.</span></p>

<p><span style="color:black">Ce constat sera prochainement compl&eacute;t&eacute; pour au plus tard l&rsquo;avertissement du 05 mars. </span></p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, G. Jacquemin et M. De Proft</p>
', 1, NULL, 71, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (153, 'Herbicides céréales dont la date de fin d’utilisation autorisée est connue', 'left', 300, 225, '<p><strong><u>en 2019</u></strong> : ALISTER (30/09) ; ATLANTIS WG (30/09) ; CALIBAN DUO (30/09) ; CALIBAN TOP (30/09) ; COSSACK (30/09) ; FLUROX 180 EC (31/01) ; PACIFICA (30/09) ; VERIGAL D (01/03) ;</p>

<p><strong><u>en 2020</u></strong> : STOMP 400 SC (29/02) ; TOUCHDOWN QUATTRO (15/06) ;</p>

<p style="text-align:justify">Une fois cette date d&eacute;pass&eacute;e, le produit devient un produit phyto non utilisable (PPNU).</p>
', 2, NULL, 71, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (152, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les herbicides, fongicides &hellip; <strong> </strong></p>

<p style="text-align:justify"><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits au lien suivant : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>
', 2, NULL, 71, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (159, 'Orge de printemps', 'left', 300, 225, '<p style="text-align:justify">L&rsquo;<strong>orge de printemps </strong>peut &ecirc;tre sem&eacute;e &agrave; condition que la terre soit bien ressuy&eacute;e. Id&eacute;alement avant le 15 mars si les conditions de sol sont correctes. Apr&egrave;s le 15-20 mars, la r&eacute;ussite est plus al&eacute;atoire pour une s&eacute;rie de param&egrave;tres.</p>

<p style="text-align:justify">Plus le semis est tardif, plus le travail du sol devra &ecirc;tre affin&eacute; pour favoriser une lev&eacute;e rapide. Le semis h&acirc;tif a comme inconv&eacute;nient, d&rsquo;&ecirc;tre potentiellement plus impact&eacute;e par les oiseaux vu la lev&eacute;e plus lente. Tr&egrave;s pr&eacute;coce, il peut aussi &ecirc;tre plus colonis&eacute; de vulpins.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u>Fumure</u></strong></p>

<p style="text-align:justify">Semis de f&eacute;vrier : Ne pas apporter de fumure avant la lev&eacute;e.</p>

<p style="text-align:justify">Appliquer la fumure en deux applications (lev&eacute;e, redressement) permet de mieux maitriser celle-ci et de l&rsquo;adapter en fonction du d&eacute;veloppement de la culture.</p>

<p style="text-align:justify">En orge brassicole, si les reliquats azot&eacute;s sont de 60 unit&eacute;s d&rsquo;azote sur les 90 premiers centim&egrave;tres de sol, une fumure de 90 unit&eacute;s d&rsquo;azote peut &ecirc;tre appliqu&eacute;e &agrave; la lev&eacute;e. Par la suite, si la culture montre une carence, une correction de 20 &agrave; 40 unit&eacute;s d&rsquo;azote peut&ndash;&ecirc;tre apport&eacute;e au redressement.</p>

<p style="text-align:justify"><strong>Attention &agrave; ne pas sur-fertiliser</strong>, notamment, dans le cas de pr&eacute;c&eacute;dents comme pomme de terre, &eacute;pinard, sols riches en humus, o&ugrave; les reliquats sont importants. Dans ces situations, apporter 90 unit&eacute;s d&rsquo;azote &agrave; la lev&eacute;e risque de conduire &agrave; un d&eacute;passement de la teneur en prot&eacute;ines et d&rsquo;avoir une production importante de talles qui engendrerait la production de tardillons avant la r&eacute;colte, ce qui poserait des probl&egrave;mes de qualit&eacute;.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u>Gestion des adventices</u></strong></p>

<p style="text-align:justify">Il n&rsquo;est en g&eacute;n&eacute;ral pas utile d&rsquo;utiliser d&rsquo;anti-gramin&eacute;. Vu la date de semis, les vulpins sont rarement probl&eacute;matiques.</p>

<p style="text-align:justify">Un traitement en pr&eacute;-semis au triallate (AVADEX 480) ne se justifie que dans le cas de risque d&rsquo;envahissement par de la folle avoine et/ou des jouets du vent.</p>

<p style="text-align:justify">Dans la gestion des dicotyl&eacute;es, la gamme des produits est large :</p>

<ul>
	<li style="text-align:justify">En pr&eacute;-&eacute;mergence : <a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf">HerbPreC.pdf</a></li>
	<li style="text-align:justify">De la lev&eacute;e au d&eacute;but tallage : <a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf">HerbLTalC.pdf</a></li>
	<li style="text-align:justify">Du d&eacute;but tallage au gonflement de la gaine : <a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFC.pdf">HerbTalDFC.pdf</a></li>
</ul>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> R. Meurs, O. Mahieu &nbsp;et F. Henriet</p>

<p style="text-align:right">&nbsp;</p>
', 1, NULL, 74, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (251, 'Emergence de cécidomyie orange', 'left', 300, 225, '<p>Hier soir, comme pr&eacute;vu par le mod&egrave;le d&eacute;velopp&eacute; au CRA-W, la c&eacute;cidomyie orange du bl&eacute; a &eacute;merg&eacute; assez fortement dans la r&eacute;gion de Gembloux. Ces &eacute;mergences se sont produites dans les champs de betteraves et de ma&iuml;s, o&ugrave; le sol se r&eacute;chauffe un peu plus t&ocirc;t qu&rsquo;en c&eacute;r&eacute;ales. Des attaques sont donc imminentes sur le bl&eacute; et le triticale&nbsp;; dans une moindre mesure l&rsquo;&eacute;peautre.</p>

<p>Cette vague devrait se prolonger durant la semaine &agrave; venir. Dans de nombreux cas, les froments sont au stade &eacute;piaison et donc en pleine p&eacute;riode de sensibilit&eacute;. Les temp&eacute;ratures annonc&eacute;es pour les prochaines soir&eacute;es seront douces et le vent sera faible. Cela convient aux c&eacute;cidomyies.</p>

<p>La chaleur et la s&eacute;cheresse de la fin juin &ndash; d&eacute;but juillet, ont heureusement tu&eacute; beaucoup de larves dans les &eacute;pis, avant leur retour au sol. Ce retour au sol n&rsquo;a r&eacute;ussi que dans un rayon d&rsquo;une dizaine de kilom&egrave;tres autour de Gembloux, o&ugrave; les pluies ont &eacute;t&eacute; plus cons&eacute;quentes. Il est probable peu de champ seront attaqu&eacute;s. Toutefois, certaines attaques pourraient &ecirc;tre fortes.</p>

<p>Il est donc important d&rsquo;aller au champ vers 21h30-22h.&nbsp; Une observation simple consiste &agrave; fr&ocirc;ler les &eacute;pis avec une baguette tenue horizontalement. Si ce geste provoque l&rsquo;envol d&rsquo;au moins 30 insectes par m&sup2;, un traitement insecticide est recommand&eacute;, mais &agrave; condition que&nbsp;:</p>

<ul>
	<li>La vari&eacute;t&eacute; soit sensible (voir <a href="http://www.cadcoasbl.be/p09_biblio/art0017/variétésRésistantes19.pdf">liste des vari&eacute;t&eacute;s r&eacute;sistantes</a>)</li>
	<li>Le stade de la culture soit compris entre <strong>l&rsquo;&eacute;clatement des gaines et la floraison </strong>(sortie des &eacute;tamines).</li>
</ul>

<p style="text-align:justify">Les observations &quot;ravageurs&quot; continuent la semaine prochaine, prochain avertissement mardi 04 juin.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>

<p><span style="color:black"><u>Insecticides autoris&eacute;s contre c&eacute;cidomyies orange</u> : </span></p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>21 mai 2019</td>
			<td>C&eacute;cidomyies</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf">CecidSA.pdf</a></td>
		</tr>
	</tbody>
</table>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 04 juin.</span></p>
', 1, NULL, 98, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (171, 'Nouvelle saison : rappels et premiers constats en escourgeon', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Comme pr&eacute;sent&eacute; dans l&rsquo;&eacute;dition du <strong><em>Livre Blanc des C&eacute;r&eacute;ales</em></strong> de f&eacute;vrier dernier, la protection des cultures doit &ecirc;tre raisonn&eacute;e en fonction de la pression des principales maladies, des conditions m&eacute;t&eacute;os annonc&eacute;es et de la vari&eacute;t&eacute; emblav&eacute;e. Les moyens de lutte chimique sont de plus en plus restreints (&eacute;tat des lieux des triazoles et du produit BRAVO qui est limit&eacute; &agrave; une application par saison aux stades 39-59, page 8 &agrave; consulter </span><a href="http://www.livre-blanc-cereales.be/wp-content/uploads/2019/03/LBFEV19-5-lutte-contre-les-maladies.pdf">ici</a><span style="color:black">) et il est essentiel de combiner toutes les strat&eacute;gies disponibles pour arriver aux rendements souhait&eacute;s. Evaluer les pathog&egrave;nes pr&eacute;sents sur sa parcelle permet d&rsquo;&eacute;valuer si l&rsquo;infection est pr&eacute;judiciable ou non. Les avis du CADCO vous renseignent de la pression en maladies observ&eacute;e dans un r&eacute;seau de parcelles distribu&eacute;es en Wallonie. Ces avis sont l&agrave; pour vous guider dans la strat&eacute;gie &agrave; appliquer qui sera toujours optimis&eacute;e sur base des conditions phytotechniques et vos propres observations de l&rsquo;&eacute;tat sanitaire de vos parcelles. </span></p>

<p style="text-align:justify"><strong><span style="color:black">En froment, </span></strong><span style="color:black">les maladies comme le pi&eacute;tin-verse, la septoriose et l&rsquo;o&iuml;dium sont commun&eacute;ment d&eacute;tect&eacute;es dans les champs et c&rsquo;est leur s&eacute;v&eacute;rit&eacute; qui va indiquer les risques encourus par la culture. La septoriose quoique plus discr&egrave;te ces derni&egrave;res ann&eacute;es, peut entrainer des d&eacute;g&acirc;ts et est &agrave; surveiller sur les vari&eacute;t&eacute;s sensibles &agrave; partir du stade 32. Sur les vari&eacute;t&eacute;s tol&eacute;rantes, il n&rsquo;est pas n&eacute;cessaire d&rsquo;envisager un traitement contre cette maladie avant le stade derni&egrave;re feuille. Les rouilles doivent &ecirc;tre sous surveillance d&egrave;s les premi&egrave;res d&eacute;tections notamment sur les vari&eacute;t&eacute;s sensibles et un traitement sp&eacute;cifique est parfois n&eacute;cessaire au stade 1Ier n&oelig;ud pour ces vari&eacute;t&eacute;s sensibles. </span></p>

<p style="text-align:justify"><u><span style="color:black">Pour d&eacute;cider de la strat&eacute;gie de protection fongique </span></u><span style="color:black">: il faut donc &eacute;valuer l&rsquo;&eacute;tat sanitaire de sa parcelle en tenant compte de la vari&eacute;t&eacute;, &eacute;valuer les pathog&egrave;nes par ordre d&rsquo;importance et le positionnement du/des traitement(s) sera fonction de cette analyse. Une ou deux applications fongicides suffisent g&eacute;n&eacute;ralement pour contr&ocirc;ler l&rsquo;ensemble des maladies. Les ann&eacute;es ant&eacute;rieures ont montr&eacute; que les traitements h&acirc;tifs avant le stade 31 (premier n&oelig;ud) ne sont pas utiles. L&rsquo;int&eacute;r&ecirc;t ou non d&rsquo;un traitement en montaison n&rsquo;est &agrave; envisager qu&rsquo;&agrave; partir de ce stade (fig.1). En fonction des vari&eacute;t&eacute;s emblav&eacute;es et de leurs profils face aux maladies, des stades cl&eacute;s seront &agrave; surveiller et les diagrammes d&eacute;cisionnels repris dans le Livre Blanc des C&eacute;r&eacute;ales permettront d&rsquo;orienter vos choix &agrave; chaque &eacute;tape de la saison (page 53 &agrave; consulter </span><a href="http://www.livre-blanc-cereales.be/wp-content/uploads/2019/03/LBFEV19-5-lutte-contre-les-maladies.pdf">ici</a><span style="color:black">).</span></p>

<p style="text-align:center"><span style="color:black"><img align="" filer_id="404" height="" original_image="false" src="/filer/canonical/1552390326/404/" thumb_option="" title="" width="" /> </span></p>

<p style="text-align:justify"><strong><span style="color:black">Figure 1</span></strong><span style="color:black">. P&eacute;riode d&rsquo;infection des principales maladies en bl&eacute; (fl&egrave;ches horizontales) et stades de croissance cl&eacute; de la protection fongicide en bl&eacute; (fl&egrave;ches verticales). Source : Equipe <em>Lutte int&eacute;gr&eacute;e</em>, Livre Blanc des C&eacute;r&eacute;ales, 2018.</span></p>

<p style="text-align:justify"><strong><span style="color:black">En escourgeon</span></strong><span style="color:black">, la protection phytosanitaire vise &agrave; contr&ocirc;ler quatre maladies : la rhynchosporiose, l&rsquo;helminthosporiose, la rouille naine et l&rsquo;o&iuml;dium. </span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u><span style="color:black">Pour d&eacute;cider de la strat&eacute;gie de protection fongique</span></u><span style="color:black"> : trois leviers agronomiques permettent de diminuer la lutte chimique: 1) privil&eacute;gier les vari&eacute;t&eacute;s r&eacute;sistantes, 2) semer &agrave; une densit&eacute; peu &eacute;lev&eacute;e car la lev&eacute;e est souvent rapide et permet au tallage de d&eacute;marrer t&ocirc;t et 3) ne pas intensifier exag&eacute;r&eacute;ment la fumure azot&eacute;e.</span></p>

<p style="text-align:justify"><span style="color:black">Un traitement unique au stade derni&egrave;re feuille est en g&eacute;n&eacute;ral conseill&eacute;. Un premier traitement (dose fractionn&eacute;e) est parfois recommand&eacute; en cas de forte pression sur les vari&eacute;t&eacute;s sensibles.&nbsp; Les conditions ult&eacute;rieures de la saison d&eacute;termineront de la pression phytosanitaire et des strat&eacute;gies &agrave; mettre en &oelig;uvre.</span></p>

<p style="text-align:justify"><u><span style="color:black">Premiers constats de la saison </span></u><span style="color:black">: Comme mentionn&eacute; dans les deux derniers avis, les champs semblent d&eacute;barrass&eacute;s des populations de pucerons. Les d&eacute;g&acirc;ts de jaunisse sont faibles vu la tr&egrave;s maigre proportion de pucerons porteurs du virus. La mosa&iuml;que de l&rsquo;escourgeon (maladie virale transmise par un microorganisme du sol) est parfois observ&eacute;e et le seul moyen de lutte contre celle-ci reste les vari&eacute;t&eacute;s r&eacute;sistantes.</span></p>

<p style="text-align:center"><span style="color:black"><img align="" filer_id="405" height="" original_image="false" src="/filer/canonical/1552390685/405/" thumb_option="" title="" width="" /> </span></p>

<p style="text-align:justify"><strong><span style="color:black">Figure 2</span></strong><span style="color:black">. P&eacute;riode d&rsquo;infection des principales maladies en escourgeon (fl&egrave;ches horizontales) et stades de croissance cl&eacute; de la protection fongicide (fl&egrave;ches verticales). Source : Equipe <em>Lutte int&eacute;gr&eacute;e</em>, Livre Blanc des C&eacute;r&eacute;ales, 2018</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p>&nbsp;</p>
', 1, NULL, 75, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (170, 'Reliquats de sortie hiver', 'left', 300, 225, '<p>Ci-apr&egrave;s le lien vers les r&eacute;sultats de profils azot&eacute;s de Protecteau : <a href="https://protecteau.be/fr/nitrate/agriculteurs/fertilisation-raisonnee/reliquat-azote" target="_blank">https://protecteau.be/fr/nitrate/agriculteurs/fertilisation-raisonnee/reliquat-azote</a></p>

<p>Voir au milieu de page les liens nomm&eacute;s : <a href="https://protecteau.be/fr/nitrate/agriculteurs/fertilisation-raisonnee/reliquat-azote">RSH colza</a>,&hellip; d&rsquo;autres r&eacute;sultats (c&eacute;r&eacute;ales,&hellip;) y seront &eacute;galement pr&eacute;sent&eacute;s d&rsquo;ici fin de journ&eacute;e.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Protect&rsquo;eau</span></u></em></strong></p>
', 2, NULL, 75, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (160, 'Jaunisse nanisante : constat de sortie d’hiver ', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Les derni&egrave;res observations confirment la tendance : quasi tous les champs de c&eacute;r&eacute;ales sont actuellement d&eacute;barrass&eacute;s des pucerons. Quelques exceptions peuvent n&eacute;anmoins &ecirc;tre trouv&eacute;es dans l&rsquo;ouest du pays, o&ugrave; l&rsquo;on a compt&eacute; jusqu&rsquo;&agrave; 4 % de plantes colonis&eacute;es dans un champ d&rsquo;escourgeon. M&ecirc;me dans de tels champs, le risque de d&eacute;g&acirc;t est faible, du fait de la tr&egrave;s faible proportion de pucerons porteurs du virus. M&ecirc;me si l&rsquo;on ne peut pas exclure le d&eacute;veloppement de quelques taches de jaunisse nanisante, cette virose ne devrait pas porter pr&eacute;judice au rendement, et les traitements insecticides qui seraient appliqu&eacute;s n&rsquo;auraient qu&rsquo;un effet cosm&eacute;tique.</span></p>

<p style="text-align:justify"><span style="color:black">Il reste &eacute;videmment conseill&eacute; d&rsquo;aller soi-m&ecirc;me v&eacute;rifier le niveau d&rsquo;infestation de ses parcelles et de signaler au CADCO d&rsquo;&eacute;ventuelles observations discordantes. La surveillance va se poursuivre et se pr&eacute;ciser au cours des prochaines semaines.</span></p>

<p style="text-align:justify"><span style="color:black">Les </span><strong><u><span style="color:#292934">vari&eacute;t&eacute;s tol&eacute;rantes</span></u></strong> <span style="color:black">au virus de la jaunisse nanisante de l&rsquo;orge (</span><strong><span style="color:#292934">RAFAELA</span></strong><span style="color:#292934">, <strong>DOMINO, NOVIRA, LG ZEBRA et HIRONDELLA</strong>) </span><span style="color:black">ne justifient JAMAIS de traitement insecticide et ce, quel que soit le niveau d&rsquo;infestation des pucerons.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; ravageurs &raquo;, G. Jacquemin et M. De Proft</p>
', 1, NULL, 74, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (178, 'Points phytolicence', 'left', 300, 225, '<p style="text-align:justify"><strong><u><span style="color:black">Comment se connecter &agrave; son compte ligne</span></u></strong><strong><span style="color:black"> ?</span></strong></p>

<p style="text-align:justify"><span style="color:black">Suite &agrave; de nombreuses demandes, vous trouverez ci-apr&egrave;s la d&eacute;marche &agrave; suivre pour consulter votre compte phytolicence : </span><a href="https://www.pwrp.be/compte-en-ligne">ici</a></p>

<p><span style="color:black">Pour ce faire vous avez besoin de </span>votre lecteur de carte d&rsquo;identit&eacute;, de votre carte d&rsquo;identit&eacute; et du code PIN de votre carte d&rsquo;identit&eacute;</p>

<p><strong><u>Nombre de formations &agrave; suivre par p&eacute;riode et par type de phytolicence</u></strong><strong> :</strong></p>

<p>2 modules pour la NP / 3 modules pour la P1 / 4 modules pour la P2 &nbsp;/ 6 modules pour la P3</p>

<p style="text-align:right"><em><u><span style="color:black">avec la collaboration du PWRP</span></u></em></p>
', 2, NULL, 77, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (165, 'A l''agenda', 'left', 300, 225, '<p><u><span style="color:red">Conf&eacute;rences comptant dans le cadre de formations continues pour la phytolicence (**) </span>dans le cadre des c&eacute;r&eacute;ales</u> :</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td style="width:68.4pt">
			<p style="text-align:center"><span style="color:#ff0000">Jeudi 14/03/2019</span></p>
			</td>
			<td style="width:69.35pt">
			<p style="text-align:center"><span style="color:#ff0000">Kain<br />
			9h30 et/ou 13h20</span></p>
			</td>
			<td style="width:379.05pt">
			<p><strong><span style="color:red">** deux demi-journ&eacute;e d&#39;information</span></strong> :</p>

			<p>9h30 l&eacute;gumes industriels</p>

			<p>13h20 c&eacute;r&eacute;ales et pommes de terre. A la ferme du reposoir (CARAH)</p>

			<p><a href="http://www.cadcoasbl.be/p10_agenda/190314%20CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>
', 3, NULL, 74, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (161, 'Stade phénologique et fertilisation azotée', 'left', 300, 225, '<p style="text-align:justify">En ce d&eacute;but mars, les stades de d&eacute;veloppement des cultures des c&eacute;r&eacute;ales sont normaux et d&eacute;pendent bien s&ucirc;r des dates de semis : les escourgeons sem&eacute;s fin septembre d&eacute;but octobre sont au stade fin tallage, les froments et &eacute;peautres de mi-octobre au stade plein tallage, les semis de mi-novembre sont au stade d&eacute;but tallage et les semis tardifs de mi-d&eacute;cembre au stade 2 feuilles. Compte tenu des bonnes conditions automnales et l&rsquo;absence jusqu&rsquo;&agrave; pr&eacute;sent de conditions hivernales stressantes, la plupart des emblavements sont r&eacute;guliers et en bon &eacute;tat.</p>

<p style="text-align:justify">Les reliquats d&rsquo;azote r&eacute;alis&eacute;s dans 179 profils de sol indiquent en moyenne en froment une richesse &eacute;lev&eacute;e &eacute;gale &agrave; quasi le double de la normale (85kgN/ha&nbsp; sur 90cm de profondeur). Cependant la variabilit&eacute; est aussi exceptionnelle, allant du simple au quadruple. Ceci est d&ucirc; &agrave; la variabilit&eacute; importante des rendements des principales cultures sem&eacute;es au printemps et aux diff&eacute;rences de potentiel de min&eacute;ralisation en fin d&rsquo;&eacute;t&eacute; et durant l&rsquo;automne&nbsp; en fonction des niveaux de restitution&nbsp; en mati&egrave;re organique&nbsp; entre parcelles et ou exploitations.</p>

<p style="text-align:justify">Les fumures de r&eacute;f&eacute;rences des diff&eacute;rentes c&eacute;r&eacute;ales ont &eacute;t&eacute; r&eacute;duites de 10 unit&eacute;s au deux derni&egrave;res fractions pour tenir compte que les profils sont surtout plus riches en profondeur (60-90cm).</p>

<p style="text-align:justify">Pour le froment, la fumure de r&eacute;f&eacute;rence en 3 fractions est&nbsp; 60-50-65&nbsp; et en deux fractions&nbsp; 90-85</p>

<p style="text-align:justify">Pour l&rsquo;escourgeon, la fumure de r&eacute;f&eacute;rence pour les vari&eacute;t&eacute;s lign&eacute;es 45-55-60 et pour les vari&eacute;t&eacute;s hybrides 25-75-75. Pour l&rsquo;&eacute;peautre, elle est en r&eacute;gion limoneuse de 75-60-0 et en Ardenne de 75-30-0.</p>

<p style="text-align:justify">En raison de la grande variabilit&eacute; des disponibilit&eacute;s en azote du sol, il faudra tenir des correctifs li&eacute;s aux pr&eacute;c&eacute;dents culturaux tr&egrave;s riches (l&eacute;gumineuses et pomme de terre), mais aussi des rendements trop faibles du pr&eacute;c&eacute;dent cultural et de l&rsquo;&eacute;tat de la culture juste avant l&rsquo;application des diff&eacute;rentes fractions. Dans beaucoup de cas, la culture risque d&rsquo;&ecirc;tre trop dense, d&rsquo;avoir un feuillage vert&nbsp; trop fonc&eacute; ou de pr&eacute;senter en sous-&eacute;tage des sympt&ocirc;mes d&rsquo;o&iuml;dium. Si c&rsquo;est le cas, il faudra r&eacute;duire de 10 &agrave; 20 N la dose de la fraction &agrave; apporter. Chaque parcelle aura son propre comportement &agrave; surveiller pour &eacute;viter les sur-fumures synonymes de risques accrus de maladies et de verse et aussi de gaspillages.</p>

<p style="text-align:justify">Vous trouverez dans le Livre Blanc et sur le site internet du Livre Blanc C&eacute;r&eacute;ales, des conseils plus d&eacute;taill&eacute;s.</p>

<p style="text-align:justify">Nous reviendrons aussi au stade redressement (en d&eacute;but avril) pour vous faire part de l&rsquo;&eacute;volution de la situation et de nos recommandations en fonction du suivi effectu&eacute; dans des parcelles de r&eacute;f&eacute;rence.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson et R. Blanchard</p>
', 1, NULL, 74, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (189, 'Reconnaitre les maladies foliaires du froment', 'left', 300, 225, '<p style="text-align:justify"><span style="color:black">Comme propos&eacute; la semaine pass&eacute;e pour l&rsquo;escourgeon, nous vous proposons cette semaine quelques illustrations et caract&eacute;ristiques des principales maladies<strong> </strong>observ&eacute;es en <strong>froment.</strong></span></p>

<p style="text-align:justify"><strong><span style="color:black">Absence de germination &agrave; la lev&eacute;e et taches (pourritures) brunes sur les plantules</span></strong><span style="color:black">. Ces sympt&ocirc;mes peuvent &ecirc;tre dus &agrave; l&rsquo;infection des semences par diverses esp&egrave;ces de champignons appartenant au genre <em>Fusarium </em>et <em>Microdochium, </em>et<em> </em>responsables de fusarioses.<em>&nbsp; </em>La qualit&eacute; des semences joue ici un r&ocirc;le pr&eacute;pond&eacute;rant. </span></p>

<p style="text-align:justify"><strong><span style="color:black">Pr&eacute;sence de taches sur les feuilles: </span></strong><span style="color:black">plusieurs esp&egrave;ces de champignons sont responsables de taches foliaires et peuvent &ecirc;tre diff&eacute;renci&eacute;es par leurs sympt&ocirc;mes. </span></p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">&nbsp;La septoriose, </span></strong>due &agrave; <em><span style="color:black">Zymoseptoria tritici,</span></em><strong> </strong><span style="color:black">provoque des n&eacute;croses brun clair caract&eacute;ris&eacute;es par des points noirs (pycnides) &agrave; l&rsquo;int&eacute;rieur de ces n&eacute;croses (photo 1). La maladie se propage par les &eacute;claboussures de pluie ainsi que par diss&eacute;mination a&eacute;rienne des spores (photo 2). Elle est souvent pr&eacute;sente dans le bas de la culture d&egrave;s l&rsquo;automne ou l&rsquo;hiver mais n&rsquo;est pr&eacute;judiciable que lorsqu&rsquo;elle atteint les derniers &eacute;tages foliaires. Les conditions climatiques pendant la montaison sont donc d&eacute;terminantes du moment d&rsquo;infection de ces &eacute;tages, de l&rsquo;impact potentiel de la maladie et donc de la protection &agrave; envisager. </span></li>
</ul>

<p style="text-align:center"><img align="" filer_id="416" height="" src="/filer/canonical/1553598763/416/" thumb_option="" title="" width="" /></p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">La rouille jaune </span></strong>est caus&eacute;e par<strong> </strong><em>Puccinia striiformis </em>f. sp<em>. tritici </em>et appara&icirc;t dans les champs sous forme de foyers ou plages de plantes jaunes (photo 3). Les feuilles atteintes pr&eacute;sentent des pustules jaunes align&eacute;es le long des nervures (photos 4 et 5). Cette distribution des taches en stries la distingue de la rouille brune. La r&eacute;sistance vari&eacute;tale constitue le moyen de lutte le plus efficace. Des nouvelles races de rouille jaune contournant les r&eacute;sistances vari&eacute;tales peuvent occasionnellement appara&icirc;tre.&nbsp; L&rsquo;observation de toutes les parcelles au printemps &agrave; partir du stade montaison (BBCH30) est recommand&eacute;e. La rouille jaune peut appara&icirc;tre pr&eacute;cocement, d&egrave;s le tallage, en cas d&rsquo;hiver doux. Comme la r&eacute;sistance vari&eacute;tale ne s&rsquo;exprime parfois qu&rsquo;&agrave; partir de la montaison,&nbsp;il ne faut <strong>pas</strong> traiter si quelques pustules sont visibles sur les vari&eacute;t&eacute;s r&eacute;sistantes avant ce stade.&nbsp; Un traitement contre cette maladie n&rsquo;est justifi&eacute; sur vari&eacute;t&eacute;s sensibles au stade &eacute;pi 1cm (BBCH 31) et qu&rsquo;en cas d&rsquo;apparition tr&egrave;s pr&eacute;coce et significative.</li>
</ul>

<p style="text-align:center"><img align="" filer_id="417" height="" src="/filer/canonical/1553598890/417/" thumb_option="" title="" width="" /></p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">La rouille brune, </span></strong><em>Puccinia triticina, </em>provoque &eacute;galement des pustules mais de couleur orange &agrave; brune (photos 6 et 7), dispers&eacute;es sur toute la feuille (photo 8) et la r&eacute;partition des plantes infect&eacute;es dans les champs est plus homog&egrave;ne. Les &eacute;pid&eacute;mies de rouille brune ne d&eacute;butent g&eacute;n&eacute;ralement pas avant le stade derni&egrave;re feuille d&eacute;ploy&eacute;e (BBCH 39) vu que le champignon responsable n&eacute;cessite des temp&eacute;ratures plus &eacute;lev&eacute;es que celles favorisant la rouille jaune.&nbsp; Le choix vari&eacute;tal est &eacute;galement primordial pour cette rouille puisqu&rsquo;il conditionne l&rsquo;intensit&eacute; des &eacute;pid&eacute;mies. L&rsquo;observation du niveau de cette maladie en saison est recommand&eacute;e &agrave; partir du stade 2 n&oelig;uds.</li>
	<li style="text-align:justify"><img align="" filer_id="418" height="" src="/filer/canonical/1553599056/418/" thumb_option="" title="" width="" /></li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">L&#39;o&iuml;dium </span></strong>caus&eacute; par<strong> </strong><em>Blumeria graminis</em><span style="background-color:white"> se distingue par </span>la pr&eacute;sence d&rsquo;un duvet blanc cotonneux qui appara&icirc;t principalement sur la face sup&eacute;rieure des feuilles (photo 9). Au fil du temps, le feutrage peut prendre une teinte brune ou grise et des petites ponctuations noires peuvent appara&icirc;tre (photo 10). Apr&egrave;s une pluie, les traces de l&rsquo;attaque restent visibles sous forme de taches chlorotiques. Cette maladie est moins fr&eacute;quente ces derni&egrave;res ann&eacute;es et peu dommageable notamment si la fumure et la densit&eacute; de culture sont raisonn&eacute;es. Cette maladie n&rsquo;est pr&eacute;occupante que si elle atteint les feuilles sup&eacute;rieures, il ne convient donc pas de traiter si elle reste dans les &eacute;tages inf&eacute;rieurs.</li>
</ul>

<p style="text-align:center"><span style="color:black"><img align="" filer_id="415" height="" src="/filer/canonical/1553598414/415/" thumb_option="" title="" width="" /></span></p>

<p><span style="color:black">&nbsp;<strong>Des compl&eacute;ments d&rsquo;informations sont disponibles sur les sites :</strong></span></p>

<ul style="list-style-type:square">
	<li><a href="https://appi.be/fr/fiches-maladies-ravageurs">https://appi.be/fr/fiches-maladies-ravageurs</a></li>
	<li><a href="http://www.livre-blanc-cereales.be/thematiques/maladies/">http://www.livre-blanc-cereales.be/thematiques/maladies/</a></li>
	<li><a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">http://www.cadcoasbl.be/p09_biblio.html#art0002</a></li>
</ul>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 02 avril.</span></p>
', 1, NULL, 82, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (162, 'Azote : quantités maximales épandables ; calcul du complément minéral', 'left', 300, 225, '<p style="text-align:justify"><strong><u><span style="color:black">Sur terre arable</span></u></strong><span style="color:black"> : La quantit&eacute; moyenne d&#39;azote organique applicable par an et par hectare est de maximum<strong> 115 kg</strong>. Pour chaque parcelle la quantit&eacute; maximale d&#39;azote organique applicable est de <strong>230 kg/ha.</strong></span></p>

<p style="text-align:justify"><span style="color:black">L&#39;apport annuel d&#39;azote total (organique + min&eacute;ral) est de maximum <strong>250 kg/ha</strong>.</span></p>

<p><u><span style="color:black">En zone vuln&eacute;rable</span></u><span style="color:black"> : </span><span style="color:black">La quantit&eacute;</span> <span style="color:black">moyenne</span> <span style="color:black">d&#39;azote organique apport&eacute;e par hectare de l&#39;ensemble de l&#39;exploitation (cultures et prairies) ne peut d&eacute;passer </span><span style="color:black">170 kg/ha</span><span style="color:black">.</span></p>

<p style="text-align:justify">Afin de <strong><u><span style="color:blue"><a href="https://protecteau.be/fr/nitrate/agriculteurs/fertilisation-raisonnee/ferti-culture"><span style="color:blue">calculer vous-m&ecirc;me le compl&eacute;ment azot&eacute; min&eacute;ral de votre culture</span></a></span></u></strong>, Protect&#39;eau a cr&eacute;&eacute; un module de calcul interactif. En quelques clics, vous aurez un conseil personnalis&eacute; de la dose d&#39;azote min&eacute;ral &agrave; apporter &agrave; votre culture. La m&eacute;thode de calcul est le fruit de la collaboration entre Protect&#39;eau et diff&eacute;rents partenaires (GRENeRA, UCL, IRBAB, CIPF, FIWAP, CRA-W et REQUASUD).</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Protect&rsquo;eau</span></u></em></strong></p>
', 2, NULL, 74, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (257, 'Avoine', 'left', 300, 225, '<p style="text-align:justify">Les traitements fongicides en avoine sont rarement rentabilis&eacute;s. Les avoines observ&eacute;es sont saines, sauf cas particulier, &agrave; v&eacute;rifier par une observation de votre terre, il n&#39;est pas conseill&eacute; de mener une intervention fongicide.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>
', 1, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (194, 'Réseau « maladie en escourgeon »', 'left', 300, 225, '<p style="text-align:justify"><img align="" filer_id="419" height="" src="/filer/canonical/1554198729/419/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><strong>R&eacute;sum&eacute;</strong></p>

<p style="text-align:justify">La majorit&eacute; des parcelles sont actuellement au stade 1er n&oelig;ud (31). Certains champs emblav&eacute;s sont encore parfois au stade 30 (&eacute;pi 1cm) contrairement aux trois parcelles pr&egrave;s de Ath qui sont un peu en avance avec une majorit&eacute; des champs au stade 32. L&rsquo;helminthosporiose, la rhynchosporiose et l&rsquo;o&iuml;dium sont observ&eacute;s sur les &eacute;tages foliaires inf&eacute;rieurs dans plusieurs champs mais le niveau d&rsquo;infection est faible et ne n&eacute;cessite pas de traitement. Seule la pr&eacute;sence de la rouille naine sur certaines parcelles peut &ecirc;tre pr&eacute;occupante.</p>

<p style="text-align:justify"><strong>Avancement des cultures</strong></p>

<p style="text-align:justify">Ce lundi 1 avril, la plupart des parcelles du r&eacute;seau sont au stade &laquo; 1ier n&oelig;ud &raquo; (BBCH31), presque un quart des parcelles est d&eacute;j&agrave; au stade &laquo; 2i&egrave;me n&oelig;ud &raquo; (BBCH32) mais quelques parcelles ne sont qu&rsquo;au stade ph&eacute;nologique &laquo; &eacute;pi 1cm &raquo; (BBCH30).</p>

<p style="text-align:justify"><img align="" filer_id="421" height="" src="/filer/canonical/1554199074/421/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><strong>Pression en maladies</strong></p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a><strong> </strong>est observ&eacute;e &agrave; Ath, Mainvaut, Pailhe et Fexhe-Slins soit dans 4 des 13 parcelles du r&eacute;seau CADCO. Dans ces parcelles, elle est visible sur la F-3 &agrave; tr&egrave;s faible pourcentage &agrave; Pailhe et Fexhe-Slins. A Ath et Mainvaut, elle est observ&eacute;e sur 10% des F-1 et 40% des F-2 sur des vari&eacute;t&eacute;s sensibles mais le pourcentage de surface foliaire touch&eacute; reste faible dans tous les cas.</p>

<p style="text-align:justify"><span style="color:black">La </span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rhynchosporiose</a></strong><span style="color:black"> est pr&eacute;sente dans 2 des 13 parcelles, mais la proportion de plantes touch&eacute;es est faible (maximum 3% des F-1 observ&eacute;es avec </span>moins d&rsquo;1% <span style="color:black">de surface touch&eacute;e et reste dans le fond de la v&eacute;g&eacute;tation). Cette maladie n&rsquo;est donc pas pr&eacute;occupante pour l&rsquo;instant.</span></p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">o&iuml;dium</a></strong><span style="color:black"> est observ&eacute; dans 10 des 13 parcelles du r&eacute;seau. Elle est pr&eacute;sente notamment sur 15% des F-2 &agrave; Pailhe, moins de 5% des F-2 &agrave; Lonz&eacute;e et maximum 7% des F-1 &agrave; Mainvaut. Toutefois, cette maladie est peu pr&eacute;occupante &agrave; ce stade.</span></p>

<p style="text-align:justify">La <strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rouille naine</a></strong> est pr&eacute;sente dans toutes les parcelles du r&eacute;seau. Dans le Hainaut, &agrave; Ath et Mainvaut : 2 &agrave; 10% des F-1 et 10 &agrave; 45% des F-2 sont touch&eacute;es sur les vari&eacute;t&eacute;s sensibles. Dans la r&eacute;gion de Li&egrave;ge, cette maladie a &eacute;t&eacute; observ&eacute;e avec 5 &agrave; 30% des F-2 de vari&eacute;t&eacute;s sensibles &agrave; Fexhe-Slins et Pailhe. Cette maladie a &eacute;t&eacute; aussi observ&eacute;e dans les parcelles &agrave; Lonz&eacute;e : 5% des F-1 et 25% &agrave; 40% des F-2.</p>

<p style="text-align:justify"><img align="" filer_id="420" height="" src="/filer/canonical/1554198847/420/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><strong>Recommandations </strong></p>

<p style="text-align:justify">L&rsquo;&eacute;tat sanitaire des parcelles du r&eacute;seau d&rsquo;observation est en g&eacute;n&eacute;ral assez bon. Seule la pression en rouille naine dans certaines parcelles notamment dans le Hainaut pourrait &ecirc;tre pr&eacute;occupante et n&eacute;cessiter un traitement. En effet, rappelons que pour lutter contre les maladies fongiques de l&rsquo;escourgeon, <span style="color:black">un traitement unique au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH 39) est la solution g&eacute;n&eacute;ralement la plus adapt&eacute;e.&nbsp; Pour les parcelles ayant atteint le stade 2i&egrave;me n&oelig;ud (BBCH 32), un traitement peut n&eacute;anmoins &ecirc;tre envisag&eacute; si la vari&eacute;t&eacute; emblav&eacute;e est <strong>fortement</strong> sensible &agrave; une maladie pr&eacute;sente dans le champ au-dessus d&rsquo;un seuil de nuisibilit&eacute;. Dans le cas de la rouille naine, une attention particuli&egrave;re doit notamment &ecirc;tre port&eacute;e sur les vari&eacute;t&eacute;s : KWS Orbit, KWS Tonic, Novira, Quadriga, Rafaela et Verity. Le profil de votre vari&eacute;t&eacute; peut &ecirc;tre consult&eacute; dans l&rsquo;&eacute;dition de septembre 2018 du Livre Blanc des C&eacute;r&eacute;ales (<a href="http://www.livre-blanc-cereales.be/telecharger-les-editions-de-septembre/">page 2/41</a>). Comme mentionn&eacute; dans l&rsquo;&eacute;dition de f&eacute;vrier 2019 (</span><strong><a href="http://www.livre-blanc-cereales.be/wp-content/uploads/2017/02/LBFEV-2019-livre-blanc.pdf">page 5/74</a></strong><span style="color:black">), il est conseill&eacute; de privil&eacute;gier un fongicide &agrave; base de triazole ou de cyprodinil voire une strobilurine en m&eacute;lange avec un triazole pour le traitement de montaison. En pr&eacute;sence faible de maladies et/ou de march&eacute; d&eacute;favorable, on pourrait se contenter d&rsquo;une dose r&eacute;duite de fongicide &agrave; ce stade.</span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 09 avril.</span></p>
', 1, NULL, 85, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (169, 'Le « Livre blanc céréales » du 27 février dernier est en ligne', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez au lien suivant le &laquo; Livre blanc c&eacute;r&eacute;ales &raquo; de f&eacute;vrier 2019 : <a href="http://www.livre-blanc-cereales.be/wp-content/uploads/2019/03/LBFEV19-3-fertilisation.pdf">ici</a></p>

<p style="text-align:justify">En ce qui concerne les tableaux relatifs aux produits phytopharmaceutiques, vous les trouverez au lien suivants (mis &agrave; jour lorsque n&eacute;cessaire tout au long de la saison) : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>
', 2, NULL, 75, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (168, 'Utilisation de semences traitées', 'left', 300, 225, '<p>Lors du semis de semences trait&eacute;es avec un produit phytopharmaceutique, <strong>une zone tampon de 1 m&egrave;tre &agrave; pr&eacute;sent aux eaux de surface </strong>s&#39;applique<strong> </strong>pour prot&eacute;ger les organismes aquatiques.</p>

<p>La mesure s&#39;applique &agrave; tous les types de produits phytopharmaceutiques (insecticide, fongicide, ...) et doit &ecirc;tre mentionn&eacute;e sur l&#39;emballage des semences.</p>
', 2, NULL, 75, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (193, 'Point phytotechnique en froment', 'left', 300, 225, '<p><u><span style="color:black">Fumure et r&eacute;gulateur</span></u></p>

<p>Les semis r&eacute;alis&eacute;s durant le <strong>mois d&rsquo;octobre</strong> sont proches du stade &laquo; <u>&eacute;pis &agrave; 1 cm</u> &raquo;. A ce stade (<u>redressement</u>), il y a lieu de penser &agrave; apporter le second apport d&rsquo;azote (lorsque l&rsquo;on travaille avec un sch&eacute;ma en 3 fractions) ou d&#39;appliquer le premier apport (lorsque l&#39;on travaille selon un sch&eacute;ma 2 fractions).</p>

<p>Le traitement r&eacute;gulateur peut &eacute;galement &ecirc;tre appliquer d&egrave;s le stade redressement et jusqu&rsquo;au stade 2 n&oelig;uds. L&rsquo;efficacit&eacute; de ce traitement est principalement bas&eacute;e sur les conditions d&rsquo;applications. Il faut donc une c&eacute;r&eacute;ale en pleine croissance ainsi que de bonne condition climatique.</p>

<p>Afin de pouvoir d&eacute;terminer vous-m&ecirc;me le stade de vos cultures. Vous trouverez ci-dessous un rapide descriptif pour la <u>d&eacute;termination</u> du <strong>stade redressement ou &eacute;pis &agrave; 1 cm</strong> de vos froments.</p>

<ul>
	<li>La premi&egrave;re observation concerne votre culture de mani&egrave;re globale. Si celle-ci poss&egrave;de un port dress&eacute; alors il faut prendre quelques plants de froments et r&eacute;aliser une coupe &agrave; la base de ces plants.</li>
	<li>Sur un plant, il faut s&eacute;lectionner le maitre brun (c&rsquo;est-&agrave;-dire la talle le plus grande), enlever les quelques feuilles s&eacute;nescentes et couper le maitre brun au niveau du plateau de tallage (&eacute;liminer les racines).</li>
	<li>Ensuite une incision est r&eacute;alis&eacute;e du bas vers le haut de la plante afin de mesurer la distance entre le plateau de tallage et le sommet de l&rsquo;&eacute;pi. Lorsque cette distance est de 1 cm (comme illustr&eacute; sur le photo ci-dessous), on se trouve au stade redressement.</li>
	<li>Cette observation doit &ecirc;tre r&eacute;p&eacute;t&eacute;e au moins 5 fois &agrave; diff&eacute;rents endroits de la parcelle afin d&rsquo;avoir une bonne repr&eacute;sentation de l&rsquo;&eacute;tat de votre culture.</li>
</ul>

<p style="text-align:center"><img align="" filer_id="422" height="" original_image="false" src="/filer/canonical/1554200413/422/" thumb_option="" title="" width="" /></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard, R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 09 avril.</span></p>
', 1, NULL, 85, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (230, 'Réseau « maladie en froment »', 'left', 300, 225, '<p><img align="" filer_id="457" height="" src="/filer/canonical/1557827174/457/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute; </strong></u></p>

<p style="text-align:justify">Un tiers des parcelles du r&eacute;seau d&rsquo;observation sont au stade 2i&egrave;me n&oelig;ud (BBCH32) et deux-tiers au stade derni&egrave;re feuille pointante (BBCH37). D&rsquo;autres ont atteint le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39) et 3 parcelles sont d&eacute;j&agrave; au stade &eacute;piaison. La situation sanitaire est globalement similaire &agrave; la semaine derni&egrave;re. Les rouilles jaunes et brunes sont &agrave; surveiller dans les prochains jours vu les temp&eacute;ratures plus douces annonc&eacute;es cette semaine. Le stade derni&egrave;re feuille &eacute;tal&eacute;e est le moment o&ugrave; la protection compl&egrave;te est recommand&eacute;e si aucun traitement n&rsquo;a &eacute;t&eacute; r&eacute;alis&eacute; auparavant, de mani&egrave;re &agrave; prot&eacute;ger les feuilles sup&eacute;rieures dont d&eacute;pend le rendement.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p><img align="" filer_id="456" height="" src="/filer/canonical/1557826372/456/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La </strong><strong>septoriose</strong> est pr&eacute;sente dans le fond de la v&eacute;g&eacute;tation de la plupart des parcelles (tableau 2). Dans la r&eacute;gion du Hainaut uniquement, elle a &eacute;t&eacute; observ&eacute;e sur 20% des F2 sur vari&eacute;t&eacute;s sensibles et 20% des F3 sur vari&eacute;t&eacute;s tol&eacute;rantes.</p>

<p><img align="" filer_id="458" height="" src="/filer/canonical/1557829222/458/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">Selon le mod&egrave;le de pr&eacute;vision &eacute;pid&eacute;miologique PROCULTURE, la septoriose n&rsquo;a pas &eacute;volu&eacute; de la m&ecirc;me mani&egrave;re dans les diff&eacute;rentes r&eacute;gions de Wallonie. Dans les parcelles encore <u>non trait&eacute;es</u>, cette maladie est en incubation sur les derniers &eacute;tages foliaires des <strong>vari&eacute;t&eacute;s tr&egrave;s sensibles</strong> principalement dans des parcelles du Hainaut. Dans les autres r&eacute;gions, elle reste &agrave; des &eacute;tages inf&eacute;rieurs et atteint maximum la F3 sur vari&eacute;t&eacute;s sensibles.</p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong> est observ&eacute; dans 21 parcelles du r&eacute;seau. Dans certaines parcelles, elle atteint la F2 mais &agrave; des fr&eacute;quences et incidences tr&egrave;s faibles.</p>

<p style="text-align:justify"><strong>La rouille jaune </strong>est visible dans la moiti&eacute; des parcelles du r&eacute;seau. Elle est pr&eacute;sente &agrave; des fr&eacute;quences variant de 5% des F4 sur les vari&eacute;t&eacute;s tol&eacute;rantes &agrave; 45% des F2 dans les cas les plus graves sur les vari&eacute;t&eacute;s sensibles. Des foyers actifs ont &eacute;galement &eacute;t&eacute; signal&eacute;s dans des parcelles &agrave; Wiers, Thuin, Ath, Havinnes ou Gembloux.</p>

<p style="text-align:justify"><u>Les vari&eacute;t&eacute;s suivantes sont &agrave; surveiller vis-&agrave;-vis de la rouille jaune :</u></p>

<p style="text-align:justify">BENCHMARK, NEMO, REFLECTION, RGT REFORM, SAHARA, KWS DORSET et RGT TEXACO.</p>

<p style="text-align:justify"><strong>La rouille brune</strong><span style="color:black"> n&rsquo;a pas encore &eacute;t&eacute; observ&eacute;e dans les parcelles du r&eacute;seau.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u>Recommandations</u> : </strong></p>

<p style="text-align:justify">Selon la date de semis et la r&eacute;gion, des stades ph&eacute;nologiques tr&egrave;s diff&eacute;rents, d&eacute;sormais <strong>entre le stade 32 &agrave; 43, </strong>sont observ&eacute;s cette semaine.</p>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles au stade 32 &agrave; 37 et qui n&rsquo;ont pas &eacute;t&eacute; trait&eacute;es</strong>, un traitement peut &ecirc;tre justifi&eacute; en cas de pr&eacute;sence significative de rouille jaune (foyer actif) ou de septoriose (plus de 20% des F3), sur vari&eacute;t&eacute;s sensibles &agrave; l&rsquo;une ou l&rsquo;autre de ces maladies. <strong>Pour ces parcelles et celles qui ont d&eacute;j&agrave; &eacute;t&eacute; trait&eacute;es avant le stade derni&egrave;re feuille</strong>, un second traitement englobant l&rsquo;ensemble des maladies sera r&eacute;alis&eacute; 3 &agrave; 4 semaines apr&egrave;s ce premier traitement. Si aucune maladie n&rsquo;est observ&eacute;e sur la parcelle pour l&rsquo;instant, la protection peut &ecirc;tre report&eacute;e <strong><u>avec vigilance au stade 39</u></strong>.</li>
</ul>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39</strong>, derni&egrave;re feuille compl&egrave;tement &eacute;tal&eacute;e, <strong>et qui n&rsquo;ont pas encore &eacute;t&eacute; trait&eacute;es</strong>, le traitement complet contre les maladies du feuillage peut &ecirc;tre r&eacute;alis&eacute;. Le produit ou le m&eacute;lange sera choisi en fonction des sensibilit&eacute;s propres &agrave; la vari&eacute;t&eacute;.</li>
</ul>

<p><strong><u>Substances actives &agrave; pr&eacute;coniser dans un programme fongicide en froment</u></strong></p>

<p><img align="" filer_id="454" height="" src="/filer/canonical/1557227651/454/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">Pour rappel, les mol&eacute;cules SDHI ne doivent &ecirc;tre utilis&eacute;es qu&rsquo;une fois par saison et l&rsquo;alternance des substances actives est importante pour conserver leur efficacit&eacute;. Ex : Si utilisation du prothioconazole au stade 39, ne plus l&rsquo;utiliser dans un traitement &agrave; l&rsquo;&eacute;piaison et privil&eacute;gier alors le tebuconazole ou metconazole pour prot&eacute;ger l&rsquo;&eacute;pi.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A. Nysten, Ch. Bataille</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 21 mai.</span></p>
', 1, NULL, 95, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (231, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p><u>A l&rsquo;agenda</u> :</p>

<ul>
	<li style="text-align:justify">Voir toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></li>
</ul>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 95, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (196, 'Actualisation des listes des produits autorisés en céréales ', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les r&eacute;gulateurs de croissance, les herbicides, fongicides, les insecticides &hellip; <strong> </strong></p>

<p style="text-align:justify"><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits au lien suivant : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>
', 2, NULL, 85, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (261, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les r&eacute;gulateurs de croissance, les herbicides, fongicides, les insecticides &hellip; <strong> </strong></p>

<p style="text-align:justify"><strong>Cliquez sur le lien &quot;cellule de droite du tableau ci-dessous&quot; pour ouvrir le fichier ad hoc.<em> </em></strong><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits.</p>

<table class="tableauavecbords" style="height:1070px; width:505px">
	<tbody>
		<tr>
			<td>01 avril 2019</td>
			<td><strong>AGENT ANTI-MOUSSANT</strong></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/09AgentAntiMoussant.pdf">Anti-moussant.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES FONGICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongAvoine.pdf">FongA.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froments, Seigles, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0705FongEpFroSeiTri.pdf">FongEpFroSeiTri.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Froments-Autoris&eacute;s contre Rouille jaune</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0704FroRouilleJaune.pdf">FongFRJ.pdf</a></td>
		</tr>
		<tr>
			<td>11 f&eacute;v. 2019</td>
			<td>Froments-Autoris&eacute;s contre Fusariose des &eacute;pis</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1207FroFusariose.pdf">FongFusariose.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongOrges.pdf">FongO.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES HERBICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Version 2019</td>
			<td>Sensibilit&eacute; des adventices</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14sensibiliteadventices.pdf">SensAdv.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Mode d&#39;actions des herbicides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14ModeAction.pdf">ModeActcion.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Sensibilit&eacute; au Chlortoluron</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/2016PJ- sensibChlortoluron.pdf">SensChlo.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Les additifs</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/08HerbicidesAdditifs.pdf">HerbAdd.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>En pr&eacute;-&eacute;mergence de la c&eacute;r&eacute;ale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf">HerbPreC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1510HerbPreEp.pdf">HerbPreEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>De la lev&eacute;e au d&eacute;but tallage</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf">HerbLTalC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalEp.pdf">HerbLTalEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Du d&eacute;but tallage au gonflement de la gaine</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFC.pdf">HerbTalDFC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFEp.pdf">HerbTalDFEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Sur c&eacute;r&eacute;ales &agrave; maturit&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbM.pdf">HerbM.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Toutes cultures</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCultures.pdf">HerbTC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>toutes cultures, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCulturessa.pdf">HerbTCsa.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Terres agricoles en interculture</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCultures.pdf">HerbIC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>interculture, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCulturessa.pdf">HerbICSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES INSECTICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>C&eacute;cidomyies</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf">CecidSA.pdf</a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons-&Eacute;t&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsEteCerealesSA.pdf">InsEteCSA.pdf</a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons vecteurs de JNO</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">InsJNOCSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES MOLLUSCICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707MolluscicideCereales.pdf">MolC.pdf</a></td>
		</tr>
		<tr>
			<td>INFO</td>
			<td>Granul&eacute;s anti-limaces : pas sans risques !</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0809AntiLimacesSPF.pdf">AntiLim.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines et Froment de printemps</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf">RegAFrop.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froment d&#39;hiver, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf">RegFhET.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges et Seigles</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf">RegOS.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>Les retraits d&#39;autorisation &quot;pr&eacute;-d&eacute;termin&eacute;s&quot;</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>Date limite d&#39;utilisation &quot;produits c&eacute;r&eacute;ales&quot;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0709LiquidationStock.pdf">LiquidationStock.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES TRAITEMENTS </strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>20 ao&ucirc;t 2018</td>
			<td>pour c&eacute;r&eacute;ales stock&eacute;es<br />
			pour locaux de stockage vides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TraitStoLocStoCereales.pdf">TStoLocStoC.pdf</a></td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>de semence : C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">TSemC.pdf</a></td>
		</tr>
	</tbody>
</table>
', 2, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (228, 'Point phytotechnique en froment', 'left', 300, 225, '<p><u><span style="color:black">Fumure et r&eacute;gulateur</span></u><span style="color:black"> :&nbsp;</span><strong> </strong></p>

<p style="text-align:justify">La semaine derni&egrave;re, les semis d&rsquo;octobre avaient atteint le stade 37 et cette semaine, les semis r&eacute;alis&eacute;s en novembre sont caract&eacute;ris&eacute;s par ce m&ecirc;me stade (BBCH 37), il y a lieu d&rsquo;appliquer la derni&egrave;re fraction de la fumure azot&eacute;e pour ces semis.</p>

<p style="text-align:justify">A titre de rappel, la dose de r&eacute;f&eacute;rence &agrave; appliquer est de 65 kg N/ha pour une <u>fumure en trois fractions</u> et de 85 kg N/ha pour une <u>fumure en deux fractions</u>.&nbsp; Cette dose est &agrave; moduler selon les conditions culturales de la parcelle, les doses d&eacute;j&agrave; appliqu&eacute;es et l&rsquo;&eacute;tat de la culture d&eacute;finie lors dans le Livre blanc c&eacute;r&eacute;ales 2019. L&rsquo;ensemble des informations afin d&rsquo;adapter cette fumure sont reprises <a href="http://www.cereales.be">ici</a>. Un outil d&rsquo;aide &agrave; la d&eacute;cision au niveau de la fumure est disponible <a href="http://www.livre-blanc-cereales.be/outils/">ici</a></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 21 mai.</span></p>
', 1, NULL, 95, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (226, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p><u>A l&rsquo;agenda</u> :</p>

<ul>
	<li style="text-align:justify">Voir toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">Pour information : du vendredi 13 au dimanche 15 septembre 2019. <strong>Formation micro-malterie </strong>au mus&eacute;e Fran&ccedil;ais de la Brasserie. Nombre de places limit&eacute; &agrave; 20. <strong><a href="http://www.passionbrasserie.com/wp/wp-content/uploads/2019/04/formations-brasseur-2019.pdf">Plus d&#39;info ici</a></strong></p>
', 2, NULL, 94, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (223, 'Point phytotechnique en froment', 'left', 300, 225, '<p><u><span style="color:black">Fumure et r&eacute;gulateur</span></u><span style="color:black"> :&nbsp;</span><strong> </strong></p>

<p style="text-align:justify"><span style="color:black">Les semis r&eacute;alis&eacute;s en octobre sont caract&eacute;ris&eacute;s par une derni&egrave;re feuille pointe (BBCH 37), il y a lieu d&rsquo;appliquer la derni&egrave;re fraction de la fumure azot&eacute;e. </span></p>

<p style="text-align:justify"><span style="color:black">La dose de r&eacute;f&eacute;rence &agrave; appliquer est de 65 kg N/ha pour une fumure en trois fractions et de 85 kg N/ha pour une fumure en deux fractions.&nbsp; Cette dose est &agrave; moduler selon les conditions culturales de la parcelle, les doses d&eacute;j&agrave; appliqu&eacute;es et l&rsquo;&eacute;tat de la culture d&eacute;finie lors dans le Livre blanc c&eacute;r&eacute;ales 2019. L&rsquo;ensemble des informations afin d&rsquo;adapter cette fumure sont reprises </span><a href="http://www.cereales.be">ici</a>. <span style="color:black">De plus, un outil d&rsquo;aide &agrave; la d&eacute;cision au niveau de la fumure est disponible </span><a href="http://www.livre-blanc-cereales.be/outils/">ici</a><span style="color:black">.</span></p>

<p style="text-align:justify"><span style="color:black">L&rsquo;application peut &ecirc;tre r&eacute;alis&eacute;e sous forme solide ou liquide. Si on choisit de l&rsquo;appliquer sous forme liquide, il convient d&rsquo;&ecirc;tre prudent afin d&rsquo;&eacute;viter d&rsquo;occasionner des br&ucirc;lures importantes aux feuilles sup&eacute;rieures de la culture. Les pr&eacute;cautions suivantes doivent &ecirc;tre prises :</span></p>

<p><span style="color:black">- Ne pas appliquer en plein soleil et lorsqu&rsquo;il y a des vents du nord et de l&rsquo;est</span></p>

<p><span style="color:black">- Utiliser des jets adapt&eacute;s qui permettront &agrave; l&rsquo;engrais liquide d&rsquo;atteindre le sol</span></p>

<p><span style="color:black">- Il est souhaitable que la culture re&ccedil;oive des pr&eacute;cipitations m&ecirc;me l&eacute;g&egrave;res dans les jours qui suivent l&rsquo;application</span></p>

<p><span style="color:black">- Ne pas appliquer en m&eacute;lange de l&rsquo;engrais liquide avec des produits phytosanitaires.</span></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 14 mai.</span></p>
', 1, NULL, 94, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (225, 'Réseau « maladie en froment »', 'left', 300, 225, '<p><img align="" filer_id="451" height="" src="/filer/canonical/1557224250/451/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute; </strong></u></p>

<p style="text-align:justify">Plus de 70% des parcelles d&eacute;passent le stade &laquo; 2i&egrave;me n&oelig;ud &raquo; (BBCH32). Les 30% restants, encore au stade &laquo; 1ier n&oelig;ud &raquo; (BBCH31), correspondent aux semis du mois de novembre. Les maladies progressent et une vigilance est requise pour la septoriose qui est visible dans toutes les parcelles du r&eacute;seau d&rsquo;observation. La rouille jaune est visible sur les vari&eacute;t&eacute;s sensibles principalement.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p><img align="" filer_id="453" height="" src="/filer/canonical/1557224404/453/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La </strong><strong>septoriose</strong><span style="color:black"> est pr&eacute;sente dans toutes les parcelles du r&eacute;seau mais l&rsquo;intensit&eacute; de cette maladie varie en fonction des vari&eacute;t&eacute;s et des r&eacute;gions. Sur les parcelles ayant atteint le stade 32, elle est observ&eacute;e sur les F3 &agrave; Ath et Ellignies et sur les F4 &agrave; Lonz&eacute;e, Fexhe et Pailhe. D&rsquo;apr&egrave;s le mod&egrave;le &eacute;pid&eacute;miologique PROCULTURE, des infections primaires sont en incubation sur les &eacute;tages foliaires sup&eacute;rieurs (F4 &agrave; Namur, F3 &agrave; Li&egrave;ge et F2 dans le Hainaut). Il est conseill&eacute; de r&eacute;aliser un traitement &agrave; partir du stade 32 sur vari&eacute;t&eacute;s sensibles pr&eacute;sentant des sympt&ocirc;mes de septoriose sur plus de 20% des F-2 afin de prot&eacute;ger les &eacute;tages foliaires sup&eacute;rieurs. </span></p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong><span style="color:black"> est observ&eacute; dans 18 parcelles du r&eacute;seau. Dans la r&eacute;gion de Li&egrave;ge, il n&rsquo;est observ&eacute; que sur les F-2 du moment &agrave; des fr&eacute;quences de 1 &agrave; 2 plantes sur les 20 observ&eacute;es donc ne d&eacute;passe donc pas la F4 d&eacute;finitive. Dans la r&eacute;gion de Namur, cette maladie est plus timide contrairement &agrave; la r&eacute;gion du Hainaut o&ugrave; quelques touffes blanches sont visibles sur les F3 d&eacute;finitives.</span></p>

<p style="text-align:justify"><strong>La rouille jaune</strong> est observ&eacute;e dans 11 parcelles du r&eacute;seau (= parcelles non trait&eacute;es) avec une distribution et une s&eacute;v&eacute;rit&eacute; variable selon les r&eacute;gions et les vari&eacute;t&eacute;s emblav&eacute;es. Les fr&eacute;quences des feuilles infect&eacute;es, exprim&eacute;es en feuilles d&eacute;finitives (voir sch&eacute;ma ci-dessus), sont les suivantes :</p>

<ul>
	<li style="text-align:justify">25% des F5 (RELFECTION) &agrave; Pailhe,</li>
	<li style="text-align:justify"><span style="color:black">30% des F4 </span>(RELFECTION) <span style="color:black">&agrave; Fexhe, </span></li>
	<li style="text-align:justify"><span style="color:black">50% des F4 </span>(RELFECTION) &agrave; Mortroux,</li>
	<li style="text-align:justify">5% seulement des F5 (RAGNAR) &agrave; Mettet</li>
	<li style="text-align:justify">6% des F3 (NEMO) et 1% des F5 (RAGNAR) &agrave; Ath</li>
	<li style="text-align:justify">20% des F3 (NEMO) et 1% des F4 (RAGNAR) &agrave; Ellignies</li>
</ul>

<p style="text-align:justify"><u>Les vari&eacute;t&eacute;s suivantes sont &agrave; surveiller vis-&agrave;-vis de la rouille jaune :</u></p>

<p style="text-align:justify">BENCHMARK, NEMO, REFLECTION, RGT REFORM, SAHARA, KWS DORSET et RGT TEXACO.</p>

<p style="text-align:justify"><strong>La rouille brune</strong> <span style="color:black">n&rsquo;est pas observ&eacute;e dans les parcelles du r&eacute;seau mais certaines pustules ont d&eacute;j&agrave; &eacute;t&eacute; signal&eacute;es dans la r&eacute;gion de Li&egrave;ge.</span></p>

<p style="text-align:justify"><strong>Recommandations : </strong></p>

<p style="text-align:justify">En fonction de la date de semis et de la r&eacute;gion, les parcelles peuvent avoir atteint des stades tr&egrave;s diff&eacute;rents, qui se situent d&eacute;sormais <strong>entre le stade 31 &agrave; 39</strong>. Il convient de v&eacute;rifier &agrave; quel stade ph&eacute;nologique votre parcelle se situe ainsi que la pression en maladies dans celle-ci afin d&rsquo;y adapter la protection.</p>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles qui n&rsquo;ont pas encore atteint le stade 32, </strong>seule la pr&eacute;sence de foyers actifs de rouille jaune sur une vari&eacute;t&eacute; sensible peut amener &agrave; envisager un traitement.</li>
</ul>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles au stade 32 &agrave; 37 et qui n&rsquo;ont pas &eacute;t&eacute; trait&eacute;es</strong>, un traitement peut &ecirc;tre justifi&eacute; en cas de pr&eacute;sence significative de rouille jaune (foyer actif) ou de septoriose (plus de 20% des F-2), sur vari&eacute;t&eacute;s sensibles &agrave; l&rsquo;une ou l&rsquo;autre de ces maladies. Pour ces parcelles et celles qui ont d&eacute;j&agrave; &eacute;t&eacute; trait&eacute;es avant le stade derni&egrave;re feuille, un second traitement englobant l&rsquo;ensemble des maladies sera r&eacute;alis&eacute; 3 &agrave; 4 semaines apr&egrave;s ce premier traitement. Si aucune maladie n&rsquo;est observ&eacute;e sur la parcelle pour l&rsquo;instant, la protection peut &ecirc;tre report&eacute;e <strong><u>avec vigilance au stade 39</u></strong>.</li>
</ul>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39</strong>, derni&egrave;re feuille compl&egrave;tement &eacute;tal&eacute;e, et qui n&rsquo;ont pas encore &eacute;t&eacute; trait&eacute;es, le traitement complet contre les maladies du feuillage peut &ecirc;tre r&eacute;alis&eacute;. Le produit ou le m&eacute;lange sera choisi en fonction des sensibilit&eacute;s propres &agrave; la vari&eacute;t&eacute;.</li>
</ul>

<p>&nbsp;</p>

<p><strong><u>Substances actives &agrave; pr&eacute;coniser dans un programme fongicide en froment</u></strong></p>

<p><img align="" filer_id="454" height="" original_image="false" src="/filer/canonical/1557227651/454/" thumb_option="" title="" width="" /></p>

<p>&nbsp;
<p style="text-align:justify">Pour rappel, les mol&eacute;cules SDHI ne doivent &ecirc;tre utilis&eacute;es qu&rsquo;une fois par saison et l&rsquo;alternance des substances actives est importante pour conserver leur efficacit&eacute;. Ex : Si utilisation du prothioconazole au stade 39, ne plus l&rsquo;utiliser dans un traitement &agrave; l&rsquo;&eacute;piaison et privil&eacute;gier alors le tebuconazole ou metconazole pour prot&eacute;ger l&rsquo;&eacute;pi.</p>
</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A. Nysten, Ch. Bataille</p>

<p style="text-align:right">&nbsp;</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 14 mai.</span></p>
', 1, NULL, 94, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (263, 'Vos terres sont-elles infestées de vulpin résistant ?', 'left', 300, 225, '<p style="text-align:justify">La r&eacute;sistance des vulpins aux herbicides est un ph&eacute;nom&egrave;ne largement r&eacute;pandu en Europe, notamment en France et au Royaume-Uni et cela n&#39;est pas sans poser certains probl&egrave;mes. Les agriculteurs anglais, par exemple, sont parfois contraints &agrave; mettre en &oelig;uvre des programmes herbicides &agrave; 3 passages incluant en moyenne 6 substances actives diff&eacute;rentes!</p>

<p style="text-align:justify">Actuellement, le ph&eacute;nom&egrave;ne en Belgique semble &ecirc;tre en expansion et n&rsquo;est plus confin&eacute; aux zones g&eacute;ographiques pr&eacute;c&eacute;demment connues comme les Polders, le Tournaisis et la r&eacute;gion de Fosses-la-Ville. Il n&#39;est pas rare de retrouver un peu partout dans nos campagnes des taches de vulpins d&eacute;passant des c&eacute;r&eacute;ales. Cela est-il d&ucirc; &agrave; traitement mal positionn&eacute;, &agrave; des conditions climatiques non optimales au moment du traitement, &agrave; un mauvais recouvrement, ou bien &agrave; la pr&eacute;sence de vulpins r&eacute;sistants?</p>

<p style="text-align:justify">L&rsquo;an pass&eacute;, l&rsquo;Unit&eacute; Protection des Plantes et Ecotoxicologie du CRA-W a men&eacute; une enqu&ecirc;te afin d&#39;&eacute;valuer la proportion de vulpins r&eacute;sistants en Wallonie et de d&eacute;terminer les pratiques permettant un meilleur contr&ocirc;le de ces r&eacute;sistances. Les agriculteurs ayant particip&eacute; ont re&ccedil;u par la suite une lettre d&rsquo;information concernant la r&eacute;sistance de leurs vulpins ainsi que certaines recommandations.</p>

<p style="text-align:justify">Cette ann&eacute;e encore, le CRA-W vous propose de participer &agrave; cette enqu&ecirc;te et de <strong>tester gratuitement la r&eacute;sistance des vulpins pr&eacute;sents dans vos terres</strong>. Il vous est simplement demand&eacute; de r&eacute;colter les semences de vulpin &agrave; maturit&eacute; (fin juin &ndash; d&eacute;but juillet) ainsi que de nous communiquer quelques informations culturales sur la parcelle. Les vulpins pr&eacute;lev&eacute;s seront test&eacute;s en serres durant l&rsquo;hiver et les r&eacute;sultats vous seront communiqu&eacute;s. Ce type de renseignement peut vous aider &agrave; mieux appr&eacute;hender le d&eacute;sherbage de vos c&eacute;r&eacute;ales et la lutte contre le vulpin en particulier.</p>

<p style="text-align:justify">Int&eacute;ress&eacute; ? Pour recevoir plus de d&eacute;tails sur la proc&eacute;dure, merci de prendre contact avec Pierre Hellin via l&rsquo;adresse <a href="mailto:p.hellin@cra.wallonie.be">p.hellin@cra.wallonie.be</a> ou via le num&eacute;ro de t&eacute;l&eacute;phone 081 87 40 06.</p>
', 2, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (232, 'Ne dites plus CADCO mais bien CePiCOP', 'left', 300, 225, '<p>L&#39;asbl CADCO s&#39;int&egrave;gre au centre pilote pour les c&eacute;r&eacute;ales les o&eacute;lagineux et les prot&eacute;agineux : le CePiCOP asbl,... quels cons&eacute;quences?</p>

<p><img align="" filer_id="455" height="" src="/filer/canonical/1557738826/455/" thumb_option="" title="" width="" /></p>

<p>Les activit&eacute;s men&eacute;es par le CADCO seront poursuivies au sein du CePiCOP et son personnel y sera comme auparavant &agrave; votre &eacute;coute.</p>
', 3, NULL, 95, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (166, 'Enquête sur la gestion des adventices en froment d’hiver ', 'left', 300, 225, '<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Vous cultivez du froment d&rsquo;hiver en syst&egrave;me conventionnel ou biologique ?</p>

<p><strong>Vos pratiques de d&eacute;sherbage nous int&eacute;ressent</strong></p>

<p><br />
&nbsp;</p>

<p>&nbsp;</p>

<p><span style="color:#1f497d"><u>Photo</u> : Coquelicot (<em>Papaver rhoeas </em>L.) en froment d&rsquo;hiver </span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><span style="color:black">A ce jour, plus d&rsquo;une centaine de r&eacute;ponses</span></strong><span style="color:black">, si ce n&rsquo;est pas encore fait et que vous avez quelques minutes, voici une enqu&ecirc;te du CRA-W sur vos techniques de d&eacute;sherbage</span> <a href="https://tinyurl.com/enquete-adventices">lien ici</a></p>

<p style="text-align:justify"><span style="color:black">Vos r&eacute;ponses sont importantes. Elles permettront de faire &eacute;voluer les techniques de gestion des adventices. Dur&eacute;e du questionnaire : 15 minutes.</span></p>

<p style="text-align:justify"><span style="color:black">Cette enqu&ecirc;te a pour but de faire un &eacute;tat des lieux et d&rsquo;orienter la recherche relative &agrave; la gestion des adventices. </span></p>

<p style="text-align:justify"><span style="color:black">Merci pour votre participation.</span></p>

<p style="text-align:right">Plus d&rsquo;informations : E. Malice, <a href="mailto:e.malice@cra.wallonie.be">e.malice@cra.wallonie.be</a>, 081/874813</p>
', 2, 403, 74, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (167, 'Points phytolicence', 'left', 300, 225, '<p style="text-align:justify"><strong><u><span style="color:black">Comment se connecter &agrave; son compte ligne</span></u></strong><strong><span style="color:black"> ?</span></strong></p>

<p style="text-align:justify"><span style="color:black">Suite &agrave; de nombreuses demandes, vous trouverez ci-apr&egrave;s la d&eacute;marche &agrave; suivre pour consulter votre compte phytolicence : </span><a href="https://www.pwrp.be/compte-en-ligne">ici</a></p>

<p><span style="color:black">Pour ce faire vous avez besoin de </span>votre lecteur de carte d&rsquo;identit&eacute;, de votre carte d&rsquo;identit&eacute; et du code PIN de votre carte d&rsquo;identit&eacute;</p>

<p><strong><u>Nombre de formations &agrave; suivre par p&eacute;riode et par type de phytolicence</u></strong><strong> :</strong></p>

<p>2 modules pour la NP / 3 modules pour la P1 / 4 modules pour la P2 &nbsp;/ 6 modules pour la P3</p>

<p style="text-align:right"><em><u><span style="color:black">avec la collaboration du PWRP</span></u></em></p>
', 2, NULL, 75, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (182, 'Phytotechnie', 'left', 300, 225, '<p style="text-align:justify">Apr&egrave;s les &eacute;pisodes de pluies plus ou moins abondantes selon les endroits, les cultures de c&eacute;r&eacute;ales sont pour leur grande majorit&eacute; en bon &eacute;tat.</p>

<p style="text-align:justify">Avant toute intervention dans les cultures de c&eacute;r&eacute;ales, laissez ressuyer convenablement vos sols pour que les plantes soient aptes d&#39;une part &agrave; pouvoir pr&eacute;lever des apports d&#39;azote ou supporter parfaitement les traitements herbicides. De m&ecirc;me s&#39;il s&#39;agit d&#39;encore semer des c&eacute;r&eacute;ales de printemps, la pr&eacute;paration du sol doit &agrave; cette &eacute;poque de l&#39;ann&eacute;e &ecirc;tre de tr&egrave;s bonne qualit&eacute; et le sol suffisamment r&eacute;chauff&eacute; pour obtenir des lev&eacute;es rapides et homog&egrave;nes.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u>FROMENT :</u></strong> Les stades de d&eacute;veloppement des froments ont &eacute;volu&eacute; normalement depuis la fin f&eacute;vrier :</p>

<p style="margin-left:70.9pt; margin-right:0cm; text-align:justify">pour les semis de mi-octobre : plein &agrave; fin tallage (26-29)</p>

<p style="margin-left:70.9pt; margin-right:0cm; text-align:justify">pour les semis de mi-novembre : 2 - 3 talles (22-23)</p>

<p style="margin-left:70.9pt; margin-right:0cm; text-align:justify">pour les semis tardifs de mi-d&eacute;cembre 3 &agrave; 4 feuilles (13-20)</p>

<p style="text-align:justify">Les applications de tallage pour les semis d&#39;octobre ou de novembre ont g&eacute;n&eacute;ralement d&eacute;j&agrave; &eacute;t&eacute; effectu&eacute;es. La seconde application devra &ecirc;tre envisag&eacute;e d&#39;ici deux semaines.</p>

<p style="text-align:justify">Pour les semis tardifs, effectuer votre premier apport en fin de semaine.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u>ESCOURGEON :</u></strong> Les escourgeons sont au stade fin tallage, l&#39;&eacute;pi n&#39;a encore dans les observations que 6 mm de longueur. Il faut encore attendre pour la seconde application au stade redressement, le tallage est abondant et il faut &eacute;viter une mont&eacute;e d&#39;un trop grand nombre de tiges (risque de verse et trop forte comp&eacute;tition entre tiges avec pour cons&eacute;quence un nombre r&eacute;duit de grain par &eacute;pi.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><u>Orge Brassicole et Avoine :</u></strong></p>

<p style="text-align:justify">Les orges brassicoles et les avoines sem&eacute;es en fin f&eacute;vrier ou d&eacute;but mars sont lev&eacute;es. L&#39;application d&#39;azote apr&egrave;s la lev&eacute;e devra se faire lorsque les sols seront bien ressuy&eacute;s et que la croissance<br />
de ces cultures sera bien lanc&eacute;e.</p>

<p style="text-align:justify">Les outils de calcul des fertilisations &laquo; livre blanc c&eacute;r&eacute;ales &raquo; ont &eacute;t&eacute; mis &agrave; jour sur le site <a href="http://www.cereales.be">ici</a></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard, R. Meurs</p>
', 1, NULL, 77, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (260, 'Réseau « maladie en froment »', 'left', 150, 225, '<p style="text-align:justify">Dans notre r&eacute;seau de parcelles non trait&eacute;es, les observations syst&eacute;matiques sont termin&eacute;es. La plupart des parcelles sont au stade &eacute;piaison ou floraison. Cette ann&eacute;e, la septoriose a &eacute;t&eacute; plus pr&eacute;sente comparativement &agrave; l&rsquo;ann&eacute;e 2018, la rouille jaune a &eacute;t&eacute; probl&eacute;matique sur plusieurs vari&eacute;t&eacute;s sensibles ou moins sensibles et la rouille brune est observ&eacute;e depuis plusieurs semaines.</p>

<p style="text-align:justify"><span style="background-color:#ffc000">Il est recommand&eacute; <strong>d&rsquo;observer</strong> vos terres avant de d&eacute;cider d&rsquo;un &eacute;ventuel traitement !</span></p>

<p style="text-align:justify"><strong><u>Recommandations : </u></strong></p>

<p style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39</strong>, derni&egrave;re feuille compl&egrave;tement &eacute;tal&eacute;e, <strong>et qui n&rsquo;ont pas encore &eacute;t&eacute; trait&eacute;es</strong>, le traitement <u>complet</u> contre les maladies du feuillage peut &ecirc;tre r&eacute;alis&eacute;. Le produit ou le m&eacute;lange sera choisi en fonction des sensibilit&eacute;s propres &agrave; la vari&eacute;t&eacute;.</p>

<p style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39 et qui ont d&eacute;j&agrave; &eacute;t&eacute; trait&eacute;es avant ce stade</strong>, un second traitement englobant l&rsquo;ensemble des maladies est &agrave; r&eacute;aliser 3 &agrave; 4 semaines apr&egrave;s le premier traitement.</p>

<p style="text-align:justify">Dans le cas o&ugrave; le deuxi&egrave;me traitement est r&eacute;alis&eacute; dans les prochains jours, il convient de prendre en compte le risque d&rsquo;infection par la <strong>fusariose des &eacute;pis</strong> dans les situations &agrave; risque et par la rouille brune sur vari&eacute;t&eacute;s sensibles. En ce qui concerne la fusariose, les situations &agrave; risque sont les cultures de froment (les &eacute;peautres sont g&eacute;n&eacute;ralement moins sensibles) de vari&eacute;t&eacute;s sensibles dans les champs o&ugrave; le travail du sol a &eacute;t&eacute; r&eacute;duit et les froments (vari&eacute;t&eacute; sensible) apr&egrave;s froment ou ma&iuml;s, particuli&egrave;rement lorsque les cannes sont encore apparentes dans la parcelle. Le temps humide (orage, &hellip;) favorise le d&eacute;veloppement de cette maladie et pourrait donc &ecirc;tre favoris&eacute; ces derniers jours. C&rsquo;est au stade floraison (au plus tard entre le d&eacute;but et la mi-floraison, stade 61 &agrave; 65) que l&rsquo;on peut intervenir si n&eacute;cessaire contre la fusariose de l&rsquo;&eacute;pi. L&rsquo;utilisation de prothioconazole est le plus indiqu&eacute; pour lutter contre les deux &laquo; types &raquo; de pathog&egrave;nes de la fusariose (<em>Fusarium </em>spp<em>.</em> et <em>Microdochium </em>spp.) mais s&rsquo;il est utilis&eacute; en 39, d&rsquo;autres mol&eacute;cules doivent &ecirc;tre privil&eacute;gi&eacute;es pour ce traitement. Le t&eacute;buconazole et le metconazole sont, quant &agrave; eux, utiles uniquement contre les <em>Fusarium </em>spp.</p>

<p style="text-align:center"><img align="" filer_id="471" height="845" src="/filer/canonical/1559634722/471/" thumb_option="" title="" width="645" /></p>

<p style="text-align:center">Sympt&ocirc;me sur &eacute;pi de <em>Microdochium</em> spp. (deux &eacute;pis &agrave; gauche) et de <em>Fusarium </em>spp. (deux &eacute;pis de droite)</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A. Nysten, Ch. Bataille</p>
', 1, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (172, 'A l''agenda', 'left', 300, 225, '<p><u><span style="color:red">Conf&eacute;rences comptant dans le cadre de formations continues pour la phytolicence (**) </span>dans le cadre des c&eacute;r&eacute;ales</u> :</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td style="width:68.4pt">
			<p style="text-align:center"><span style="color:#ff0000">Jeudi 14/03/2019</span></p>
			</td>
			<td style="width:69.35pt">
			<p style="text-align:center"><span style="color:#ff0000">Kain<br />
			9h30 et/ou 13h20</span></p>
			</td>
			<td style="width:379.05pt">
			<p><strong><span style="color:red">** deux demi-journ&eacute;e d&#39;information</span></strong> :</p>

			<p>9h30 l&eacute;gumes industriels</p>

			<p>13h20 c&eacute;r&eacute;ales et pommes de terre. A la ferme du reposoir (CARAH)</p>

			<p><a href="http://www.cadcoasbl.be/p10_agenda/190314%20CARAH.pdf">Programme.pdf</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>
', 3, NULL, 75, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (173, 'Non renouvellement du flurtamone et du quinoxyfen', 'left', 300, 225, '<p style="text-align:justify">Suite &agrave; l&rsquo;&eacute;valuation de la demande de renouvellement europ&eacute;enne des substances actives flurtamone et du quinoxyfen celles-ci n&rsquo;ont pas &eacute;t&eacute; renouvel&eacute;es.</p>

<ul>
	<li style="text-align:justify">L&rsquo;utilisation du produit phytopharmaceutique BACARA (9127P/B &ndash; 250 g/l flurtamone + 100 g/l difluf&eacute;nican) <strong>est autoris&eacute;e jusqu&rsquo;au 27/03/2020</strong>.</li>
	<li style="text-align:justify">L&rsquo;utilisation du produit phytopharmaceutique FORTRESS (9063P/B &ndash; 500 g/l quinoxyfen) <strong>est autoris&eacute;e jusqu&rsquo;au 27/03/2020</strong>.</li>
</ul>

<p style="text-align:justify">A cette date ces produits deviennent des produits phytopharmaceutiques non utilisables et donc &agrave; ranger dans le local phyto partie &laquo; produits p&eacute;rim&eacute;s &raquo; (PPNU).</p>

<p style="text-align:justify">En ce qui concerne les tableaux relatifs aux produits phytopharmaceutiques, vous les trouverez au lien suivants (mis &agrave; jour lorsque n&eacute;cessaire tout au long de la saison) : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>

<p style="text-align:right"><strong><u>Coordonnateur CePiCOP et CADCO</u></strong><strong><em> </em></strong><strong>: </strong>X. Bertel (0468 383972), visitez notre site : <a href="http://www.cadcoasbl.be/"><span style="color:blue">www.cadcoasbl.be</span></a></p>
', 2, NULL, 75, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (184, 'Vérification envoi email', 'left', 300, 225, '<p>Bonjour,</p>

<p>Ceci est un test d&#39;envoi pour certaines personnes qui re&ccedil;oivent les messages colza en double.</p>

<p>Veuillez ne pas tenir compte de ce message.</p>

<p>Cordialement,</p>
', 1, NULL, 80, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (175, 'Reconnaitre les maladies foliaires de l’escourgeon  ', 'left', 300, 225, '<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">Des sympt&ocirc;mes de mosa&iuml;que </span></strong><span style="color:black">sur feuilles, c&rsquo;est &agrave; dire une d&eacute;coloration des limbes sous forme de tirets chlorotiques (Photo1), peuvent &ecirc;tre provoqu&eacute;s par le virus de la mosa&iuml;que jaune de l&rsquo;orge (BaYMV) transmis par <em>Polymyxa graminis,</em> un microorganisme du sol. Les plantes atteintes par ce virus, souvent distribu&eacute;es en plages vert clair &agrave; jaune, peuvent &ecirc;tre visibles dans les champs atteints au d&eacute;but du printemps. Ces sympt&ocirc;mes tendront &agrave; disparaitre vers la fin avril avec les temp&eacute;ratures plus douces. Comme il s&rsquo;agit d&rsquo;une maladie virale sp&eacute;cifique &agrave; l&rsquo;escourgeon et transmise par le sol, il convient de rep&eacute;rer les parcelles infest&eacute;es en mars ou d&eacute;but avril afin d&rsquo;emblaver ces derni&egrave;res avec des vari&eacute;t&eacute;s r&eacute;sistantes au BaYMV lors de la prochaine culture d&rsquo;escourgeon.</span></li>
</ul>

<p style="text-align:center"><img align="" filer_id="410" height="160" src="/filer/canonical/1552980633/410/" thumb_option="" title="" width="835" /></p>

<p style="text-align:center">&nbsp;</p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">La rhynchosporiose,</span></strong><span style="color:black"> due au champignon <em>Rhynchosporium secalis,</em> est caract&eacute;ris&eacute;e par des taches sur les limbes, de forme irr&eacute;guli&egrave;re, avec un centre plus clair et un contour brun &agrave; violac&eacute; bien d&eacute;limit&eacute; (photo.2). La maladie d&eacute;bute souvent, apr&egrave;s un hiver froid et humide, &agrave; l&rsquo;aisselle des feuilles (photo.3) et se propage par &eacute;claboussures de pluie ainsi que par diss&eacute;mination a&eacute;rienne des spores. Il est donc important d&rsquo;&eacute;carter le feuillage pour v&eacute;rifier sa pr&eacute;sence et l&rsquo;abondance &agrave; la base des plantes.</span></li>
</ul>

<p style="text-align:center"><img align="" filer_id="409" height="198" src="/filer/canonical/1552980603/409/" thumb_option="" title="" width="970" /></p>

<p>&nbsp;</p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">L&rsquo;helminthosporiose de l&rsquo;orge</span></strong><span style="color:black">, due &agrave; <em>Dechslera teres</em>, se caract&eacute;rise par de longues stries brunes (photo.4) habituellement entour&eacute;es d&rsquo;un halo jaune ou clair et parall&egrave;les aux nervures des feuilles (photo.5). Les taches sont visibles de fa&ccedil;on sym&eacute;trique sur les deux faces des feuilles. La maladie est souvent r&eacute;partie de fa&ccedil;on homog&egrave;ne dans la parcelle et l&rsquo;infection monte du bas vers le haut des plantes. Les attaques s&eacute;v&egrave;res commencent r&eacute;ellement apr&egrave;s le d&eacute;ploiement de la derni&egrave;re feuille et jusqu&rsquo;&agrave; la fin de la floraison lorsque le climat est favorable.</span></li>
</ul>

<p>&nbsp;</p>

<p style="text-align:center"><img align="" filer_id="408" height="" src="/filer/canonical/1552980530/408/" thumb_option="" title="" width="" /></p>

<p>&nbsp;</p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">La rouille naine de l&rsquo;orge</span></strong><span style="color:black">, caus&eacute;e par <em>Puccinia hordei</em>, est caract&eacute;ris&eacute;e par la pr&eacute;sence de pustules orang&eacute;es isol&eacute;es et entour&eacute;es d&#39;un contour chlorotique &agrave; la face sup&eacute;rieure du feuillage (photo.6). Ces pustules contiennent des spores qui se dispersent par le vent. Cette maladie ne forme pas de foyer au niveau de la parcelle et se r&eacute;partie partout dans le champ infect&eacute;. La maladie ne devient vraiment dangereuse qu&rsquo;apr&egrave;s le d&eacute;ploiement de la derni&egrave;re feuille</span>.</li>
</ul>

<p>&nbsp;</p>

<p style="text-align:center"><img align="" filer_id="407" height="" src="/filer/canonical/1552980462/407/" thumb_option="" title="" width="" /></p>

<ul>
	<li style="text-align:justify"><strong><span style="color:#ffc000">La ramulariose</span></strong><span style="color:black">, <em>Ramulario collo-cygni</em>, provoque des petites l&eacute;sions rectangulaires de couleur brun fonc&eacute;, souvent entour&eacute;es d&#39;un halo jaune (photos 7 et 8). Cette maladie n&rsquo;appara&icirc;t que tard dans la saison et elle peut &ecirc;tre souvent confondue avec des sympt&ocirc;mes de troubles physiologiques (stress lumineux, taches l&eacute;opard ou des brulures polliniques) ou encore avec des traces d&rsquo;o&iuml;dium. Ce sont les structures en forme de &laquo;&nbsp;col de cygne&nbsp;&raquo; retrouv&eacute;es sur la face inf&eacute;rieure des feuilles qui permettent de confirmer le diagnostic</span>.</li>
</ul>

<p style="text-align:center"><img align="" filer_id="406" height="" src="/filer/canonical/1552980409/406/" thumb_option="" title="" width="" /></p>

<p>&nbsp;</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
', 1, NULL, 77, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (174, 'Situation au 19 mars 2019', 'left', 300, 225, '<p style="text-align:justify">Nous avons connu une m&eacute;t&eacute;o tr&egrave;s chahut&eacute;e ces derni&egrave;res semaines avec des pluies abondantes, du vent tr&egrave;s fort et des temp&eacute;ratures nettement plus fra&icirc;ches par rapport au printemps arriv&eacute; tr&egrave;s pr&eacute;cocement en f&eacute;vrier.</p>

<p style="text-align:justify">Depuis le d&eacute;but du mois de mars, de rares charan&ccedil;ons de la tige et de m&eacute;lig&egrave;thes (voire aucuns) ont &eacute;t&eacute; pi&eacute;g&eacute;s dans ces conditions peu favorables aux vols d&rsquo;insectes.&nbsp; Ces insectes ne sont actuellement pas visibles sur les plantes.</p>

<p style="text-align:justify">Entretemps, la culture de colza se d&eacute;veloppe : on observe l&rsquo;allongement des tiges avec les boutons floraux d&eacute;j&agrave; visibles dans les champs les plus avanc&eacute;s.&nbsp; Les hauteurs de v&eacute;g&eacute;tation varient fortement d&rsquo;un champ &agrave; l&rsquo;autre.</p>

<p style="text-align:justify">Le retour annonc&eacute; cette semaine &agrave; des conditions plus calmes et plus ensoleill&eacute;es la journ&eacute;e, doit maintenir l&rsquo;attention vis-&agrave;-vis des ravageurs car le colza en montaison est sensible aux piq&ucirc;res de charan&ccedil;ons de la tige du colza et les m&eacute;lig&egrave;thes sont attir&eacute;s par les boutons floraux pour chercher le pollen qui leur sert de nourriture.&nbsp; Leur surveillance se poursuit.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 76, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (191, 'Situation au 26 mars 2019', 'left', 300, 225, '<p style="text-align:justify">Le colza poursuit son d&eacute;veloppement et l&rsquo;arriv&eacute;e de la floraison sera quelque peu ralentie suite au retour de temp&eacute;ratures plus fra&icirc;ches aussi bien en journ&eacute;e que la nuit.</p>

<p style="text-align:justify">Apr&egrave;s l&rsquo;observation des vols de m&eacute;lig&egrave;thes lors des r&eacute;centes journ&eacute;es printani&egrave;res, de nombreux m&eacute;lig&egrave;thes (parfois plusieurs centaines) ont &eacute;t&eacute; pi&eacute;g&eacute;s en peu de temps dans les bassins.</p>

<p style="text-align:justify">On peut actuellement observer la pr&eacute;sence de ces insectes dans les boutons floraux.&nbsp; Leur nombre varie fortement d&rsquo;un champ &agrave; l&rsquo;autre, de 1 &agrave; 8 m&eacute;lig&egrave;thes en moyenne par plante, dans le cadre du r&eacute;seau d&rsquo;observations.</p>

<p style="text-align:justify">Certaines parcelles ont d&eacute;pass&eacute; le seuil de traitement ; le colza est toujours au stade sensible vis-&agrave;-vis des morsures de m&eacute;lig&egrave;thes &agrave; la recherche de pollen dans les boutons floraux.</p>

<p style="text-align:justify">Avant tout traitement insecticide, il est recommand&eacute; de v&eacute;rifier le nombre d&rsquo;insectes pr&eacute;sents dans chacune des parcelles de colza, car la situation peut varier.</p>

<p style="text-align:justify">Lorsqu&rsquo;un champ a &eacute;t&eacute; trait&eacute;, il convient de v&eacute;rifier l&rsquo;efficacit&eacute; du traitement r&eacute;alis&eacute;.</p>

<p style="text-align:justify">Il faut &eacute;galement rappeler que les n&eacute;onicotino&iuml;des (thiacloprid, acetamiprid) sont interdits d&rsquo;utilisation en R&eacute;gion wallonne depuis le 1er juin 2018, m&ecirc;me si ces produits restent agr&eacute;&eacute;s en Belgique et sont toujours visibles sur Phytoweb.</p>

<p style="text-align:justify">Les charan&ccedil;ons de la tige du colza sont absents ; seuls, les charan&ccedil;ons de la tige du chou sont encore pi&eacute;g&eacute;s dans les bassins.</p>

<p style="text-align:justify">La surveillance des m&eacute;lig&egrave;thes continue jusqu&rsquo;&agrave; la floraison du colza.</p>

<p>&nbsp;</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 83, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (192, 'Situation au 01 avril 2019', 'left', 300, 225, '<p style="text-align:justify">Nous assistons &agrave; une pr&eacute;sence importante de m&eacute;lig&egrave;thes (plus de 8 adultes par plante) dans la culture de colza d&rsquo;hiver qui est &agrave; un stade actuellement toujours sensible aux d&eacute;g&acirc;ts de ces insectes.</p>

<p style="text-align:justify">La floraison du colza n&rsquo;a pas encore commenc&eacute; ; les boutons floraux pr&eacute;sentent des tailles variables mais les m&eacute;lig&egrave;thes se trouvent &eacute;galement dans les inflorescences secondaires avec des boutons plus petits donc plus vuln&eacute;rables.</p>

<p style="text-align:justify">Etant donn&eacute; l&rsquo;annonce de journ&eacute;es plus froides, le colza ne va pas se d&eacute;velopper rapidement.&nbsp; Il est conseill&eacute; de v&eacute;rifier la pr&eacute;sence de m&eacute;lig&egrave;thes vivants dans toutes les inflorescences, m&ecirc;me si un traitement insecticide a d&eacute;j&agrave; &eacute;t&eacute; r&eacute;alis&eacute;.</p>

<p style="text-align:justify">Les m&eacute;lig&egrave;thes ont d&eacute;velopp&eacute; des r&eacute;sistances aux pyr&eacute;thrino&iuml;des (cyhalothrine, cyperm&eacute;thrine, deltam&eacute;thrine,&hellip;).&nbsp; Les n&eacute;onicotino&iuml;des sont interdits en R&eacute;gion wallonne.&nbsp; Les produits utilisables contre m&eacute;lig&egrave;thes ayant encore une efficacit&eacute; sont le tau-fluvalinate (MAVRIK, EVURE), l&rsquo;esfenval&eacute;rate (SUMI ALPHA), l&rsquo;indoxacarbe (STEWARD), le phosmet (BORAVI) et la pymetrozine (PLENUM dont c&rsquo;est la derni&egrave;re ann&eacute;e d&rsquo;utilisation).</p>

<p style="text-align:justify">Chacun de ces produits ne peut &ecirc;tre utilis&eacute; qu&rsquo;une seule fois dans la culture de colza ; si un traitement insecticide a d&eacute;j&agrave; &eacute;t&eacute; r&eacute;alis&eacute; avec un produit, il faut utiliser un autre produit pour varier le mode d&rsquo;action et pour respecter la r&eacute;glementation.</p>

<p style="text-align:justify">La surveillance des m&eacute;lig&egrave;thes continue jusqu&rsquo;&agrave; la floraison du colza.</p>

<p>&nbsp;</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 84, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (201, 'Situation au 08 avril 2019', 'left', 300, 225, '<p style="text-align:justify">La culture du colza d&rsquo;hiver poursuit son d&eacute;veloppement.&nbsp; Les parcelles les plus avanc&eacute;es pr&eacute;sentent les premi&egrave;res fleurs.&nbsp; Les pluies sont b&eacute;n&eacute;fiques &agrave; la culture.</p>

<p style="text-align:justify">C&ocirc;t&eacute; insectes, le gel et la pluie ne g&ecirc;nent pas les m&eacute;lig&egrave;thes toujours pr&eacute;sents dans la culture de colza.&nbsp; Leur nombre reste variable d&rsquo;un champ &agrave; l&rsquo;autre (de 1 &agrave; 16 adultes par plante).&nbsp; Quelques charan&ccedil;ons de la tige sont encore observ&eacute;s &agrave; l&rsquo;heure actuelle.</p>

<p style="text-align:justify">Lorsque la culture n&rsquo;est pas encore en fleurs, les m&eacute;lig&egrave;thes repr&eacute;sentent toujours un danger, s&rsquo;ils sont nombreux.</p>

<p style="text-align:justify">Les <a href="http://www.gembloux.ulg.ac.be/phytotechnie-temperee/appo/Menu/conduite_des_cultures/Colza/INFO/2019/COLZA%20INSECTICIDES%20CHARANCON%20TIGE%20MELIGETHE%202019.pdf">insecticides actuellement autoris&eacute;s</a> pr&eacute;sentent une efficacit&eacute; incompl&egrave;te par rapport aux produits ant&eacute;rieurs, mais sont plus doux pour l&rsquo;entomofaune utile.</p>

<p style="text-align:justify">Les d&eacute;g&acirc;ts de morsures des m&eacute;lig&egrave;thes apparaissent au niveau des boutons floraux dess&eacute;ch&eacute;s, visibles actuellement.&nbsp; Dans quelques parcelles de colza, on peut &eacute;galement voir des tiges courb&eacute;es et des tiges creuses qui se fendent : ce sont les d&eacute;g&acirc;ts provoqu&eacute;s par les pontes du charan&ccedil;on de la tige du colza.</p>

<p style="text-align:justify">Les conditions m&eacute;t&eacute;o plus fra&icirc;ches annonc&eacute;es en fin de semaine vont ralentir l&rsquo;arriv&eacute;e de la floraison ou le d&eacute;but floraison, stade &agrave; partir duquel les m&eacute;lig&egrave;thes vont aller pr&eacute;f&eacute;rentiellement sur les fleurs ouvertes et ne repr&eacute;senteront alors plus de danger pour la culture.</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 87, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (195, 'Veille concernant la rouille jaune en froment', 'left', 300, 225, '<table cellspacing="0" class="MsoTableLightShadingAccent1" style="border-collapse:collapse; border:none; width:529.9pt">
	<tbody>
		<tr>
			<td style="width:63.8pt">
			<p><strong>Dates</strong></p>
			</td>
			<td style="width:466.1pt">
			<p>Observations du lundi 01 avril 2019 ; Semis du 22 octobre au 05 novembre 2018</p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">Des pustules de rouille jaune sont visibles notamment sur des vari&eacute;t&eacute;s r&eacute;sistantes, toutefois, nous veillons &agrave; rappeler que certaines vari&eacute;t&eacute;s n&rsquo;expriment pas toujours leur r&eacute;sistance face &agrave; cette maladie au stade juv&eacute;nile et les quelques pustules &agrave; cette p&eacute;riode ne peuvent indiquer la situation future du champ surtout pour les vari&eacute;t&eacute;s r&eacute;sistantes emblav&eacute;es dans vos champs.</p>

<p style="text-align:justify">Rappelons que la recommandation du CADCO est la suivante : aucun traitement ne doit &ecirc;tre envisag&eacute; avant le stade 31. Au stade 31, un traitement peut &eacute;ventuellement &ecirc;tre mis en place seulement dans les parcelles emblav&eacute;es avec une vari&eacute;t&eacute; <strong><u>tr&egrave;s sensible</u></strong> &agrave; la rouille jaune <strong><u>et</u></strong> qui pr&eacute;senteraient des gros <strong><u>foyers actifs</u></strong> de rouille jaune. Ce cas de figure est assez rare et nous ne sommes pas confront&eacute;s &agrave; cette situation dans nos essais.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 09 avril.</span></p>
', 1, NULL, 85, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (198, 'Réseau « maladie en escourgeon »', 'left', 300, 225, '<p style="text-align:justify"><img align="" filer_id="424" height="" src="/filer/canonical/1554798061/424/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">Plus de 60% des parcelles ont atteint le stade 2i&egrave;me n&oelig;ud (BBCH 32) notamment dans le Hainaut mais &eacute;galement &agrave; Fexhe-Slins ou encore &agrave; Lonz&eacute;e. Les autres parcelles observ&eacute;es sont encore au stade 1er n&oelig;ud (BBCH 31). L&rsquo;helminthosporiose est visible sur les vari&eacute;t&eacute;s tr&egrave;s sensibles. La rhynchosporiose reste discr&egrave;te et l&rsquo;o&iuml;dium est pr&eacute;sent dans le Hainaut mais les sympt&ocirc;mes sont faibles. La rouille naine est observ&eacute;e dans toutes les parcelles du r&eacute;seau.&nbsp; Dans le Hainaut, sur vari&eacute;t&eacute;s sensibles, elle atteint parfois la F-1.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify">Ce lundi 8 avril, la plupart des parcelles du r&eacute;seau sont au stade &laquo; 2i&egrave;me n&oelig;ud &raquo; (BBCH32), mais quelques parcelles ne sont qu&rsquo;au stade ph&eacute;nologique &laquo;1ier n&oelig;ud &raquo; (BBCH31).</p>

<p style="text-align:justify"><img align="" filer_id="426" height="" src="/filer/canonical/1554800128/426/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a><strong> </strong>est observ&eacute;e &agrave; Ath, Mainvaut et Pailhe, sur vari&eacute;t&eacute;s sensibles uniquement, soit dans 3 des 13 parcelles du r&eacute;seau d&rsquo;observation du CADCO. A Pailhe, elle est pr&eacute;sente sur 5% des F-2. A Mainvaut, elle est observ&eacute;e sur 3% des F-1 et 32% des F-2 et 50% des F-3 sur les vari&eacute;t&eacute;s sensibles &agrave; cette maladie. A Ath, elle est observ&eacute;e sur les trois derni&egrave;res feuilles actuelles &eacute;galement, &agrave; raison de 2 ; 2 et 10%. La s&eacute;v&eacute;rit&eacute; de l&rsquo;infection est faible dans tous les cas sauf &agrave; Mainvaut sur la F-3.</p>

<p style="text-align:justify"><span style="color:black">La </span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rhynchosporiose</a></strong><span style="color:black"> est pr&eacute;sente dans 6 des 13 parcelles. On observe seulement 5% des F-3 &agrave; Pailhe. A Lonz&eacute;e, 25% de plantes sont touch&eacute;es sur les vari&eacute;t&eacute;s sensibles mais avec </span>moins d&rsquo;2% <span style="color:black">de surface touch&eacute;e et reste dans le fond de la v&eacute;g&eacute;tation (F-3). Dans le Hainaut, 2 &agrave; 8% des F-2 pr&eacute;sentent quelques taches mais la surface foliaire touch&eacute;e est de moins d&rsquo;1%. Cette maladie n&rsquo;est donc pas pr&eacute;occupante pour l&rsquo;instant.</span></p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">o&iuml;dium</a></strong><span style="color:black"> est observ&eacute; dans 8 des 13 parcelles du r&eacute;seau. Elle est pr&eacute;sente sur 30% des F-3 &agrave; Fexhe-Slins et 5% du m&ecirc;me &eacute;tage foliaire &agrave; Pailhe. Dans ces parcelles, aucun sympt&ocirc;me de cette maladie n&rsquo;est visible sur les &eacute;tages foliaires sup&eacute;rieurs. A Mainvaut et Ath, de 3 &agrave; 23% des F-2 et moins de 2% des F-1 pr&eacute;sentent des sympt&ocirc;mes de cette maladie mais la surface foliaire infect&eacute;e reste faible. </span></p>

<p style="text-align:justify">La <strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rouille naine</a></strong> a &eacute;t&eacute; observ&eacute;e dans toutes les parcelles du r&eacute;seau mais de fa&ccedil;on tr&egrave;s contrast&eacute;e selon les vari&eacute;t&eacute;s. Dans le Hainaut, &agrave; Ath et Mainvaut : 30 &agrave; 50% des F-2 sont touch&eacute;es et 2 &agrave; 18% des F-1 sur les vari&eacute;t&eacute;s sensibles. Dans la r&eacute;gion de Li&egrave;ge, cette maladie a &eacute;t&eacute; observ&eacute;e sur 15 &agrave; 70% des F-3 selon les vari&eacute;t&eacute;s et maximum 5% des F-2 de vari&eacute;t&eacute;s sensibles &agrave; Fexhe-Slins et Pailhe. Cette maladie a &eacute;t&eacute; aussi observ&eacute;e dans les parcelles &agrave; Lonz&eacute;e : 5% des F-2 et 55% &agrave; 60% des F-3.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u><strong>Recommandations </strong></u></p>

<p style="text-align:justify">Actuellement, la pression phytosanitaire peut dans l&rsquo;ensemble &ecirc;tre consid&eacute;r&eacute;e comme faible. Seule la pression en rouille naine ou en helminthosporiose sur vari&eacute;t&eacute;s sensibles &agrave; l&rsquo;une ou l&rsquo;autre de ces maladies pourrait &ecirc;tre pr&eacute;occupante et n&eacute;cessiter un premier traitement. Le temps sec et les temp&eacute;ratures plus basses pour cette fin de semaine ne devraient cependant pas &ecirc;tre favorables aux maladies.</p>

<p style="text-align:justify"><span style="color:black">Comme mentionn&eacute; la semaine derni&egrave;re, un traitement g&eacute;n&eacute;ral contre l&rsquo;ensemble des maladies est g&eacute;n&eacute;ralement effectu&eacute; au stade BBCH39. Toutefois, si une pression importante d&rsquo;une maladie est observ&eacute;e sur vari&eacute;t&eacute; sensible, un traitement peut &ecirc;tre envisag&eacute; &agrave; ce stade. Il convient donc d&rsquo;observer l&rsquo;&eacute;tat sanitaire des parcelles et traiter <strong>uniquement</strong> l&agrave; o&ugrave; les maladies sont bien visibles sur les &eacute;tages sup&eacute;rieurs, ceci pour &eacute;viter que les maladies ne s&rsquo;installent sur les deux derni&egrave;res feuilles. </span></p>

<p style="text-align:justify"><span style="color:black">Dans ce cas, il est conseill&eacute; de privil&eacute;gier un fongicide &agrave; base de triazole voire une strobilurine (contre la rouille naine) en m&eacute;lange avec un triazole pour le traitement de montaison. En pr&eacute;sence faible de maladies et/ou de march&eacute; d&eacute;favorable, on pourrait se contenter d&rsquo;une dose r&eacute;duite de fongicide &agrave; ce stade, voire de faire l&rsquo;impasse</span>. Les produits SDHI (efficaces sur la rouille et la rhynchosporiose) ainsi que le chlorothalonil (efficace sur ramulariose) sont &agrave; r&eacute;server pour le traitement de derni&egrave;re feuille.<span style="color:black"> Parmi les triazoles, l&rsquo;efficacit&eacute; du prothioconazole se d&eacute;marque sur l&rsquo;helminthosporiose et est &agrave; privil&eacute;gier <u>id&eacute;alement</u> pour le traitement en 39. Par souci de lutte contre les r&eacute;sistances, il est vivement conseill&eacute; de n&rsquo;utiliser qu&rsquo;une seule fois par saison chaque mol&eacute;cule fongicide et d&rsquo;alterner les diff&eacute;rentes substances. </span></p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 16 avril.</span></p>
', 1, NULL, 86, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (199, 'Réseau « maladie en froment »', 'left', 300, 225, '<p><img align="" filer_id="427" height="" src="/filer/canonical/1554800605/427/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">La majorit&eacute; des parcelles (78,5 %) est au stade &laquo; fin de tallage &raquo; (BBCH 29) quelques-unes (3,5%) sont encore en &laquo; plein tallage &raquo; (BBCH 26) et certaines (18%) atteignent le stade &laquo; &eacute;pi 1 cm &raquo; (BBCH 30).</p>

<p style="text-align:justify">La pression des maladies est faible et ne devrait pas &eacute;voluer dans les prochains jours vu les conditions climatiques froides et s&egrave;ches annonc&eacute;es en fin de semaine.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify">Le stade &laquo; &eacute;pi 1 cm &raquo; est observable dans les champs, lorsqu&rsquo;on r&eacute;alise une d&eacute;coupe longitudinale de la tige principale (maitre-brin) et qu&rsquo;on observe plus d&rsquo;1 cm entre le plateau de tallage et le sommet de l&rsquo;&eacute;pi en formation au sein de la tige: Voir <a href="http://www.cadcoasbl.be/p09_biblio/art00015/Stade%20Epi%201.pdf">ici</a></p>

<p><img align="" filer_id="428" height="" src="/filer/canonical/1554800631/428/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><img align="" filer_id="432" height="" original_image="false" src="/filer/canonical/1554824001/432/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La </strong><strong>septoriose</strong><span style="color:black"> est observ&eacute;e sur l&rsquo;ensemble des parcelles du r&eacute;seau dans le bas de la v&eacute;g&eacute;tation. Dans le Hainaut, elle est plus intense mais n&rsquo;atteint jamais un &eacute;tage foliaire sup&eacute;rieur &agrave; la F-2 du moment (7 &agrave; 47% des F-2 avec maximum 6% de surface foliaire touch&eacute;e et 7 &agrave; 50% des F-3 avec des surfaces foliaires de 6 &agrave; 50%). </span></p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong><span style="color:black"> est peu pr&eacute;sent dans les parcelles. Seules 6 parcelles situ&eacute;es &agrave; Mortoux, Ath et Ellignies montrent des sympt&ocirc;mes sur les F-2 du moment &agrave; des fr&eacute;quences faibles (moins de 3% des plantes).</span></p>

<p style="text-align:justify"><strong>La rouille jaune</strong><span style="color:black"> est observ&eacute;e dans seulement 2 des 28 parcelles du r&eacute;seau: &agrave; Mortoux et Ellignies sur des vari&eacute;t&eacute;s sensibles comme NEMO ou REFLECTION. On nous rapporte qu&rsquo;elle est &eacute;galement observ&eacute;e dans des champs emblav&eacute;s avec KWS SMART, toutefois, nous rappelons que certaines vari&eacute;t&eacute;s r&eacute;sistantes peuvent &ecirc;tre sensibles au stade juv&eacute;nile. Il n&rsquo;est donc pas n&eacute;cessaire de traiter ces derni&egrave;res malgr&eacute; l&rsquo;observation de quelques pustules de rouille.</span></p>

<p style="text-align:justify"><strong>La rouille brune</strong><span style="color:black"> n&rsquo;a pas &eacute;t&eacute; observ&eacute;e dans les parcelles du r&eacute;seau.</span></p>

<p style="text-align:justify"><u><strong>Recommandations </strong></u></p>

<ul>
	<li style="text-align:justify">Les conditions actuelles permettent <strong>d&rsquo;&eacute;carter</strong> l&rsquo;id&eacute;e d&rsquo;un quelconque traitement.</li>
</ul>

<p style="text-align:justify">Pour rappel, <strong>aucun traitement</strong> ne doit &ecirc;tre envisag&eacute; avant le stade 31. Au stade 31, un traitement pourrait &eacute;ventuellement &ecirc;tre envisag&eacute;, uniquement dans les parcelles emblav&eacute;es avec une vari&eacute;t&eacute; tr&egrave;s sensible &agrave; la rouille jaune et qui pr&eacute;sentent des foyers actifs de rouille jaune. Ce cas de figure est assez rare et actuellement nous ne sommes pas confront&eacute;s &agrave; cette situation.</p>

<p style="text-align:justify"><u>Les vari&eacute;t&eacute;s suivantes sont &agrave; surveiller vis-&agrave;-vis de la rouille jaune :</u></p>

<p style="text-align:justify">BENCHMARK, NEMO, REFLECTION, RGT REFORM, SAHARA, KWS DORSET et RGT TEXACO.</p>

<p>&nbsp;</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 16 avril.</span></p>
', 1, NULL, 86, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (259, 'Orge brassicole', 'left', 300, 225, '<table align="left" border="1" cellspacing="0" class="MsoTableLightShadingAccent1" style="border-collapse:collapse; border:solid #93a299 1.0pt; margin-left:4.8pt; margin-right:4.8pt; width:525.05pt">
	<tbody>
		<tr>
			<td style="width:63.2pt">
			<p style="text-align:justify"><strong>R&eacute;seau</strong></p>
			</td>
			<td style="width:461.85pt">
			<p style="text-align:justify">3 parcelles r&eacute;parties dans les localit&eacute;s : Hainaut (Vaudignies) et Namur (Gembloux, Liernu)</p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:justify">Les derniers semis (Liernu 27/03) de notre r&eacute;seau de champs d&#39;observations a atteint le stade derni&egrave;re feuille&nbsp; (BBCH39). La pression des maladies dans la parcelle observ&eacute;e est faible. Cependant, nous conseillons d&rsquo;appliquer un traitement pr&eacute;ventif pour les orges ayant atteint ce stade afin de prot&eacute;ger les 4 derni&egrave;res feuilles.</p>

<p style="text-align:justify">L&rsquo;emploi d&rsquo;un r&eacute;gulateur en orge brassicole n&rsquo;est normalement pas n&eacute;cessaire, cependant, si le traitement est jug&eacute; n&eacute;cessaire, les r&eacute;gulateurs autoris&eacute;s en escourgeon sont pour la plupart autoris&eacute;s en orge de printemps mais &agrave; des doses plus faibles.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>
', 1, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (200, 'Actualisation des listes des produits autorisés en céréales ', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les r&eacute;gulateurs de croissance, les herbicides, fongicides, les insecticides &hellip; <strong> </strong></p>

<p><strong><em>&nbsp;&nbsp; Cliquez sur le lien &quot;cellule de droite du tableau ci-dessous&quot; pour ouvrir le fichier ad hoc. </em></strong></p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td style="text-align:center">01 avril 2019</td>
			<td style="text-align:center"><strong>AGENT ANTI-MOUSSANT</strong></td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/09AgentAntiMoussant.pdf">Anti-moussant.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center"><strong>LES FONGICIDES</strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center">11 f&eacute;v. 2019</td>
			<td style="text-align:center">Avoines</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongAvoine.pdf">FongA.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">11 f&eacute;v. 2019</td>
			<td style="text-align:center">Epeautre, Froments, Seigles, Triticale</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0705FongEpFroSeiTri.pdf">FongEpFroSeiTri.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">01 avril 2019</td>
			<td style="text-align:center">Froments-Autoris&eacute;s contre Rouille jaune</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0704FroRouilleJaune.pdf">FongFRJ.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">11 f&eacute;v. 2019</td>
			<td style="text-align:center">Froments-Autoris&eacute;s contre Fusariose des &eacute;pis</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1207FroFusariose.pdf">FongFusariose.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">01 avril 2019</td>
			<td style="text-align:center">Orges</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongOrges.pdf">FongO.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center"><strong>LES HERBICIDES</strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center">Version 2019</td>
			<td style="text-align:center">Sensibilit&eacute; des adventices</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14sensibiliteadventices.pdf">SensAdv.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center">Mode d&#39;actions des herbicides</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14ModeAction.pdf">ModeActcion.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center">Sensibilit&eacute; au Chlortoluron</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/2016PJ- sensibChlortoluron.pdf">SensChlo.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center">Les additifs</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/08HerbicidesAdditifs.pdf">HerbAdd.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center">En pr&eacute;-&eacute;mergence de la c&eacute;r&eacute;ale</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf">HerbPreC.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center"><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1510HerbPreEp.pdf">HerbPreEp.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center">De la lev&eacute;e au d&eacute;but tallage</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf">HerbLTalC.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center"><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalEp.pdf">HerbLTalEp.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center">Du d&eacute;but tallage au gonflement de la gaine</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFC.pdf">HerbTalDFC.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center"><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFEp.pdf">HerbTalDFEp.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">15 f&eacute;v. 2019</td>
			<td style="text-align:center">Sur c&eacute;r&eacute;ales &agrave; maturit&eacute;</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbM.pdf">HerbM.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">30 octobre 2018</td>
			<td style="text-align:center">Toutes cultures</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCultures.pdf">HerbTC.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">30 octobre 2018</td>
			<td style="text-align:center"><em>toutes cultures, tri&eacute; par substance active</em></td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCulturessa.pdf">HerbTCsa.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">30 octobre 2018</td>
			<td style="text-align:center">Terres agricoles en interculture</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCultures.pdf">HerbIC.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">30 octobre 2018</td>
			<td style="text-align:center"><em>interculture, tri&eacute; par substance active</em></td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCulturessa.pdf">HerbICSA.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center"><strong>LES INSECTICIDES</strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center">19 janv. 2019</td>
			<td style="text-align:center">C&eacute;cidomyies</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf">CecidSA.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">19 janv. 2019</td>
			<td style="text-align:center">Pucerons-&Eacute;t&eacute;</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsEteCerealesSA.pdf">InsEteCSA.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">19 janv. 2019</td>
			<td style="text-align:center">Pucerons vecteurs de JNO</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">InsJNOCSA.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center"><strong>LES MOLLUSCICIDES</strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center">19 janv. 2019</td>
			<td style="text-align:center">C&eacute;r&eacute;ales</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707MolluscicideCereales.pdf">MolC.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">INFO</td>
			<td style="text-align:center">Granul&eacute;s anti-limaces : pas sans risques !</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0809AntiLimacesSPF.pdf">AntiLim.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center"><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center">01 avril 2019</td>
			<td style="text-align:center">Avoines et Froment de printemps</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf">RegAFrop.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">01 avril 2019</td>
			<td style="text-align:center">Epeautre, Froment d&#39;hiver, Triticale</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf">RegFhET.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">01 avril 2019</td>
			<td style="text-align:center">Orges et Seigles</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf">RegOS.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center"><strong>Les retraits d&#39;autorisation &quot;pr&eacute;-d&eacute;termin&eacute;s&quot;</strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center">19 janv. 2019</td>
			<td style="text-align:center">Date limite d&#39;utilisation &quot;produits c&eacute;r&eacute;ales&quot;</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0709LiquidationStock.pdf">LiquidationStock.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">&nbsp;</td>
			<td style="text-align:center"><strong>LES TRAITEMENTS </strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center">20 ao&ucirc;t 2018</td>
			<td style="text-align:center">pour c&eacute;r&eacute;ales stock&eacute;es<br />
			pour locaux de stockage vides</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TraitStoLocStoCereales.pdf">TStoLocStoC.pdf</a></td>
		</tr>
		<tr>
			<td style="text-align:center">19 janv. 2019</td>
			<td style="text-align:center">de semence : C&eacute;r&eacute;ales</td>
			<td style="text-align:center"><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">TSemC.pdf</a></td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits.</p>
', 2, NULL, 86, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (264, 'Commission communale de constat des dégâts - appel à candidature', 'left', 300, 225, '<p><u>Commission communale de constat des d&eacute;g&acirc;ts - appel &agrave; candidature</u></p>

<p style="text-align:justify">Le SPW Agriculture, Ressources naturelles et Environnement a lanc&eacute; un appel &agrave; candidature comme expert-agriculteur ou expert agricole ou horticole, d&eacute;sign&eacute; par l&rsquo;Administration pour &ecirc;tre membre d&rsquo;une commission communale de constat des d&eacute;g&acirc;ts. <strong>Les informations sur cet appel peuvent &ecirc;tre obtenues via le lien suivant </strong>: <a href="https://agriculture.wallonie.be/-/candidature-en-tant-qu-expert-au-sein-de-la-commission-communale-de-constat-de-degats?inheritRedirect=true&amp;redirect=%2Faccueil">Portail Agriculture</a></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (197, 'Point phytotechnique en froment', 'left', 300, 225, '<p><u><span style="color:black">Fumure et r&eacute;gulateur</span></u><span style="color:black"> :</span></p>

<p>Avec les temp&eacute;ratures relativement fra&icirc;ches (notamment les nuits), les froments n&rsquo;ont pas beaucoup &eacute;volu&eacute; lors de la semaine pr&eacute;c&eacute;dente.</p>

<p style="text-align:justify">En fonction des vari&eacute;t&eacute;s et des r&eacute;gions agricoles, les semis r&eacute;alis&eacute;s durant le <strong>mois d&rsquo;octobre</strong> sont proches du stade ou ont atteint le stade &laquo; <u>&eacute;pis &agrave; 1 cm</u> &raquo;. &Agrave; ce stade (<u>redressement</u>), il y a lieu de penser &agrave; apporter le second apport d&rsquo;azote (lors d&rsquo;un sch&eacute;ma en 3 fractions la fumure de r&eacute;f&eacute;rence conseill&eacute;e est de 50 N, Livre blanc de F&eacute;vrier 2019) ou d&#39;appliquer le premier apport (lors d&rsquo;un sch&eacute;ma en 2 fractions la fumure de r&eacute;f&eacute;rence conseill&eacute;e est de 90 N, Livre blanc de F&eacute;vrier 2019). Pour cette fertilisation, il ne faut pas oublier de porter une attention particuli&egrave;re &agrave; <u>l&rsquo;&eacute;tat de votre culture</u>, &agrave; votre <u>r&eacute;gion agricole</u> ainsi qu&rsquo;au <strong><u>pr&eacute;c&eacute;dent cultural </u></strong>notamment. En effet, comme cela a &eacute;t&eacute; illustr&eacute; lors du dernier livre blanc, une forte h&eacute;t&eacute;rog&eacute;n&eacute;it&eacute; est pr&eacute;sente. On a donc une variation importante des reliquats azot&eacute;s. C&rsquo;est pourquoi, en fonction de vos analyses de sols, nous vous conseillons d&rsquo;ajuster votre fumure en fonction du tableau ci-dessous :</p>

<p style="text-align:justify"><img align="" filer_id="430" height="" src="/filer/canonical/1554805405/430/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">Pour rappel, vous pouvez aussi retrouver, les outils de calculs pour vos fertilisations sur le site <a href="http://www.cereales.be">www.cereales.be</a></p>

<p style="text-align:justify">Ensuite, le <u>traitement r&eacute;gulateur</u> peut &eacute;galement &ecirc;tre appliqu&eacute; d&egrave;s le stade redressement et jusqu&rsquo;au stade 2 n&oelig;uds. L&rsquo;efficacit&eacute; de ce traitement est principalement bas&eacute;e sur les conditions d&rsquo;applications. Il faut donc une c&eacute;r&eacute;ale en pleine croissance ainsi que des bonnes conditions climatiques (temp&eacute;rature sup&eacute;rieure &agrave; 12 degr&eacute;s pendant plusieurs heures).</p>

<p style="text-align:justify">Enfin, pour les <strong>semis de novembre et de d&eacute;cembre</strong>, nous sommes respectivement au stade fin tallage et plein tallage.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 16 avril.</span></p>
', 1, NULL, 86, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (202, 'Phytotechnie en escourgeons et orges', 'left', 300, 225, '<p style="text-align:justify">Les <strong>escourgeons</strong> se trouvent dans la plupart des parcelles au stade 1er n&oelig;ud (31). Au niveau phytotechnique, il n&rsquo;y a rien de sp&eacute;cifique &agrave; faire &agrave; ce stade.</p>

<p style="text-align:justify">Les <strong>orges brassicoles</strong> et les avoines sem&eacute;es fin f&eacute;vrier se trouvent au stade 3 feuilles. Au vu vue de l&rsquo;&eacute;tat de ces cultures, aucune action ne doit &ecirc;tre prise pour l&rsquo;instant.</p>

<p style="text-align:justify">Les semis plus tardif <strong>d&rsquo;orge de printemps</strong> (27 mars) se trouvent au stade 1 feuille. Pour ces derniers, la premi&egrave;re fraction azot&eacute;e pourra &ecirc;tre appliqu&eacute;e d&egrave;s que les conditions m&eacute;t&eacute;orologiques le permettront. Dans le cas o&ugrave; les reliquats azot&eacute;s sont de l&rsquo;ordre de 60 kg N/ha sur les 90 premiers centim&egrave;tres, il est conseill&eacute; d&rsquo;appliquer 90 kg N/ha d&egrave;s le d&eacute;but de la v&eacute;g&eacute;tation. Si les reliquats sont plus importants, il est conseill&eacute; de diminuer la premi&egrave;re fraction afin d&rsquo;&eacute;viter une teneur en prot&eacute;ine trop &eacute;lev&eacute;e &agrave; la r&eacute;colte, ce qui entrainerait un d&eacute;classement de l&rsquo;orge. Une application de 20 &agrave; 40 unit&eacute;s pourra ensuite &ecirc;tre appliqu&eacute;e au stade redressement si la culture parait carenc&eacute;e, le sujet sera trait&eacute; dans un prochain avertissement si n&eacute;cessaire.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 16 avril.</span></p>
', 1, NULL, 86, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (262, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p><u>A l&rsquo;agenda</u> :</p>

<p>&nbsp;</p>

<p>En <strong><span style="color:red">rouge</span></strong> les activit&eacute;s reconnues dans le cadre de la phytolicence <a href="http://www.pwrp.be/agenda-phytolicence">(voir agenda phytolicence complet ici)</a> Prenez votre carte d&#39;identit&eacute;. En <strong><span style="color:#00b050">vert</span></strong> : Rencontres en ferme &quot;alternatives aux pesticides chimiques de synth&egrave;se&quot;</p>

<p>&nbsp;</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td>
			<p style="text-align:center"><strong>Date</strong></p>
			</td>
			<td style="width:57.5pt">
			<p style="text-align:center"><strong>Lieu</strong></p>
			</td>
			<td style="width:393.2pt">
			<p style="text-align:center"><strong>Ev&eacute;nement</strong></p>
			</td>
		</tr>
		<tr>
			<td>
			<p><span style="color:red">Mardi 04/06/2019</span></p>
			</td>
			<td style="width:57.5pt">
			<p><span style="color:red">Fromi&eacute;e<br />
			13h00</span></p>
			</td>
			<td style="width:393.2pt">
			<p>Visite Colza associ&eacute; et essai en semis direct de froment dans diff&eacute;rents couverts (Greenotec, Gal Entre S et M, PROTECTeau).&nbsp; <a href="http://www.cadcoasbl.be/p10_agenda/190604%20Greenotec.pdf">Inscription et programme</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mardi 04/06/2019</p>
			</td>
			<td style="width:57.5pt">
			<p>Gembloux<br />
			14h00</p>
			</td>
			<td style="width:393.2pt">
			<p>Visite de champ d&#39;essais vari&eacute;t&eacute;s escourgeon (CRA-W/CePiCOP). Chemin de Liroux 9</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><span style="color:#00b050">Mercredi 05/06/2019</span></p>
			</td>
			<td style="width:57.5pt">
			<p><span style="color:#00b050">Lantin<br />
			14h00-17h00</span></p>
			</td>
			<td style="width:393.2pt">
			<p>Rencontre en ferme polyculture &eacute;l&eacute;vage et brasserie &agrave; la ferme (Nature et progr&egrave;s). Ferme de l&#39;arbre de Li&egrave;ge. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Jeudi 06/06/2019</p>
			</td>
			<td style="width:57.5pt">
			<p>Horion<br />
			14h00</p>
			</td>
			<td style="width:393.2pt">
			<p>Visite de champ d&#39;essais bio froment d&#39;hiver, triticale et &eacute;peautre (CPL Vegemar/CePiCOP). Rue du Saou. Chez Carles-Albert de Grady.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><span style="color:#00b050">Samedi 08/06/2019</span></p>
			</td>
			<td style="width:57.5pt">
			<p><span style="color:#00b050">Attert<br />
			10h00-17h00</span></p>
			</td>
			<td style="width:393.2pt">
			<p>Rencontre en 2 fermes polyculture &eacute;l&eacute;vage du parc naturel d&#39;Attert (Nature et progr&egrave;s). Ferme des Loups et ferme Fran&ccedil;ois Debilde. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p><span style="color:#00b050">Mardi 11/06/2019</span></p>
			</td>
			<td style="width:57.5pt">
			<p><span style="color:#00b050">Upigny<br />
			10-13h</span></p>
			</td>
			<td style="width:393.2pt">
			<p>Rencontre en ferme grande cultureen conversion bio (Nature et progr&egrave;s). Ferme d&#39;Upigny. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><u><strong>Voir la suite de l&rsquo;agenda</strong></u> : toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <strong><a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></strong></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (203, 'Phytotechnie en escourgeons et orges', 'left', 300, 225, '<p><strong><u><span style="color:black">En escourgeon</span></u></strong></p>

<p style="text-align:justify">Les escourgeons n&rsquo;ont actuellement pas encore atteint le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39), les prochaines op&eacute;rations culturales auront lieu lorsque les escourgeons auront atteint ce stade.</p>

<p><strong><u><span style="color:black">Culture de printemps </span></u></strong></p>

<p><strong><u><span style="color:black">Orge brassicole </span></u></strong></p>

<p style="text-align:justify">Les orges sem&eacute;es fin f&eacute;vrier &agrave; Gembloux se trouvent actuellement au stade d&eacute;but tallage. Les orges sem&eacute;es plus tardivement (fin mars &agrave; Liernu) se trouvent au stade 3 feuilles.</p>

<p><u><span style="color:black">D&eacute;sherbage </span></u></p>

<p style="text-align:justify">Etant donn&eacute; les temp&eacute;ratures plus douces annonc&eacute;es cette semaine, les d&eacute;sherbages en orge de printemps vont pouvoir &ecirc;tre r&eacute;alis&eacute;s sans risque pour la culture. Attention, un produit autoris&eacute; en Escourgeon ne l&rsquo;est pas forc&eacute;ment en orge de printemps.</p>

<p><strong><u><span style="color:black">Avoine</span></u></strong></p>

<p>Les avoines sem&eacute;es &agrave; la fin du mois de f&eacute;vrier &agrave; Gembloux se trouvent actuellement au stade 3 feuilles.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 23 avril.</span></p>
', 1, NULL, 88, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (208, 'Situation au 16 avril 2019', 'left', 300, 225, '<p style="text-align:justify">Apr&egrave;s une p&eacute;riode de gel nocturne accompagn&eacute;e de vent du nord-est ralentissant le d&eacute;veloppement de la floraison du colza, la prochaine remont&eacute;e de temp&eacute;ratures va acc&eacute;l&eacute;rer cette floraison.</p>

<p style="text-align:justify">Toutes les parcelles de colza ne sont pas encore entr&eacute;es en floraison.&nbsp; Les diff&eacute;rences vari&eacute;tales sont bien marqu&eacute;es.</p>

<p style="text-align:justify">Au cours de la semaine froide &eacute;coul&eacute;e, il y a eu peu d&rsquo;activit&eacute;s d&rsquo;insectes.&nbsp; A partir de la floraison, les m&eacute;lig&egrave;thes ne repr&eacute;sentent plus de danger pour la culture de colza car ils vont s&rsquo;alimenter en pollen sur les fleurs ouvertes.</p>

<p style="text-align:justify">Dans plusieurs champs, des tiges courb&eacute;es en &laquo; S &raquo; font appara&icirc;tre les d&eacute;g&acirc;ts li&eacute;s aux piq&ucirc;res de ponte des charan&ccedil;ons de la tige du colza apparus pr&eacute;c&eacute;demment dans la culture.&nbsp; Des tiges &eacute;clat&eacute;es sont visibles.</p>

<p style="text-align:justify">Il ne faut pas confondre ces courbures avec l&rsquo;effet du gel nocturne courbant la partie sup&eacute;rieure des tiges qui se redressent par la suite.</p>

<p style="text-align:justify">Pour les parcelles les plus avanc&eacute;es en floraison, la protection fongicide pr&eacute;ventive contre le scl&eacute;rotinia sera appliqu&eacute;e avant la chute des premiers p&eacute;tales.&nbsp; A partir de la floraison, la surveillance des insectes concernera les charan&ccedil;ons des siliques.&nbsp; Les abeilles profiteront de la floraison du colza sous le soleil dans les prochains jours.</p>

<p style="text-align:justify">Pour prot&eacute;ger les abeilles, il est recommand&eacute; de r&eacute;aliser les traitements fongicides en dehors des heures de butinage. Lien vers les listes des <strong><a href="http://www.cadcoasbl.be/p03_archives/avert2019/190416%20Fongicolza.pdf">fongicides autoris&eacute;s</a></strong>.</p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 89, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (258, 'Cécidomyie orange, pucerons et criocères', 'left', 300, 225, '<p style="text-align:justify">Des vols et des pontes de c&eacute;cidomyies orange ont encore &eacute;t&eacute; observ&eacute;s ces trois derni&egrave;res soir&eacute;es avec des intensit&eacute;s tr&egrave;s diff&eacute;rentes selon les champs. Dans de nombreux champs, on ne voit pas, ou quasi pas de c&eacute;cidomyie orange. Toutefois, &agrave; proximit&eacute; de champs sources, les vols peuvent atteindre, voire d&eacute;passer les 30 individus par m&sup2;. Un champ source est un champ dans lequel du froment sensible, cultiv&eacute; en 2018, a permis &agrave; l&rsquo;insecte de se multiplier et de produire une r&eacute;serve dans le sol.</p>

<p style="text-align:justify">Il est possible que de nouvelles &eacute;mergences se produisent encore au cours des prochains jours, en pleine &eacute;piaison (= stade vuln&eacute;rable) des champs les moins avanc&eacute;s. Le risque est assez faible et, sauf exception, ne justifiera pas de traitement insecticide sp&eacute;cifique. Toutefois, on peut profiter de l&rsquo;effet secondaire des fongicides eux-m&ecirc;mes sur la c&eacute;cidomyie orange. En effet, des essais men&eacute;s au CRA-W ont montr&eacute; que l&rsquo;application de divers fongicides tuait les c&eacute;cidomyies orange. C&rsquo;&eacute;tait particuli&egrave;rement le cas du fluxapyroxad, de l&rsquo;azoxystrobine et du t&eacute;buconazole. L&agrave; o&ugrave; cette opportunit&eacute; se pr&eacute;sente, le traitement fongicide a tout int&eacute;r&ecirc;t &agrave; &ecirc;tre appliqu&eacute; en soir&eacute;e, lorsque les femelles volent haut dans la v&eacute;g&eacute;tation et sont expos&eacute;es au traitement.</p>

<p style="text-align:justify">Concernant les pucerons, les populations sont faibles et les ennemis naturels, d&eacute;j&agrave; bien pr&eacute;sents devraient rapidement &eacute;teindre les populations. Quant aux crioc&egrave;res (= l&eacute;mas), les niveaux sont n&eacute;gligeables. Plus pr&eacute;sents dans les cultures de printemps, les crioc&egrave;res m&ecirc;me si les sympt&ocirc;mes sont forts visibles, ne justifient pas une intervention.</p>

<p style="text-align:justify">Les observations continuent la semaine prochaine.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>

<p>&nbsp;</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 11 juin.</span></p>
', 1, NULL, 99, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (205, 'Réseau « maladie en escourgeon »', 'left', 300, 225, '<p style="text-align:justify"><img align="" filer_id="435" height="" src="/filer/canonical/1555398543/435/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">La moiti&eacute; des parcelles ont atteint le stade 2i&egrave;me n&oelig;ud (BBCH32). Un peu moins d&rsquo;un quart des parcelles est d&eacute;j&agrave; au stade 3i&egrave;me n&oelig;ud (BBCH33) et le dernier quart au stade &laquo; derni&egrave;re feuille visible &raquo; (BBCH37). Les temp&eacute;ratures assez froides de la semaine n&rsquo;ont pas &eacute;t&eacute; favorables au d&eacute;veloppement des maladies.&nbsp; Les temp&eacute;ratures douces attendues pour cette semaine vont acc&eacute;l&eacute;rer le d&eacute;veloppement des plantes.&nbsp; Il convient d&rsquo;attendre maintenant le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39) pour r&eacute;aliser le traitement fongicide.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify"><img align="" filer_id="433" height="" src="/filer/canonical/1555398329/433/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a><strong> </strong>est observ&eacute;e &agrave; Ath, Frasnes et Lonz&eacute;e sur vari&eacute;t&eacute;s sensibles uniquement, soit dans 5 des 13 parcelles du r&eacute;seau. A Lonz&eacute;e, elle est pr&eacute;sente sur 5% des F-2. A Frasnes, elle est observ&eacute;e sur 2 &agrave; 10% des F-2 sur les vari&eacute;t&eacute;s sensibles &agrave; cette maladie. A Ath, elle est observ&eacute;e uniquement sur 10% des F-3. La s&eacute;v&eacute;rit&eacute; de l&rsquo;infection est faible dans tous les cas.</p>

<p style="text-align:justify"><span style="color:black">La </span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rhynchosporiose</a></strong><span style="color:black"> est pr&eacute;sente dans 5 des 13 parcelles, uniquement dans le bas des plantes. On l&rsquo;observe sur seulement 5% des F-3 &agrave; Pailhe. A Lonz&eacute;e, 15 &agrave; 55% des F-3 des vari&eacute;t&eacute;s sensibles sont touch&eacute;es mais avec </span>moins de 3% <span style="color:black">de surface infect&eacute;e. Dans le Hainaut, 8% des F-3 pr&eacute;sentent quelques taches mais la surface foliaire touch&eacute;e est de moins d&rsquo;1%. Cette maladie n&rsquo;est donc pas pr&eacute;occupante pour l&rsquo;instant.</span></p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">o&iuml;dium</a></strong><span style="color:black"> est observ&eacute; dans 9 des 13 parcelles du r&eacute;seau. Elle est pr&eacute;sente sur 5 &agrave; 30% des F-3 &agrave; Pailhe et 5% des F-2 dans une des parcelles. A Ath, on l&rsquo;observe sur 5 &agrave; 17% des F-2. A Frasnes, elle est pr&eacute;sente sur 12 &agrave; 27% des F-2 et moins de 4% des F-1 pr&eacute;sentent des sympt&ocirc;mes de cette maladie mais la surface foliaire infect&eacute;e reste faible. A Lonz&eacute;e, elle reste dans le bas de la v&eacute;g&eacute;tation sur 30% des F-3.</span></p>

<p style="text-align:justify">La <strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rouille naine</a></strong> est toujours visible dans presque toutes les parcelles du r&eacute;seau mais cette maladie a <strong>tr&egrave;s peu &eacute;volu&eacute; </strong>depuis la semaine derni&egrave;re. Dans le Hainaut, &agrave; Ath et Frasnes : 2 &agrave; 15% des F-1 sont touch&eacute;es et 46 &agrave; 50% des F-2 sur les vari&eacute;t&eacute;s sensibles. Dans la r&eacute;gion de Li&egrave;ge, cette maladie a &eacute;t&eacute; observ&eacute;e sur 10 &agrave; 15% des F-2 et 20 &agrave; 55% des F-3 selon les vari&eacute;t&eacute;s. Cette maladie a &eacute;t&eacute; aussi observ&eacute;e dans les parcelles &agrave; Lonz&eacute;e : 5 &agrave; 15% des F-2 et 5% &agrave; 85% des F-3.</p>

<p style="text-align:justify"><img align="" filer_id="438" height="" src="/filer/canonical/1555403185/438/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Recommandations </strong></u></p>

<p style="text-align:justify">De nouveau cette semaine, la pression phytosanitaire peut dans l&rsquo;ensemble &ecirc;tre consid&eacute;r&eacute;e comme faible et aucune des parcelles observ&eacute;es du r&eacute;seau n&rsquo;a<strong> </strong>encore atteint le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39). Jusqu&rsquo;ici cette saison, seule la pression en rouille naine ou en o&iuml;dium a pu localement &ecirc;tre pr&eacute;occupante sur vari&eacute;t&eacute;s sensibles &agrave; l&rsquo;une ou l&rsquo;autre de ces maladies (cf. avis de la semaine pass&eacute;e). Les temp&eacute;ratures plus douces de cette semaine et les faibles averses &eacute;parses pr&eacute;vues au milieu de la semaine ne devraient pas &ecirc;tre favorables aux maladies.</p>

<p style="text-align:justify"><strong>A ce stade, il est judicieux d&rsquo;attendre le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39) pour effectuer la protection fongicide de la culture. </strong>Pour les parcelles qui auraient &eacute;t&eacute; trait&eacute;es en d&eacute;but de montaison, il est souhaitable de respecter un d&eacute;lai de trois semaines avant l&rsquo;application du deuxi&egrave;me traitement. De cette mani&egrave;re, le premier traitement est mieux rentabilis&eacute; et la protection est &eacute;tal&eacute;e sur une plus longue p&eacute;riode.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 23 avril.</span></p>
', 1, NULL, 88, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (204, 'Point phytotechnique en froment', 'left', 300, 225, '<p><u><span style="color:black">Fumure et r&eacute;gulateur</span></u><span style="color:black"> : </span>Avec le regain de temp&eacute;rature plus &eacute;lev&eacute;e, il y a lieu de penser &agrave; r&eacute;aliser le r&eacute;gulateur sur vos froments plant&eacute;s en Octobre. De plus, ces froments approchent (en fonction des r&eacute;gions) le stade de redressement. La seconde fraction azot&eacute;e peut d&egrave;s lors &ecirc;tre appliqu&eacute;e (si ce n&rsquo;est d&eacute;j&agrave; fait).</p>

<p style="text-align:justify">Enfin pour les froments dont le semis a &eacute;t&eacute; r&eacute;alis&eacute; tardivement, on est toujours au stade tallage et la culture continue sa croissance.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 23 avril.</span></p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines et Froment de printemps</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf">RegAFrop.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froment d&#39;hiver, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf">RegFhET.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges et Seigles</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf">RegOS.pdf</a></td>
		</tr>
	</tbody>
</table>
', 1, NULL, 88, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (206, 'Réseau « maladie en froment »', 'left', 300, 225, '<p><img align="" filer_id="439" height="" original_image="false" src="/filer/canonical/1555412404/439/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p>La majorit&eacute; des parcelles (61%) de froment d&rsquo;hiver est au stade &laquo; &eacute;pi 1cm &raquo; (BBCH30), quelques-unes sont encore en &laquo; fin de tallage &raquo; (BBCH29) et certaines atteignent d&eacute;j&agrave; le stade &laquo; 1ier n&oelig;ud &raquo; (BBCH 31). La pression des maladies <strong>est faible </strong>et ne devrait pas &eacute;voluer dans les prochains jours vu les temp&eacute;ratures douces et les tr&egrave;s faibles pr&eacute;cipitations annonc&eacute;es cette semaine.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p><img align="" filer_id="440" height="" original_image="false" src="/filer/canonical/1555412589/440/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><img align="" filer_id="437" height="" src="/filer/canonical/1555399814/437/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La </strong><strong>septoriose</strong><span style="color:black"> est observ&eacute;e sur l&rsquo;ensemble des parcelles du r&eacute;seau dans le bas de la v&eacute;g&eacute;tation et principalement sur les vari&eacute;t&eacute;s sensibles. Les conditions m&eacute;t&eacute;orologiques pr&eacute;vues pour les prochains jours ne sont pas favorables aux nouvelles infections.</span></p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong><span style="color:black"> est peu pr&eacute;sent dans les parcelles. Seules 8 parcelles situ&eacute;es &agrave; Mortoux, Ath et Ellignies montrent des sympt&ocirc;mes sur les F-2 du moment &agrave; des fr&eacute;quences faibles (moins de 8% des plantes).</span></p>

<p style="text-align:justify"><strong>La rouille jaune</strong><span style="color:black"> n&rsquo;a &eacute;t&eacute; observ&eacute;e que dans 1 des 28 parcelles du r&eacute;seau: &agrave; Ellignies sur une vari&eacute;t&eacute; sensible. </span></p>

<p style="text-align:justify"><strong>La rouille brune</strong><span style="color:black"> n&rsquo;a pas &eacute;t&eacute; observ&eacute;e dans les parcelles du r&eacute;seau.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u><strong>Recommandations </strong></u></p>

<p style="text-align:justify"><strong>Les conditions actuelles permettent d&rsquo;&eacute;carter l&rsquo;id&eacute;e d&rsquo;un quelconque traitement en froment cette semaine</strong>. Les froments sont encore au stade &eacute;pi 1 cm ou 1er n&oelig;ud.&nbsp; La pression des maladies est faible et ne devrait pas &eacute;voluer vu les conditions climatiques douces et plut&ocirc;t s&egrave;ches annonc&eacute;es. Ces conditions permettent de ne pas envisager de traitement. Au stade 31, un traitement peut parfois &ecirc;tre envisag&eacute; contre la rouille jaune uniquement dans les parcelles emblav&eacute;es avec une vari&eacute;t&eacute; tr&egrave;s sensible &agrave; cette maladie et qui pr&eacute;sentent des <strong>foyers actifs</strong> de rouille jaune. Ce cas de figure est assez rare et actuellement nous ne sommes pas confront&eacute;s &agrave; cette situation.</p>

<p style="text-align:justify"><u>Les vari&eacute;t&eacute;s suivantes sont &agrave; surveiller vis-&agrave;-vis de la rouille jaune :</u></p>

<p style="text-align:justify">BENCHMARK, NEMO, REFLECTION, RGT REFORM, SAHARA, KWS DORSET et RGT TEXACO.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 23 avril.</span></p>
', 1, NULL, 88, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (209, 'Phytotechnie en escourgeons et orges', 'left', 300, 225, '<p style="text-align:justify"><strong>Escourgeon : </strong></p>

<p style="text-align:justify">Lorsque que les escourgeons auront atteint le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo;, la derni&egrave;re fraction d&rsquo;azote pourra &ecirc;tre appliqu&eacute;e <strong>en solide</strong>. Pour un sch&eacute;ma en 3 fractions, la recommandation du livre blanc est d&rsquo;appliquer 75N pour les vari&eacute;t&eacute;s hybrides et 60N pour les vari&eacute;t&eacute;s lign&eacute;es. Ces recommandations doivent &ecirc;tre adapt&eacute;es en fonction des conditions propres &agrave; chaque parcelle et &agrave; l&rsquo;&eacute;tat de la culture (cfr. &laquo; Calcul de la fumure azot&eacute;e &raquo; dans le livre blanc 2019).</p>

<p style="text-align:justify">Un r&eacute;gulateur de croissance pourra lui aussi &ecirc;tre appliqu&eacute; &agrave; ce stade (BBCH39).</p>

<p style="text-align:justify"><strong>Orge brassicole : </strong></p>

<p style="text-align:justify">Les orges brassicoles sem&eacute;es au mois de f&eacute;vrier &agrave; Gembloux ont atteint le stade &laquo; plein tallage &raquo;. Les orges sem&eacute;es fin mars &agrave; Liernu se trouvent eux au stade &laquo; d&eacute;but tallage &raquo;.</p>

<p style="text-align:justify"><strong>Avoine : </strong></p>

<p style="text-align:justify">Les avoines sem&eacute;es le 27 f&eacute;vrier &agrave; Gembloux ont atteint le stade &laquo; d&eacute;but tallage &raquo;, la premi&egrave;re fraction azot&eacute;e peut &ecirc;tre appliqu&eacute;e. La recommandation du livre blanc est d&rsquo;appliquer 40N au tallage.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 30 avril.</span></p>
', 1, NULL, 90, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (213, 'Adaptation de la liste des jets anti-dérives', 'left', 300, 225, '<p><strong><u>Mesures effectives visant &agrave; r&eacute;duire l&rsquo;impact des pesticides</u></strong></p>

<ul>
	<li style="text-align:justify">Interdiction de d&eacute;buter une pulv&eacute;risation lorsque la <strong>vitesse du vent</strong> est sup&eacute;rieure &agrave; 20 km/heure.</li>
</ul>

<ul>
	<li style="text-align:justify">Interdiction de pulv&eacute;riser &agrave; moins de 50 m&egrave;tres des <strong>bords de toute parcelle qui jouxte un site d&rsquo;&eacute;tablissement</strong> (cours de r&eacute;cr&eacute;ation, &eacute;coles, internats, cr&egrave;ches et infrastructures d&#39;accueil de l&#39;enfance) durant les heures de fr&eacute;quentation de celui-ci.</li>
</ul>

<p style="margin-left:18.0pt; margin-right:0cm"><span style="color:red">!!! Adaptation de la liste des jets anti-d&eacute;rives</span></p>

<ul>
	<li style="text-align:justify">Obligation d&rsquo;utiliser un <strong>mat&eacute;riel d&rsquo;application qui r&eacute;duit la d&eacute;rive</strong> de minimum 50%. Vu la r&eacute;duction du pourcentage d&rsquo;anti d&eacute;rive de certains jet dans la liste pr&eacute;c&eacute;demment &eacute;tablie, une p&eacute;riode transitoire de 3 ans a &eacute;t&eacute; mise en place pour ceux qui auraient achet&eacute;s des jets qui ne &laquo; correspondent pas aux nouvelles normes &raquo; (surlign&eacute; en jaune fluo dans le <a href="https://fytoweb.be/sites/default/files/guide/attachments/protection_des_eaux_de_surface_20190411.pdf">fichier suivant page 17</a> et suivantes)</li>
</ul>

<p style="text-align:justify">***</p>

<p style="text-align:justify">Les produits phyto ne comportant pas les m&ecirc;mes restrictions d&rsquo;utilisation en mati&egrave;re de <strong>zone tampon et de r&eacute;duction de la d&eacute;rive</strong>, le CADCO en collaboration avec Protect&rsquo;eau, a, pour votre facilit&eacute;, repris les diff&eacute;rentes zones tampon (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) dans les tableaux des produits au lien suivant : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>
', 2, NULL, 90, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (210, 'Point phytotechnique en froment', 'left', 300, 225, '<p><u><span style="color:black">Fumure et r&eacute;gulateur</span></u><span style="color:black"> :</span></p>

<p>Avec les temp&eacute;ratures que nous avons connues la semaine pass&eacute;e, les froments ont continu&eacute;s &agrave; se d&eacute;velopper.</p>

<p>Pour les semis d&rsquo;octobre, les froments sont &agrave; un stade 31 (parfois bien avanc&eacute; en fonction des vari&eacute;t&eacute;s et des r&eacute;gions).</p>

<p>Les semis tardifs sont plut&ocirc;t au redressement. Pour ces semis, il y a donc lieu de penser &agrave; r&eacute;aliser la fertilisation pour le redressement ainsi que l&rsquo;application d&rsquo;un r&eacute;gulateur.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 30 avril.</span></p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines et Froment de printemps</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf">RegAFrop.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froment d&#39;hiver, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf">RegFhET.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges et Seigles</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf">RegOS.pdf</a></td>
		</tr>
	</tbody>
</table>
', 1, NULL, 90, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (212, 'Réseau « maladie en froment »', 'left', 300, 225, '<p><img align="" filer_id="442" height="" src="/filer/canonical/1555962567/442/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute; </strong></u></p>

<p style="text-align:justify">Le temps sec et chaud de la semaine &eacute;coul&eacute;e a favoris&eacute; le d&eacute;veloppement des plantes plus que celui des maladies. Les parcelles &eacute;taient, le vendredi 19 avril, <strong>principalement au stade &laquo; &eacute;pi 1cm &raquo; (BBCH30) ou au stade &laquo; 1ier n&oelig;ud &raquo; (BBCH31</strong>). La rouille jaune est signal&eacute;e &agrave; quelques endroits sur des vari&eacute;t&eacute;s tr&egrave;s sensibles (REFLECTION, NEMO). L&rsquo;observation des parcelles est <strong>tr&egrave;s importante &agrave;</strong><strong> l&rsquo;arriv&eacute;e du stade 32</strong> de la culture afin de relever la pression en maladies &agrave; ce stade et adapter la protection en fonction de la pression notamment en septoriose ou en rouille jaune. Des pluies sont notamment annonc&eacute;es fin de la semaine.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p><img align="" filer_id="445" height="" src="/filer/canonical/1555962969/445/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La </strong><strong>septoriose</strong><span style="color:black"> </span> <span style="color:black">est observ&eacute;e sur l&rsquo;ensemble des parcelles du r&eacute;seau dans le bas de la v&eacute;g&eacute;tation et principalement sur les vari&eacute;t&eacute;s sensibles. Les conditions climatiques s&egrave;ches n&rsquo;ont pas favoris&eacute; de nouvelles infections sur les &eacute;tages sup&eacute;rieurs actuels. D&rsquo;apr&egrave;s le mod&egrave;le de pr&eacute;vision &eacute;pid&eacute;miologique PROCULTURE, des pluies pr&eacute;vues cette semaine pourraient permettre de nouvelles infections sur les feuilles sup&eacute;rieures mais il est encore trop t&ocirc;t pour juger de la n&eacute;cessit&eacute; d&rsquo;un traitement &agrave; ce stade. En effet, c&rsquo;est le stade &laquo;&nbsp;2i&egrave;me n&oelig;ud&nbsp;&raquo; (BBCH32) qui est le stade clef pour &eacute;valuer de l&rsquo;int&eacute;r&ecirc;t d&rsquo;un traitement contre cette maladie en froment, et la <strong>surveillance des parcelles &agrave; ce stade est donc fortement conseill&eacute;e</strong>. Toutefois, la combinaison d&rsquo;une vari&eacute;t&eacute; tol&eacute;rante et les observations au champ peuvent permettre de s&rsquo;affranchir d&rsquo;un premier traitement au stade 32 et de ne r&eacute;aliser que le traitement primordial au stade &laquo;&nbsp;derni&egrave;re feuille&nbsp;&raquo; (BBCH39).</span></p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong><span style="color:black"> </span> <span style="color:black">est pr&eacute;sent dans plusieurs parcelles. A Mortroux, des sympt&ocirc;mes sont visibles sur les F-2 du moment &agrave; des fr&eacute;quences faibles (moins de 15% des plantes). Dans le Namurois (Gesves et Mettet), des sympt&ocirc;mes sont visibles sur les F-2 du moment &agrave; des fr&eacute;quences faibles (moins de 5 &agrave; 15% des plantes).</span></p>

<p style="text-align:justify"><strong>La rouille jaune</strong><span style="color:black"> </span> <span style="color:black">n&rsquo;a &eacute;t&eacute; observ&eacute;e que dans 2 des 16 parcelles&nbsp;du r&eacute;seau: &agrave; Mortroux et Fexhe sur une vari&eacute;t&eacute; sensible (REFLECTION). Toutefois, des foyers actifs ont &eacute;t&eacute; signal&eacute;s dans la r&eacute;gion de Tournai et d&rsquo;Ath sur la vari&eacute;t&eacute; NEMO la semaine derni&egrave;re.</span></p>

<p style="text-align:justify"><strong>La rouille brune</strong><span style="color:black"> </span> <span style="color:black">n&rsquo;a pas encore &eacute;t&eacute; observ&eacute;e dans les parcelles du r&eacute;seau.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u><strong>Recommandations </strong></u></p>

<p style="text-align:justify"><strong>L&rsquo;observation des parcelles est primordiale &agrave; l&rsquo;approche du stade BCCH32</strong>. <span style="color:black">Les parcelles sont principalement au stade &eacute;pi 1 cm ou 1er n&oelig;ud, certaines (semis d&rsquo;octobre) sont proche du 2i&egrave;me n&oelig;ud (BBCH32). &nbsp;Au stade 31, un traitement peut parfois &ecirc;tre envisag&eacute; contre la rouille jaune uniquement dans les parcelles emblav&eacute;es avec une vari&eacute;t&eacute; tr&egrave;s sensible &agrave; cette maladie et qui pr&eacute;sentent des </span><strong>foyers actifs</strong> de rouille jaune. La septoriose est bien pr&eacute;sente dans les parcelles mais ne d&eacute;passent pas la F-2 du moment.</p>

<p style="text-align:justify"><u>Les vari&eacute;t&eacute;s suivantes sont &agrave; surveiller vis-&agrave;-vis de la rouille jaune :</u></p>

<p style="text-align:justify">BENCHMARK, NEMO, REFLECTION, RGT REFORM, SAHARA, KWS DORSET et RGT TEXACO.</p>

<p>&nbsp;
<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>
</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 30 avril.</span></p>
', 1, NULL, 90, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (214, 'Situation au 23 avril 2019', 'left', 300, 225, '<p style="text-align:justify">Les temp&eacute;ratures estivales du week-end pascal ont &eacute;t&eacute; favorables &agrave; la floraison du colza et &agrave; l&rsquo;activit&eacute; des abeilles.</p>

<p style="text-align:justify">Les charan&ccedil;ons des siliques, petits et de couleur gris-ardoise, sont &eacute;galement arriv&eacute;s ; ils sont visibles sur les hampes principales du colza.&nbsp; Le stade sensible des siliques se situe entre 2 et 4 cm ; l&rsquo;insecte y perfore des trous qui seront des portes d&rsquo;entr&eacute;e pour les c&eacute;cidomyies des siliques qui viendront plus tard et pourront y d&eacute;poser des &oelig;ufs qui, en se d&eacute;veloppant en larves, vont faire &eacute;clater pr&eacute;matur&eacute;ment les siliques.</p>

<p style="text-align:justify">Les m&eacute;lig&egrave;thes sont toujours tr&egrave;s pr&eacute;sents dans la culture.&nbsp; Certains champs pr&eacute;sentent des d&eacute;g&acirc;ts tr&egrave;s importants d&rsquo;avortement de boutons floraux qui n&rsquo;&eacute;volueront ni en fleurs ni en siliques ; la coloration jaune tarde &agrave; venir dans ces parcelles.&nbsp; Il faut donc v&eacute;rifier la formation des siliques avant un traitement fongicide.</p>

<p style="text-align:justify">En ce qui concerne la lutte contre les charan&ccedil;ons des siliques, le traitement insecticide ne doit s&#39;envisager que si les insectes sont abondants (seuil : 1 charan&ccedil;on pour 2 plantes). En effet, ce traitement touche &eacute;galement les m&eacute;lig&egrave;thes (les adultes, mais aussi les larves dans les fleurs), et participe donc au d&eacute;veloppement de la r&eacute;sistance de cet insecte.</p>

<p style="text-align:justify">Le traitement insecticide peut souvent &ecirc;tre limit&eacute; aux bordures de parcelles, o&ugrave; l&#39;insecte se concentre. Il doit imp&eacute;rativement &ecirc;tre appliqu&eacute; en dehors des heures de butinage des abeilles tr&egrave;s actives actuellement. Lien vers les listes des <strong><a href="http://www.cadcoasbl.be/p03_archives/avert2019/190423%20Insectcolza.pdf">insecticides autoris&eacute;s</a></strong>.</p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 91, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (211, 'Réseau « maladie en escourgeon »', 'left', 300, 225, '<p style="text-align:justify"><img align="" filer_id="443" height="" src="/filer/canonical/1555962611/443/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">A la veille de ce weekend de P&acirc;ques, la majorit&eacute; des parcelles avaient atteint le stade &laquo; derni&egrave;re feuille visible &raquo; (BBCH37), mais aucune n&rsquo;avait encore atteint le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39). La pression en maladies est faible et toujours localis&eacute;e dans le bas de la v&eacute;g&eacute;tation sur l&rsquo;ensemble des parcelles du r&eacute;seau, on peut d&egrave;s lors<strong> attendre le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo;</strong> (BBCH39) pour r&eacute;aliser un traitement fongicide contre les maladies du feuillage.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify"><img align="" filer_id="441" height="" src="/filer/canonical/1555962494/441/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a><strong> </strong> est peu pr&eacute;occupante&nbsp;: observ&eacute;e &agrave; Pailhe sur vari&eacute;t&eacute; sensible et sur seulement 5% des F-3 du moment. La s&eacute;v&eacute;rit&eacute; de l&rsquo;infection est faible dans tous les cas.</p>

<p style="text-align:justify"><span style="color:black">La </span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rhynchosporiose</a></strong><span style="color:black"> </span> <span style="color:black">n&rsquo;</span>est que rarement observ&eacute;e: <span style="color:black">pr&eacute;sente dans 4 des 7 parcelles, les sympt&ocirc;mes sont faibles et uniquement visibles dans le bas des plantes. On l&rsquo;observe sur 5 &agrave; 25% des F-3 &agrave; Pailhe. A Lonz&eacute;e, on observe quelques taches sur 10% des F-3 des vari&eacute;t&eacute;s sensibles. </span></p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">o&iuml;dium</a></strong><span style="color:black"> est observ&eacute; dans 2 des 7 parcelles du r&eacute;seau. Elle est pr&eacute;sente sur 10% des F-3 &agrave; Pailhe et 5% des F-3 &agrave; Lonz&eacute;e. </span></p>

<p style="text-align:justify">La <strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rouille naine</a></strong> est peu pr&eacute;occupante &eacute;galement&nbsp;: visible dans presque toutes les parcelles du r&eacute;seau, elle n&rsquo;atteint jamais les &eacute;tages foliaires sup&eacute;rieurs. Dans la r&eacute;gion de Li&egrave;ge, cette maladie a &eacute;t&eacute; observ&eacute;e sur 5 &agrave; 10% des F-2 et 10 &agrave; 55% des F-3 selon les vari&eacute;t&eacute;s. Cette maladie a &eacute;t&eacute; aussi observ&eacute;e dans les parcelles &agrave; Lonz&eacute;e&nbsp;: de 4 &agrave; 70% des F-3 selon la sensibilit&eacute; de la vari&eacute;t&eacute; &agrave; cette maladie.</p>

<p style="text-align:justify"><u><strong>Recommandations </strong></u></p>

<p style="text-align:justify">De nouveau cette semaine, la pression phytosanitaire peut dans l&rsquo;ensemble &ecirc;tre consid&eacute;r&eacute;e comme faible et aucune des parcelles observ&eacute;es du r&eacute;seau n&rsquo;a<strong> </strong>encore atteint le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39).<strong> Il est judicieux d&rsquo;attendre le stade derni&egrave;re feuille &eacute;tal&eacute;e pour effectuer la protection fongicide de la culture. </strong></p>

<p style="text-align:justify">Pour les parcelles qui auraient &eacute;t&eacute; trait&eacute;es en d&eacute;but de montaison (cas d&eacute;crit dans le <a href="http://www.cadcoasbl.be/p03_archives/avert2019/cadco2019-07.pdf">C07</a>) il est souhaitable de respecter un d&eacute;lai de trois semaines avant l&rsquo;application du deuxi&egrave;me traitement.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 30 avril.</span></p>
', 1, NULL, 90, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (271, 'Commission communale de constat des dégâts - appel à candidature', 'left', 300, 225, '<p><u>Commission communale de constat des d&eacute;g&acirc;ts - appel &agrave; candidature</u></p>

<p style="text-align:justify">Le SPW Agriculture, Ressources naturelles et Environnement a lanc&eacute; un appel &agrave; candidature comme expert-agriculteur ou expert agricole ou horticole, d&eacute;sign&eacute; par l&rsquo;Administration pour &ecirc;tre membre d&rsquo;une commission communale de constat des d&eacute;g&acirc;ts. <strong>Les informations sur cet appel peuvent &ecirc;tre obtenues via le lien suivant </strong>: <a href="https://agriculture.wallonie.be/-/candidature-en-tant-qu-expert-au-sein-de-la-commission-communale-de-constat-de-degats?inheritRedirect=true&amp;redirect=%2Faccueil">Portail Agriculture</a></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 100, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (275, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p><u>A l&rsquo;agenda</u> :</p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>Mardi 11/06/2019</td>
			<td>Upigny<br />
			10-13h</td>
			<td>Rencontre <strong>en ferme grande culture</strong>en conversion bio (Nature et progr&egrave;s). Ferme d&#39;Upigny. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></td>
		</tr>
		<tr>
			<td>Jeudi 13/06/2019</td>
			<td>Ath<br />
			13h00</td>
			<td>&deg;) <strong>Essais vari&eacute;t&eacute;s</strong> : 38 vari&eacute;t&eacute;s de <strong>froment d&#39;hiver</strong>, 11 vari&eacute;t&eacute;s de <strong>triticale</strong>, 6 vari&eacute;t&eacute;s <strong>d&#39;&eacute;peautre</strong><br />
			&deg;) <strong>Essai fumure</strong>, comparant diff&eacute;rents engrais organiques et diff&eacute;rentes doses<br />
			&deg;) Une parcelle de <strong>pois en agriculture Biologique</strong> avec une d&eacute;monstration d&eacute;sherbage<br />
			Rdvs &agrave; Ath, 301 rue de l&rsquo;agriculture (CARAH/CePiCOP). <a href="http://carah.be/33-experimentation-agronomique-avertissements/236-visite-des-essais-2019.html">Informations</a></td>
		</tr>
		<tr>
			<td>Jeudi 13/06/2019</td>
			<td>Rhisnes<br />
			10h00</td>
			<td>Visites <strong>essais vari&eacute;&eacute;ts bio froment d&#39;hiver, triticale et &eacute;peautre </strong> Domaine de la Falize(CRA-W)</td>
		</tr>
		<tr>
			<td>Vendredi 14/06/2019</td>
			<td>Acosse<br />
			14h00</td>
			<td>Visite de <strong>champ d&#39;essais vari&eacute;t&eacute;s froment d&#39;hiver et escourgeon </strong> (CRA-W/CePiCOP). Rue de Burdinne 8. Chez O. Gathy.</td>
		</tr>
		<tr>
			<td>Lundi 17/06/2019</td>
			<td>Gesves<br />
			14h00</td>
			<td>Visite de <strong>champ d&#39;essais vari&eacute;t&eacute;s froment</strong> (CRA-W/CePiCOP). Rue Borsu 1. Chez M. Luc Delloy.</td>
		</tr>
	</tbody>
</table>

<p>En <strong><span style="color:red">rouge</span></strong> les activit&eacute;s reconnues dans le cadre de la phytolicence <a href="http://www.pwrp.be/agenda-phytolicence">(voir agenda phytolicence complet ici)</a> Prenez votre carte d&#39;identit&eacute;. En <strong><span style="color:#00b050">vert</span></strong> : Rencontres en ferme &quot;alternatives aux pesticides chimiques de synth&egrave;se&quot;</p>

<p>&nbsp;</p>

<p style="text-align:justify"><u><strong>Voir la suite de l&rsquo;agenda</strong></u> : toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <strong><a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></strong></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 101, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (272, 'Vos terres sont-elles infestées de vulpin résistant ?', 'left', 300, 225, '<p style="text-align:justify">La r&eacute;sistance des vulpins aux herbicides est un ph&eacute;nom&egrave;ne largement r&eacute;pandu en Europe, notamment en France et au Royaume-Uni et cela n&#39;est pas sans poser certains probl&egrave;mes. Les agriculteurs anglais, par exemple, sont parfois contraints &agrave; mettre en &oelig;uvre des programmes herbicides &agrave; 3 passages incluant en moyenne 6 substances actives diff&eacute;rentes!</p>

<p style="text-align:justify">Actuellement, le ph&eacute;nom&egrave;ne en Belgique semble &ecirc;tre en expansion et n&rsquo;est plus confin&eacute; aux zones g&eacute;ographiques pr&eacute;c&eacute;demment connues comme les Polders, le Tournaisis et la r&eacute;gion de Fosses-la-Ville. Il n&#39;est pas rare de retrouver un peu partout dans nos campagnes des taches de vulpins d&eacute;passant des c&eacute;r&eacute;ales. Cela est-il d&ucirc; &agrave; traitement mal positionn&eacute;, &agrave; des conditions climatiques non optimales au moment du traitement, &agrave; un mauvais recouvrement, ou bien &agrave; la pr&eacute;sence de vulpins r&eacute;sistants?</p>

<p style="text-align:justify">L&rsquo;an pass&eacute;, l&rsquo;Unit&eacute; Protection des Plantes et Ecotoxicologie du CRA-W a men&eacute; une enqu&ecirc;te afin d&#39;&eacute;valuer la proportion de vulpins r&eacute;sistants en Wallonie et de d&eacute;terminer les pratiques permettant un meilleur contr&ocirc;le de ces r&eacute;sistances. Les agriculteurs ayant particip&eacute; ont re&ccedil;u par la suite une lettre d&rsquo;information concernant la r&eacute;sistance de leurs vulpins ainsi que certaines recommandations.</p>

<p style="text-align:justify">Cette ann&eacute;e encore, le CRA-W vous propose de participer &agrave; cette enqu&ecirc;te et de <strong>tester gratuitement la r&eacute;sistance des vulpins pr&eacute;sents dans vos terres</strong>. Il vous est simplement demand&eacute; de r&eacute;colter les semences de vulpin &agrave; maturit&eacute; (fin juin &ndash; d&eacute;but juillet) ainsi que de nous communiquer quelques informations culturales sur la parcelle. Les vulpins pr&eacute;lev&eacute;s seront test&eacute;s en serres durant l&rsquo;hiver et les r&eacute;sultats vous seront communiqu&eacute;s. Ce type de renseignement peut vous aider &agrave; mieux appr&eacute;hender le d&eacute;sherbage de vos c&eacute;r&eacute;ales et la lutte contre le vulpin en particulier.</p>

<p style="text-align:justify">Int&eacute;ress&eacute; ? Pour recevoir plus de d&eacute;tails sur la proc&eacute;dure, merci de prendre contact avec Pierre Hellin via l&rsquo;adresse <a href="mailto:p.hellin@cra.wallonie.be">p.hellin@cra.wallonie.be</a> ou via le num&eacute;ro de t&eacute;l&eacute;phone 081 87 40 06.</p>
', 2, NULL, 100, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (218, 'Réseau « maladie en froment »', 'left', 300, 225, '<p><img align="" filer_id="449" height="" src="/filer/canonical/1556567502/449/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute; </strong></u></p>

<p style="text-align:justify">Selon les dates de semis et les localit&eacute;s, les parcelles se situent majoritairement au stade premier n&oelig;ud (BBCH31) ou deuxi&egrave;me n&oelig;ud (BBCH32). <strong>L&rsquo;observation des parcelles &agrave; ce stade est indispensable</strong> pour adapter la bonne strat&eacute;gie.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p><img align="" filer_id="450" height="" src="/filer/canonical/1556567631/450/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La </strong><strong>septoriose</strong><span style="color:black"> est signal&eacute;e dans toutes les parcelles du r&eacute;seau &agrave; des fr&eacute;quences minimes voire nulles dans les emblavements r&eacute;alis&eacute;s avec des vari&eacute;t&eacute;s tol&eacute;rantes (CHEVIGNON, KWS DORSET, &hellip;). Sur vari&eacute;t&eacute;s sensibles, elle est pr&eacute;sente sur 30% des F-2 &agrave; Pailhe, sur 50% &agrave; Fexhe et 55% &agrave; Mortroux mais seulement sur 10% des F-2 &agrave; Lonz&eacute;e et autour de 20% des F-2 dans le Hainaut. La pression sur vari&eacute;t&eacute;s sensibles varie donc fort d&rsquo;une localit&eacute; &agrave; l&rsquo;autre. D&rsquo;apr&egrave;s</span> le mod&egrave;le &eacute;pid&eacute;miologique PROCULTURE, les infections pr&eacute;sentes actuellement sur la F-3 ne pourraient contaminer les &eacute;tages sup&eacute;rieurs qu&rsquo;aux environs du 10 mai et celles pr&eacute;sentent sur les F-2 ne seraient pas infectieuses avant trois semaines.</p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong><span style="color:black"> est pr&eacute;sent dans 13 parcelles du r&eacute;seau. A Li&egrave;ge, il est principalement signal&eacute; sur les F-3 et dans certains rares cas, il est visible sur 10% des F-2. Dans le Hainaut, il atteint maximum 2% des F-2 du moment. A Lonz&eacute;e, on l&rsquo;observe sur 5% des F-2.</span></p>

<p style="text-align:justify"><strong>La rouille jaune</strong> est observ&eacute;e dans 7 parcelles dans notre r&eacute;seau. A Pailhe, Mortroux et Fexhe, des foyers sont observ&eacute;s dans les champs et des pustules sont visibles notamment sur les F-2 de vari&eacute;t&eacute;s sensibles. A Ath et Ellignies, des pustules sont &eacute;galement visibles sur F-2 de vari&eacute;t&eacute;s sensibles (NEMO et RAGNAR). Des foyers ont &eacute;galement &eacute;t&eacute; mentionn&eacute;s dans le Namurois, &agrave; Meux sur RELFECTION et vers Namur sur COSMOS (vari&eacute;t&eacute; d&rsquo;&eacute;peautre).</p>

<p style="text-align:justify"><strong>La rouille brune</strong> <span style="color:black">n&rsquo;est pas encore signal&eacute;e dans les parcelles du r&eacute;seau d&rsquo;observation.</span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong>Recommandations : </strong></p>

<ul>
	<li style="text-align:justify">Pour les quelques parcelles encore au stade 31, seule la pr&eacute;sence de foyers actifs de rouille jaune sur une vari&eacute;t&eacute; sensible peut amener &agrave; envisager un traitement.</li>
</ul>

<ul>
	<li style="text-align:justify">Pour les parcelles au stade 32, un traitement est envisageable si une pr&eacute;sence significative (plus de 10% des plantes) de sympt&ocirc;mes de rouille jaune est observ&eacute;e sur une vari&eacute;t&eacute; peu tol&eacute;rante. Un traitement au stade 32 peut aussi &ecirc;tre envisag&eacute; sur une vari&eacute;t&eacute; tr&egrave;s peu tol&eacute;rante &agrave; la septoriose si plus de 20% des F-2 pr&eacute;sentent des sympt&ocirc;mes de cette maladie (tr&egrave;s rares cas). <strong>Dans les parcelles o&ugrave; la rouille jaune est absente ou ne n&eacute;cessite pas d&rsquo;intervenir, et o&ugrave; la pression des autres maladies est faible, le traitement peut &ecirc;tre postpos&eacute; au stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39). </strong></li>
</ul>

<p style="text-align:justify">Faire l&rsquo;impasse sur le traitement au stade 32 permet d&rsquo;&eacute;viter deux inconv&eacute;nients : premi&egrave;rement, cela &eacute;vite de devoir effectuer un second traitement 3-4 semaines apr&egrave;s le premier et d&rsquo;autre part, comme les bl&eacute;s sont en g&eacute;n&eacute;ral au stade &eacute;piaison 3-4 semaines apr&egrave;s le stade 32, la derni&egrave;re feuille n&rsquo;est donc pas prot&eacute;g&eacute;e d&egrave;s sa sortie. Faire l&rsquo;impasse donne ainsi la possibilit&eacute; de ne traiter qu&rsquo;une fois sur la saison si les conditions le permettent et la derni&egrave;re feuille est prot&eacute;g&eacute;e d&egrave;s sa sortie. Au besoin, un traitement relais au stade floraison permet de contr&ocirc;ler les maladies plus tardives.</p>

<p style="text-align:justify"><u>Les vari&eacute;t&eacute;s suivantes sont &agrave; surveiller vis-&agrave;-vis de la rouille jaune :</u></p>

<p style="text-align:justify">BENCHMARK, NEMO, REFLECTION, RGT REFORM, SAHARA, KWS DORSET et RGT TEXACO.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 07 mai.</span></p>
', 1, NULL, 92, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (215, 'Phytotechnie en orge brassicole et avoine', 'left', 300, 225, '<p><strong>Orge brassicole : </strong>Les orges brassicoles sem&eacute;es les 27 f&eacute;vrier ont atteint le stade fin tallage &agrave; Gembloux. A Liernu, les orges sem&eacute;es le 27 mars ont atteint le stade plein tallage.</p>

<p style="text-align:justify"><strong>Avoine : </strong> Les avoines sem&eacute;es &agrave; Gembloux le 27 mars se trouvent au stade plein tallage.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 07 mai.</span></p>
', 1, NULL, 92, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (270, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p><u>A l&rsquo;agenda</u> :</p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>Mardi 11/06/2019</td>
			<td>Upigny<br />
			10-13h</td>
			<td>Rencontre <strong>en ferme grande culture</strong>en conversion bio (Nature et progr&egrave;s). Ferme d&#39;Upigny. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></td>
		</tr>
		<tr>
			<td>Jeudi 13/06/2019</td>
			<td>Ath<br />
			13h00</td>
			<td>&deg;) <strong>Essais vari&eacute;t&eacute;s</strong> : 38 vari&eacute;t&eacute;s de <strong>froment d&#39;hiver</strong>, 11 vari&eacute;t&eacute;s de <strong>triticale</strong>, 6 vari&eacute;t&eacute;s <strong>d&#39;&eacute;peautre</strong><br />
			&deg;) <strong>Essai fumure</strong>, comparant diff&eacute;rents engrais organiques et diff&eacute;rentes doses<br />
			&deg;) Une parcelle de <strong>pois en agriculture Biologique</strong> avec une d&eacute;monstration d&eacute;sherbage<br />
			Rdvs &agrave; Ath, 301 rue de l&rsquo;agriculture (CARAH/CePiCOP). <a href="http://carah.be/33-experimentation-agronomique-avertissements/236-visite-des-essais-2019.html">Informations</a></td>
		</tr>
		<tr>
			<td>Jeudi 13/06/2019</td>
			<td>Rhisnes<br />
			10h00</td>
			<td>Visites <strong>essais vari&eacute;&eacute;ts bio froment d&#39;hiver, triticale et &eacute;peautre </strong> Domaine de la Falize(CRA-W)</td>
		</tr>
		<tr>
			<td>Vendredi 14/06/2019</td>
			<td>Acosse<br />
			14h00</td>
			<td>Visite de <strong>champ d&#39;essais vari&eacute;t&eacute;s froment d&#39;hiver et escourgeon </strong> (CRA-W/CePiCOP). Rue de Burdinne 8. Chez O. Gathy.</td>
		</tr>
		<tr>
			<td>Lundi 17/06/2019</td>
			<td>Gesves<br />
			14h00</td>
			<td>Visite de <strong>champ d&#39;essais vari&eacute;t&eacute;s froment</strong> (CRA-W/CePiCOP). Rue Borsu 1. Chez M. Luc Delloy.</td>
		</tr>
	</tbody>
</table>

<p>En <strong><span style="color:red">rouge</span></strong> les activit&eacute;s reconnues dans le cadre de la phytolicence <a href="http://www.pwrp.be/agenda-phytolicence">(voir agenda phytolicence complet ici)</a> Prenez votre carte d&#39;identit&eacute;. En <strong><span style="color:#00b050">vert</span></strong> : Rencontres en ferme &quot;alternatives aux pesticides chimiques de synth&egrave;se&quot;</p>

<p>&nbsp;</p>

<p style="text-align:justify"><u><strong>Voir la suite de l&rsquo;agenda</strong></u> : toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <strong><a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></strong></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 100, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (217, 'Réseau « maladie en escourgeon »', 'left', 300, 225, '<p style="text-align:justify"><img align="" filer_id="446" height="" src="/filer/canonical/1556567145/446/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>R&eacute;sum&eacute;</strong></u></p>

<p style="text-align:justify">Toutes parcelles du r&eacute;seau ont d&eacute;pass&eacute; le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39) et certaines atteignent m&ecirc;me le stade d&rsquo;&eacute;mergence de l&rsquo;&eacute;pi (BBCH50). L&rsquo;o&iuml;dium ainsi que la rouille naine sont toujours bien pr&eacute;sentes dans les parcelles du r&eacute;seau, qui pour rappel, sont des parcelles non-trait&eacute;es.</p>

<p style="text-align:justify"><u><strong>Avancement des cultures</strong></u></p>

<p style="text-align:justify"><img align="" filer_id="448" height="" src="/filer/canonical/1556567384/448/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><strong>Pression en maladies </strong>(observations sur des parcelles non-trait&eacute;es)</p>

<p style="text-align:justify">L<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a> est rare dans le Hainaut sur des &eacute;tages foliaires F3 (feuille d&eacute;finitive, voir sch&eacute;ma ci-dessus) &agrave; une fr&eacute;quence de maximum 5%. Il en est de m&ecirc;me &agrave; Lonz&eacute;e. En r&eacute;gion li&eacute;geoise, on l&rsquo;observe sur 15% des F3.</p>

<p style="text-align:justify"><span style="color:black">La </span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rhynchosporiose</a></strong><span style="color:black"> n&rsquo;est signal&eacute; que dans 4 parcelles (&agrave; Lonz&eacute;e et Pailhe). Elle atteint &agrave; Lonz&eacute;e 5% des F3 et 10% des F4 &agrave; Pailhe.</span></p>

<p style="text-align:justify"><span style="color:black">L&rsquo;</span><strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">o&iuml;dium</a></strong><span style="color:black"> </span> <span style="color:black">est observ&eacute; &agrave; une fr&eacute;quence de maximum 3% sur des F2 dans le Hainaut (Frasnes et Ath). A Lonz&eacute;e, il est observ&eacute; les F4 &agrave; des fr&eacute;quences de maximum 20%.</span></p>

<p style="text-align:justify">La <strong><a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">rouille naine</a></strong><span style="color:black"> se retrouve sur les F2 dans la r&eacute;gion de Namur et du Hainaut. Elle est pr&eacute;sente &agrave; Lonz&eacute;e sur 10% des plantes observ&eacute;es, sur la F2 d&eacute;finitive des vari&eacute;t&eacute;s sensibles et sur 40% des F2 d&eacute;finitives &agrave; Ath. </span></p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u><strong>Recommandations </strong></u></p>

<p style="text-align:justify">La p&eacute;riode s&rsquo;&eacute;talant entre le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39) et le stade &laquo; sortie des barbes &raquo; (BBCH49) est <strong>l&rsquo;intervalle pivot</strong> pour la protection fongicide de l&rsquo;escourgeon. Un traitement fongicide <strong>complet et r&eacute;manent</strong> &agrave; ce stade permettra de lutter contre les maladies d&eacute;j&agrave; pr&eacute;sentes et de pr&eacute;venir l&rsquo;apparition de la ramulariose Il est conseill&eacute; d&rsquo;utiliser les sp&eacute;cialit&eacute;s &agrave; base de carboxamides en m&eacute;lange avec une triazole et/ou une strobilurine. Il est &eacute;galement conseill&eacute; d&rsquo;appliquer du chlorothalonil car c&rsquo;est le dernier produit encore r&eacute;ellement efficace contre la ramulariose.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 07 mai.</span></p>
', 1, NULL, 92, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (216, 'Point phytotechnique en froment', 'left', 300, 225, '<p><u><span style="color:black">Fumure et r&eacute;gulateur</span></u><span style="color:black"> : </span>Au niveau des fumures et des r&eacute;gulateurs, aucune intervention n&rsquo;est &agrave; r&eacute;aliser cette semaine. En effet, pour les froments sem&eacute;s en octobre, ceux-ci ont atteint un stade 32 et les semis tardifs sont caract&eacute;ris&eacute;s par un stade 30 (seconde application et r&eacute;gulateur normalement d&eacute;j&agrave; r&eacute;alis&eacute;).<strong> </strong></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 07 mai.</span></p>
', 1, NULL, 92, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (249, 'Avoine', 'left', 300, 225, '<p style="text-align:justify">Les avoines sem&eacute;es le 27 f&eacute;vrier &agrave; Gembloux sont actuellement au stade d&eacute;but gonflement (BBCH 41). Les plantes observ&eacute;es sont saines, il n&rsquo;y a donc pas lieu d&rsquo;appliquer un traitement fongicide pour le moment.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p style="text-align:justify"><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 04 juin.</span></p>
', 1, NULL, 97, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (219, 'Adaptation de la liste des jets anti-dérives', 'left', 300, 225, '<p><strong><u>Mesures effectives visant &agrave; r&eacute;duire l&rsquo;impact des pesticides</u></strong></p>

<ul>
	<li style="text-align:justify">Interdiction de d&eacute;buter une pulv&eacute;risation lorsque la <strong>vitesse du vent</strong> est sup&eacute;rieure &agrave; 20 km/heure.</li>
</ul>

<ul>
	<li style="text-align:justify">Interdiction de pulv&eacute;riser &agrave; moins de 50 m&egrave;tres des <strong>bords de toute parcelle qui jouxte un site d&rsquo;&eacute;tablissement</strong> (cours de r&eacute;cr&eacute;ation, &eacute;coles, internats, cr&egrave;ches et infrastructures d&#39;accueil de l&#39;enfance) durant les heures de fr&eacute;quentation de celui-ci.</li>
</ul>

<p style="margin-left:18.0pt; margin-right:0cm"><span style="color:red">!!! Adaptation de la liste des jets anti-d&eacute;rives</span></p>

<ul>
	<li style="text-align:justify">Obligation d&rsquo;utiliser un <strong>mat&eacute;riel d&rsquo;application qui r&eacute;duit la d&eacute;rive</strong> de minimum 50%. Vu la r&eacute;duction du pourcentage d&rsquo;anti d&eacute;rive de certains jet dans la liste pr&eacute;c&eacute;demment &eacute;tablie, une p&eacute;riode transitoire de 3 ans a &eacute;t&eacute; mise en place pour ceux qui auraient achet&eacute;s des jets qui ne &laquo; correspondent pas aux nouvelles normes &raquo; (surlign&eacute; en jaune fluo dans le <a href="https://fytoweb.be/sites/default/files/guide/attachments/protection_des_eaux_de_surface_20190411.pdf">fichier suivant page 17</a> et suivantes)</li>
</ul>

<p>La fiche technique PROTECT&rsquo;eau sur les buses anti-d&eacute;rive reconnues en Belgique vient d&rsquo;&ecirc;tre mise &agrave; jour. Vous la trouverez sur le <a href="https://protecteau.be/resources/shared/publications/fiches-techniques/Phyto/19.04.Buses_anti-derive_reconnues.pdf" target="_blank">site internet</a></p>

<p>Notez &eacute;galement qu&rsquo;un syst&egrave;me de r&eacute;tention sera obligatoire dans tous les locaux phyto de plus de 25kg, &agrave; partir du 1er juin 2019. Plus d&rsquo;infos <span style="color:#0070c0"><a href="https://gallery.mailchimp.com/541d7c6321871d27d3c43ca72/files/ca565b85-d75a-4bdf-8d1a-7df47e4c39dc/flyer_phytoVF.pdf" target="_blank"><span style="color:#0070c0">ici</span></a></span></p>

<p style="text-align:right">La cellule phyto de PROTECT&rsquo;eau, 081 25 76 79</p>

<p style="text-align:justify">***</p>

<p style="text-align:justify">Les produits phyto ne comportant pas les m&ecirc;mes restrictions d&rsquo;utilisation en mati&egrave;re de <strong>zone tampon et de r&eacute;duction de la d&eacute;rive</strong>, le CADCO en collaboration avec PROTECT&rsquo;eau, a, pour votre facilit&eacute;, repris les diff&eacute;rentes zones tampon (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) dans les tableaux des produits au lien suivant : <a href="http://www.cadcoasbl.be/p09_biblio.html#art0002">ici</a></p>
', 2, NULL, 92, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (224, 'Réseau « maladie en escourgeon »', 'left', 300, 225, '<p><u><strong>Rappel des recommandations </strong></u></p>

<p style="text-align:justify">Toutes les parcelles ayant d&eacute;pass&eacute; le stade derni&egrave;re feuille &eacute;tal&eacute;e, il n&rsquo;y a plus d&rsquo;observation syst&eacute;matique pr&eacute;vue dans notre r&eacute;seau. La p&eacute;riode s&rsquo;&eacute;talant entre le stade &laquo; derni&egrave;re feuille &eacute;tal&eacute;e &raquo; (BBCH39) et le stade &laquo; sortie des barbes &raquo; (BBCH49) est <strong>l&rsquo;intervalle pivot</strong> pour la protection fongicide de l&rsquo;escourgeon. Un traitement fongicide <strong>complet et r&eacute;manent</strong> &agrave; ce stade permet de lutter contre les maladies d&eacute;j&agrave; pr&eacute;sentes et de pr&eacute;venir l&rsquo;apparition de la ramulariose Il est conseill&eacute; d&rsquo;utiliser les sp&eacute;cialit&eacute;s &agrave; base de carboxamides en m&eacute;lange avec une triazole et/ou une strobilurine. Il est &eacute;galement conseill&eacute; d&rsquo;appliquer du chlorothalonil car c&rsquo;est le dernier produit encore r&eacute;ellement efficace contre la ramulariose.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A.Nysten</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 14 mai.</span></p>
', 1, NULL, 94, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (221, 'Situation au 30 avril 2019', 'left', 300, 225, '<p style="text-align:justify">Alors que le colza est actuellement en pleine floraison et pr&eacute;sente une belle coloration jaune, certains champs pr&eacute;sentent des difficult&eacute;s &agrave; fleurir.</p>

<p style="text-align:justify">De nombreux boutons floraux n&rsquo;&eacute;voluent ni en fleurs ni en siliques.&nbsp; L&agrave; o&ugrave; les boutons ont avort&eacute;, il reste des p&eacute;doncules seuls sur les hampes principales et secondaires.&nbsp; La forte pression des m&eacute;lig&egrave;thes lors de ce printemps en est, en partie, responsable.&nbsp;</p>

<p style="text-align:justify">L&rsquo;alimentation des plantes qui ne fleurissent pas normalement, est perturb&eacute;e.&nbsp; On observe des tiges courb&eacute;es et creuses.&nbsp; A l&rsquo;int&eacute;rieur de ces tiges, des larves de charan&ccedil;ons de la tige sont en train de s&rsquo;attaquer &agrave; la moelle.&nbsp; Le manque de pr&eacute;cipitations et la s&eacute;cheresse accentuent le stress hydrique de ces plantes.</p>

<p style="text-align:justify">Les d&eacute;g&acirc;ts caus&eacute;s par ces insectes ravageurs sont un gros probl&egrave;me pour lequel il n&rsquo;y a actuellement plus de solution insecticide &agrave; appliquer.&nbsp; Les champs qui n&rsquo;ont pas re&ccedil;u d&rsquo;insecticide au printemps sont les plus touch&eacute;s ; d&rsquo;autres champs trait&eacute;s souffrent &eacute;galement.</p>

<p style="text-align:justify">La pression des insectes ravageurs est tr&egrave;s importante en colza cette ann&eacute;e, comme en 2018.&nbsp; Toute implantation difficile du colza qui aurait d&eacute;j&agrave; souffert de la s&eacute;cheresse &agrave; l&rsquo;automne, se marque actuellement par une mauvaise alimentation des plantes dont les besoins sont tr&egrave;s &eacute;lev&eacute;s &agrave; cette p&eacute;riode de floraison.</p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">Christine Cartrysse, APPO, Centre Pilote CePiCOP<br />
Michel De Proft, Expert scientifique CRA-W</p>

<p>&nbsp;</p>

<p style="text-align:right"><em>Les donn&eacute;es d&rsquo;observation sont issues d&rsquo;un r&eacute;seau de pi&eacute;geage du colza rassemblant des<br />
observateurs de l&rsquo;APPO, du CADCO, du CARAH, du CPL-V&eacute;g&eacute;mar, du CRA-W, de la DGARNED&eacute;veloppement<br />
et de l&rsquo;OPA de Ciney, et couvrant les diff&eacute;rentes r&eacute;gions de production du colza.</em></p>
', 1, NULL, 93, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (222, 'Phytotechnie en orge brassicole et avoine', 'left', 300, 225, '<p style="text-align:justify"><strong>Avoine : </strong></p>

<p style="text-align:justify">Les avoines sem&eacute;es le 27 f&eacute;vrier &agrave; Gembloux se trouvent actuellement au stade redressement. A ce stade la deuxi&egrave;me fraction d&rsquo;azote peut &ecirc;tre appliqu&eacute;e.</p>

<p style="text-align:justify">La fumure de r&eacute;f&eacute;rence est :</p>

<ul>
	<li>80-100 unit&eacute;s fractionn&eacute;es en deux applications : 1/3 au tallage, 2/3 au redressement</li>
	<li>En r&eacute;gion froide, 120 unit&eacute;s fractionn&eacute;es en deux applications : 1/3 au tallage, 2/3 au redressement</li>
</ul>

<p>&nbsp;</p>

<p><strong>Orge brassicole : </strong></p>

<table align="left" border="1" cellspacing="0" class="MsoTableLightShadingAccent1" style="border-collapse:collapse; border:solid #93a299 1.0pt; margin-left:4.8pt; margin-right:4.8pt; width:525.05pt">
	<tbody>
		<tr>
			<td style="width:63.2pt">
			<p style="text-align:justify"><strong>R&eacute;seau</strong></p>
			</td>
			<td style="width:461.85pt">
			<p style="text-align:justify"><strong>3 parcelles r&eacute;parties dans les localit&eacute;s : Hainaut (Vaudignies) et Namur (Gembloux, Liernu)</strong></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">Les orges brassicoles sem&eacute;es le 21 f&eacute;vrier &agrave; Vaudignies se trouvent actuellement au stade BBCH 31 (1er n&oelig;ud). Les premiers sympt&ocirc;mes d&rsquo;helminthosporiose ont &eacute;t&eacute; observ&eacute;s dans cet essai, cependant, la pression de la maladie est faible (50% des F-3 touch&eacute;es avec une s&eacute;v&eacute;rit&eacute; de 0.42%). La pression de la maladie n&rsquo;est donc pas pr&eacute;occupante pour le moment. Nous rappelons qu&rsquo;en g&eacute;n&eacute;ral, un traitement contre l&rsquo;ensemble des maladies au stade BBCH39 (derni&egrave;re feuille &eacute;tal&eacute;e) est suffisant.</p>

<p style="text-align:justify">A Gembloux, les orges sem&eacute;es le 27 f&eacute;vrier ont atteint le stade redressement (BBCH30). A Liernu, les orges sem&eacute;es le 27 mars se trouvent au stade plein-tallage.</p>

<p style="text-align:justify">Pour les parcelles ayant atteint le stade BBCH30 (&eacute;pi 1cm), une deuxi&egrave;me application de fumure peut &ecirc;tre appliqu&eacute;e <strong>si la culture parait carenc&eacute;e</strong>, notre recommandation est d&rsquo;appliquer entre 20 et 40 kg N/ha. Dans le cas o&ugrave; les reliquats azot&eacute;s de la parcelle sont &eacute;lev&eacute;s, il est recommand&eacute; de ne pas appliquer cette deuxi&egrave;me fraction afin de ne pas d&eacute;passer la teneur en prot&eacute;ine maximale autoris&eacute;e.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 14 mai.</span></p>
', 1, NULL, 94, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (276, 'Commission communale de constat des dégâts - appel à candidature', 'left', 300, 225, '<p><u>Commission communale de constat des d&eacute;g&acirc;ts - appel &agrave; candidature</u></p>

<p style="text-align:justify">Le SPW Agriculture, Ressources naturelles et Environnement a lanc&eacute; un appel &agrave; candidature comme expert-agriculteur ou expert agricole ou horticole, d&eacute;sign&eacute; par l&rsquo;Administration pour &ecirc;tre membre d&rsquo;une commission communale de constat des d&eacute;g&acirc;ts. <strong>Les informations sur cet appel peuvent &ecirc;tre obtenues via le lien suivant </strong>: <a href="https://agriculture.wallonie.be/-/candidature-en-tant-qu-expert-au-sein-de-la-commission-communale-de-constat-de-degats?inheritRedirect=true&amp;redirect=%2Faccueil">Portail Agriculture</a></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 101, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (227, 'Phytotechnie en orge brassicole et avoine', 'left', 300, 225, '<p style="text-align:justify"><strong>Avoine : </strong></p>

<p>Les avoines sem&eacute;es le 27 f&eacute;vrier &agrave; Gembloux se trouvent actuellement au stade &eacute;pis &agrave; 1cm (BBCH31). Si vos avoines se trouvent au stade redressement (BBCH30), la deuxi&egrave;me fraction d&rsquo;azote peut &ecirc;tre appliqu&eacute;e. La fumure de r&eacute;f&eacute;rence est :</p>

<ul>
	<li>80-100 unit&eacute;s fractionn&eacute;es en deux applications : 1/3 au tallage, 2/3 au redressement</li>
	<li>En r&eacute;gion froide, 120 unit&eacute;s fractionn&eacute;es en deux applications : 1/3 au tallage, 2/3 au redressement</li>
</ul>

<p><strong>Orge brassicole : </strong></p>

<table align="left" border="1" cellspacing="0" class="MsoTableLightShadingAccent1" style="border-collapse:collapse; border:solid #93a299 1.0pt; margin-left:4.8pt; margin-right:4.8pt; width:525.05pt">
	<tbody>
		<tr>
			<td style="width:63.2pt">
			<p style="text-align:justify"><strong>R&eacute;seau</strong></p>
			</td>
			<td style="width:461.85pt">
			<p style="text-align:justify"><strong>2 parcelles r&eacute;parties dans les localit&eacute;s : Namur (Gembloux, Liernu)</strong></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u>A Gembloux</u> : Les orges brassicoles sem&eacute;es &agrave; la fin du mois de f&eacute;vrier se trouvent au stade &eacute;pis &agrave; 1cm (BBCH31). La pression des maladies est actuellement faible (la rhynchosporiose est pr&eacute;sente sur 20% des F6 avec une s&eacute;v&eacute;rit&eacute; de 1%, la rouille naine est pr&eacute;sente sur 15% des F6 avec une s&eacute;v&eacute;rit&eacute; de 0.75%). Notre conseil est donc d&rsquo;attendre le stade BBCH39 (derni&egrave;re feuille &eacute;tal&eacute;e) pour appliquer un traitement fongicide.</p>

<p style="text-align:justify"><u>A Liernu</u> : Les orges brassicoles sem&eacute;es fin mars se trouvent au stade fin tallage. <strong>Si la culture parait carenc&eacute;e,</strong> une deuxi&egrave;me application de fumure pourra &ecirc;tre appliqu&eacute;e au stade redressement (BBCH30), notre recommandation est d&rsquo;appliquer entre 20 et 40 kgN/ha. Dans le cas o&ugrave; les reliquats azot&eacute;s de la parcelle sont &eacute;lev&eacute;s, il est recommand&eacute; de ne pas appliquer cette deuxi&egrave;me fraction afin de ne pas d&eacute;passer la teneur en prot&eacute;ine maximale autoris&eacute;e.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 21 mai.</span></p>
', 1, NULL, 95, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (233, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les r&eacute;gulateurs de croissance, les herbicides, fongicides, les insecticides &hellip; <strong> </strong></p>

<p style="text-align:justify"><strong>Cliquez sur le lien &quot;cellule de droite du tableau ci-dessous&quot; pour ouvrir le fichier ad hoc.<em> </em></strong><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits.</p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>01 avril 2019</td>
			<td><strong>AGENT ANTI-MOUSSANT</strong></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/09AgentAntiMoussant.pdf">Anti-moussant.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES FONGICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongAvoine.pdf">FongA.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froments, Seigles, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0705FongEpFroSeiTri.pdf">FongEpFroSeiTri.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Froments-Autoris&eacute;s contre Rouille jaune</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0704FroRouilleJaune.pdf">FongFRJ.pdf</a></td>
		</tr>
		<tr>
			<td>11 f&eacute;v. 2019</td>
			<td>Froments-Autoris&eacute;s contre Fusariose des &eacute;pis</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1207FroFusariose.pdf">FongFusariose.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongOrges.pdf">FongO.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES HERBICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Version 2019</td>
			<td>Sensibilit&eacute; des adventices</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14sensibiliteadventices.pdf">SensAdv.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Mode d&#39;actions des herbicides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14ModeAction.pdf">ModeActcion.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Sensibilit&eacute; au Chlortoluron</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/2016PJ- sensibChlortoluron.pdf">SensChlo.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Les additifs</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/08HerbicidesAdditifs.pdf">HerbAdd.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>En pr&eacute;-&eacute;mergence de la c&eacute;r&eacute;ale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf">HerbPreC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1510HerbPreEp.pdf">HerbPreEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>De la lev&eacute;e au d&eacute;but tallage</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf">HerbLTalC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalEp.pdf">HerbLTalEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Du d&eacute;but tallage au gonflement de la gaine</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFC.pdf">HerbTalDFC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFEp.pdf">HerbTalDFEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Sur c&eacute;r&eacute;ales &agrave; maturit&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbM.pdf">HerbM.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Toutes cultures</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCultures.pdf">HerbTC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>toutes cultures, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCulturessa.pdf">HerbTCsa.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Terres agricoles en interculture</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCultures.pdf">HerbIC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>interculture, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCulturessa.pdf">HerbICSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES INSECTICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>C&eacute;cidomyies</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf">CecidSA.pdf</a></td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>Pucerons-&Eacute;t&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsEteCerealesSA.pdf">InsEteCSA.pdf</a></td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>Pucerons vecteurs de JNO</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">InsJNOCSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES MOLLUSCICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707MolluscicideCereales.pdf">MolC.pdf</a></td>
		</tr>
		<tr>
			<td>INFO</td>
			<td>Granul&eacute;s anti-limaces : pas sans risques !</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0809AntiLimacesSPF.pdf">AntiLim.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines et Froment de printemps</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf">RegAFrop.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froment d&#39;hiver, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf">RegFhET.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges et Seigles</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf">RegOS.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>Les retraits d&#39;autorisation &quot;pr&eacute;-d&eacute;termin&eacute;s&quot;</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>Date limite d&#39;utilisation &quot;produits c&eacute;r&eacute;ales&quot;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0709LiquidationStock.pdf">LiquidationStock.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES TRAITEMENTS </strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>20 ao&ucirc;t 2018</td>
			<td>pour c&eacute;r&eacute;ales stock&eacute;es<br />
			pour locaux de stockage vides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TraitStoLocStoCereales.pdf">TStoLocStoC.pdf</a></td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>de semence : C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">TSemC.pdf</a></td>
		</tr>
	</tbody>
</table>
', 2, NULL, 95, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (266, 'Fin de saison «ravageurs»', 'left', 300, 225, '<p style="text-align:justify">Les fortes bourrasques et les pluies battantes qui les ont accompagn&eacute;es ont d&eacute;cim&eacute; les c&eacute;cidomyies. D&eacute;sormais ce risque est &eacute;teint partout en Belgique.</p>

<p style="text-align:justify">Quant aux pucerons, m&ecirc;me si l&rsquo;on peut observer quelques grosses colonies sur certaines feuilles, leur infestation est faible et ne d&eacute;passe nulle part les 200 individus par 100 talles. Apr&egrave;s avoir, eux aussi, souffert de la pluie et des bourrasques, les pucerons sont maintenant confront&eacute;s &agrave; leurs ennemis naturels, particuli&egrave;rement actifs cette ann&eacute;e.</p>

<p style="text-align:justify">Enfin, les crioc&egrave;res, tr&egrave;s &laquo; voyants &raquo;, sont n&eacute;anmoins peu nuisibles. Cette ann&eacute;e, ils sont peu nombreux et ne justifient aucune intervention, m&ecirc;me en c&eacute;r&eacute;ales de printemps o&ugrave; ils sont quasi syst&eacute;matiquement plus nombreux qu&rsquo;en c&eacute;r&eacute;ales d&rsquo;hiver.</p>

<p style="text-align:justify">Cet avis termine la saison d&rsquo;avertissements. Nous profitons de l&rsquo;occasion pour remercier toutes les &eacute;quipes ayant contribu&eacute;es au bon d&eacute;roulement de cette saison d&rsquo;avertissements.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>

<p>&nbsp;</p>
', 1, NULL, 100, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (239, 'Ne dites plus CADCO mais bien CePiCOP', 'left', 300, 225, '<p>L&#39;asbl CADCO s&#39;int&egrave;gre au centre pilote pour les c&eacute;r&eacute;ales les o&eacute;lagineux et les prot&eacute;agineux : le CePiCOP asbl,... quels cons&eacute;quences?</p>

<p><img align="" filer_id="455" height="" src="/filer/canonical/1557738826/455/" thumb_option="" title="" width="" /></p>

<p>Les activit&eacute;s men&eacute;es par le CADCO seront poursuivies au sein du CePiCOP et son personnel y sera comme auparavant &agrave; votre &eacute;coute.</p>
', 3, NULL, 96, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (234, 'Phytotechnie en orge brassicole et avoine', 'left', 300, 225, '<p style="text-align:justify"><strong>Avoine : </strong></p>

<p style="text-align:justify">Les avoines sem&eacute;es le 27 f&eacute;vrier &agrave; Gembloux sont actuellement au stade BBCH 37. Les plantes observ&eacute;es sont saines, il n&rsquo;y a donc pas lieu d&rsquo;appliquer un traitement fongicide pour le moment.</p>

<p><strong>Orge brassicole : </strong></p>

<table align="left" border="1" cellspacing="0" class="MsoTableLightShadingAccent1" style="border-collapse:collapse; border:solid #93a299 1.0pt; margin-left:4.8pt; margin-right:4.8pt; width:525.05pt">
	<tbody>
		<tr>
			<td style="width:63.2pt">
			<p style="text-align:justify"><strong>R&eacute;seau</strong></p>
			</td>
			<td style="width:461.85pt">
			<p style="text-align:justify">3 parcelles r&eacute;parties dans les localit&eacute;s : Hainaut (Vaudignies) et Namur (Gembloux, Liernu)</p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><u>A Vaudignies</u> : Les orges sem&eacute;es le 21 f&eacute;vrier &agrave; se trouvent au stade BBCH 37. La rhynchosporiose est pr&eacute;sente sur 15% des F4 avec une s&eacute;v&eacute;rit&eacute; de 16%, rouille naine est pour sa part pr&eacute;sente sur 10% des F4 avec une s&eacute;v&eacute;rit&eacute; de 0.75%.</p>

<p style="text-align:justify"><u>A Gembloux</u> : Les orges sem&eacute;es le 27 f&eacute;vrier sont actuellement au stade BBCH 32, la rhynchosporiose y est pr&eacute;sente sur 15% des F4 avec une s&eacute;v&eacute;rit&eacute; de 1%.</p>

<p style="text-align:justify"><u>A Liernu</u> : Les orges sem&eacute;es le 27 mars sont au stade BBCH 31. Aucune maladie n&rsquo;y a &eacute;t&eacute; observ&eacute;e &agrave; l&rsquo;exception de quelques pustules de rouille naine dans les &eacute;tages inf&eacute;rieurs de certaines plantes.</p>

<p style="text-align:justify">A la vue des observations r&eacute;alis&eacute;es cette semaine, nous vous conseillons d&rsquo;attendre le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39) pour effectuer un traitement fongicide.&nbsp;</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 28 mai.</span></p>
', 1, NULL, 96, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (235, 'Point phytotechnique en froment', 'left', 300, 225, '<p>Cette semaine, le stade 37 (derni&egrave;re feuille pointante) est &eacute;galement atteint pour les semis r&eacute;alis&eacute;s en d&eacute;cembre, il y a lieu d&rsquo;appliquer la derni&egrave;re fraction de la fumure azot&eacute;e pour ces semis.</p>

<p style="text-align:justify">A titre de rappel, la dose de r&eacute;f&eacute;rence &agrave; appliquer est de 65 kg N/ha pour une <u>fumure en trois fractions</u> et de 85 kg N/ha pour une <u>fumure en deux fractions</u>. Cette dose est &agrave; moduler selon les conditions culturales de la parcelle, les doses d&eacute;j&agrave; appliqu&eacute;es et l&rsquo;&eacute;tat de la culture d&eacute;finie lors dans le Livre blanc c&eacute;r&eacute;ales 2019. L&rsquo;ensemble des informations afin d&rsquo;adapter cette fumure sont reprises <a href="http://www.cereales.be">ici</a>. Un outil d&rsquo;aide &agrave; la d&eacute;cision au niveau de la fumure est disponible <a href="http://www.livre-blanc-cereales.be/outils/">ici</a></p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>B. Bodson, R. Blanchard</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 28 mai.</span></p>
', 1, NULL, 96, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (237, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les r&eacute;gulateurs de croissance, les herbicides, fongicides, les insecticides &hellip; <strong> </strong></p>

<p style="text-align:justify"><strong>Cliquez sur le lien &quot;cellule de droite du tableau ci-dessous&quot; pour ouvrir le fichier ad hoc.<em> </em></strong><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits.</p>

<table class="tableauavecbords" style="height:1070px; width:505px">
	<tbody>
		<tr>
			<td>01 avril 2019</td>
			<td><strong>AGENT ANTI-MOUSSANT</strong></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/09AgentAntiMoussant.pdf">Anti-moussant.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES FONGICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongAvoine.pdf">FongA.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froments, Seigles, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0705FongEpFroSeiTri.pdf">FongEpFroSeiTri.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Froments-Autoris&eacute;s contre Rouille jaune</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0704FroRouilleJaune.pdf">FongFRJ.pdf</a></td>
		</tr>
		<tr>
			<td>11 f&eacute;v. 2019</td>
			<td>Froments-Autoris&eacute;s contre Fusariose des &eacute;pis</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1207FroFusariose.pdf">FongFusariose.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongOrges.pdf">FongO.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES HERBICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Version 2019</td>
			<td>Sensibilit&eacute; des adventices</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14sensibiliteadventices.pdf">SensAdv.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Mode d&#39;actions des herbicides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14ModeAction.pdf">ModeActcion.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Sensibilit&eacute; au Chlortoluron</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/2016PJ- sensibChlortoluron.pdf">SensChlo.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Les additifs</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/08HerbicidesAdditifs.pdf">HerbAdd.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>En pr&eacute;-&eacute;mergence de la c&eacute;r&eacute;ale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf">HerbPreC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1510HerbPreEp.pdf">HerbPreEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>De la lev&eacute;e au d&eacute;but tallage</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf">HerbLTalC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalEp.pdf">HerbLTalEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Du d&eacute;but tallage au gonflement de la gaine</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFC.pdf">HerbTalDFC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFEp.pdf">HerbTalDFEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Sur c&eacute;r&eacute;ales &agrave; maturit&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbM.pdf">HerbM.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Toutes cultures</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCultures.pdf">HerbTC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>toutes cultures, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCulturessa.pdf">HerbTCsa.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Terres agricoles en interculture</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCultures.pdf">HerbIC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>interculture, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCulturessa.pdf">HerbICSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES INSECTICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>C&eacute;cidomyies</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf">CecidSA.pdf</a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons-&Eacute;t&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsEteCerealesSA.pdf">InsEteCSA.pdf</a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons vecteurs de JNO</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">InsJNOCSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES MOLLUSCICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707MolluscicideCereales.pdf">MolC.pdf</a></td>
		</tr>
		<tr>
			<td>INFO</td>
			<td>Granul&eacute;s anti-limaces : pas sans risques !</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0809AntiLimacesSPF.pdf">AntiLim.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines et Froment de printemps</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf">RegAFrop.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froment d&#39;hiver, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf">RegFhET.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges et Seigles</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf">RegOS.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>Les retraits d&#39;autorisation &quot;pr&eacute;-d&eacute;termin&eacute;s&quot;</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>Date limite d&#39;utilisation &quot;produits c&eacute;r&eacute;ales&quot;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0709LiquidationStock.pdf">LiquidationStock.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES TRAITEMENTS </strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>20 ao&ucirc;t 2018</td>
			<td>pour c&eacute;r&eacute;ales stock&eacute;es<br />
			pour locaux de stockage vides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TraitStoLocStoCereales.pdf">TStoLocStoC.pdf</a></td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>de semence : C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">TSemC.pdf</a></td>
		</tr>
	</tbody>
</table>
', 2, NULL, 96, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (240, 'Ravageurs du froment', 'left', 300, 225, '<p style="text-align:justify"><strong><u>C&eacute;cidomyie orange au rendez-vous</u></strong></p>

<p style="text-align:justify">Les toutes premi&egrave;res c&eacute;cidomyies orange ont &eacute;t&eacute; observ&eacute;es au cours du week-end dernier. Ces &eacute;mergences de faible intensit&eacute; r&eacute;pondaient aux pluies inductrices des 2 et 3 avril. Ces pluies, fort irr&eacute;guli&egrave;res, ont principalement touch&eacute; l&rsquo;ouest et le centre de la Wallonie. Le bl&eacute; n&rsquo;&eacute;tant (sauf tr&egrave;s rares exceptions) pas encore en &eacute;pi, ces insectes n&rsquo;ont commis aucun d&eacute;g&acirc;t.</p>

<p style="text-align:justify">Une deuxi&egrave;me pluie inductrice est tomb&eacute;e les 7-8-9 avril, et devrait provoquer des &eacute;mergences cette semaine, d&rsquo;autant plus abondantes que les pluies du 7 au 9 avril ont &eacute;t&eacute; importantes, c&rsquo;est-&agrave;-dire plut&ocirc;t dans le centre et dans l&rsquo;est de la Wallonie. Cette deuxi&egrave;me vague d&rsquo;&eacute;mergence pourrait correspondre au d&eacute;but de l&rsquo;&eacute;piaison dans les champs les plus pr&eacute;coces, et y constituer une menace.</p>

<p style="text-align:justify">Les pluies d&rsquo;orage des derni&egrave;res heures, en ameublissant la surface du sol, favorisent les &eacute;mergences.</p>

<p style="text-align:justify">Dans cette situation, notre conseil est le suivant :</p>

<ol>
	<li style="text-align:justify">Surveiller attentivement les stades ph&eacute;nologiques du bl&eacute;. Tant que l&rsquo;&eacute;pi n&rsquo;est pas visible (gaine &eacute;clat&eacute;e), le bl&eacute; ne risque rien.</li>
	<li style="text-align:justify">Lorsque les premi&egrave;res gaines &eacute;clatent, il faut se tenir pr&ecirc;t &agrave; intervenir en cas d&rsquo;infestation importante. L&rsquo;infestation se mesure vers 21h-22h, en fr&ocirc;lant les &eacute;pis avec une baguette de 50 cm tenue horizontalement. Si ce geste provoque l&rsquo;envol de plus de 30 c&eacute;cidomyies orange par m&sup2;, un traitement insecticide effectu&eacute; tard le soir ou t&ocirc;t le matin (dans la ros&eacute;e) est recommand&eacute;.</li>
	<li style="text-align:justify">Aucun traitement insecticide n&rsquo;est recommand&eacute; sur les vari&eacute;t&eacute;s r&eacute;sistantes &agrave; la c&eacute;cidomyie orange.</li>
	<li style="text-align:justify">Une fois atteint le stade fin floraison, le bl&eacute; n&rsquo;est plus sensible &agrave; la c&eacute;cidomyie orange.</li>
</ol>

<p style="text-align:justify"><strong>Les attaques de c&eacute;cidomyies orange devraient &ecirc;tre nettement moins importantes que celles de l&rsquo;ann&eacute;e derni&egrave;re</strong>. En effet, la s&eacute;cheresse et la chaleur qui ont pr&eacute;valu du 20 juin au 10 juillet ont tu&eacute; la plupart des larves dans les &eacute;pis. Seule la r&eacute;gion de Gembloux a re&ccedil;u assez de pr&eacute;cipitations &agrave; cette &eacute;poque critique pour permettre aux c&eacute;cidomyies orange de quitter les &eacute;pis pour regagner le sol.</p>

<p style="text-align:justify">Nous suivrons attentivement la situation au cours des soir&eacute;es de cette semaine, et nous invitons chacun &agrave; faire de m&ecirc;me dans ses champs. Toute observation jug&eacute;e surprenante peut &ecirc;tre signal&eacute;e par sms au n&deg; 0476/760532. En cas d&rsquo;&eacute;volution brutale, ou de niveau d&rsquo;&eacute;mergence exceptionnel, un avis sera imm&eacute;diatement &eacute;mis.</p>

<p style="text-align:justify">Vous trouverez la liste des vari&eacute;t&eacute;s r&eacute;sistantes <a href="http://www.cadcoasbl.be/p09_biblio/art0017/vari%C3%A9t%C3%A9sR%C3%A9sistantes18.pdf">ici</a>, et la liste des insecticides autoris&eacute;s <a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf">ici</a></p>

<p style="text-align:justify">Au niveau des pucerons et crioc&egrave;res, une prospection dans une partie du r&eacute;seau r&eacute;v&egrave;le que les populations sont tr&egrave;s faibles. Les observations continuent la semaine prochaine.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>

<p>&nbsp;</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 28 mai.</span></p>
', 1, NULL, 96, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (236, 'Réseau « maladie en froment »', 'left', 150, 225, '<p><img align="" filer_id="459" height="" src="/filer/canonical/1558434385/459/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><strong>R&eacute;sum&eacute; </strong></p>

<p style="text-align:justify">La derni&egrave;re feuille est visible dans pratiquement toutes les parcelles. Certaines parcelles sont au stade gonflement de l&rsquo;&eacute;pi voire m&ecirc;me d&eacute;j&agrave; &agrave; l&rsquo;&eacute;piaison. Dans certaines localit&eacute;s et sur vari&eacute;t&eacute;s sensibles uniquement, la septoriose est visible sur les &eacute;tages foliaires sup&eacute;rieurs dans des parcelles non trait&eacute;es. Des foyers de rouille jaune sont observ&eacute;s et la rouille brune appara&icirc;t doucement.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong>Avancement des cultures</strong></p>

<p><img align="" filer_id="460" height="" src="/filer/canonical/1558434415/460/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La septoriose</strong> est toujours pr&eacute;sente dans le fond de la v&eacute;g&eacute;tation de la plupart des parcelles mais dans certains cas, elle est visible sur les &eacute;tages sup&eacute;rieurs de parcelles non trait&eacute;es (tableau 2). Dans la r&eacute;gion du Hainaut, elle a &eacute;t&eacute; observ&eacute;e sur 5% des F1 sur vari&eacute;t&eacute;s sensibles et 15% des F2 sur vari&eacute;t&eacute;s tol&eacute;rantes mais la s&eacute;v&eacute;rit&eacute; (pourcentage de surface foliaire atteinte) est tr&egrave;s faible.</p>

<p><img align="" filer_id="461" height="" src="/filer/canonical/1558436239/461/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">Selon le mod&egrave;le de pr&eacute;vision &eacute;pid&eacute;miologique PROCULTURE, la septoriose n&rsquo;a pas &eacute;volu&eacute; de la m&ecirc;me mani&egrave;re dans les diff&eacute;rentes r&eacute;gions de Wallonie notamment en raison de <strong>l&rsquo;h&eacute;t&eacute;rog&eacute;n&eacute;it&eacute; de la distribution des pluies</strong>. Dans les parcelles encore <u>non trait&eacute;es</u>, cette maladie est en <u>incubation</u> sur les derniers &eacute;tages foliaires des <strong>vari&eacute;t&eacute;s tr&egrave;s sensibles</strong> principalement dans des parcelles du Hainaut et &eacute;galement dans la r&eacute;gion de Gembloux (parcelle d&rsquo;observation de Lonz&eacute;e) et de Pailhe. Dans les autres r&eacute;gions, elle reste &agrave; des &eacute;tages inf&eacute;rieurs et atteint maximum la F2 sur vari&eacute;t&eacute;s sensibles.</p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong><span style="color:black"> est pr&eacute;sent dans 13 parcelles du r&eacute;seau. Dans certaines parcelles, il atteint la F2 mais &agrave; des fr&eacute;quences et s&eacute;v&eacute;rit&eacute;s tr&egrave;s faibles.</span></p>

<p style="text-align:justify"><strong>La rouille jaune</strong><span style="color:black"> est observ&eacute;e dans 13 parcelles du r&eacute;seau. Des foyers importants sont observ&eacute;s depuis ces trois derni&egrave;res semaines vu les conditions m&eacute;t&eacute;orologiques id&eacute;ales pour le d&eacute;veloppement de cette maladie. Des foyers de diff&eacute;rentes tailles sont donc visibles sur les vari&eacute;t&eacute;s sensibles et tr&egrave;s sensibles comme par exemple REFLECTION ou NEMO. Dans les autres parcelles o&ugrave; elle est observ&eacute;e, la rouille jaune n&rsquo;est pr&eacute;sente que sous forme de quelques pustules.</span></p>

<p style="text-align:center"><img align="" filer_id="464" height="" src="/filer/canonical/1558437210/464/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><strong>La rouille brune</strong><span style="color:black"> : quelques pustules ont &eacute;t&eacute; observ&eacute;es sur la F3 dans une parcelle &agrave; Mortroux en r&eacute;gion Li&eacute;geoise. </span></p>

<p>&nbsp;</p>

<p style="text-align:justify"><strong><u>Recommandations</u> : </strong></p>

<p style="text-align:justify">Cette semaine, nos recommandations sont les suivantes selon le stade ph&eacute;nologique des parcelles :&nbsp;</p>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles au stade 37 et qui n&rsquo;ont pas &eacute;t&eacute; trait&eacute;es</strong>, un traitement peut &ecirc;tre r&eacute;alis&eacute; en cas de pr&eacute;sence significative de rouille jaune (foyer actif) ou de septoriose (plus de 20% des F3), sur vari&eacute;t&eacute;s sensibles &agrave; l&rsquo;une ou l&rsquo;autre de ces maladies. <strong>Pour ces parcelles</strong>, un second traitement englobant l&rsquo;ensemble des maladies sera r&eacute;alis&eacute; 3 &agrave; 4 semaines apr&egrave;s ce premier traitement. Si aucune maladie n&rsquo;est observ&eacute;e sur la parcelle pour l&rsquo;instant, la protection peut &ecirc;tre report&eacute;e <strong><u>avec vigilance au stade 39</u></strong>.</li>
</ul>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39</strong>, derni&egrave;re feuille compl&egrave;tement &eacute;tal&eacute;e, <strong>et qui n&rsquo;ont pas encore &eacute;t&eacute; trait&eacute;es</strong>, le traitement complet contre les maladies du feuillage peut &ecirc;tre r&eacute;alis&eacute;. Le produit ou le m&eacute;lange sera choisi en fonction des sensibilit&eacute;s propres &agrave; la vari&eacute;t&eacute;.</li>
</ul>

<ul style="list-style-type:square">
	<li style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39 et qui ont d&eacute;j&agrave; &eacute;t&eacute; trait&eacute;es avant ce stade</strong>, un second traitement englobant l&rsquo;ensemble des maladies est &agrave; r&eacute;aliser 3 &agrave; 4 semaines apr&egrave;s le premier traitement. Des pr&eacute;cipitations &eacute;tant attendues cette semaine, dans le cas o&ugrave; le deuxi&egrave;me traitement est r&eacute;alis&eacute; dans les prochains jours, il convient de prendre en compte le risque d&rsquo;infection par la <strong>fusariose des &eacute;pis</strong> sur vari&eacute;t&eacute;s sensibles &agrave; cette maladie. Les situations &agrave; risque sont les cultures de froment (les &eacute;peautres sont g&eacute;n&eacute;ralement moins sensibles) de vari&eacute;t&eacute;s sensibles dans lesquelles le travail du sol a &eacute;t&eacute; r&eacute;duit et les froments (vari&eacute;t&eacute; sensible) apr&egrave;s froment ou ma&iuml;s, particuli&egrave;rement lorsque les cannes sont encore apparentes dans la parcelle. Le temps humide (orage, &hellip;) favorise le d&eacute;veloppement de la maladie. C&rsquo;est au stade floraison (au plus tard entre le d&eacute;but et la mi-floraison, stade 61 &agrave; 65) que l&rsquo;on peut intervenir si n&eacute;cessaire contre la fusariose de l&rsquo;&eacute;pi. L&rsquo;utilisation de prothioconazole est le plus indiqu&eacute; pour lutter contre les deux &laquo; types &raquo; de pathog&egrave;nes de la fusariose mais s&rsquo;il est utilis&eacute; en 39, d&rsquo;autres mol&eacute;cules doivent &ecirc;tre privil&eacute;gier pour ce traitement. Le t&eacute;buconazole et le metconazole sont, quant &agrave; eux, utiles uniquement contre les Fusarium spp.</li>
</ul>

<p><strong><u>Substances actives &agrave; pr&eacute;coniser dans un programme fongicide en froment</u></strong></p>

<p><img align="" filer_id="463" height="" src="/filer/canonical/1558436474/463/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">Pour rappel, les mol&eacute;cules SDHI ne doivent &ecirc;tre utilis&eacute;es qu&rsquo;une fois par saison et l&rsquo;alternance des substances actives est importante pour conserver leur efficacit&eacute;. Ex : Si utilisation du prothioconazole au stade 39, ne plus l&rsquo;utiliser dans un traitement &agrave; l&rsquo;&eacute;piaison et privil&eacute;gier alors le tebuconazole ou metconazole pour prot&eacute;ger l&rsquo;&eacute;pi.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A. Nysten, Ch. Bataille</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 28 mai.</span></p>
', 1, NULL, 96, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (238, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p><u>A l&rsquo;agenda</u> :</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td>
			<p style="text-align:center"><strong>Date</strong></p>
			</td>
			<td>
			<p style="text-align:center"><strong>Lieu</strong></p>
			</td>
			<td>
			<p style="text-align:center"><strong>Ev&eacute;nement</strong></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>mardi<br />
			28/05/2019</p>
			</td>
			<td>
			<p>Houdeng Goegnies<br />
			18h30</p>
			</td>
			<td>
			<p><strong>Assembl&eacute;e Sectorielle &quot;Grandes cultures et pomme de terre&quot;</strong> &agrave; la grange des jonquilles, rue de Wavrin 113 &agrave; 7110 Houdeng Goegnies. Inscription : 081/240430 ou info.socopro@collegedesproducteurs.be <a href="http://www.cadcoasbl.be/p10_agenda/190528%20coll%C3%A8ge%20Invit%20AS%20GC%20et%20pdt.pdf">Programme.pdf</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mercredi 29/05/2019</p>
			</td>
			<td>
			<p>Havelange<br />
			14-17h</p>
			</td>
			<td>
			<p>Rencontre <strong>en ferme polyculture &eacute;l&eacute;vage </strong>D&eacute;chaumage avec des moutons (Nature et progr&egrave;s). Ferme Agribio. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><u><strong>Voir la suite de l&rsquo;agenda</strong></u> : toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <strong><a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></strong></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 96, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (274, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les r&eacute;gulateurs de croissance, les herbicides, fongicides, les insecticides &hellip; <strong> </strong></p>

<p style="text-align:justify"><strong>Cliquez sur le lien &quot;cellule de droite du tableau ci-dessous&quot; pour ouvrir le fichier ad hoc.<em> </em></strong><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits.</p>

<table border="1" cellpadding="1" cellspacing="1" style="width:500px">
	<tbody>
		<tr>
			<td>01 avril 2019</td>
			<td><strong>AGENT ANTI-MOUSSANT</strong></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/09AgentAntiMoussant.pdf"><span style="color:blue">Anti-moussant.pdf</span></a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td><strong>LES FONGICIDES</strong></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongAvoine.pdf"><span style="color:blue">FongA.pdf</span></a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froments, Seigles, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0705FongEpFroSeiTri.pdf"><span style="color:blue">FongEpFroSeiTri.pdf</span></a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Froments-Autoris&eacute;s contre Rouille jaune</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0704FroRouilleJaune.pdf"><span style="color:blue">FongFRJ.pdf</span></a></td>
		</tr>
		<tr>
			<td>11 f&eacute;v. 2019</td>
			<td>Froments-Autoris&eacute;s contre Fusariose des &eacute;pis</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1207FroFusariose.pdf"><span style="color:blue">FongFusariose.pdf</span></a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongOrges.pdf"><span style="color:blue">FongO.pdf</span></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES HERBICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>2019</td>
			<td>Sensibilit&eacute; des adventices</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14sensibiliteadventices.pdf"><span style="color:blue">SensAdv.pdf</span></a></td>
		</tr>
		<tr>
			<td>2019</td>
			<td>Mode d&#39;actions des herbicides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14ModeAction.pdf"><span style="color:blue">ModeActcion.pdf</span></a></td>
		</tr>
		<tr>
			<td>2019</td>
			<td>Sensibilit&eacute; au Chlortoluron</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/2016PJ-%20sensibChlortoluron.pdf"><span style="color:blue">SensChlo.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Les additifs</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/08HerbicidesAdditifs.pdf"><span style="color:blue">HerbAdd.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>En pr&eacute;-&eacute;mergence de la c&eacute;r&eacute;ale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf"><span style="color:blue">HerbPreC.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1510HerbPreEp.pdf"><span style="color:blue">HerbPreEp.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>De la lev&eacute;e au d&eacute;but tallage</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf"><span style="color:blue">HerbLTalC.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalEp.pdf"><span style="color:blue">HerbLTalEp.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Du d&eacute;but tallage au gonflement de la gaine</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFC.pdf"><span style="color:blue">HerbTalDFC.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFEp.pdf"><span style="color:blue">HerbTalDFEp.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Sur c&eacute;r&eacute;ales &agrave; maturit&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbM.pdf"><span style="color:blue">HerbM.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Toutes cultures</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCultures.pdf"><span style="color:blue">HerbTC.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>toutes cultures, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCulturessa.pdf"><span style="color:blue">HerbTCsa.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Terres agricoles en interculture</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCultures.pdf"><span style="color:blue">HerbIC.pdf</span></a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>interculture, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCulturessa.pdf"><span style="color:blue">HerbICSA.pdf</span></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES INSECTICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>C&eacute;cidomyies</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf"><span style="color:blue">CecidSA.pdf</span></a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons-&Eacute;t&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsEteCerealesSA.pdf"><span style="color:blue">InsEteCSA.pdf</span></a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons vecteurs de JNO</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf"><span style="color:blue">InsJNOCSA.pdf</span></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES MOLLUSCICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707MolluscicideCereales.pdf"><span style="color:blue">MolC.pdf</span></a></td>
		</tr>
		<tr>
			<td style="text-align:right">INFO</td>
			<td>Granul&eacute;s anti-limaces : pas sans risques !</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0809AntiLimacesSPF.pdf"><span style="color:blue">AntiLim.pdf</span></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines et Froment de printemps</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf"><span style="color:blue">RegAFrop.pdf</span></a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froment d&#39;hiver, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf"><span style="color:blue">RegFhET.pdf</span></a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges et Seigles</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf"><span style="color:blue">RegOS.pdf</span></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>Les retraits d&#39;autorisation &quot;pr&eacute;-d&eacute;termin&eacute;s&quot;</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>Date limite d&#39;utilisation &quot;produits c&eacute;r&eacute;ales&quot;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0709LiquidationStock.pdf"><span style="color:blue">LiquidationStock.pdf</span></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES TRAITEMENTS </strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>20 ao&ucirc;t 2018</td>
			<td>pour c&eacute;r&eacute;ales stock&eacute;es<br />
			pour locaux de stockage vides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TraitStoLocStoCereales.pdf"><span style="color:blue">TStoLocStoC.pdf</span></a></td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>de semence : C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf"><span style="color:blue">TSemC.pdf</span></a></td>
		</tr>
	</tbody>
</table>
', 2, NULL, 101, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (245, 'Actualisation des listes des produits autorisés en céréales', 'left', 300, 225, '<p style="text-align:justify">Vous trouverez les listes des produits autoris&eacute;s en c&eacute;r&eacute;ales r&eacute;alis&eacute;es &agrave; partir des donn&eacute;es du Phytoweb, r&eacute;cemment remises &agrave; jour : les r&eacute;gulateurs de croissance, les herbicides, fongicides, les insecticides &hellip; <strong> </strong></p>

<p style="text-align:justify"><strong>Cliquez sur le lien &quot;cellule de droite du tableau ci-dessous&quot; pour ouvrir le fichier ad hoc.<em> </em></strong><u><span style="color:red">Nouveaut&eacute;</span></u> de cette ann&eacute;e : <strong>en collaboration avec Protect&rsquo;eau</strong>, et pour votre facilit&eacute;, les diff&eacute;rentes zones tampons (en m) ainsi que le pourcentage minimum de r&eacute;duction de d&eacute;rive (en %) sont reprises dans ces tableaux des produits.</p>

<table class="tableauavecbords" style="height:1070px; width:505px">
	<tbody>
		<tr>
			<td>01 avril 2019</td>
			<td><strong>AGENT ANTI-MOUSSANT</strong></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/09AgentAntiMoussant.pdf">Anti-moussant.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES FONGICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongAvoine.pdf">FongA.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froments, Seigles, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0705FongEpFroSeiTri.pdf">FongEpFroSeiTri.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Froments-Autoris&eacute;s contre Rouille jaune</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0704FroRouilleJaune.pdf">FongFRJ.pdf</a></td>
		</tr>
		<tr>
			<td>11 f&eacute;v. 2019</td>
			<td>Froments-Autoris&eacute;s contre Fusariose des &eacute;pis</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1207FroFusariose.pdf">FongFusariose.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070417FongOrges.pdf">FongO.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES HERBICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Version 2019</td>
			<td>Sensibilit&eacute; des adventices</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14sensibiliteadventices.pdf">SensAdv.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Mode d&#39;actions des herbicides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/14ModeAction.pdf">ModeActcion.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Sensibilit&eacute; au Chlortoluron</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/2016PJ- sensibChlortoluron.pdf">SensChlo.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Les additifs</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/08HerbicidesAdditifs.pdf">HerbAdd.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>En pr&eacute;-&eacute;mergence de la c&eacute;r&eacute;ale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbPreC.pdf">HerbPreC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/1510HerbPreEp.pdf">HerbPreEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>De la lev&eacute;e au d&eacute;but tallage</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalC.pdf">HerbLTalC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbLTalEp.pdf">HerbLTalEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Du d&eacute;but tallage au gonflement de la gaine</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFC.pdf">HerbTalDFC.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td><em>idem ci-dessus, uniquement pour &eacute;peautre</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbTalDFEp.pdf">HerbTalDFEp.pdf</a></td>
		</tr>
		<tr>
			<td>15 f&eacute;v. 2019</td>
			<td>Sur c&eacute;r&eacute;ales &agrave; maturit&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0708HerbM.pdf">HerbM.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Toutes cultures</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCultures.pdf">HerbTC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>toutes cultures, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbToutesCulturessa.pdf">HerbTCsa.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td>Terres agricoles en interculture</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCultures.pdf">HerbIC.pdf</a></td>
		</tr>
		<tr>
			<td>30 octobre 2018</td>
			<td><em>interculture, tri&eacute; par substance active</em></td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0807HerbInterCulturessa.pdf">HerbICSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES INSECTICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>C&eacute;cidomyies</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/110810InsCecidCerealesSA.pdf">CecidSA.pdf</a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons-&Eacute;t&eacute;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsEteCerealesSA.pdf">InsEteCSA.pdf</a></td>
		</tr>
		<tr>
			<td>21 mai 2019</td>
			<td>Pucerons vecteurs de JNO</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/070410InsJNOCerealesSA.pdf">InsJNOCSA.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES MOLLUSCICIDES</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707MolluscicideCereales.pdf">MolC.pdf</a></td>
		</tr>
		<tr>
			<td>INFO</td>
			<td>Granul&eacute;s anti-limaces : pas sans risques !</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0809AntiLimacesSPF.pdf">AntiLim.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES REGULATEURS DE CROISSANCE</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Avoines et Froment de printemps</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurAvoinesFrop.pdf">RegAFrop.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Epeautre, Froment d&#39;hiver, Triticale</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurFromentHivEpeautreTriticale.pdf">RegFhET.pdf</a></td>
		</tr>
		<tr>
			<td>01 avril 2019</td>
			<td>Orges et Seigles</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707RegulateurOrgesSeigle.pdf">RegOS.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>Les retraits d&#39;autorisation &quot;pr&eacute;-d&eacute;termin&eacute;s&quot;</strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>Date limite d&#39;utilisation &quot;produits c&eacute;r&eacute;ales&quot;</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0709LiquidationStock.pdf">LiquidationStock.pdf</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>LES TRAITEMENTS </strong></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>20 ao&ucirc;t 2018</td>
			<td>pour c&eacute;r&eacute;ales stock&eacute;es<br />
			pour locaux de stockage vides</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TraitStoLocStoCereales.pdf">TStoLocStoC.pdf</a></td>
		</tr>
		<tr>
			<td>19 janv. 2019</td>
			<td>de semence : C&eacute;r&eacute;ales</td>
			<td><a href="http://www.cadcoasbl.be/p09_biblio/art0002/0707TSemencesCereales.pdf">TSemC.pdf</a></td>
		</tr>
	</tbody>
</table>
', 2, NULL, 97, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (273, 'Fin de saison «ravageurs»', 'left', 300, 225, '<p>xxx</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>

<p>&nbsp;</p>
', 1, NULL, 101, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (243, 'Cécidomyie orange, pucerons et criocères : infestation insignifiante', 'left', 300, 225, '<p style="text-align:justify">Le bl&eacute; commence &agrave; &eacute;pier et pose la question du risque d&rsquo;attaque par la c&eacute;cidomyie orange. D&rsquo;apr&egrave;s le mod&egrave;le pr&eacute;visionnel des &eacute;mergences d&eacute;velopp&eacute; au CRA-W, un deuxi&egrave;me pic d&rsquo;&eacute;mergence devrait se produire cette semaine. Toutefois, les observations effectu&eacute;es lors de la premi&egrave;re vague d&rsquo;&eacute;mergences ont montr&eacute; que ces derni&egrave;res &eacute;taient extr&ecirc;mement faibles. Cette situation est tr&egrave;s vraisemblablement la cons&eacute;quence de la chaleur et de la s&eacute;cheresse de la fin juillet-d&eacute;but ao&ucirc;t 2018, o&ugrave; les larves de l&rsquo;insecte sont mortes dans les &eacute;pis.</p>

<p style="text-align:justify">Cette s&eacute;cheresse a donc eu un effet assainissant, et c&rsquo;est tant mieux.</p>

<p style="text-align:justify">Quant aux autres ravageurs, des pucerons et des crioc&egrave;res peuvent &ecirc;tre observ&eacute;s, mais leurs populations sont tr&egrave;s faibles, elles aussi. Les prochaines soir&eacute;es seront encore pass&eacute;es &agrave; observer les champs de bl&eacute;, afin de s&rsquo;assurer de l&rsquo;absence de tout risque. A moins d&rsquo;un tr&egrave;s improbable renversement de situation, tout traitement insecticide appara&icirc;t inutile cette ann&eacute;e dans le bl&eacute;.</p>

<p style="text-align:justify">Les observations continuent la semaine prochaine.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination&nbsp; scientifique</span></u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; ravageurs &raquo;, M. De Proft</span></p>

<p>&nbsp;</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 04 juin.</span></p>
', 1, NULL, 97, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (246, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p><u>A l&rsquo;agenda</u> :</p>

<table border="1" class="Table" style="border:solid windowtext 1.0pt">
	<tbody>
		<tr>
			<td>
			<p style="text-align:center"><strong>Date</strong></p>
			</td>
			<td>
			<p style="text-align:center"><strong>Lieu</strong></p>
			</td>
			<td>
			<p style="text-align:center"><strong>Ev&eacute;nement</strong></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>mardi<br />
			28/05/2019</p>
			</td>
			<td>
			<p>Houdeng Goegnies<br />
			18h30</p>
			</td>
			<td>
			<p><strong>Assembl&eacute;e Sectorielle &quot;Grandes cultures et pomme de terre&quot;</strong> &agrave; la grange des jonquilles, rue de Wavrin 113 &agrave; 7110 Houdeng Goegnies. Inscription : 081/240430 ou info.socopro@collegedesproducteurs.be <a href="http://www.cadcoasbl.be/p10_agenda/190528%20coll%C3%A8ge%20Invit%20AS%20GC%20et%20pdt.pdf">Programme.pdf</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mercredi 29/05/2019</p>
			</td>
			<td>
			<p>Havelange<br />
			14-17h</p>
			</td>
			<td>
			<p>Rencontre <strong>en ferme polyculture &eacute;l&eacute;vage </strong>D&eacute;chaumage avec des moutons (Nature et progr&egrave;s). Ferme Agribio. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Samedi 01/06/2019</p>
			</td>
			<td>
			<p>Herseaux<br />
			10-13h</p>
			</td>
			<td>
			<p>Rencontre en ferme grande culture 50 ans en bio (Nature et progr&egrave;s). Ferme de la Roussellerie. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Lundi 03/06/2019</p>
			</td>
			<td>
			<p>Longchamps<br />
			17h30-20h30</p>
			</td>
			<td>
			<p>Rencontre en ferme polyculture &eacute;l&eacute;vage et agroforesterie (Nature et progr&egrave;s). Ferme Rolley. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mardi 04/06/2019</p>
			</td>
			<td>
			<p>Fromi&eacute;e<br />
			matin&eacute;e</p>
			</td>
			<td>
			<p>Visite <strong>Colza associ&eacute; et essai en semis direct de froment dans diff&eacute;rents couverts </strong>(Greenotec, Gal Entre S et M, PROTECTeau). Plus d&#39;info &agrave; venir <a href="http://www.cadcoasbl.be/p10_agenda/190604%20Greenotec.pdf">inscription et programme</a></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mardi 04/06/2019</p>
			</td>
			<td>
			<p>Gembloux<br />
			14h00</p>
			</td>
			<td>
			<p>Visite de champ d&#39;essais vari&eacute;t&eacute;s escourgeon (CRA-W/CePiCOP). Chemin de Liroux 9</p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><u><strong>Voir la suite de l&rsquo;agenda</strong></u> : toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <strong><a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></strong></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 97, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (244, 'Réseau « maladie en froment »', 'left', 150, 225, '<p><img align="" filer_id="466" height="" src="/filer/canonical/1559037643/466/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><strong>R&eacute;sum&eacute; </strong></p>

<p style="text-align:justify">Cette semaine, toutes les parcelles ont d&eacute;pass&eacute; le stade derni&egrave;re feuille &eacute;tal&eacute;e (BBCH39, &agrave; ce stade, toutes les feuilles sont d&eacute;ploy&eacute;es et donc visibles). La septoriose a peu progress&eacute; cette semaine. La rouille jaune et la rouille brune sont &agrave; surveiller particuli&egrave;rement sur les vari&eacute;t&eacute;s sensibles.</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong>Avancement des cultures</strong></p>

<p><img align="" filer_id="469" height="" original_image="false" src="/filer/canonical/1559038779/469/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify"><u><strong>Pression en maladies</strong></u></p>

<p style="text-align:justify"><strong>La septoriose</strong> est visible sur 32 des 36 parcelles du r&eacute;seau d&rsquo;observation. Rappelons que ces parcelles n&rsquo;ont pas &eacute;t&eacute; trait&eacute;es. Dans la r&eacute;gion du Hainaut, elle a &eacute;t&eacute; observ&eacute;e sur 7% des F1 sur vari&eacute;t&eacute;s sensibles et seulement 10% des F2 des vari&eacute;t&eacute;s tol&eacute;rantes. Dans les autres r&eacute;gions, les sympt&ocirc;mes ne sont visibles que sur les F2 des vari&eacute;t&eacute;s sensibles ou &agrave; peine sur les F3 des vari&eacute;t&eacute;s tol&eacute;rantes (tableau 2). Il est &agrave; noter &eacute;galement que les semis tardifs (fin novembre-d&eacute;cembre) pr&eacute;sentent moins de sympt&ocirc;mes que ceux r&eacute;alis&eacute;s en octobre.</p>

<p><img align="" filer_id="467" height="" src="/filer/canonical/1559037675/467/" thumb_option="" title="" width="" /></p>

<p style="text-align:justify">Selon le mod&egrave;le de pr&eacute;vision &eacute;pid&eacute;miologique PROCULTURE, cette maladie est en <u>incubation</u> sur les derniers &eacute;tages foliaires des <strong>vari&eacute;t&eacute;s sensibles</strong> principalement dans des parcelles du Hainaut et &eacute;galement dans la r&eacute;gion de Gembloux (parcelle d&rsquo;observation de Lonz&eacute;e) et de Pailhe. Dans les autres r&eacute;gions, elle reste &agrave; des &eacute;tages inf&eacute;rieurs et atteint maximum la F2 sur vari&eacute;t&eacute;s sensibles.</p>

<p style="text-align:justify"><strong>L&rsquo;o&iuml;dium</strong><span style="color:black"> est pr&eacute;sent </span>dans 14 parcelles du r&eacute;seau. Dans certaines parcelles, il est visible sur la F1 ou F2 des plantes (RGT SACRAMENTO, NEMO, GEDSER) mais les fr&eacute;quences et s&eacute;v&eacute;rit&eacute;s sont tr&egrave;s faibles.</p>

<p><strong>La rouille jaune</strong> La rouille jaune est observ&eacute;e dans 24 parcelles (non-trait&eacute;es!) du r&eacute;seau. Des foyers importants sont visibles sur les vari&eacute;t&eacute;s tr&egrave;s sensibles comme par exemple REFLECTION ou NEMO mais la rouille jaune a &eacute;galement &eacute;t&eacute; signal&eacute;e sur SAHARA, GLEAM, KWS SMART, WPB DURAN, BENNINGTON, GEDSER, GRAHAM, RAGNAR, RGT SACRAMENTO, BENCHMARK, KWS TALENT.</p>

<p>Les vari&eacute;t&eacute;s comme ANAPOLIS, ALCIDES, ALBERT, CHEVIGNON, EDGAR, SAFARI, SKYSCRAPER ou LIMABEL ont jusqu&rsquo;&agrave; pr&eacute;sent &eacute;t&eacute; &eacute;pargn&eacute;es dans les essais.</p>

<p style="text-align:justify"><strong>La rouille brune</strong><span style="color:black"> a &eacute;t&eacute; observ&eacute;e sur 7 parcelles (non-trait&eacute;es) du r&eacute;seau.&nbsp; A Mortroux, sur les parcelles les plus touch&eacute;es, elle est visible sur 35% des F2 et dans les autres cas sur 5% des F3. A Lonz&eacute;e, quelques pustules sont visibles sur diff&eacute;rents &eacute;tages foliaires.</span></p>

<p style="text-align:justify"><strong>L&rsquo;helminthosporiose</strong> <span style="color:black">est signal&eacute;e &agrave; Lonz&eacute;e sur KWS SMART, GLEAM ou MENTOR. Cette maladie foliaire est reconnaissable par les taches plus fonc&eacute;es que celles induites par la septoriose (voir fig.1). </span></p>

<p style="text-align:center"><img align="" filer_id="468" height="" src="/filer/canonical/1559038106/468/" thumb_option="" title="" width="" /></p>

<p style="text-align:center"><strong>Figure 1</strong> : Taches foliaires de septoriose (&agrave; gauche), d&rsquo;helminthosporiose (&agrave; droite)</p>

<p>&nbsp;</p>

<p style="text-align:justify"><strong><u>Recommandations</u> : </strong></p>

<p style="text-align:justify">Cette semaine, nos recommandations sont les suivantes selon le stade ph&eacute;nologique des parcelles : &nbsp;</p>

<p style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39</strong>, derni&egrave;re feuille compl&egrave;tement &eacute;tal&eacute;e, <strong>et qui n&rsquo;ont pas encore &eacute;t&eacute; trait&eacute;es</strong>, le traitement <u>complet</u> contre les maladies du feuillage peut &ecirc;tre r&eacute;alis&eacute;. Le produit ou le m&eacute;lange sera choisi en fonction des sensibilit&eacute;s propres &agrave; la vari&eacute;t&eacute;.</p>

<p style="text-align:justify"><strong>Pour les parcelles ayant atteint le stade 39 et qui ont d&eacute;j&agrave; &eacute;t&eacute; trait&eacute;es avant ce stade</strong>, un second traitement englobant l&rsquo;ensemble des maladies est &agrave; r&eacute;aliser 3 &agrave; 4 semaines apr&egrave;s le premier traitement.</p>

<p style="text-align:justify">Dans le cas o&ugrave; le deuxi&egrave;me traitement est r&eacute;alis&eacute; dans les prochains jours, il convient de prendre en compte le risque d&rsquo;infection par la <strong>fusariose des &eacute;pis</strong> sur vari&eacute;t&eacute;s sensibles &agrave; cette maladie et par la rouille brune sur vari&eacute;t&eacute;s sensibles &agrave; cette derni&egrave;re. En ce qui concerne la fusariose, les situations &agrave; risque sont les cultures de froment (les &eacute;peautres sont g&eacute;n&eacute;ralement moins sensibles) de vari&eacute;t&eacute;s sensibles dans lesquelles le travail du sol a &eacute;t&eacute; r&eacute;duit et les froments (vari&eacute;t&eacute; sensible) apr&egrave;s froment ou ma&iuml;s, particuli&egrave;rement lorsque les cannes sont encore apparentes dans la parcelle. Le temps humide (orage, &hellip;) favorise le d&eacute;veloppement de cette maladie. C&rsquo;est au stade floraison (au plus tard entre le d&eacute;but et la mi-floraison, stade 61 &agrave; 65) que l&rsquo;on peut intervenir si n&eacute;cessaire contre la fusariose de l&rsquo;&eacute;pi. L&rsquo;utilisation de prothioconazole est le plus indiqu&eacute; pour lutter contre les deux &laquo; types &raquo; de pathog&egrave;nes de la fusariose (<em>Fusarium </em>spp<em>.</em> et <em>Microdochium </em>spp.) mais s&rsquo;il est utilis&eacute; en 39, d&rsquo;autres mol&eacute;cules doivent &ecirc;tre privil&eacute;gi&eacute;es pour ce traitement. Le t&eacute;buconazole et le metconazole sont, quant &agrave; eux, utiles uniquement contre les <em>Fusarium </em>spp.</p>

<p style="text-align:right"><strong><em><u><span style="color:black">Coordination scientifique</span></u></em></strong><em> <strong>:</strong></em> Groupe &laquo; maladies &raquo; A. Legr&egrave;ve, A. Nysten, Ch. Bataille</p>

<p>&nbsp;</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 04 juin.</span></p>
', 1, NULL, 97, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (248, 'Vos terres sont-elles infestées de vulpin résistant ?', 'left', 300, 225, '<p style="text-align:justify">La r&eacute;sistance des vulpins aux herbicides est un ph&eacute;nom&egrave;ne largement r&eacute;pandu en Europe, notamment en France et au Royaume-Uni et cela n&#39;est pas sans poser certains probl&egrave;mes. Les agriculteurs anglais, par exemple, sont parfois contraints &agrave; mettre en &oelig;uvre des programmes herbicides &agrave; 3 passages incluant en moyenne 6 substances actives diff&eacute;rentes!</p>

<p style="text-align:justify">Actuellement, le ph&eacute;nom&egrave;ne en Belgique semble &ecirc;tre en expansion et n&rsquo;est plus confin&eacute; aux zones g&eacute;ographiques pr&eacute;c&eacute;demment connues comme les Polders, le Tournaisis et la r&eacute;gion de Fosses-la-Ville. Il n&#39;est pas rare de retrouver un peu partout dans nos campagnes des taches de vulpins d&eacute;passant des c&eacute;r&eacute;ales. Cela est-il d&ucirc; &agrave; traitement mal positionn&eacute;, &agrave; des conditions climatiques non optimales au moment du traitement, &agrave; un mauvais recouvrement, ou bien &agrave; la pr&eacute;sence de vulpins r&eacute;sistants?</p>

<p style="text-align:justify">L&rsquo;an pass&eacute;, l&rsquo;Unit&eacute; Protection des Plantes et Ecotoxicologie du CRA-W a men&eacute; une enqu&ecirc;te afin d&#39;&eacute;valuer la proportion de vulpins r&eacute;sistants en Wallonie et de d&eacute;terminer les pratiques permettant un meilleur contr&ocirc;le de ces r&eacute;sistances. Les agriculteurs ayant particip&eacute; ont re&ccedil;u par la suite une lettre d&rsquo;information concernant la r&eacute;sistance de leurs vulpins ainsi que certaines recommandations.</p>

<p style="text-align:justify">Cette ann&eacute;e encore, le CRA-W vous propose de participer &agrave; cette enqu&ecirc;te et de <strong>tester gratuitement la r&eacute;sistance des vulpins pr&eacute;sents dans vos terres</strong>. Il vous est simplement demand&eacute; de r&eacute;colter les semences de vulpin &agrave; maturit&eacute; (fin juin &ndash; d&eacute;but juillet) ainsi que de nous communiquer quelques informations culturales sur la parcelle. Les vulpins pr&eacute;lev&eacute;s seront test&eacute;s en serres durant l&rsquo;hiver et les r&eacute;sultats vous seront communiqu&eacute;s. Ce type de renseignement peut vous aider &agrave; mieux appr&eacute;hender le d&eacute;sherbage de vos c&eacute;r&eacute;ales et la lutte contre le vulpin en particulier.</p>

<p style="text-align:justify">Int&eacute;ress&eacute; ? Pour recevoir plus de d&eacute;tails sur la proc&eacute;dure, merci de prendre contact avec Pierre Hellin via l&rsquo;adresse <a href="mailto:p.hellin@cra.wallonie.be">p.hellin@cra.wallonie.be</a> ou via le num&eacute;ro de t&eacute;l&eacute;phone 081 87 40 06.</p>
', 2, NULL, 97, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (277, 'Vos terres sont-elles infestées de vulpin résistant ?', 'left', 300, 225, '<p style="text-align:justify">La r&eacute;sistance des vulpins aux herbicides est un ph&eacute;nom&egrave;ne largement r&eacute;pandu en Europe, notamment en France et au Royaume-Uni et cela n&#39;est pas sans poser certains probl&egrave;mes. Les agriculteurs anglais, par exemple, sont parfois contraints &agrave; mettre en &oelig;uvre des programmes herbicides &agrave; 3 passages incluant en moyenne 6 substances actives diff&eacute;rentes!</p>

<p style="text-align:justify">Actuellement, le ph&eacute;nom&egrave;ne en Belgique semble &ecirc;tre en expansion et n&rsquo;est plus confin&eacute; aux zones g&eacute;ographiques pr&eacute;c&eacute;demment connues comme les Polders, le Tournaisis et la r&eacute;gion de Fosses-la-Ville. Il n&#39;est pas rare de retrouver un peu partout dans nos campagnes des taches de vulpins d&eacute;passant des c&eacute;r&eacute;ales. Cela est-il d&ucirc; &agrave; traitement mal positionn&eacute;, &agrave; des conditions climatiques non optimales au moment du traitement, &agrave; un mauvais recouvrement, ou bien &agrave; la pr&eacute;sence de vulpins r&eacute;sistants?</p>

<p style="text-align:justify">L&rsquo;an pass&eacute;, l&rsquo;Unit&eacute; Protection des Plantes et Ecotoxicologie du CRA-W a men&eacute; une enqu&ecirc;te afin d&#39;&eacute;valuer la proportion de vulpins r&eacute;sistants en Wallonie et de d&eacute;terminer les pratiques permettant un meilleur contr&ocirc;le de ces r&eacute;sistances. Les agriculteurs ayant particip&eacute; ont re&ccedil;u par la suite une lettre d&rsquo;information concernant la r&eacute;sistance de leurs vulpins ainsi que certaines recommandations.</p>

<p style="text-align:justify">Cette ann&eacute;e encore, le CRA-W vous propose de participer &agrave; cette enqu&ecirc;te et de <strong>tester gratuitement la r&eacute;sistance des vulpins pr&eacute;sents dans vos terres</strong>. Il vous est simplement demand&eacute; de r&eacute;colter les semences de vulpin &agrave; maturit&eacute; (fin juin &ndash; d&eacute;but juillet) ainsi que de nous communiquer quelques informations culturales sur la parcelle. Les vulpins pr&eacute;lev&eacute;s seront test&eacute;s en serres durant l&rsquo;hiver et les r&eacute;sultats vous seront communiqu&eacute;s. Ce type de renseignement peut vous aider &agrave; mieux appr&eacute;hender le d&eacute;sherbage de vos c&eacute;r&eacute;ales et la lutte contre le vulpin en particulier.</p>

<p style="text-align:justify">Int&eacute;ress&eacute; ? Pour recevoir plus de d&eacute;tails sur la proc&eacute;dure, merci de prendre contact avec Pierre Hellin via l&rsquo;adresse <a href="mailto:p.hellin@cra.wallonie.be">p.hellin@cra.wallonie.be</a> ou via le num&eacute;ro de t&eacute;l&eacute;phone 081 87 40 06.</p>
', 2, NULL, 101, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (241, 'Orge brassicole', 'left', 300, 225, '<table align="left" border="1" cellspacing="0" class="MsoTableLightShadingAccent1" style="border-collapse:collapse; border:solid #93a299 1.0pt; margin-left:4.8pt; margin-right:4.8pt; width:525.05pt">
	<tbody>
		<tr>
			<td style="width:63.2pt">
			<p style="text-align:justify"><strong>R&eacute;seau</strong></p>
			</td>
			<td style="width:461.85pt">
			<p style="text-align:justify">3 parcelles r&eacute;parties dans les localit&eacute;s : Hainaut (Vaudignies) et Namur (Gembloux, Liernu)</p>
			</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:center">&nbsp;</p>

<p><img align="" filer_id="465" height="" src="/filer/canonical/1559037093/465/" thumb_option="" title="" width="" /></p>

<p>La s&eacute;v&eacute;rit&eacute; des attaques est actuellement relativement faible sur tous les sites observ&eacute;s. Un traitement fongicide peut &ecirc;tre appliqu&eacute; sur les parcelles ayant atteint le stade 39 afin de prot&eacute;ger les principales feuilles de rendement. L&rsquo;emploi d&rsquo;un r&eacute;gulateur en orge brassicole n&rsquo;est normalement pas n&eacute;cessaire, cependant, si le traitement est jug&eacute; n&eacute;cessaire, les r&eacute;gulateurs autoris&eacute;s en escourgeon sont pour la plupart autoris&eacute;s en orge de printemps mais &agrave; des doses plus faibles.</p>

<p style="text-align:right"><strong><em><u>Coordination&nbsp; scientifique</u></em></strong><em> <strong>:</strong></em> <span style="color:black">Groupe &laquo; phytotechnie &raquo;, </span>R. Meurs</p>

<p><span style="color:#00b050">Le prochain avis est pr&eacute;vu pour le 04 juin.</span></p>
', 1, NULL, 97, '');
INSERT INTO public.msg_msgdet (id, title, img_pos, img_width, img_height, short_text, categorie_id, imgillu_id, msg_id, img_label) VALUES (280, 'A l''agenda : visites d''essais,...', 'left', 300, 225, '<p>En <strong><span style="color:red">rouge</span></strong> les activit&eacute;s reconnues dans le cadre de la phytolicence <a href="http://www.pwrp.be/agenda-phytolicence">(voir agenda phytolicence complet ici)</a> Prenez votre carte d&#39;identit&eacute;.</p>

<p>En <strong><span style="color:#00b050">vert</span></strong> : Rencontres en ferme &quot;alternatives aux pesticides chimiques de synth&egrave;se&quot;</p>

<p>&nbsp;</p>

<p><u>A l&rsquo;agenda</u> : <span style="color:#ff0000">25 juin, visite de <strong>champ d&#39;essais de Lonz&eacute;e et d&#39;Isnes</strong></span></p>

<p>Pour les visites du 25 juin, le pr&eacute;-enregistrement est souhait&eacute; <a href="https://docs.google.com/forms/d/e/1FAIpQLSfDaIq5kVXHW7ybgdPd-I8WGMPxMT9dsFO_Bxv265MvI0Qg2g/viewform">ici</a>. Toutefois, l&#39;inscription est &eacute;galement possible le jour m&ecirc;me.</p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td><span style="color:#ff0000">Mardi<br />
			25/06/2019</span></td>
			<td>Lonz&eacute;e<br />
			9 et 14h00</td>
			<td>Visite de <strong>champ d&#39;essais de froment d&#39;hiver, association froment d&#39;hiver et pois d&#39;hiver, escourgeon, avoine, orge brassicole de printemps </strong>(CePiCOP). Vieux chemin de Namur. (CePiCOP) Contact 081/62.21.39 <a href="file:///C:/Users/ASBL%20CADCO/CADCO/Site%20CADCO/p10_agenda/190625CePiCOP.pdf">Programme.pdf</a></td>
		</tr>
		<tr>
			<td><span style="color:#ff0000">Mardi<br />
			25/06/2019</span></td>
			<td>Isnes<br />
			9 et 14h00</td>
			<td>Visite de <strong>champ d&#39;essais de colza, pois prot&eacute;agineux, f&eacute;veroles, lupin et cultures innovantes </strong>(CePiCOP). N912 chauss&eacute;e d&#39;Eghez&eacute;e. Contact 081/62.21.39 <a href="file:///C:/Users/ASBL%20CADCO/CADCO/Site%20CADCO/p10_agenda/190625CePiCOP.pdf">Programme.pdf</a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p><u>D&#39;autres visites</u> &agrave; l&#39;agenda :</p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>Jeudi 20/06/2019</td>
			<td>Noville<br />
			18h00</td>
			<td>Visite de <strong>champ d&#39;essais froment d&#39;hiver</strong> (CPL Vegemar/CePiCOP). Grand&#39;Route. Chez Louis Streel.</td>
		</tr>
		<tr>
			<td><span style="color:#008000">Vendredi 21/06/2019</span></td>
			<td>Habbey-la-Vieille<br />
			14h00-17h00</td>
			<td>Rencontre <strong>en ferme polyculture &eacute;l&eacute;vage</strong> M&eacute;lange semencier (Nature et progr&egrave;s). Biovall&eacute;e. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></td>
		</tr>
		<tr>
			<td>Lundi 24/06/2019</td>
			<td>Mortoux<br />
			10h00</td>
			<td>Visite de <strong>champ d&#39;essais froment d&#39;hiver</strong> (CPL Vegemar/CePiCOP). Chauss&eacute;e des Wallons. Chez Joseph Crauwels.</td>
		</tr>
		<tr>
			<td>Lundi 24/06/2019</td>
			<td>Pailhe<br />
			16h00</td>
			<td>Visite de <strong>champ d&#39;essais froment d&#39;hiver</strong> (CPL Vegemar/CePiCOP). Route de Givet. Chez Bertrand et Fr&eacute;d&eacute;ric Tasiaux.</td>
		</tr>
		<tr>
			<td>Lundi 24/06/2019</td>
			<td>Mettet<br />
			14h00</td>
			<td>Visite de <strong>champ d&#39;essais vari&eacute;t&eacute;s froment</strong> (CRA-W/CePiCOP). Rue de Bure 16-14 &agrave; 5640 Mettet chez Mr Y. Van den Abeele.</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table class="tableauavecbords">
	<tbody>
		<tr>
			<td>Mercredi 26/06/2019</td>
			<td>Ath<br />
			9h30 et 14h00</td>
			<td>
			<p>Visites <strong>essais c&eacute;r&eacute;ales vari&eacute;t&eacute;s et protection &quot;maladies&quot;.</strong> Rdvs &agrave; Ath, 301 rue de l&rsquo;agriculture (CARAH/CePiCOP). <a href="http://carah.be/33-experimentation-agronomique-avertissements/236-visite-des-essais-2019.html">Informations</a></p>
			</td>
		</tr>
		<tr>
			<td><span style="color:#008000">Mercredi 26/06/2019</span></td>
			<td>Montignies-Lez-Lens<br />
			14h00-17h00</td>
			<td>Rencontre <strong>en ferme polyculture &eacute;l&eacute;vage</strong> M&eacute;lange semencier (Nature et progr&egrave;s). Ferme Philippe Loeckx. Plus d&#39;info 081/323066 <a href="https://www.walloniesanspesticides.com/cereale">inscription et programme</a></td>
		</tr>
		<tr>
			<td>vendredi 05/07/2019</td>
			<td>Horion-Hoz&eacute;mont<br />
			10h00 &agrave; 19h00</td>
			<td>Journ&eacute;e <strong>Interprofessionnelle du BIO</strong> (Province de Li&egrave;ge et CPL Vegemar). <a href="file:///C:/Users/ASBL%20CADCO/CADCO/Site%20CADCO/p10_agenda/190705JourInterprofBio.pdf">Programme.pdf</a></td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify"><u><strong>Voir tout l&rsquo;agenda</strong></u> : toutes les visites d&rsquo;essais en cultures de c&eacute;r&eacute;ales d&rsquo;ol&eacute;agineux et de prot&eacute;agineux, cliquez <strong><a href="http://www.cadcoasbl.be/p10_agenda.html">ici</a></strong></p>

<p style="text-align:justify">&nbsp;</p>
', 2, NULL, 102, '');


--
-- Data for Name: msg_msgdet_culture; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (1, 1, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (2, 4, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (4, 9, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (7, 11, 23);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (8, 6, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (362, 145, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (11, 15, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (141, 72, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (142, 73, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (143, 73, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (144, 73, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (145, 71, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (147, 44, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (148, 68, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (149, 74, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (150, 75, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (151, 84, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (152, 86, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (153, 86, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (154, 86, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (155, 85, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (156, 85, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (157, 85, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (33, 18, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (34, 18, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (35, 17, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (36, 17, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (37, 19, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (38, 19, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (39, 16, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (40, 16, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (41, 20, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (42, 22, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (43, 22, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (272, 126, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (273, 126, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (47, 23, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (48, 23, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (49, 23, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (165, 89, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (166, 89, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (52, 25, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (53, 25, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (54, 24, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (55, 24, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (56, 27, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (274, 126, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (276, 127, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (61, 28, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (62, 28, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (64, 29, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (67, 31, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (68, 31, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (70, 32, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (71, 30, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (72, 30, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (73, 33, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (75, 34, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (76, 35, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (77, 37, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (78, 37, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (79, 38, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (80, 40, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (81, 41, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (82, 41, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (83, 42, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (84, 42, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (85, 43, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (86, 43, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (88, 45, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (89, 46, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (90, 47, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (176, 90, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (177, 90, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (178, 90, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (179, 87, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (180, 87, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (181, 87, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (182, 87, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (99, 50, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (100, 48, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (101, 51, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (102, 51, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (103, 52, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (105, 53, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (106, 54, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (107, 56, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (108, 57, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (186, 94, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (188, 92, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (190, 98, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (191, 102, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (192, 102, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (117, 60, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (119, 64, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (195, 101, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (122, 67, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (123, 67, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (196, 99, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (197, 99, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (198, 99, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (199, 103, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (128, 66, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (200, 104, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (201, 100, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (132, 65, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (136, 70, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (138, 69, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (208, 106, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (213, 109, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (215, 111, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (216, 112, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (305, 128, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (306, 128, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (307, 129, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (223, 107, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (319, 131, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (320, 131, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (321, 131, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (322, 130, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (323, 130, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (240, 117, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (241, 117, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (242, 117, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (243, 116, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (244, 116, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (246, 122, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (252, 123, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (253, 123, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (255, 125, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (333, 132, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (334, 132, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (335, 132, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (345, 139, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (346, 139, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (347, 139, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (355, 142, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (359, 141, 17);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (360, 141, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (361, 141, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (665, 280, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (666, 280, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (667, 280, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (373, 147, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (374, 147, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (379, 146, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (380, 146, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (381, 148, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (382, 148, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (385, 149, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (386, 150, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (387, 150, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (388, 151, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (395, 153, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (396, 153, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (397, 152, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (398, 152, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (399, 154, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (400, 154, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (405, 161, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (406, 161, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (409, 162, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (410, 162, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (411, 159, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (418, 160, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (419, 160, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (427, 166, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (428, 171, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (429, 171, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (433, 169, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (434, 169, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (435, 170, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (444, 175, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (447, 182, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (448, 182, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (449, 174, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (450, 183, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (451, 184, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (465, 187, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (466, 187, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (467, 189, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (469, 191, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (471, 192, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (506, 200, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (507, 200, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (513, 197, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (514, 202, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (515, 198, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (516, 201, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (517, 199, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (485, 195, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (488, 196, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (489, 196, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (491, 194, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (492, 193, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (526, 208, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (527, 203, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (529, 204, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (530, 205, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (531, 206, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (536, 211, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (540, 209, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (541, 210, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (542, 212, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (544, 214, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (557, 216, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (560, 215, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (564, 219, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (565, 219, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (566, 219, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (569, 217, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (570, 218, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (572, 221, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (574, 224, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (575, 222, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (577, 226, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (578, 226, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (581, 223, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (586, 225, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (590, 227, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (591, 228, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (595, 233, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (596, 233, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (600, 230, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (601, 234, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (602, 235, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (603, 240, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (604, 236, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (607, 248, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (608, 248, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (614, 241, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (615, 243, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (617, 244, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (622, 251, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (631, 259, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (632, 260, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (633, 262, 27);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (634, 262, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (635, 262, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (637, 258, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (641, 270, 14);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (642, 270, 6);
INSERT INTO public.msg_msgdet_culture (id, msgdet_id, culture_id) VALUES (646, 266, 6);


--
-- Name: msg_msgdet_culture_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.msg_msgdet_culture_id_seq', 667, true);


--
-- Name: msg_msgdet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.msg_msgdet_id_seq', 282, true);


--
-- Data for Name: msg_msgdet_ram; Type: TABLE DATA; Schema: public; Owner: cpadmin
--

INSERT INTO public.msg_msgdet_ram (id, msgdet_id, ram_id) VALUES (2, 6, 63);
INSERT INTO public.msg_msgdet_ram (id, msgdet_id, ram_id) VALUES (6, 16, 29);
INSERT INTO public.msg_msgdet_ram (id, msgdet_id, ram_id) VALUES (7, 162, 56);


--
-- Name: msg_msgdet_ram_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpadmin
--

SELECT pg_catalog.setval('public.msg_msgdet_ram_id_seq', 7, true);


--
-- PostgreSQL database dump complete
--

