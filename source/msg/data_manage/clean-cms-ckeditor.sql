select ck.cmsplugin_ptr_id, ck.body, det.short_text2_id, det.msg_id, det.id, pl.plugin_type
from djangocms_text_ckeditor_text ck, cms_cmsplugin pl, msg_msgdet det
where ck.cmsplugin_ptr_id = pl.id and det.short_text2_id = pl.placeholder_id
--and det.title like 'Le colza : Les altises, à surveiller de près sur petites plantes !.%'
--and cmsplugin_ptr_id = 19
and body like '%Times New Roman%'


update djangocms_text_ckeditor_text set body = replace(replace(replace(body, '<font face="Times New Roman, serif">',''), '<font size="3">',''), '<font size="2">', '')
where cmsplugin_ptr_id > 25;

update djangocms_text_ckeditor_text set body = replace(replace(replace(body, 'font-size: 11.0pt;',''), 'line-height: normal;',''), 'font-size: 11pt;', '')
where cmsplugin_ptr_id > 25;

update djangocms_text_ckeditor_text set body = replace(replace(replace(body, 'font-family: "Times New Roman",serif;',''), 'font-size: 10.0pt;',''), 'line-height: 115%;', '')
where cmsplugin_ptr_id > 25;

update djangocms_text_ckeditor_text set body = replace(replace(body, 'font-family: Arial,sans-serif;',''), 'margin-bottom: 13px;',''))
where cmsplugin_ptr_id > 25;

/*
img tag must be fluid 
to do!!! */
update djangocms_text_ckeditor_text set body = replace(body, '<img alt=""','<img class="img-fluid" alt=""')
where cmsplugin_ptr_id > 25;

select body from  djangocms_text_ckeditor_text  where body like '%<img %'
--<figure><img
select count(*) from  djangocms_text_ckeditor_text  where body like '%<figure><img%'



select ck.cmsplugin_ptr_id, ck.body, det.short_text2_id, det.msg_id, det.id, pl.plugin_type
from djangocms_text_ckeditor_text ck, cms_cmsplugin pl, msg_msgdet det
where ck.cmsplugin_ptr_id = pl.id and det.short_text2_id = pl.placeholder_id
--and det.title like 'Le colza : Les altises, à surveiller de près sur petites plantes !.%'
--and cmsplugin_ptr_id = 19
and ck.body like '%http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert%'


select * from  djangocms_text_ckeditor_text
where cmsplugin_ptr_id > 25
and body like '%www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>o%'


select * from  djangocms_text_ckeditor_text
where cmsplugin_ptr_id > 25
and body like '%<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille naine</strong></a>%'

/* oidium */
update djangocms_text_ckeditor_text set body = replace(body, 
    'www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>o',
    'https://www.livre-blanc-cereales.be/thematiques/maladies/froment/oidium/" target="_blank"><strong>o')
where cmsplugin_ptr_id > 25;

/* rouille naine */
update djangocms_text_ckeditor_text set body = replace(body, 
    '<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rouille naine</strong></a>',
    '<a href="https://www.livre-blanc-cereales.be/thematiques/maladies/maladies-escourgeon/rouille-naine/" target="_blank"><strong>rouille naine</strong></a>')
where cmsplugin_ptr_id > 25;

/* rhynchosporiose */
update djangocms_text_ckeditor_text set body = replace(body, 
    '<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert"><strong>rhynchosporiose</strong></a>',
    '<a href="https://www.livre-blanc-cereales.be/thematiques/maladies/maladies-escourgeon/rhynchosporiose/" target="_blank"><strong>rhynchosporiose</strong></a>')
where cmsplugin_ptr_id > 25;

/* helminto 
en dernier car impossible de chercher sur &rsquo;
<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a>
replaced by
<a href="https://www.livre-blanc-cereales.be/thematiques/maladies/maladies-escourgeon/helminthosporiose/" target="_blank">&rsquo;<strong>helminthosporiose</strong></a>
 */
update djangocms_text_ckeditor_text set body = replace(body, 
    '<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">',
    '<a href="https://www.livre-blanc-cereales.be/thematiques/maladies/maladies-escourgeon/helminthosporiose/" target="_blank">')
where cmsplugin_ptr_id > 25;


/* ci-dessous, tests pour tenter de recherche sur des caractères accentués HTML comme apostrophe (&rsquo;)*/
select to_tsvector(body) from djangocms_text_ckeditor_text
WHERE body @@ to_tsquery('helminthosporiose');  */
--WHERE body @@ to_tsquery('<a href="http://www.cadcoasbl.be/p08_brochures.html#plaquetteavert">&rsquo;<strong>helminthosporiose</strong></a>');

select * from djangocms_text_ckeditor_text
WHERE body like concat('%',chr(38),'<strong>helminthosporiose%';


select ASCII('&rsquo;')
select chr(38)