select * from msg_msgdet where msg_id = 166
select * from cms_placeholder where id in (61,62,63)
select * from djangocms_text_ckeditor_text

-- Below update run on 2020/10/19
update djangocms_text_ckeditor_text set body = replace(replace(replace(body, '<font face="Times New Roman, serif">',''), '<font size="3">',''), '<font size="2">', '')
where cmsplugin_ptr_id > 25;

update djangocms_text_ckeditor_text set body = replace(replace(body, '<i>',''), '</i>','');