# -*- coding: utf-8 -*-
"""
msg.urls description

.. todo:: ajouter la navigation dans les centres pilotes
.. todo:: ajouter la navigation dans les cultures
"""
# from django.conf.urls import url
from django.urls import path
from django.conf import settings

from . import views

__author__ = "jph"
__email__ = "jph@openjph.be"
__copyright__ = "Copyright 2016, Jean Pierre Huart"
__license__ = "GPLv3"
__date__ = "2016-12-01"
__version__ = "1.1"
__status__ = "Development"

app_name = "msg"
# pour pouvoir utiliser msg:rech_mult dans les url

urlpatterns = [
    path("", views.MsgListView.as_view(), name="msg_list"),
    # path("oleagineux?listeenvoi=15", views.MsgListView.as_view(), name="msg_list_oleo"),
    path("rechmult/", views.MsgListView.as_view(), name="rechmult"),
    path("new/", views.msg_new, name="new"),
    path("<pk>", views.MsgDetailView.as_view(), name="msg_consult"),
    path("<pk>/envoyer_confirm/", views.MsgDetailView.as_view(), name="envoyer_confirm"),
    path("<pk>/publier_confirm/", views.MsgDetailView.as_view(), name="publier_confirm"),
    path("<pk>/copier_confirm/", views.MsgDetailView.as_view(), name="copier_confirm"),
    path("<pk>/del_confirm/", views.MsgDetailView.as_view(), name="supprimer_confirm"),
    path("<msg_id>/envoyer/", views.msg_envoyer, name="envoyerok"),
    path("<msg_id>/publier/", views.msg_publier, name="publierok"),
    path("<msg_id>/copier/", views.msg_copier, name="copierok"),
    path("<msg_id>/del/", views.msg_delete, name="supprimerok"),
    path("<msg_id>/upd/", views.msg_update, name="msg_upd"),
    path("<msg_id>/preview/", views.preview_email, name="preview_email"),
    # path("<pk>/del/", views.MsgDeleteView.as_view(), name="supprimer"),
    path("<msg_id>/<msgdet_id>/viewdetail/", views.msgdet_consult, name="msgdet_consult"),
]
