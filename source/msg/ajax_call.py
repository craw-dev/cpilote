
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from .models import MsgDet

@login_required
def msgdet_delete(request):
    if request.method == "POST":
        # if "pk" in request.GET:
        msgdet_id = request.POST.get("msgdet_id")
        try:
            msgdet = get_object_or_404(MsgDet, pk=msgdet_id)
            msgdet.delete()
        except Exception as e:
            data = {
                "msg": "Erreur {0} lors de la suppression msgdet_id: {1}.".format(
                    e, msgdet_id
                )
            }
        data = {"msg": "{0} supprimé.".format(msgdet_id)}

    return JsonResponse(data)