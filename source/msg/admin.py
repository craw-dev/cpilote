# -*- coding: utf-8 -*-
"""
msg.admin description
"""
from django.contrib import admin
from cms.admin.placeholderadmin import PlaceholderAdminMixin
from .models import Msg, MsgDet, Categorie

class MsgDetAdmin(PlaceholderAdminMixin, admin.StackedInline):
    list_filter = ['title', 'short_text2', 'statut']
    search_fields = ['title', 'short_text2']
    model = MsgDet
    extra = 2

class MsgAdmin(PlaceholderAdminMixin, admin.ModelAdmin):
    # list_display = ('full_name_display', 'listeenvoi')
    list_filter = ('centrepilote', 'listeenvoi',)
    search_fields = ('title',)
    ordering = ('-msg_created',)
#     fieldsets = [
#         (None,               {'fields': ['question_text']}),
#         ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
#     ]
#     filter_vertical = ('culture','ram',)
    inlines = [MsgDetAdmin]
#     list_display = ('title', 'short_text', 'statut')


admin.site.register(Msg, MsgAdmin)
admin.site.register(Categorie)