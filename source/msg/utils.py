"""
Created on 7 nov. 2017

@author: p.houben
"""
# import django
# django.setup()
import os, bs4, re
import logging

# External lib
from filer.models.filemodels import File
from email.mime.image import MIMEImage

# Django lib
from django.contrib.auth import get_user_model
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import unquote_plus
from cms.models.pluginmodel import CMSPlugin
from djangocms_text_ckeditor.models import Text

# settings
from config import settings

# models
from accounts.models import Profile, Inscription
from msg.models import Msg, MsgDet, Categorie
from accounts.models import ListeEnvoi


LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")


def rm_last_digit(text):
    textnew = re.sub('\d*$', '', text)
    return textnew


def render_to_pdf(template_src, context_dict={}):
   template = get_template(template_src)
   html  = template.render(context_dict)
   result = BytesIO()
   pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
   if not pdf.err:
       return HttpResponse(result.getvalue(), content_type='application/pdf')
   return None


def get_email_list(listeenvoi):
    """
    Return list of list of 99 email inscrit à la liste d'envoi AdHoc
    Cut list over 99 to avoid possible limitation
    """
    if settings.SERVER_NAME in ("pythonprod", "scw-cpilote"):
        emaillist_block = []
        emaillist = Inscription.objects.filter(listeenvoi=listeenvoi, is_valid=True).values_list('profile__email', flat=True)
        if len(emaillist) > 0:
            chunck_size = settings.EMAIL_ADDRESSES_CHUNCK_MAX
            emaillist_block = [
                emaillist[i : i + chunck_size]
                for i in range(0, len(emaillist), chunck_size)
            ]
    else:
        emaillist_block = [['houbenpatrick1@yahoo.fr', 'houbenp@gmail.com', 'p.houben@cra.wallonie.be']]

    return emaillist_block


class SendEmailImages(EmailMultiAlternatives):
    """
    Inherit from EmailMultiAlternatives
    """

    content_subtype = "html"  # Main content is text/html
    mixed_subtype = (
        "related"
    )  # This is critical, otherwise images will be displayed as attachments!

    def __init__(
        self,
        subject="",
        body="",
        from_email=None,
        to=None,
        bcc=None,
        connection=None,
        attachments=None,
        headers=None,
        alternatives=None,
        cc=None,
        reply_to=None,
    ):
        super(SendEmailImages, self).__init__(
            subject,
            body,
            from_email,
            to,
            bcc,
            connection,
            attachments,
            headers,
            cc,
            reply_to,
        )

    def add_imgfile(self, filepath, filename):
        img = self.attach_image(filepath, filename)
        self.attach(img) # attach() defined in EmailMessage

    def load_static(self, staticpath, filename):
        """
        filename: name of the image file
        staticpath: static path after static folder
        for banner staticpath=static/img/banner/
        1. Attach image to the mail through add_imgfile
        2. Return a dict with 'file', 'path' and 'url' keys
        """
        mydict = {}
        mydict['file'] = filename
        pathvar = os.path.join(staticpath, mydict['file'])
        mydict['url'] = os.path.join("/", pathvar)
        mydict['path'] = os.path.join(settings.BASE_DIR, pathvar)
        self.add_imgfile(mydict['path'], mydict['file'])
        return mydict

    # def load_logo(self, keyword):
    #     """
    #     Search logo files in DB Filer logo folder
    #     original_filename: the filename only
    #     file: relative path to the file from /filer_public/
    #     """
    #     mydict = {'file': keyword, 'url': 'nada'}
    #     logo = File.objects.all().filter(folder__name='centrespilotes', original_filename__icontains=keyword)
    #     if logo:
    #         logo = logo[0]
    #         self.add_imgfile(logo.file.path, logo.original_filename)
    #         mydict['file'] = logo.original_filename
    #         mydict['url'] = os.path.join(settings.MEDIA_URL, str(logo.file))
    #     return mydict

    def build_msg_nextdate_comm(self, msg):
        mytxt = ''
        if msg.display_nextdate:
            if msg.msg_nextdate is None:
                mytxt = "Dernier avis de la saison. À l'année prochaine."
            else:
                mytxt = f'Prochain avis prévu le {msg.msg_nextdate.strftime("%d/%m/%Y")}'
        return mytxt

    def process_msgdet(self, request, msg, template_name):
        current_site = get_current_site(request)
        msgdet = msg.msgdet_set.all()

        # If user with centrespilotes email use it
        if "centrespilotes.be" in request.user.email:
            self.from_email = f"Plateforme Centre Pilote <{request.user.email}>"
        else:
            self.from_email = settings.DEFAULT_FROM_EMAIL
        # BANNER FILE
        cpcode = msg.centrepilote.code
        banner = self.load_static("static/img/banner/", f"banner-{cpcode}.webp")
        # LOGO
        # logocp = self.load_logo(cpcode)
        logocp = {}
        logocp['file'] = str(msg.centrepilote.logo)
        logocp['url'] = msg.centrepilote.logo.url
        logocp['path'] = msg.centrepilote.logo.path
        self.add_imgfile(logocp['path'], logocp['file'])
        # logospw = self.load_static("static/img/logo/", "spw172.png")
        # Summary Image
        summaryfile = {}
        if msg.summary_img is not None:
            size_option = {'size': (300, 225), 'crop': True}
            thumb_url = msg.summary_img.file.get_thumbnail(size_option)
            thumb_url_str = str(thumb_url)
            lpos = thumb_url_str.rfind('/', 0) + 1
            summaryfile['file'] = thumb_url_str[lpos:]
            # summaryfile['file'] = msg.summary_img.original_filename # nom fichier original sans accents et espaces!=>ne fonctionne pas avec cid:
            summaryfile['url'] = os.path.join(settings.MEDIA_URL, thumb_url_str)
            summaryfile['path'] = thumb_url.path # OR summaryfile['path'] = os.path.join(settings.MEDIA_ROOT, thumb_url_str)
            #LOG_DEBUG.debug(f"filepath1={summaryfile['path']} - filename1={summaryfile['file']}")
            self.add_imgfile(summaryfile['path'], summaryfile['file'])
        else:
            summaryfile['file'] = str(msg.centrepilote.logo)
            summaryfile['url'] = msg.centrepilote.logo.url
            summaryfile['path'] = msg.centrepilote.logo.path
            #LOG_DEBUG.debug(f"filepath2={summaryfile['path']} - filename2={summaryfile['file']}")
            self.add_imgfile(summaryfile['path'], summaryfile['file'])

        protocol = "https" if request.is_secure() else "http"
        base_url = "{0}://{1}".format(protocol, current_site.domain)
        cploc_list = msg.centrepilote.centrepilotelocation_set.exclude(importancenr=0).order_by('importancenr')
        # get team members
        user_model = get_user_model()
        cpteam_list = user_model.objects.filter(work4_cp=msg.centrepilote).order_by('jobdesc_order')

        msg_nextdate_comm = self.build_msg_nextdate_comm(msg)

        if msg.listeenvoi.get_partenaires_list is not None:
            for p in msg.listeenvoi.partenaires.all():
                path = p.get_logopath()
                lpos = path.rfind('/', 0) + 1
                filename = path[lpos:]
                pathok = path.replace('/media/', '')
                pathname = os.path.join(settings.MEDIA_ROOT, pathok)
                self.add_imgfile(pathname, filename)
        
        categorie = Categorie.objects.all()

        # Build context for HTML rendering
        context = {
            "msg": msg,
            "msgdet": msgdet,
            "categorie": categorie,
            "bannerfile": "cid:" + banner["file"],
            "logocp": "cid:" + logocp["file"],
            # "logospw": "cid:" + logospw["file"],
            "summaryfile": "cid:" + summaryfile['file'],
            "cploc_list": cploc_list,
            "cpteam_list": cpteam_list,
            "partenaires": msg.listeenvoi.get_partenaires_list4email,
            "base_url": base_url,
            "site_name": current_site.name,
            "msg_nextdate_comm": msg_nextdate_comm,
        }

        # Build body
        mybody = render_to_string(template_name, request=request, context=context).strip()
        with open("msg/templates/msg/email/email_tmp_before.html", "w") as file:
            file.write(mybody)
        self.body = self.body_parsing(mybody, base_url)
        with open("msg/templates/msg/email/email_tmp_after.html", "w") as file:
            file.write(self.body)

        #
        # OVERWRITING some context key for display purpose !!!
        #
        context["bannerfile"] = banner["url"]
        context["logocp"] = logocp["url"]
        context["summaryfile"] = summaryfile["url"]
        context["partenaires"] = msg.listeenvoi.get_partenaires_list4email_preview
        context["preview_mail"] = True
        context["body"] = self.body
        return context


    def body_parsing(self, htmlbody, root_url):
        """
        - 1. IMAGES Search img tag in email body, attach file & add cid to tag
        - 2. DOCUMENTS HREF Search in email body & attach file
        - 3. remove all attrs from msgdet table and add cellpadding on all tds
        - 4. font must be Lato,sans-serif;
        """
        def path_parsing(original_path):
            original_path = original_path.replace("/media/", "")
            lpos = original_path.rfind('/', 0) + 1
            filename = original_path[lpos:]
            pathname = original_path[:lpos]
            filepath = os.path.join(settings.MEDIA_ROOT, pathname, filename)
            return filepath, filename

        def get_filerfile(original_path):
            """
            Récupération de l'image dans File
            File.file = path complet
            original_filename => avec espaces et accents or il nous faut le fichier tel qu'il est strocké sur le serveur
            => on reprend file (path complet et on garde le nom du fichier à la fin)
            """
            # size_option = {'size': (300, 225), 'crop': True}
            filename = ''
            filepath = ''
            try:
                path_search = original_path.replace("/media/", "")
                path_search = path_search.replace("filer_public_thumbnails/", "")
                # Cut path into 2
                path_search = path_search.split("__")
                fileimg = File.objects.get(file=path_search[0])
                #LOG_DEBUG.debug(f"original_filename={fileimg.original_filename} - _file_size={fileimg._file_size} - _width={fileimg._width}")
                # Supérieur à 100 Ko
                if fileimg._file_size > 100000 or fileimg._width > 500:
                    if fileimg._height > fileimg._width:
                        size_option = {'size': (225, 300)}
                    else:
                        size_option = {'size': (300, 225)}
                    filepath = fileimg.file.get_thumbnail(size_option).path
                    #filename = fileimg.original_filename => avec espaces et accents or il nous faut le fichier tel qu'il est strocké sur le serveur
                    pathlist = filepath.split('/')
                    pathlen = len(pathlist)
                    filename_full = pathlist[pathlen-1]
                    filename = filename_full.split('__')[0]
                    #LOG_DEBUG.debug(f"filepath={filepath} - filename={filename}")
                else:
                    # if the size is ok, we keep the original path
                    return path_parsing(original_path)
            except File.DoesNotExist:
                # This should never happen, since the file is in media/filer_public
                LOG.warning("FILE DOES NOT EXIST: {}".format(original_path))
                return path_parsing(original_path)
            return filepath, filename

        soup = bs4.BeautifulSoup(htmlbody, "html.parser")
        # LOG.warning(soup)
        # 1. IMAGES
        imgs = soup.body.find_all("img")
        for img in imgs:
            # LOG.debug(img["src"])
            src = str(img["src"])
            # unquote_plus (url decode pour garder les , espaces, etc.)
            src = unquote_plus(src)
            # Keep only filer images
            if src.find("/media/filer_public") == 0:
                filepath, filename = get_filerfile(src)
                self.add_imgfile(filepath, filename)
                img["src"] = "cid:{0}".format(filename)

        # 2. ATTACHED DOCUMENTS
        for a in soup.find_all("a", href=True):
            if a['href'].find("/media/filer_public") == 0:
                filepath, filename = path_parsing(str(a['href']))
                self.attach_file(filepath)
                # a['href'] = "cid:{}".format(filename) ne fonctionne pas...
                a['href'] = root_url + a['href']

        # 3. msgdet placeholder Table formating
        for mytd in soup.find_all("td", {"class": "msgdet"}):
            for tbl in mytd.find_all("table"):
                tbl.attrs.clear()
                tbl["class"] = "table-cell-padding"
                for tds in tbl.find_all("td"):
                    if "style" in tds.attrs:
                        content_bg = keep_tag_style(mytag=tds["style"], style_attr='background')
                        content_txt = keep_tag_style(mytag=tds["style"], style_attr='text-align')
                        style_keep = f"{content_bg} {content_txt}"
                        tds["style"] = ""
                        tds["style"] = style_keep

            for myuls in mytd.find_all("ul"):
                myuls.attrs.clear()
                for mylis in myuls.find_all("li"):
                    mylis.attrs.clear()

        # 4. change font
            # for font in mytd.find_all("font"):
        for font in soup.find_all("font"):
            font.attrs.clear()
            # font["face"] = "Helvetica,sans-serif"

        # 5. rm <script> tag containing cms-pluing
        for script_tag in soup.find_all("script"):
            script_tag.decompose()

        # 6. rm <template> tag containing cms-pluing
        for template_tag in soup.find_all("template"):
            template_tag.decompose()

        # 7. rm <div class="cms-placeholder">
        for cmsplaceholder_tag in soup.find_all("div", {"class": "cms-placeholder"}):
            cmsplaceholder_tag.decompose()

        # Comment 09/06/2021! span tag and p attr should remain in email after publication!
        # # 8. clean <p style>
        # for p_tag in soup.find_all("p"):
        #     p_tag["style"] = ""
        #     p_tag["class"] = ""

        # # 9. clean <span> tag
        # for span_tag in soup.find_all("span"):
        #     span_tag.attrs.clear()

        emailbody = str(soup)
        # emailbody_clean = emailbody.replace('<span>','').replace('</span>', '').replace(' style="">','>')
        emailbody_clean = emailbody.replace(' style="">','>')
        # LOG_DEBUG.debug(emailbody_clean)
        return emailbody_clean

    def attach_image(self, imgpath, imgfile):
        """
        imgpath: absolute path with filename
            ie: /var/www/cpilote/media/filer_public_thumbnails/filer_public/42/ea/42ea004e-8404-48ea-bf0e-41e674a09ec6/velo1.jpg__400x300_q85_crop_subsampling-2.jpg
        imgfile: file name (colza.jpg)
        Open an image file based on a provided path, add it 2 header
        1) Content-ID in order to be able to use the tag <img src="cid:{{ logofile }}"
        2) Content-Disposition in order to name the file
        Returns an instance of a MIMEImage
        """
        with open(imgpath, mode="rb") as fp:
            image = MIMEImage(fp.read())
        image.add_header("Content-ID", "<{0}>".format(imgfile))
        image.add_header(
            "Content-Disposition", "inline", filename="{0}".format(imgfile)
        )
        return image


def keep_tag_style(mytag, style_attr='color'):
    """
    Args:
    - tag: soup tag item
    - stylekeep: style item to be kept
    """
    keep_content_style = ""
    pos = mytag.find(style_attr)
    if pos >= 0:
        next_sep = mytag.find(';', pos)
        keep_content_style = mytag[pos:next_sep+1]
    return keep_content_style


def msg_clean(from_id, to_id=None):
    """
    - Called from msg.views.msg_publish
    - Launch from command line with 2 arguments
    Args: from_id=153 MINIMUM, to_id=...
    without to_id arg fetch only from_id arg
    Clean up is done in CMSPlugin of PlaceholderField (model) from_id 152 onwards
    short_text with RichTextField: before id 152
    short_text2 with PlaceholderField (msgdet_slot) : before id 152
    """
    try:
        # Check from_id
        if from_id is None:
            return f'You must provide a from_id argument'
        try:
            from_id = int(from_id)
        except ValueError:
            return f'from_id argument must be an integer'

        # Check to_id
        if to_id is None:
            msg = Msg.objects.filter(id=from_id)
            result = f'msg {from_id} Cleaned'
        else:
            try:
                to_id = int(to_id)
            except ValueError:
                return f'to_id argument must be an integer'
            msg = Msg.objects.filter(id__gte=from_id, id__lte=to_id)
            result = f'msg {from_id} to {to_id} Cleaned'

        # run code
        for m in msg:
            msgdet = m.msgdet_set.all()

            for d in msgdet:
                if d.short_text2 is not None:
                    try:
                        plugin = CMSPlugin.objects.get(placeholder_id=d.short_text2.id, plugin_type='TextPlugin')
                        cketxt = Text.objects.get(cmsplugin_ptr=plugin)
                        soup = bs4.BeautifulSoup(cketxt.body, "html.parser")
                        # LOG_DEBUG.debug(soup)
                        # <p> clean
                        for p_tag in soup.find_all("p"):
                            # p_tag["style"] = ""
                            # p_tag["class"] = "" # do not clean 'brute force'! Keep color and background!
                            if "style" in p_tag.attrs:
                                content_color = keep_tag_style(mytag=p_tag["style"], style_attr='color')
                                content_bg = keep_tag_style(mytag=p_tag["style"], style_attr='background')
                                style_keep = f"{content_bg} {content_color}"
                                p_tag["style"] = ""
                                p_tag["style"] = style_keep
                               
                        # <font>
                        for font in soup.find_all("font"):
                            font.attrs.clear()
                        # <span> clean
                        for span_tag in soup.find_all("span"):
                            span_tag.attrs.clear()
                        for tbl in soup.find_all("table"):
                            tbl.attrs.clear()
                            # tbl["class"] = "table-cell-padding"
                            for trs in tbl.find_all("tr"):
                                trctn = 0
                                for tds in trs.find_all("td"):
                                    style_keep = ""
                                    tdctn = trctn + 1
                                    if "style" in tds.attrs:
                                        content_bg = keep_tag_style(mytag=tds["style"], style_attr='background')
                                        content_txt = keep_tag_style(mytag=tds["style"], style_attr='text-align')
                                        style_keep = f"{content_bg} {content_txt}"
                                        tds["style"] = ""
                                        tds["style"] = style_keep

                                    # if first column and presence of image (cms-plugin) set margin 40%
                                    if tdctn == 1 and tbl.find('cms-plugin'):
                                        tds["style"] = tds["style"] + 'width: 40%;'

                                    # for ptag in tds.find_all("p"):
                                    #     if ptag.text.strip() == "":
                                    #         ptag.decompose()
                                    #     for cmsp in ptag.find_all("cms-plugin"):
                                    #         jpgfind = cmsp.find('jpg')
                                    #         pngfind = cmsp.find('png')
                                    #         if jpgfind == -1 and pngfind == -1:
                                    #             ptag.decompose()
                        bodydet = str(soup)
                        bodydet_clean = bodydet.replace('<span>','').replace('</span>', '').replace('style="">','>')
                        cketxt.body = bodydet_clean
                        cketxt.save()
                    except CMSPlugin.DoesNotExist:
                        pass

    except Exception as xcpt:
        result = f"from_id: {from_id}, to_id: {to_id}, ERROR: {xcpt}"
        LOG.error(result)

    return result



def msg_clean_rtf(from_id, to_id=None):
    """
    Launch from command line with 2 arguments
    Args: from_id=0, to_id=152 MAXIMUM!!!
    After id 152 the model use short_text2 (based on PlaceholderField)
    => use of msg.utils.msg_clean
    """    
    try:
        # Check from_id
        if from_id is None:
            return f'You must provide a from_id argument'
        try:
            from_id = int(from_id)
        except ValueError:
            return f'from_id argument must be an integer'
        # Check to_id
        if to_id is None:
            msg = Msg.objects.filter(id=from_id)
            result = f'msg {from_id} Cleaned'
        else:
            try:
                to_id = int(to_id)
            except ValueError:
                return f'to_id argument must be an integer'
            msg = Msg.objects.filter(id__gte=from_id, id__lte=to_id)
            result = f'msg {from_id} to {to_id} Cleaned'

        # run code
        for m in msg:
            msgdet = m.msgdet_set.all()

            for d in msgdet:
                soup = bs4.BeautifulSoup(d.short_text, "html.parser")
                # <p> clean
                for p_tag in soup.find_all("p"):
                    if "style" in p_tag.attrs:
                        p_tag["style"] = ""
                # <span> clean
                for span_tag in soup.find_all("span"):
                    span_tag.attrs.clear()

                bodydet = str(soup)
                bodydet_clean = bodydet.replace('<span>', '').replace('</span>', '').replace('style="">', '>')

                d.short_text = bodydet_clean
                d.save()

    except Exception as xcpt:
        result = f"from_id: {from_id}, to_id: {to_id}, ERROR: {xcpt}"
        LOG.error(result)

    return result


def get_msgid_from_image(filename):
    """
    filename image search into short_text2 of msgdet
    return a list of msg_id containing the image
    """
    msgid = []
    try:
        txt = Text.objects.filter(body__icontains=filename).values('body')
        if txt:
            txt_list = list(txt)
            for txt1 in txt_list:
                txtbody = txt1['body']
                # look for plugin image id
                idpos = txtbody.find('id="')
                idnexttag = txtbody.find('"', idpos+4)
                plugid = txtbody[idpos+4:idnexttag]

                # get plugin
                plugin = CMSPlugin.objects.get(id=plugid, plugin_type='PicturePlugin')

                # get msgdet and msg concerned
                msgdet = MsgDet.objects.get(short_text2__id=plugin.placeholder_id)
                msgid.append(msgdet.msg_id)
        else:
            msgid.append('Nothing found')
    except Exception as xcpt:
        result = f"image_name: {image_name} ERROR: {xcpt}"
        LOG.error(result)
        return result

    return msgid
