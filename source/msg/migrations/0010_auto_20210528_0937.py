# Generated by Django 3.1.6 on 2021-05-28 07:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('msg', '0009_auto_20201027_0828'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='msg',
            options={'ordering': ['-id'], 'verbose_name': 'Message'},
        ),
        migrations.AlterModelOptions(
            name='msgdet',
            options={'ordering': ['title']},
        ),
        migrations.AddField(
            model_name='msg',
            name='display_nextdate',
            field=models.BooleanField(default=False, help_text="Cochez cette case pour afficher la date prévue du prochain avis dans l'email d'avertissement.", verbose_name='Afficher la date du prochain avis'),
        ),
        migrations.AddField(
            model_name='msg',
            name='msg_nextdate',
            field=models.DateField(blank=True, help_text="Ne rien saisir pour afficher 'Dernier avertissement de la saison' dans l'email d'avertissement.", null=True, verbose_name='Prochain avis'),
        ),
    ]
