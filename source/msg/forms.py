# -*- coding: utf-8 -*-
"""
msg.forms description
"""
# import datetime
import pytz
import logging

from datetime import date
from django import forms
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from config.forms import (SearchForm, FilterForm, BaseChangeListForm)
from msg.models import (Msg, MsgDet, Categorie)
from accounts.models import CentrePilote, ListeEnvoi, Inscription


LOG = logging.getLogger(__name__)

__author__ = 'jph'
__email__ = 'jph@openjph.be'
__copyright__ = 'Copyright 2017, Jean Pierre Huart'
__license__ = 'GPLv3'
__date__ = '2017-01-04'
__version__ = '1.0'
__status__ = 'Development'

from config.utils import parse_date

class MsgForm(forms.ModelForm):
    """
    Formulaire pour création nouveau message
    NB Il ne faut pas permettre de modifier le statut
    https://stackoverflow.com/questions/7970637/how-to-resize-the-new-uploaded-images-using-pil-before-saving
    """
    user = None

    class Meta:
        model = Msg
        fields = ['id', 'listeenvoi', 'statut', 'title', 'summary_img', 'summary_txt', 'display_nextdate', 'msg_nextdate']
        widgets = {
            'summary_txt': forms.Textarea(attrs={'rows':6}),
            'msg_nextdate': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            # 'msg_nextdate': forms.SelectDateWidget()
            }

    class Media:
        extend = False
        css = {
            'all': [
                'filer/css/admin_filer.css'
                # 'bootstrap_datepicker_plus/css/datepicker-widget.css'
            ]
        }
        js = (
            'js/jquery-3.5.1.min.js',
            'js/django_global.js',
            'admin/js/core.js',
            'admin/js/jquery.init.js',
            'admin/js/admin/RelatedObjectLookups.js',
            'admin/js/actions.js',
            'admin/js/urlify.js',
            'admin/js/prepopulate.js',
            'filer/js/libs/dropzone.min.js',
            'filer/js/addons/dropzone.init.js',
            'filer/js/addons/popup_handling.js',
            'filer/js/addons/widget.js',
            'ckeditor/ckeditor-init.js',
            'ckeditor/ckeditor/ckeditor.js',
            # 'bootstrap_datepicker_plus/js/datepicker-widget.js',
        )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(MsgForm, self).__init__(*args, **kwargs)
        self.fields['listeenvoi'].queryset = ListeEnvoi.objects.filter(centrepilote=self.user.manage_cp)
        self.fields["listeenvoi"].empty_label = None
        # if self.instance.statut == 'draft':
        #     del self.fields['statut']
        self.fields["listeenvoi"].required = True
        print(self.fields["msg_nextdate"])
        # self.fields["msg_nextdate"] = parse_date("2021/10/15")

    # def clean_title(self):
    #     title = self.cleaned_data['title']
    #     cpacro = self.user.manage_cp.acronyme
    #     le_list = list(self.user.manage_cp.listeenvoi_set.values_list('nom', flat=True))
    #     le_list.append(cpacro)
    #     catch = [le for le in le_list if le.lower() in title.lower()]
    #     if catch:
    #         raise forms.ValidationError(_("Phrase ou mots clés uniquement, pas de date, le mot 'actualité', nom du centre pilote ou nom de liste d'envoi"))
    #     else:
    #         return title


class MsgDetForm(forms.ModelForm):
    """
    Nouveau détail de message
    """
    def __init__(self, *args, **kwargs):
        # Get arguments
        user = kwargs.pop('user', None)
        msg = kwargs.pop('msg', None)
        super(MsgDetForm, self).__init__(*args, **kwargs)
        self.fields['msg'].initial = msg

        # Pour appliquer la même classe à tous les widgets dans le formulaire
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = MsgDet
        widgets = {'msg': forms.HiddenInput()}
        fields = ['msg', 'title', 'categorie',]


MsgDetFormSet = forms.modelformset_factory(
    MsgDet,
    fields=('title', 'categorie'),
    extra=5,
    # max_num=1,
    widgets={
        "title": forms.TextInput(attrs={"class": "form-control"}),
        "categorie": forms.Select(attrs={"class": "form-control"}),
    }
)

class MsgSearch(forms.Form):
    YEAR_CHOICES = (
        (None, "Année"),
        (2017, "2017"),
        (2018, "2018"),
        (2019, "2019"),
        (2020, "2020"),
        (2021, "2021"),      
    )
    MONTH_CHOICES = (
        (None, "Mois"),
        (1, "01"),
        (2, "02"),
        (3, "03"),
        (4, "04"),
        (5, "05"),
        (6, "06"),
        (7, "07"),
        (8, "08"),
        (9, "09"),
        (10, "10"),
        (11, "11"),
        (12, "12"),
    )    
    """
    Form search through msgdet.short_text, msgdet.title and msg.title
    in order to be able to combine search field AND filter on a specific category!
    """
    annee = forms.ChoiceField(choices=YEAR_CHOICES,
        required=False,
        widget=forms.Select(attrs={'class': "form-control"})
        )
    mois = forms.ChoiceField(choices=MONTH_CHOICES,
        required=False,
        widget=forms.Select(attrs={'class': "form-control"})
        )         
    centrepilote = forms.ModelChoiceField(
        queryset=CentrePilote.objects.filter(is_sending_msg=True),
        required=False,
        widget=forms.Select(attrs={"class": "form-control"})
    )
    listeenvoi = forms.ModelChoiceField(
        queryset=ListeEnvoi.objects.filter(centrepilote__is_sending_msg=True),
        required=False,
        widget=forms.Select(attrs={"class": "form-control"})
    )
    txtsearch = forms.CharField(
        label=_("Rechercher"), max_length=20,
        required=False,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": _("Rechercher")})
    )
    def __init__(self, *args, **kwargs):
        """
        Takes an option named argument ``queryset`` as the base queryset used in
        the ``get_queryset`` method.
        """
        self.queryset = kwargs.pop("queryset", None)
        super().__init__(*args, **kwargs)


    def get_queryset(self, request):
        where = Q(pk__gt=0)
        # TREEEEEEEEEEEEEEES important sinon au premier chargement, cleaned_data n'est pas reconnu
        if not self.is_valid():
            return self.queryset
        txtsearch = self.cleaned_data.get("txtsearch").strip()
        if txtsearch:
            search_list = txtsearch.split()
            for search_item in search_list:
                where &= (
                    Q(msgdet__short_text__icontains=search_item)
                    # | Q(msgdet__short_text2__cmsplugin__text__body__icontains=search_item)
                    | Q(msgdet__title__icontains=search_item)
                    | Q(title__icontains=search_item)
                )

        centrepilote = self.cleaned_data.get("centrepilote")
        if centrepilote:
            where &= Q(centrepilote=centrepilote)
            self.fields['listeenvoi'].queryset=ListeEnvoi.objects.filter(centrepilote=centrepilote)

        listeenvoi = self.cleaned_data.get("listeenvoi")
        if listeenvoi:
            where &= Q(listeenvoi=listeenvoi)

        annee = self.cleaned_data.get("annee")
        if annee:
            where &= Q(msg_date__year=annee)

        mois = self.cleaned_data.get("mois")
        if mois:
            where &= Q(msg_date__month=mois)            

        qs = self.queryset.filter(where)
        return qs.distinct()