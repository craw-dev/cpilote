# -*- coding: utf-8 -*-
"""
Insert loanrefund record into Expense
input param: user_id & loan_id
"""
from django.core.management.base import BaseCommand, CommandError
from msg.utils import get_msgid_from_image


class Command(BaseCommand):
    help = 'Get list of msg_id having image searched'

    def add_arguments(self, parser):
        parser.add_argument(
            '--filename', help="image filename search in msg",
        )                   

    def handle(self, *args, **options):
        try:
            filename = options['filename']
            result = get_msgid_from_image(filename)
            self.stdout.write(self.style.SUCCESS(result))
        except Exception as xcpt:
            raise CommandError('Failed "{0}"'.format(xcpt))
