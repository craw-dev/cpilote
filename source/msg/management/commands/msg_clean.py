# -*- coding: utf-8 -*-
"""
Insert loanrefund record into Expense
input param: user_id & loan_id
"""
from django.core.management.base import BaseCommand, CommandError
from msg.utils import msg_clean


class Command(BaseCommand):
    help = 'Clean HTML code of msg between id range from_id => to_id'

    def add_arguments(self, parser):
        parser.add_argument(
            '--from_id', help="from_id msg",
        )
        parser.add_argument(
            '--to_id', help="to_id msg",
        )                   

    def handle(self, *args, **options):
        try:
            from_id = options['from_id']
            to_id = options['to_id']
            result = msg_clean(from_id, to_id)
            self.stdout.write(self.style.SUCCESS(result))
        except Exception as xcpt:
            raise CommandError('Failed "{0}"'.format(xcpt))
