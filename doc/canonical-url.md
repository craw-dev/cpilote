https://centrespilotes.be/media/filer_public/45/9f/459f9ef3-f9b6-4a8b-ab59-498a3b91fd64/feuilles_stades.png
old
/filer/canonical/1585646123/499/


https://stackoverflow.com/questions/57860790/django-filer-url-that-is-seperate-from-admin

url canonique obtenue dans module admin (advanced panel)
new
/filer/canonical/1585642523/499/


DB select
select * from filer_file where original_filename like 'feuilles_stades.png%'


dans cpilote
filer.models.filemodels.py

    @property
    def canonical_time(self):
        if settings.USE_TZ:
            return int((self.uploaded_at - datetime(1970, 1, 1, 1, tzinfo=timezone.utc)).total_seconds())
        else:
            return int((self.uploaded_at - datetime(1970, 1, 1, 1)).total_seconds())

    @property
    def canonical_url(self):
        url = ''
        if self.file and self.is_public:
            try:
                url = reverse('canonical', kwargs={
                    'uploaded_at': self.canonical_time,
                    'file_id': self.id
                })
            except NoReverseMatch:
                pass  # No canonical url, return empty string
        return url


dans old cp_cms
il n'y a que 2 fois 1 et 1
    @property
    def canonical_time(self):
        if settings.USE_TZ:
            return int((self.uploaded_at - datetime(1970, 1, 1, tzinfo=timezone.utc)).total_seconds())
        else:
            return int((self.uploaded_at - datetime(1970, 1, 1)).total_seconds())