# A. Run ANSIBLE

# B. ssh key_file
1. Add key_file in /home/prj/.ssh
2. Add config file
3. copy settings.json

# C. Load DB

# D. Check sites-enabled

# E. run start-wsgi.ini

# D. DB change
1. run msg upd
2. Change Django site in DB

# E. Application change
## msg.views.msg_consult
change msg_id seuil visibilité

## Admin user
retirer patrick as manage_cp=cepicop

## Publish CMS cepicop pages

## Change msg.views.msg_envoyer
recipients = get_email_list !!!

## filer.models.filemodels.py
### change this
    @property
    def canonical_time(self):
        if settings.USE_TZ:
            return int((self.uploaded_at - datetime(1970, 1, 1, 1, tzinfo=timezone.utc)).total_seconds())
        else:
            return int((self.uploaded_at - datetime(1970, 1, 1, 1)).total_seconds())

### to this
    @property
    def canonical_time(self):
        if settings.USE_TZ:
            return int((self.uploaded_at - datetime(1970, 1, 1, tzinfo=timezone.utc)).total_seconds())
        else:
            return int((self.uploaded_at - datetime(1970, 1, 1)).total_seconds())
