# dump
## direct
pg_dump -h localhost -p 5432 -U postgres -C -F c -v -f cpilote_local_202105.bkp cpilote
pg_dump --host localhost --port 5432 --username "postgres" -C --format custom --verbose --file 2020Avril11.bkp cpilote
## connected to postgres
pg_dump -C -F c -v -f cpilote_pr_20211008.bkp cpilote

# restore
## connected to postgres
pg_restore --host localhost --port 5432 --username "cpiloteadmin" --dbname "cpilote" --role "cpiloteadmin" --verbose "cpilote_pr_20210603.bkp"
pg_restore -d cpilote cpilote_pr_20210607.bkp


# Export specific table data as SQL statement
## dump
pg_dump --host localhost --port 5432 --username "postgres" --table="info_weblink" --data-only --column-inserts cpilote > info_weblink.sql

## restore
dans le répertoire dans lequel est stocké le dump taper
$ sudo su postgres
$ psql db_targe < info_weblink.sql

## alter table
ALTER TABLE accounts_centrepilote RENAME website TO siteweb; 
